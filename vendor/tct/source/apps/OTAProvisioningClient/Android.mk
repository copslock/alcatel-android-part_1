# Copyright (C) 2016 Tcl Corporation Limited
# Copyright 2008, The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := OTAProvisioningClient

LOCAL_JAVA_LIBRARIES := telephony-common

#[BUGFIX]-Add-BEGIN by TCTNJ.yan.zhao,2016/03/12,1779820
LOCAL_CERTIFICATE := platform
#[BUGFIX]-Add-END by TCTNJ.yan.zhao,

#[BUGFIX]-Add-BEGIN by TCTNB.anying.huang,05/13/2014,641287-
LOCAL_PRIVILEGED_MODULE := true
#[BUGFIX]-Add-END by TCTNB.anying.huang

#[FEATURE]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, CR-578398 ,OMA-CP message requirment
include $(BUILD_PLF)
#[FEATURE]-Add-END by TCTNB.gong.zhang,
include $(BUILD_PACKAGE)

# additionally, build unit tests in a separate .apk
#include $(call all-makefiles-under,$(LOCAL_PATH))
