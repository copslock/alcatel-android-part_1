/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.Iterator;

public class ApplicationHandler {

//  @Override
    public static boolean checkValidity(WapProvisioningDoc mProvDoc) {

        if(null!=mProvDoc){

            Iterator<Characteristic> remainder = mProvDoc.getAllCharacteristic();
            if(null!=remainder && remainder.hasNext()){
                ArrayList<Characteristic> appList = mProvDoc.getCharacteristic("APPLICATION");
                if(null!=appList && !appList.isEmpty()){
                    for(Characteristic app:appList){
                        checkProxy(app,mProvDoc);
                        checkNap(app,mProvDoc);
                        checkAppAddr(app,mProvDoc);
                    }

                }//end if
            }
        }
        return false;
    }

    //check whether TO-PROXY is valid
    private static void checkProxy(Characteristic app,WapProvisioningDoc mProvDoc){
        if(null!=app && null!=mProvDoc){

            ArrayList<Parm> parms = app.getParm("TO-PROXY");
            if(null!=parms && !parms.isEmpty()){
                ArrayList<Characteristic> refChara = mProvDoc.getCharacteristic("PXLOGICAL");
                ArrayList<String> refId = new ArrayList<String>();
                if(null!=refChara && !refChara.isEmpty()){//get PXLOGICAL from prov doc
                    for(Characteristic chara:refChara){
                        refId.add(chara.getParm("PROXY-ID").get(0).value);
                    }
                }

                for(Parm curParm:parms){
                    String id = curParm.value;
                    if(!refId.contains(id)){ //if the TO-PROXY is not existing,remove it from APPLICATION Chara
                        app.removeParm(curParm);
                    }

                }
            }
        }
    }

    //check whether whether TO-NAPID is valid
    private static void checkNap(Characteristic app,WapProvisioningDoc mProvDoc){

        ArrayList<Parm> napRef = app.getParm("TO-NAPID");
        if(null==napRef || napRef.isEmpty()){
            ArrayList<Characteristic> napList = mProvDoc.getCharacteristic("NAPDEF");
            ArrayList<String> napIdList = new ArrayList<String>();
            if(null!=napList && !napList.isEmpty()){
                for(Characteristic chara:napList){
                    napIdList.add(chara.getParm("NAPID").get(0).value);
                }
            }
            for(Parm tonap:napRef){
                String id = tonap.value;
                if(!id.equals("INTERNET") && !napIdList.contains(id)){
                    app.removeParm(tonap);

                }
            }
        }
    }

    //check whether APPADDR is valid
    private static void checkAppAddr(Characteristic app,WapProvisioningDoc mProvDoc){

        ArrayList<Characteristic> appAddrList = app.getInnerCharacteristic("APPADDR");
        if(null!=appAddrList && !appAddrList.isEmpty()){

            Iterator<Characteristic> appAddrIterator = appAddrList.iterator();

            while(appAddrIterator.hasNext()){
                Characteristic appAddr = appAddrIterator.next();
                 ArrayList<Parm> addrParm = appAddr.getParm("ADDR");
                 if(null==addrParm || addrParm.isEmpty()){
                     appAddrIterator.remove();
                 }else{
                     ArrayList<Characteristic> portList = appAddr.getInnerCharacteristic("PORT");

                     if(null!=portList && portList.isEmpty()){

                         Iterator<Characteristic> portIterator = portList.iterator();
                         while(portIterator.hasNext()){
                             Characteristic port = portIterator.next();
                             if(!checkPort(port)){
                                 portIterator.remove();
                             }
                         }
                         appAddr.resetCharaForType(portList,"PORT");

                     }
                 }
            }
            app.resetCharaForType(appAddrList,"APPADDR");

        }
    }

    private static boolean checkPort(Characteristic portChara){
        boolean isValid=true;
        if(null!=portChara && portChara.type.equals("PORT")){
            ArrayList<Parm> portNB = portChara.getParm("PORTNBR");
            if(null==portNB || portNB.isEmpty()){
                isValid = false;
            }
        }
        return isValid;
    }

}
