/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient.wbxml;

public interface WbxmlTokenTable {

    //ProvContv1.1 7.1 tag code space in code page zero
    public static final String[] TAG_TABLE_CODEPAGE_0 = new String[] {"wap-provisioningdoc", //token=0x05
                                                                      "characteristic",
                                                                       "parm"};

    //ProvContv1.1 7.1 tag code space in code page one
    public static final String[] TAG_TABLE_CODEPAGE_1 = new String[] {null,
                                                                      "characteristic", //token=0x06
                                                                       "parm"};


    //ProvContv1.1 7.2 attribute code space in code page zero
    public static final String[] ATTRIBUTE_START_TABLE_CODEPAGE_0 = new String[] {

        //ProvCont 7.2.3 parm Attribute Start Tokens in code page 0
        "name",  //token=0x05
        "value",
        "name=NAME",
        "name=NAP-ADDRESS",
        "name=NAP-ADDRTYPE",
        "name=CALLTYPE",
        "name=VALIDUNTIL",
        "name=AUTHTYPE",
        "name=AUTHNAME",
        "name=AUTHSECRET",
        "name=LINGER",
        "name=BEARER",
        "name=NAPID",
        "name=COUNTRY",
        "name=NETWORK",
        "name=INTERNET",
        "name=PROXY-ID",
        "name=PROXY-PROVIDER-ID",
        "name=DOMAIN",
        "name=PROVURL",
        "name=PXAUTH-TYPE",
        "name=PXAUTH-ID",
        "name=PXAUTH-PW",
        "name=STARTPAGE",
        "name=BASAUTH-ID",
        "name=BASAUTH-PW",
        "name=PUSHENABLED",
        "name=PXADDR",
        "name=PXADDRTYPE",
        "name=TO-NAPID",
        "name=PORTNBR",
        "name=SERVICE",
        "name=LINKSPEED",
        "name=DNLINKSPEED",
        "name=LOCAL-ADDR",
        "name=LOCAL-ADDRTYPE",
        "name=CONTEXT-ALLOW",
        "name=TRUST",
        "name=MASTER",
        "name=SID",
        "name=SOC",
        "name=WSP-VERSION",
        "name=PHYSICAL-PROXY-ID",
        "name=CLIENT-ID",
        "name=DELIVERY-ERR-SDU",
        "name=DELIVERY-ORDER",
        "name=TRAFFIC-CLASS",
        "name=MAX-SDU-SIZE",
        "name=MAX-BITRATE-UPLINK",
        "name=MAX-BITRATE-DNLINK",
        "name=RESIDUAL-BER",
        "name=SDU-ERROR-RATIO",
        "name=TRAFFIC-HANDL-PRIO",   //token=0x39
        "name=TRANSFER-DELAY",
        "name=GUARANTEED-BITRATE-UPLINK",
        "name=GUARANTEED-BITRATE-DNLINK",
        "name=PXADDR-FQDN",
        "name=PROXY-PW",
        "name=PPGAUTH-TYPE", //token=0x3f

        null,
        null,
        null,
        null,
        null,

        //ProvCont 7.2.1 Wap-provisioningdoc  Attribute Start Tokens in code page 0
        "version",  //token=0x45
        "version=VERSION",


        //ProvCont 7.2.3 parm Attribute Start Tokens in code page 0
        "name=PULLENABLED",   //token=0x47
        "name=DNS-ADDR",
        "name=MAX-NUM-RETRY",
        "name=FIRST-RETRY-TIMEOUT",
        "name=REREG-THRESHOLD",
        "name=T-BIT",
        null,
        "name=AUTH-ENTITY",
        "name=SPI",


        //ProvCont 7.2.2 Characteristic Attribute Start Tokens in code page 0
        "type",                      //token=0x50
        "type=PXLOGICAL",
        "type=PXPHYSICAL",
        "type=PORT",
        "type=VALIDITY",
        "type=NAPDEF",
        "type=BOOTSTRAP",
        "type=VENDORCONFIG",
        "type=CLIENTIDENTITY",
        "type=PXAUTHINFO",
        "type=NAPAUTHINFO",
        "type=ACCESS"    //token=0x5b
        };


    //ProvContv1.1  attribute code space in code page one
    public static final String[] ATTRIBUTE_START_TABLE_CODEPAGE_1 = new String[] {

        //ProvCont 7.2.3 parm Attribute Start Tokens in code page 1
        "name", //token=0x05
        "value",
        "name=NAME",
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        "name=INTERNET",  //token=0x14
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        "name=STARTPAGE", //token=0x1c
        null,
        null,
        null,
        null,
        null,
        "name=TO-NAPID", //token=0x22
        "name=PORTNBR",
        "name=SERVICE",
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        "name=AACCEPT", //token=0x2e
        "name=AAUTHDATA",
        "name=AAUTHLEVEL",
        "name=AAUTHNAME",
        "name=AAUTHSECRET",
        "name=AAUTHTYPE",
        "name=ADDR",
        "name=ADDRTYPE",
        "name=APPID",
        "name=APROTOCOL",
        "name=PROVIDER-ID",
        "name=TO-PROXY",
        "name=URI",
        "name=RULE", //token=0x3b
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,

        //ProvContv1.1 7.2.2 Characteristic Attribute Start Tokens in code page 1
        "type", //token=0x50
        null,
        null,
        "type=PORT",
        null,
        "type=APPLICATION",
        "type=APPADDR",
        "type=APPAUTH",
        "type=CLIENTIDENTITY",
        "type=RESOURCE"  //token=0x59

    };

    public static final String[] ATTRIBUTE_VALUE_TABLE_CODEPAGE_0 = new String[] {

        //ProvCont-v1.1 7.3.1 ADDRTYPE value
        "IPV4",   //token=0x85
        "IPV6",
        "E164",
        "ALPHA",
        "APN",
        "SCODE",
        "TETRA-ITSI",
        "MAN",      //token=0x8c

        null,
        null,
        null,

        //ProvCont-v1.1 7.3.2 CALLTYPE value
        "ANALOG-MODEM",  //token=0x90
        "V.120",
        "V.110",
        "X.31",
        "BIT-TRANSPARENT",
        "DIRECT-ASYNCHRONOUS-DATA-SERVICE", //token=0x95

        null,
        null,
        null,
        null,

        //ProvCont-v1.1 7.3.2 AUTHTYPE/PXAUTH-TYPE value
        "PAP",  //token=0x9A
        "CHAP",
        "HTTP-BASIC",
        "HTTP-DIGEST",
        "WTLS-SS",
        "MD5",  //token=0x9F

        null,
        null,

        //ProvCont-v1.1 7.3.2 BEARER value
        "GSM-USSD",   //0xA2
        "GSM-SMS",
        "ANSI-136-GUTS",
        "IS-95-CDMA-SMS",
        "IS-95-CDMA-CSD",
        "IS-95-CDMA-PACKET",
        "ANSI-136-CSD",
        "ANSI-136-GPRS",
        "GSM-CSD",
        "GSM-GPRS",
        "AMPS-CDPD",
        "PDC-CSD",
        "PDC-PACKET",
        "IDEN-SMS",
        "IDEN-CSD",
        "IDEN-PACKET",
        "FLEX/REFLEX",
        "PHS-SMS",
        "PHS-CSD",
        "TETRA-SDS",
        "TETRA-PACKET",
        "ANSI-136-GHOST",
        "MOBITEX-MPAK",
        "CDMA2000-1X-SIMPLE-IP",
        "CDMA2000-1X-MOBILE-IP", //token=0xba

        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,

        //ProvCont-v1.1 7.3.5 LINKSPEED value
        "AUTOBAUDING",  //token=0xc5


        null,
        null,
        null,
        null,

         //ProvCont-v1.1 7.3.2 SERVICE value
        "CL-WSP",  //token=0xca
        "CO-WSP",
        "CL-SEC-WSP",
        "CO-SEC-WSP",
        "CL-SEC-WTA",
        "CO-SEC-WTA",
        "OTA-HTTP-TO",
        "OTA-HTTP-TLS-TO",
        "OTA-HTTP-PO",
        "OTA-HTTP-TLS-PO", //token=0xd3


        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,

        //ProvCont-v1.1 7.3.8 AUTH-ENTY value
        "AAA", //token=0xe0
        "HA"};

    public static final String[] ATTRIBUTE_VALUE_TABLE_CODEPAGE_1 = new String[] {
        null,

        //ProvCont-v1.1 7.3.1 ADDRTYPE value
        "IPV6",  //0x86
        "E164",
        "ALPHA",
        null,
        null,
        null,
        null,
        "APPSRV",
        "OBEX",  //0x8e

        null,

        //ProvCont-v1.1 7.3.7 AAUTHTYPE value
        ",",  //token=0x90
        "HTTP-",
        "BASIC",
        "DIGEST"};
}
