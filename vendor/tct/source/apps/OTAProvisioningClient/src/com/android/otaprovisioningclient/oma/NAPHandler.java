/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

import java.util.ArrayList;
import java.util.Iterator;

public class NAPHandler {

    public static boolean checkValidity(WapProvisioningDoc mProvDoc) {

        boolean isValid = false;

        if (null != mProvDoc) {
            Iterator<Characteristic> remainder = mProvDoc
                    .getAllCharacteristic();
            if (null != remainder && remainder.hasNext()) {
                ArrayList<Characteristic> NAPList = mProvDoc
                        .getCharacteristic("NAPDEF");
                for (Characteristic napChara : NAPList) {
                    // NAPDEF must have a NAME parm
                    ArrayList<Parm> parms = napChara.getParm("NAME");
                    if (null == parms || parms.isEmpty()) {
                        mProvDoc.removeNAPDef(napChara);
                    } else {
                        if (checkBearer(napChara)) {
                            checkAuthInfo(napChara);
                        } else {
                            /*
                             * OMA-WAP-TS-ProvUAB-V1_1-20090728-A 4.2 a network
                             * access point definition for a bearer that is not
                             * supported MUST be ignored
                             */
                            mProvDoc.removeNAPDef(napChara);
                        }
                    }
                }
                // check PXLOGICAL
                isValid = PXLogicalHandler.checkValidity(mProvDoc);
            }
        }

        return isValid;
    }

    /*
     * check whether the bearer(s) are supported by the Operator
     */
    private static boolean checkBearer(Characteristic napChara) {
        boolean valid = true;

        ArrayList<Parm> bearerParmList = napChara.getParm("BEARER");
        if ((null != bearerParmList) && (!bearerParmList.isEmpty())) {
            ArrayList<String> bearerList = new ArrayList<String>();
            for (Parm curParm : bearerParmList) {
                bearerList.add(curParm.value);
            }
            if (!mSupportedBearer.containsAll(bearerList)) {
                valid = false;
            }
        }

        return valid;
    }

    // check whether the NAPAUTHINFO Char is valid,if invalid, filter it out
    // from NAPDEF chara
    private static void checkAuthInfo(Characteristic napChara) {

        if (null != napChara && napChara.type.equals("NAPDEF")) {
            ArrayList<Characteristic> innerNapAuthCharas = napChara
                    .getInnerCharacteristic("NAPAUTHINFO");

            if (null != innerNapAuthCharas) {
                Iterator<Characteristic> innerCharas = innerNapAuthCharas
                        .iterator();
                while (innerCharas.hasNext()) {
                    Characteristic chara = innerCharas.next();
                    // AUTHTYPE must be set
                    ArrayList<Parm> authType = chara.getParm("AUTHTYPE");
                    if (null == authType || authType.isEmpty()) {
                        innerCharas.remove();
                    }
                }
                // reset the NAPAUTHINFO chara of NAPDEF chara
                napChara.resetCharaForType(innerNapAuthCharas, "NAPAUTHINFO");
            }

        }
    }

    private static ArrayList<String> mSupportedBearer = new ArrayList<String>();
    static {
        mSupportedBearer.add("GSM-USSD");
        mSupportedBearer.add("GSM-SMS");
        mSupportedBearer.add("ANSI-136-GUTS");
        mSupportedBearer.add("IS-95-CDMA-SMS");
        mSupportedBearer.add("IS-95-CDMA-CSD");
        mSupportedBearer.add("IS-95-CDMA-PACKET");
        mSupportedBearer.add("ANSI-136-CSD");
        mSupportedBearer.add("ANSI-136-GPRS");
        mSupportedBearer.add("GSM-CSD");
        mSupportedBearer.add("GSM-GPRS");
        mSupportedBearer.add("AMPS-CDPD");
        mSupportedBearer.add("PDC-CSD");
        mSupportedBearer.add("PDC-PACKET");
        mSupportedBearer.add("IDEN-SMS");
        mSupportedBearer.add("IDEN-CSD");
        mSupportedBearer.add("IDEN-PACKET");
        mSupportedBearer.add("FLEX/REFILEX");
        mSupportedBearer.add("PHS-SMS");
        mSupportedBearer.add("PHS-CSD");
        mSupportedBearer.add("TETRA-SDS");
        mSupportedBearer.add("TETRA-PACKET");
        mSupportedBearer.add("ANSI-136-GHOST");
        mSupportedBearer.add("MOBITEX MPAK");
        mSupportedBearer.add("CDMA2000-1X-SIMPLE-IP");
        mSupportedBearer.add("CDMA2000-1X-MOBILE-IP");
    }

}
