/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/02/2014|ji.li                 |FR-570124             |The OTA profile still display */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.otaprovisioningclient.utils.HexConvertor;

import java.io.Serializable;
import java.util.HashMap;

public class ProvisioningActivity extends Activity {

    protected final static String TEST_WAP_DOCUMENT = "030B6A0B566620495350007765620045C65601870706"
            + "83000101C65501871106830001870706830001871006AB0187080603696E7465726E65740001870906"
            + "8901C65A01870C069A01870D06830701870E068307010101C65701870706034E4554574F524B494445"
            + "4E5400018705034D434300060332333400018705034D4E430006033135000101C60001550187360000"
            + "060377320001870706830001872206830001C600015901873A00000603687474703A2F2F6C6976652E"
            + "766F6461666F6E652E636F6D000187070603566F6461666F6E650001871C01010101";

/*
  <?xml version="1.0" encoding="UTF-8" ?>
  <!DOCTYPE wap-provisioningdoc (View Source for full doctype...)>
  <wap-provisioningdoc version="1.1">
  <characteristic type="BOOTSTRAP">
  <parm name="NAME" value="CTC_WEB" />
  </characteristic>
  <characteristic type="NAPDEF">
  <parm name="NAPID" value="WEB NAP ID" />
  <parm name="BEARER" value="GSM-GPRS" />
  <parm name="NAME" value="CTC_WEB" />
  <parm name="NAP-ADDRESS" value="web.vodafone.de" />
  <parm name="NAP-ADDRTYPE" value="APN" />
  <parm name="INTERNET" />
  <characteristic type="NAPAUTHINFO">
  <parm name="AUTHTYPE" value="PAP" />
  </characteristic>
  </characteristic>
  <cp:characteristic xmlns:cp="omacp:omacp" type="APPLICATION">
  <cp:parm xmlns:cp="omacp:omacp" name="APPID" value="w2" />
  <parm name="TO-NAPID" value="WEB NAP ID" />
  <parm name="NAME" value="CTC_WEB" />
  <cp:characteristic xmlns:cp="omacp:omacp" type="RESOURCE">
  <cp:parm xmlns:cp="omacp:omacp" name="URI" value="http://www.vodafone.de"  />
  <parm name="NAME" value="CTC_WEB" />
  <parm name="STARTPAGE" />
  </cp:characteristic>
  </cp:characteristic>
  </wap-provisioningdoc>
*/

    protected final static String TEST_WAP_DOCUMENT_VF = "30B6A364354435F57454200574542204E41502049"
            + "4400786D6C6E733A63700063703A63686172616374657269737469630063703A7061"
            + "726D00C54503312E310001C6560187070683000101C65501871106830801871006AB"
            + "01870706830001870806037765622E766F6461666F6E652E6465000187090689018714"
            + "01C65A01870C069A010101C41C0413036F6D6163703A6F6D6163700000015501842E0"
            + "0000413036F6D6163703A6F6D616370000001360000060377320001872206830801870"
            + "706830001C41C0413036F6D6163703A6F6D6163700000015901842E00000413036F6D"
            + "6163703A6F6D6163700000013A00000603687474703A2F2F7777772E766F6461666F6E"
            + "652E64650001870706830001871C01010101";
    //[BUGFIX]-Mod-BEGIN by TCTNB.ji.li,01/02/2014,PR-570124
    @Override
    protected void onPause() {
        super.onPause();
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    //[BUGFIX]-Mod-END by TCTNB.ji.li,01/02/2014,PR-570124
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_launcher);

        Button notifyWapUserPin = (Button) findViewById(R.id.send_wap_pdu_to_notifier_user_pin);
        notifyWapUserPin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        ProvisioningPushReceiver.class);
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("SEC", "1");
                params.put("MAC", "FA8B0A2DE72948800CA24119DFA619AE08CBFE44");

                intent.putExtra("data", HexConvertor.convert(TEST_WAP_DOCUMENT));

                intent.putExtra("contentTypeParameters", (Serializable) params);

                ProvisioningActivity.this.sendBroadcast(intent);

            }
        });

        Button notifyVFWapUserPin = (Button) findViewById(R.id.send_wap_pdu_to_notifier_user_pin_vf);
        notifyVFWapUserPin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        ProvisioningPushReceiver.class);
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("SEC", "1");
                params.put("MAC", "F2188F8D5EE1720E70ECFF8EBD9CB08BAC926022");

                intent.putExtra("data",
                        HexConvertor.convert(TEST_WAP_DOCUMENT_VF));

                intent.putExtra("contentTypeParameters", (Serializable) params);

                ProvisioningActivity.this.sendBroadcast(intent);

            }
        });

        Button notifyMmsUserPin = (Button) findViewById(R.id.send_mms_pdu_to_notifier_user_pin);
        notifyMmsUserPin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        ProvisioningPushReceiver.class);
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("SEC", "1");
                params.put("MAC", "BAA40E8B030CB220D5CF9DB69C62930BAE4C4E78");

                String testMmsDoc = "03016A811C5261696E626F777320616E6420666C6F776572730038383934354700466C6F7765722070696373006D6D732E61706E2E676F2E6865726500706963747572650073656E646572005035353500506F696E746C6573732D6E616D652D73686F756C642D69676E6F72650035352E32322E37372E363600383838007734006765742E79612E706963732E68657265004D434300323435004D4E4300363600450001C65601870001070683000101C6000055018711000106831501870706831C01870000100001060000AB0187080001068328018700000900010600008901C65A01870C00010600009A01870D0001068338018700000E0001068340010101C6000051018715000106834701870706834C01C600005201872000010683690187000021000106000085018700012206831501C65301872306837501010101C65501873606837901873906834701873406837C0101C6000057018700010583810D0683811101870583811506838119010101";

                intent.putExtra("data", HexConvertor.convert(testMmsDoc));

                intent.putExtra("contentTypeParameters", (Serializable) params);

                ProvisioningActivity.this.sendBroadcast(intent);

            }
        });

        Button notifyWapAndMmsSameApn = (Button) findViewById(R.id.send_wap_mms_combined_apn_pdu_to_notifier_user_pin);
        notifyWapAndMmsSameApn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        ProvisioningPushReceiver.class);

                HashMap<String, String> params = new HashMap<String, String>();

                params.put("SEC", "1");
                params.put("MAC", "2F795C453DA4BC41B4F4168B6624210778A0A249");

                String document = "03016A811E49766520676F742074776F20666F722079610031323334007465737420646F63006163636573732E6D792E706F696E74730074696D6D7900746865646F67007732004D434300323132004D4E43003135005035353500506F696E746C6573732D6E616D652D73686F756C642D69676E6F72650035352E32322E37372E363600383838007734006765742E79612E706963732E686572650032343500363600450001C65601870001070683000101C6000055018711000106831301870706831801870000100001060000AB0187080001068321018700000900010600008901C65A01870C00010600009A01870D0001068332018700000E0001068338010101C65501873606833F0187220683130101C600005701870001058342068346018705834A06834E0101C6000055018711000106831301870706831801870000100001060000AB0187080001068321018700000900010600008901C65A01870C00010600009A01870D0001068332018700000E0001068338010101C6000051018715000106835101870706835601C600005201872000010683730187000021000106000085018700012206831301C65301872306837F01010101C65501873606838103018739068351018734068381060101C60000570187000105834206838117018705834A0683811B010101";

                intent.putExtra("data", HexConvertor.convert(document));
                intent.putExtra("contentTypeParameters", (Serializable) params);

                ProvisioningActivity.this.sendBroadcast(intent);
            }

        });

        Button notifyWapAndMmsUserPin = (Button) findViewById(R.id.send_wap_mms_pdu_to_notifier_user_pin);
        notifyWapAndMmsUserPin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        ProvisioningPushReceiver.class);
                HashMap<String, String> params = new HashMap<String, String>();

                params.put("SEC", "1");
                params.put("MAC", "D4E9BCFCB59C64ED3F6BBA1EB2C258E124F75991");

                String wapAndMmsDocument = "03016A815049766520676F742074776F20666F722079610031323334007465737420646F63006163636573732E6D792E706F696E74730074696D6D7900746865646F67007732004D434300323132004D4E430031350038383934354700466C6F7765722070696373006D6D732E61706E2E676F2E6865726500706963747572650073656E646572005035353500506F696E746C6573732D6E616D652D73686F756C642D69676E6F72650035352E32322E37372E363600383838007734006765742E79612E706963732E686572650032343500363600450001C65601870001070683000101C6000055018711000106831301870706831801870000100001060000AB0187080001068321018700000900010600008901C65A01870C00010600009A01870D0001068332018700000E0001068338010101C65501873606833F0187220683130101C600005701870001058342068346018705834A06834E0101C6000055018711000106835101870706835801870000100001060000AB0187080001068364018700000900010600008901C65A01870C00010600009A01870D0001068374018700000E000106837C010101C60000510187150001068381030187070683810801C60000520187200001068381250187000021000106000085018700012206835101C6530187230683813101010101C6550187360683813501873906838103018734068381380101C60000570187000105834206838149018705834A0683814D010101";

                intent.putExtra("data", HexConvertor.convert(wapAndMmsDocument));

                intent.putExtra("contentTypeParameters", (Serializable) params);

                ProvisioningActivity.this.sendBroadcast(intent);

            }
        });

        Button confirm = (Button) findViewById(R.id.straight_to_confirm);
        confirm.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Intent intent = new Intent(ProvisioningActivity.this,
                        StoreProvisioning.class);

                intent.putExtra(
                        "com.android.otaprovisioningclient.provisioning-data",
                        HexConvertor.convert(TEST_WAP_DOCUMENT));

                startActivity(intent);

            }
        });

        Button error = (Button) findViewById(R.id.straight_to_error);
        error.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                ProvisioningFailed.fail(ProvisioningActivity.this,
                        R.string.bad_network_pin_message);
            }
        });
    }

}
