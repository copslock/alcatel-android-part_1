/* Copyright (C) 2016 Tcl Corporation Limited */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/* 01/09/2014|gong.zhang            |FR-579073             |OMACP client still*/
/*           |                      |                      |active after the  */
/*           |                      |                      |installation of an*/
/*           |                      |                      |OMA-CP message    */
/* ----------|----------------------|----------------------|----------------- */
/* 01/24/2014|      Xu Danbin       |      PR-594556       |fix force close   */
/*           |                      |                      |caused by illegal */
/*           |                      |                      | param            */
/* ----------|----------------------|----------------------|----------------- */
/* 08/04/2014|     xueyong.zhang    |      FR-748437       |No profile details*/
/*           |                      |                      |are visible to cus*/
/*           |                      |                      |tomer when OMA-CP */
/*           |                      |                      |message is received*/
/* ----------|----------------------|----------------------|----------------- */
/* 08/21/2014|     dagang.yang      |        767043        |[Wap push&CP]     */
/*           |                      |                      |[Dual]There       */
/*           |                      |                      |is no APN after   */
/*           |                      |                      |receive a CP and  */
/*           |                      |                      |install it        */
/* ----------|----------------------|----------------------|----------------- */
/* 08/29/2014|     dagang.yang      |        777809        |[Wap push&CP]     */
/*           |                      |                      |       [Dual]There*/
/*           |                      |                      |is APN but not    */
/*           |                      |                      |activate after    */
/*           |                      |                      |receive a CP and  */
/*           |                      |                      |install it        */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.otaprovisioningclient;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.telephony.SubscriptionManager;
import org.xmlpull.v1.XmlPullParserException;
import com.android.otaprovisioningclient.oma.Application;

import com.android.otaprovisioningclient.oma.Contacts;
import com.android.otaprovisioningclient.oma.EmailAccount;
import com.android.otaprovisioningclient.oma.OMASettingInfo;
import com.android.internal.telephony.TelephonyProperties;
import com.android.otaprovisioningclient.oma.WapProvisioningDoc;
import com.android.otaprovisioningclient.persister.ApnPersister;
import com.android.otaprovisioningclient.persister.EmailPersister;
import com.android.otaprovisioningclient.persister.HomePagePersister;
import com.android.otaprovisioningclient.wbxml.WbxmlWapProvisioningDocParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import android.util.Log;
//[BUGFIX]-Add by TCTNB.Xu Danbin,01/24/2014,PR-594556,
//fix force close caused by illegal param
import android.util.TctLog;



//[BUGFIX]-MOD-BEGIN by TSCD.xueyong.zhang,08/04/2014,PR-748437
import java.util.ArrayList;

import android.content.ContentValues;

import com.android.otaprovisioningclient.oma.ClientProvAccessPoint;
import com.android.otaprovisioningclient.persister.AccessPointFactory;
import com.android.otaprovisioningclient.oma.Characteristic;
import com.android.otaprovisioningclient.oma.Parm;
//[BUGFIX]-MOD-END by TSCD.xueyong.zhang

public class StoreProvisioning extends Activity {

    private WapProvisioningDoc mProvisioningDocument;
    //private Bundle data;
    final public static int SOTRE_SUCCESS = 1;
    private Context mContext;
    private InsertAPThread mInsertAPThread = null;
    //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, [TMO] click "cancel" shall keep CP message notification
    private byte[] mWbxmlDocument;
    //[BUGFIX]-Add-END by TCTNB.gong.zhang,
    //[BUGFIX]-Add-BEGIN by TCTNB.Xu Danbin,01/24/2014,PR-594556,
    //fix force close caused by illegal param
    private String TAG ="OTACP";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.check_provisioning);
        ((Button) findViewById(R.id.accept)).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(mInsertAPThread == null){
                   mInsertAPThread = new InsertAPThread();
                   mInsertAPThread.setName("InsertAPThread");
                   mInsertAPThread.start();
                }
               }
            });

        ((Button) findViewById(R.id.decline)).setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, [TMO] click "cancel" shall keep CP message notification
                if(getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)) {
                    storeBackToNotification(mContext, mWbxmlDocument);
                }
                //[BUGFIX]-Add-END by TCTNB.gong.zhang,
                StoreProvisioning.this.finish();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        byte[] wbxmlDocument = getIntent().getByteArrayExtra(
                "com.android.otaprovisioningclient.provisioning-data");
        //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, [TMO] click "cancel" shall keep CP message notification
        if(getResources().getBoolean(R.bool.feature_otaprovisioning_TmoProvision_on)) {
            mWbxmlDocument = getIntent().getByteArrayExtra("com.android.otaprovisioningclient.provisioning-data");
        }
        //[BUGFIX]-Add-END by TCTNB.gong.zhang,
        WbxmlWapProvisioningDocParser parser = new WbxmlWapProvisioningDocParser();
        try {
            mProvisioningDocument = parser.parse(new ByteArrayInputStream(wbxmlDocument));
            setOperatorNumeric(this, mProvisioningDocument);
          ((TextView) findViewById(R.id.check_prov_name_value))
                                     .setText(mProvisioningDocument.name);

          int provDocType = mProvisioningDocument.docType;
          Resources res = mContext.getResources();
          StringBuffer cpDes = new StringBuffer();

          if((provDocType & 0x01)==0x01  || (provDocType == 0)){
              cpDes.append(res.getString(R.string.check_prov_type_value_apn));
          }

          if((provDocType & 0x02)==0x02){
              if(cpDes.length()>0){
                  cpDes.append("\n");
              }
              cpDes.append(res.getString(R.string.check_prov_type_value_mms));
          }

          if((provDocType & 0x04)==0x04){
              if(cpDes.length()>0){
                  cpDes.append("\n");
              }
              cpDes.append(res.getString(R.string.check_prov_type_value_email));
          }

          ((TextView) findViewById(R.id.check_prov_type_value)).setText(cpDes);
          //[BUGFIX]-MOD-BEGIN by TSCD.xueyong.zhang,08/04/2014,PR-748437
          StringBuffer detailInfo = new StringBuffer();
          AccessPointFactory accessPointFactory = new AccessPointFactory(mProvisioningDocument);
          ArrayList<ClientProvAccessPoint> accessPointList = accessPointFactory.createAccessPoints();
          String apnName;
          String apnProxy;
          String apnMmsc;
          String apnMmsproxy;
          String apnMmsport;
          Boolean isApnPeresented = false;
          if (null != accessPointList || !accessPointList.isEmpty()) {
              for (ClientProvAccessPoint accessPoint : accessPointList) {
                  ContentValues contentValues = new ContentValues();

              if (null != accessPoint.proxy) {
                  contentValues.put("proxy", accessPoint.proxy.proxy);
                  contentValues.put("port", accessPoint.proxy.port);
                  contentValues.put("mmsproxy", accessPoint.proxy.mmsproxy);
                  contentValues.put("mmsport", accessPoint.proxy.mmsport);
                  contentValues.put("mmsc", accessPoint.proxy.mmsc);
              } else {
                  contentValues.put("proxy", (String) null);
                  contentValues.put("port", (String) null);
                  contentValues.put("mmsproxy", (String) null);
                  contentValues.put("mmsport", (String) null);
                  contentValues.put("mmsc", (String) null);
              }
              if (null != accessPoint.napDef) {
                  contentValues.put("name", accessPoint.napDef.name);
                  contentValues.put("apn", accessPoint.napDef.apn);
              } else {
                  contentValues.put("name", (String) null);
                  contentValues.put("apn", (String) null);
              }

              apnName = contentValues.getAsString("apn");
              apnProxy = contentValues.getAsString("proxy");
              apnMmsc = contentValues.getAsString("mmsc");
              apnMmsproxy = contentValues.getAsString("mmsproxy");
              apnMmsport = contentValues.getAsString("mmsport");

	              switch(provDocType & 0x03) {
	                  case 0x03:  //APN+MMS
                      if (apnName != null && apnName.length() != 0 && !isApnPeresented) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_apn));
                          detailInfo.append(apnName);
                          detailInfo.append("\n");
                          isApnPeresented = true;
                      }
                      if (apnProxy != null && apnProxy.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_apn_proxy));
                          detailInfo.append(apnProxy);
                          detailInfo.append("\n");
                      }
                      if (apnMmsc != null && apnMmsc.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_mmsc));
                          detailInfo.append(apnMmsc);
                          detailInfo.append("\n");
                      }
                      if (apnMmsproxy != null && apnMmsproxy.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_proxy));
                          detailInfo.append(apnMmsproxy);
                          detailInfo.append("\n");
                      }
                      if (apnMmsport != null && apnMmsport.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_port));
                          detailInfo.append(apnMmsport);
                          detailInfo.append("\n");
                      }
                      break;
                  case 0x02:  //MMS
                      if (apnName != null && apnName.length() != 0 && !isApnPeresented) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_apn));
                          detailInfo.append(apnName);
                          detailInfo.append("\n");
                          isApnPeresented = true;
                      }
                      if (apnMmsc != null && apnMmsc.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_mmsc));
                          detailInfo.append(apnMmsc);
                          detailInfo.append("\n");
                      }
                      if (apnMmsproxy != null && apnMmsproxy.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_proxy));
                          detailInfo.append(apnMmsproxy);
                          detailInfo.append("\n");
                      }
                      if (apnMmsport != null && apnMmsport.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_mms_port));
                          detailInfo.append(apnMmsport);
                          detailInfo.append("\n");
                      }
                      break;
                  case 0x01:  //APN
                  case 0x00:
                      if (apnName != null && apnName.length() != 0 && !isApnPeresented) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_apn));
                          detailInfo.append(apnName);
                          detailInfo.append("\n");
                          isApnPeresented = true;
                      }
                      if (apnProxy != null && apnProxy.length() != 0) {
                          detailInfo.append(res.getString(R.string.check_prov_detail_apn_proxy));
                          detailInfo.append(apnProxy);
                          detailInfo.append("\n");
                      }
                      break;
                  }
              }
          }

          ArrayList<Characteristic> charaList = mProvisioningDocument.getCharacteristic("APPLICATION");
          ArrayList<Parm> tempList;
          Characteristic browserApp = null;
          String homePage = null;
          if (null != charaList && !charaList.isEmpty()) {
              for (Characteristic curChara : charaList) {
                  tempList = curChara.getParm("APPID");
                  if ((tempList != null && tempList.size() > 0) && tempList.get(0).value.equals(Application.BROWSER)) {
                      browserApp = curChara;
                      ArrayList<Characteristic> resources = curChara.getInnerCharacteristic("RESOURCE");
                      if (null != resources && !resources.isEmpty()) {
                          Characteristic resChara = resources.get(0);
                          ArrayList<Parm> uriParm = resChara.getParm("URI");
                          if (null != uriParm && !uriParm.isEmpty()) {
                              homePage = uriParm.get(0).value;
                          }
                      }
                      if (null != homePage) {
                          break;
                      }

                      ArrayList<Parm> addrList = curChara.getParm("ADDR");
                      if (null != addrList && !addrList.isEmpty()) {
                          homePage = addrList.get(0).value;
                          break;
                      }
                  }
              }
          }

          if (homePage != null) {
              detailInfo.append(res.getString(R.string.check_prov_detail_homepage));
              detailInfo.append(homePage);
              detailInfo.append("\n");
          }

        //PR927454-Xiaorong-YU-516 begin

          //ArrayList<EmailAccount> emailAccountList = accessPointFactory.createEmailAccount();
          ArrayList<EmailAccount> emailAccountList = accessPointFactory.getEmailAccount();//PR934470-Quanshui-Ye-001
       	  android.util.Log.v("===yxr===", "email emailAccountList.size():"  + emailAccountList.size());
       	 if (null != emailAccountList && !emailAccountList.isEmpty()) {
               for (EmailAccount emailAccount : emailAccountList) {
            	   if (emailAccount.email_account_type_foriobound != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_account_type));
            		   if (emailAccount.email_account_type_foriobound.equals("110")) {
                        		detailInfo.append(": pop3");
                    	} else if (emailAccount.email_account_type_foriobound.equals("143")) {
                        		detailInfo.append(": imap");
                    	} else if (emailAccount.email_account_type_foriobound.equals("25"))	{
                    		detailInfo.append(": smtp");
                    	}
            		   detailInfo.append("\n");
            	   }
            	   if (emailAccount.email_account_for_authentication != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_account_name));
			   detailInfo.append(": ");
            		   detailInfo.append(emailAccount.email_account_for_authentication);
            		   detailInfo.append("\n");
            	   }
            	   if (emailAccount.email_password_for_authentication != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_pwd));
			   detailInfo.append(": ");
            		   detailInfo.append(emailAccount.email_password_for_authentication);
            		   detailInfo.append("\n");
            	   }

            	   if (emailAccount.email_address != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_address));
			   detailInfo.append(": ");
            		   detailInfo.append(emailAccount.email_address);
            		   detailInfo.append("\n");
            	   }

            	   if (emailAccount.ioboundAddress != null) {
			if (emailAccount.email_account_type_foriobound.equals("25")) {
			   detailInfo.append(res.getString(R.string.check_prov_detail_email_outbound_label));
			} else {
			   detailInfo.append(res.getString(R.string.check_prov_detail_email_inbound_label));
			}
			   detailInfo.append(" ");
            		   detailInfo.append(emailAccount.ioboundAddress);
            		   detailInfo.append("\n");
            	   }

            	   if (emailAccount.ioboundPort != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_port));
			   detailInfo.append(": ");
            		   detailInfo.append(emailAccount.ioboundPort);
            		   detailInfo.append("\n");
            	   }

            	   if (emailAccount.security_type_foriobound != null) {
            		   detailInfo.append(res.getString(R.string.check_prov_detail_email_security_type));
			   detailInfo.append(": ");
            		   detailInfo.append(emailAccount.security_type_foriobound);
            		   detailInfo.append("\n");
            	   }
            	   detailInfo.append("\n");
               }
       	 }
	          //PR927454-Xiaorong-YU-516 end


          ((TextView) findViewById(R.id.check_prov_detail_value)).setText(detailInfo);
        //[BUGFIX]-MOD-END by TSCD.xueyong.zhang

        } catch (XmlPullParserException e) {
            ProvisioningFailed.fail(this, R.string.provisioning_failure);
            finish();
            return;
        } catch (IOException e) {
            //[BUGFIX]-Add by TCTNB.Xu Danbin,01/24/2014,PR-594556,
            //fix force close caused by illegal param
            TctLog.e(TAG, e.getMessage());
            ProvisioningFailed.fail(this, R.string.provisioning_failure);
            finish();
            return;
        }
    }

    public static void provision(Context context, byte[] document) {
        Intent storeProvisioningIntent = new Intent(context, StoreProvisioning.class);
        storeProvisioningIntent.putExtra("com.android.otaprovisioningclient.provisioning-data",
                document);
        context.startActivity(storeProvisioningIntent);
    }

    // fixbug, cannot get mcc/mnc from the test web site
    // http://172.24.212.250:8800/
    private static void setOperatorNumeric(Context context, WapProvisioningDoc doc) {
        // get apn from system propget, the same as class ApnEditor in Settings application
        //[BUGFIX]-ADD-BEGIN by TSCD.dagang.yang,08/21/2014, PR-767043
        //[BUGFIX]-MOD-BEGIN by TSCD.dagang.yang,08/29/2014, PR-777809
        if (TelephonyManager.getDefault().isMultiSimEnabled()) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//            int sub = sp.getInt("getSubID", 0);
//            if(sub < 0 || sub > 1){
//               sub = 0;
//            }
            long sub = sp.getLong("getSubID", SubscriptionManager.getDefaultDataSubscriptionId());//PR918525-Quanshui-Ye-001
            int phoneId = SubscriptionManager.getPhoneId((int)sub);
            if (doc.mcc == null || doc.mcc.equals("") || doc.mnc == null || doc.mcc.equals("")) {
                //String numeric = TelephonyManager.getTelephonyProperty(TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC,sub,"");
                String numeric = TelephonyManager.getTelephonyProperty(phoneId, TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC, "");
                android.util.Log.v("===ydg===", "setOperatorNumeric1 sub="+sub + "numeric=" +numeric);
                if (numeric != null && numeric.length() > 4) {
                       doc.mcc = numeric.substring(0, 3);
                       doc.mnc = numeric.substring(3);
                    }
            }

            if (doc.mcc == null || doc.mcc.equals("") || doc.mnc == null || doc.mcc.equals("")) {
                //String numeric = TelephonyManager.getTelephonyProperty(TelephonyProperties.PROPERTY_OPERATOR_NUMERIC,sub,"");
                String numeric = TelephonyManager.getTelephonyProperty(phoneId, TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC, "");
                android.util.Log.v("===ydg===", "setOperatorNumeric1.2 sub="+sub + "numeric=" +numeric);
                if (numeric != null && numeric.length() > 4) {
                      doc.mcc = numeric.substring(0, 3);
                      doc.mnc = numeric.substring(3);
                }
            }
            android.util.Log.v("===ydg===", "MSim setOperatorNumeric doc.mcc="+doc.mcc +",doc.mnc="+doc.mnc);
        }else {
            if (doc.mcc == null || doc.mcc.equals("") || doc.mnc == null || doc.mcc.equals("")) {
                String numeric = SystemProperties
                        .get(TelephonyProperties.PROPERTY_ICC_OPERATOR_NUMERIC);
                if (numeric != null && numeric.length() > 4) {
                           doc.mcc = numeric.substring(0, 3);
                           doc.mnc = numeric.substring(3);
                        }
            }

            if (doc.mcc == null || doc.mcc.equals("") || doc.mnc == null || doc.mcc.equals("")) {
                TelephonyManager telephonyManager = (TelephonyManager) context
                       .getSystemService(Context.TELEPHONY_SERVICE);
                String numeric = telephonyManager.getNetworkOperator();
                if (numeric != null && numeric.length() > 4) {
                           doc.mcc = numeric.substring(0, 3);
                           doc.mnc = numeric.substring(3);
                    }
            }
            android.util.Log.v("===ydg===", "single setOperatorNumeric doc.mcc="+doc.mcc +",doc.mnc="+doc.mnc);
        }
        //[BUGFIX]-ADD-END by TSCD.dagang.yang,08/21/2014
        //[BUGFIX]-MOD-END by TSCD.dagang.yang,08/29/2014
    }

    private String getStringValue(Object o) {
        if (o != null && !o.equals("")) {
            return o.toString();
        } else {
            return "";
        }
    }

    //[BUGFIX]-Add-BEGIN by TCTNB.gong.zhang, 2014/01/09, FR-579073, [TMO] click "cancel" shall keep CP message notification
    private void storeBackToNotification(Context context, byte[] document) {
        ProvisioningNotification.createNotificationTmo(context, null, null, document);
    }
    //[BUGFIX]-Add-END by TCTNB.gong.zhang,

    // fuwenliang, defect1181870, begin
    private boolean isPkgInstalled(String pkgName) {
        PackageInfo packageInfo = null;
        try {
            packageInfo = mContext.getPackageManager().getPackageInfo(pkgName, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            Log.v("StoreProvisioning", "[isPkgInstalled], packageInfo == null");
            return false;
        } else {
            Log.v("StoreProvisioning", "[isPkgInstalled], return true");
            return true;
        }
    }
    // fuwenliang, defect1181870, end

    private class InsertAPThread extends Thread{
        @Override
        public void run(){
           Log.v("StoreProvisioning", "InsertAPThread start...");
           // fuwenliang, defect1181870, begin
           boolean flagEmail = false;
           if (isPkgInstalled("com.tct.email")) {
               flagEmail = EmailPersister.saveEmail(getApplicationContext(), mProvisioningDocument);
           }
           // fuwenliang, defect1181870, end
           boolean flagApn =  ApnPersister.saveAP(getApplicationContext(), mProvisioningDocument);
           boolean flagHP = HomePagePersister.save(getApplicationContext(), mProvisioningDocument);
           Message msg = mInsertAPHandler.obtainMessage(SOTRE_SUCCESS);
           Bundle d = new Bundle();
           d.putBoolean("email", flagEmail);
           d.putBoolean("apn", flagApn);
           d.putBoolean("homepage", flagHP);
           msg.setData(d);
           msg.sendToTarget();
        }
    }

    private final Handler mInsertAPHandler = new Handler(){
       @Override
        public void handleMessage(Message msg){
           Bundle d = msg.getData();
           boolean flagEmail = d.getBoolean("email");
           boolean flagApn =  d.getBoolean("apn");
           boolean flagHP = d.getBoolean("homepage");
           Log.v("StoreProvisioning", "msg = " + msg.what + "," + "flagEmail = " + flagEmail + "," + "flagApn = " + flagApn + "," + "flagHP = " + flagHP);

            switch (msg.what) {
            case SOTRE_SUCCESS:
                String result = "";
                if(flagEmail && flagApn && flagHP){
                    result = StoreProvisioning.this.getResources().getString(
                            R.string.phone_now_configured);
                }else if (!flagEmail && !flagApn && !flagHP) {
                    result = StoreProvisioning.this.getResources().getString(
                            R.string.configure_failed);
                } else {
                    if (flagEmail) {
                        result = result
                                + StoreProvisioning.this.getResources()
                                        .getString(R.string.email_configured) + "\n";
                    }
                    if (flagApn) {
                        result = result
                                + StoreProvisioning.this.getResources()
                                        .getString(R.string.apn_configured) + "\n";
                    }
                    if (flagHP) {
                        result = result
                                + StoreProvisioning.this
                                        .getResources()
                                        .getString(R.string.homepage_configured) + "\n";
                    }
                    result = result.substring(0, result.length()-1);
                }
                Toast completeMessage = Toast.makeText(StoreProvisioning.this, result,Toast.LENGTH_LONG);

                completeMessage.setGravity(Gravity.CENTER, 0, 0);
                completeMessage.show();
                StoreProvisioning.this.finish();

                break;
              default:
                break;
            }
       }
    };

}
