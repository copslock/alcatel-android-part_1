/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:11/2013 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2012 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  xian.jiang                                                      */
/*  Email  :  xian.jiang@tcl.com                                              */
/*  Role   :  OTA DOWNLOADING OF NEW PROFILES                                 */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/OTAProvisioningClient/src/com/          */
/*             android/otaprovisioningclient/ProvisioningActivity.java        */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/15/2013|xian.jiang            |FR-554419             |add OTAProvisioni */
/*           |                      |                      |ngClient,porting  */
/*           |                      |                      |FR-317808         */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.otaprovisioningclient.oma;

public class EmailAccount {

    public String email_account_type_foriobound;
    public String email_account_for_authentication;
    public String email_password_for_authentication;
    public String email_address;
    public String ioboundAddress;
    public String ioboundPort;
    public String security_type_foriobound;

    public EmailAccount() {
    };

    public EmailAccount(EmailAccount account) {

        email_account_type_foriobound = account.email_account_type_foriobound;
        email_account_for_authentication = account.email_account_for_authentication;
        email_password_for_authentication = account.email_password_for_authentication;
        email_address = account.email_address;
        ioboundAddress = account.ioboundAddress;
        ioboundPort = account.ioboundPort;
        security_type_foriobound = account.security_type_foriobound;

    }

}
