/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio;

import com.waves.maxxutil.MaxxDefines;
import com.waves.maxxaudio.widget.ImageTextButton;

import android.R.integer;
import android.app.ActionBar;
import mst.app.MstActivity;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences; // MODIFIED by chunzhi.sun, 2016-10-12,BUG-2831181
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import mst.widget.toolbar.Toolbar;

/* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
import com.waves.maxxclientbase.MaxxHandle;
import com.waves.maxxclientbase.MaxxHandle.EffectCreatedListener;
import com.waves.maxxclientbase.MaxxHandle.EffectReleasedListener;
import com.waves.maxxclientbase.MaxxHandle.EnableStateChangeListener;
import com.waves.maxxclientbase.MaxxHandle.MaxxSenseEnableChangeListener;
import com.waves.maxxclientbase.MaxxHandle.NotificationBarEnableChangeListener;
import com.waves.maxxclientbase.MaxxHandle.OutputModeChangeListener;
import com.waves.maxxclientbase.MaxxHandle.ParametersChangeListener;
import com.waves.maxxclientbase.MaxxHandle.ServiceConnectListener;
import com.waves.maxxclientbase.MaxxHandle.SoundModeChangeListener;
import com.waves.maxxclientbase.MaxxHandle.TagChangeListener;
import com.waves.maxxutil.MaxxDefines;

import com.waves.maxxaudio.receiver.MaxxAudioReceiver;
import com.waves.maxxaudio.receiver.MaxxAudioReceiver.MaxxAudioReceiverListener;

public class MainActivity extends MstActivity implements View.OnClickListener, OnCheckedChangeListener,ServiceConnectListener, 
OutputModeChangeListener, SoundModeChangeListener, EnableStateChangeListener, ParametersChangeListener, MaxxSenseEnableChangeListener, 
NotificationBarEnableChangeListener, TagChangeListener, EffectCreatedListener, EffectReleasedListener{
/* MODIFIED-END by chunzhi.sun,BUG-2831181*/

	private String Tag = "MaxxWaveSettings";
	private ActionBar mActionBar;
	private View mHeadsetSettings;
	private ImageTextButton mGeneraMode;
	private ImageTextButton mMusicPlay;
	private ImageTextButton mVideoPlay;
	private Switch mHeadsetNotificationSwitch;
	private Switch mSourceOptSwitch;
    /* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
    private SharedPreferences m_SharedPreferences;
    protected MaxxHandle mMaxxHandle;
    /* MODIFIED-END by chunzhi.sun,BUG-2831181*/

	private boolean mIsHeadsetNotificationOn = true;
	private boolean mShoudSourceOpt = true;

     private int m_nOutputMode = -2;
     private int m_nSoundMode = -2;
     private int m_AudioGenre = 0;
     private int originalSoundMode = -2;
     private int DEFAULT_SOUNDMODE = MaxxDefines.SOUNDMODE_MUSIC;

    private String MODE = "source_mode";
	private String HEADSET_NOTIFICATION_SWITCH = "headset_notification_switch";
    private static final String mServiceName = "com.waves.maxxservice.MaxxService";
    private static final String KEY_PREFERENCE_SOUNDMODE = MaxxDefines.KEY_PREFERENCE_SOUNDMODE;
    private static final String KEY_PREFERENCE_IS_SENSE_ENABLED = MaxxDefines.KEY_PREFERENCE_IS_MAXXSENSE_ENABLED;
    private static final String KEY_PREFERENCE_IS_ENABLED = MaxxDefines.KEY_PREFERENCE_IS_ENABLED; // MODIFIED by chunzhi.sun, 2016-10-12,BUG-2831181
    private static final String KEY_LAST_CHOOSED_SOUNDMODE = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setMstContentView(R.layout.activity_main);
        setTitle(R.string.sound_volume);
        showBackIcon(true);
        /* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
        m_SharedPreferences = getSharedPreferences("MaxxAudioPreference",MODE_PRIVATE);
        initMaxxHandle();
		initUI();
        registerMaxxAudioReceiver();
     }

     @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
     private void initMaxxHandle() {
        mMaxxHandle = new MaxxHandle(this);
         mMaxxHandle.SetOutputModeChangeListener(this);
         mMaxxHandle.SetSoundModeChangeListener(this);
         mMaxxHandle.SetParametersChangeListener(this);
         mMaxxHandle.SetEnableStateChangeListener(this);
         mMaxxHandle.SetMaxxSenseEnableChangeListener(this);
         mMaxxHandle.SetAlgType(MaxxDefines.ALG_TYPE_MAXXMOBILE2);
         mMaxxHandle.setServiceName(mServiceName);
         mMaxxHandle.SetServiceConnectListener(this);
         mMaxxHandle.SetNotificationBarEnableChangeListener(this);
         mMaxxHandle.SetTagChangeListener(this);
         mMaxxHandle.setEffectCreatedListener(this);
         mMaxxHandle.setEffectReleasedListener(this);
         mMaxxHandle.SetEnabled(MaxxDefines.MSG_ASYNC, true);//sound volume is turn on
         if (!this.mMaxxHandle.connectService()) {
         	mMaxxHandle.connectService();
         }
        /* MODIFIED-END by chunzhi.sun,BUG-2831181*/
	}

	private void initUI() {
		// TODO Auto-generated method stub
		mHeadsetSettings = (View) findViewById(R.id.headset_settings);
		mHeadsetNotificationSwitch = (Switch) findViewById(R.id.headset_switch);
		mSourceOptSwitch = (Switch) findViewById(R.id.source_opt_switch);

		mGeneraMode = (ImageTextButton) findViewById(R.id.general_model);
		mGeneraMode.setTitleText(R.string.general_mode_titile);
		mGeneraMode.setSummeryText(R.string.general_mode_summery);

		mMusicPlay = (ImageTextButton) findViewById(R.id.music_play);
		mMusicPlay.setTitleText(R.string.music_mode_title);
		mMusicPlay.setSummeryText(R.string.music_mode_summery);

		mVideoPlay = (ImageTextButton) findViewById(R.id.video_play);
		mVideoPlay.setTitleText(R.string.video_mode_title);
		mVideoPlay.setSummeryText(R.string.video_mode_summery);

		try {
			mIsHeadsetNotificationOn = Settings.Global.getInt(getContentResolver(), HEADSET_NOTIFICATION_SWITCH) ==1?true:false;
		} catch (SettingNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mHeadsetNotificationSwitch.setChecked(mIsHeadsetNotificationOn);
		mHeadsetNotificationSwitch.setClickable(true);
		mHeadsetNotificationSwitch.setOnCheckedChangeListener(this);

        /* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
        mShoudSourceOpt = m_SharedPreferences.getBoolean(KEY_PREFERENCE_IS_ENABLED, true);
		mSourceOptSwitch.setChecked(mShoudSourceOpt);
		mSourceOptSwitch.setClickable(true);
		mSourceOptSwitch.setOnCheckedChangeListener(this);
        setContentVisible(mShoudSourceOpt);
        /* MODIFIED-END by chunzhi.sun,BUG-2831181*/

		mHeadsetSettings.setOnClickListener(this);
		mGeneraMode.setOnClickListener(this);
		mMusicPlay.setOnClickListener(this);
		mVideoPlay.setOnClickListener(this);

	}

    private void registerMaxxAudioReceiver(){
        MaxxAudioReceiver.registerMaxxAudioHeadsetListener(new MaxxAudioReceiver.MaxxAudioReceiverListener() {
            @Override
            public void MaxxAudioHeadsetClick(Intent intent) {
                Log.d(Tag,"registerMaxxAudioReceiver action = " + intent.getAction());
                mHeadsetNotificationSwitch.setChecked(false);
            }
        });
    }

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		int id = view.getId();
		switch(id) {
		case R.id.headset_settings :
			onClickHeadsetSetting();
			break;
		case R.id.general_model:
            //onClickModeForSourceOpt(MaxxDefines.SOUNDMODE_GAMING);
            onClickModeForChangeSoundmode(MaxxDefines.SOUNDMODE_GAMING);
			break;
		case R.id.music_play:
           //onClickModeForSourceOpt(MaxxDefines.SOUNDMODE_MUSIC);
            onClickModeForChangeSoundmode(MaxxDefines.SOUNDMODE_MUSIC);
			break;
		case R.id.video_play:
           //onClickModeForSourceOpt(MaxxDefines.SOUNDMODE_MOVIE);
           onClickModeForChangeSoundmode(MaxxDefines.SOUNDMODE_MOVIE);
			break;
			default:
				Log.d(Tag, "not find id");
		}

    }

    private void onClickModeForChangeSoundmode(int soundMode) {
        changeSoundMode(soundMode);
        m_SharedPreferences.edit().putInt(KEY_LAST_CHOOSED_SOUNDMODE, soundMode).apply();
    }

    private void changeSoundMode(int soundMode) {
        originalSoundMode = GetSoundMode();
        Log.d(Tag, "changeSoundMode soundMode = " + soundMode + "orignal sound mode is " + originalSoundMode);
        SetSoundMode(soundMode);
        if(originalSoundMode != soundMode) {
            mMaxxHandle.SetSoundMode(MaxxDefines.MSG_ASYNC, soundMode);
            mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, MaxxDefines.ALG_MAAP_IPEQ_2_ACTIVE, MaxxDefines.ALG_PARAMETER_IS_ACTIVE, GetOutputMode(), GetSoundMode());
            mMaxxHandle.requestUpdateClientPresetParameters(false);
        }
    }


	private void onClickModeForSourceOpt(int mode){
		Intent intent = new Intent();
		intent.putExtra(MODE, mode);
		intent.setClass(this, SourceOptModeActivity.class);
		startActivity(intent);
	}

	private void onClickHeadsetSetting() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setClass(this, HeadsetSettingsActivity.class);
		startActivity(intent);
	}

	/*
	 * switch changed listener*/
	@Override
	public void onCheckedChanged(CompoundButton button, boolean isChecked) {
		// TODO Auto-generated method stub
		int id = button.getId();
		switch (id) {
		case R.id.headset_switch:
			setHeadsetNotification(isChecked);
			break;
		case R.id.source_opt_switch:
			setContentVisible(isChecked);
			break;

		default:
			Log.d(Tag, "not find switch");
			break;
		}

	}

	/*
	 * set three mode visible or gone
	 */

	private void setContentVisible(boolean isChecked) {
		// TODO Auto-generated method stub
        mShoudSourceOpt = isChecked;
        /* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
        m_SharedPreferences.edit().putBoolean(KEY_PREFERENCE_IS_ENABLED, mShoudSourceOpt).apply();
        mMaxxHandle.SetMaxxSenseEnabled(MaxxDefines.MSG_ASYNC, mShoudSourceOpt);//auto-mode is on/off
        /* MODIFIED-END by chunzhi.sun,BUG-2831181*/

        mGeneraMode.setClickable(!mShoudSourceOpt);
        mMusicPlay.setClickable(!mShoudSourceOpt);
        mVideoPlay.setClickable(!mShoudSourceOpt);

        onOpenSouceOpt(isChecked);
        if(!mShoudSourceOpt) {
            int lastChoosedMode = m_SharedPreferences.getInt(KEY_LAST_CHOOSED_SOUNDMODE, DEFAULT_SOUNDMODE);
            changeSoundMode(lastChoosedMode);
            Log.d(Tag, "lastchoosedmode is " + lastChoosedMode);
         }

    }

    private void onOpenSouceOpt(boolean open) {
        if (open) {
            mGeneraMode.setTextColor(getColor(R.color.summery_color));
            mMusicPlay.setTextColor(getColor(R.color.summery_color));
            mVideoPlay.setTextColor(getColor(R.color.summery_color));
            mGeneraMode.setImageModeResource(R.drawable.icon_general_mode_auto);
            mMusicPlay.setImageModeResource(R.drawable.icon_music_mode_ato);
            mVideoPlay.setImageModeResource(R.drawable.icon_video_mode_auto);
            mVideoPlay.setImageSelected(false);
            mMusicPlay.setImageSelected(false);
            mGeneraMode.setImageSelected(false);
        } else {
            mGeneraMode.setTextColor(getColor(R.color.title_color));
            mMusicPlay.setTextColor(getColor(R.color.title_color));
            mVideoPlay.setTextColor(getColor(R.color.title_color));
            mGeneraMode.setImageModeResource(R.drawable.icon_general_mode);
            mMusicPlay.setImageModeResource(R.drawable.icon_music_mode);
            mVideoPlay.setImageModeResource(R.drawable.icon_video_mode);
            soundModeHasChanged(GetSoundMode());

       }
	}

	/*
	 * control insert headset notification
	 */
	private void setHeadsetNotification(boolean isChecked) {
		// TODO Auto-generated method stub
		Settings.Global.putInt(getApplicationContext().getContentResolver(), HEADSET_NOTIFICATION_SWITCH, isChecked ? 1 : 0);
        Intent headsetIntent = new Intent();
        if (isChecked){
            headsetIntent.setAction("android.intent.action.HEADSET_NOTIFICATION_POPUP");
        } else {
            headsetIntent.setAction("android.intent.action.HEADSET_NOTIFICATION_MISS");
        }
        headsetIntent.putExtra("isHeadsetNotificationON", String.valueOf(isChecked ? 1 : 0));
        sendBroadcast(headsetIntent);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
        if (null == this.mMaxxHandle.GetAlgType()) {
            RuntimeException var1 = new RuntimeException("derived class must call SetAlgType() in onCreate()");
            var1.printStackTrace();
            throw var1;
        }
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
            return true;
		}
		return super.onOptionsItemSelected(item);
	}

    /* MODIFIED-BEGIN by chunzhi.sun, 2016-10-12,BUG-2831181*/
    @Override
    public void OnServiceConnected() {
       // TODO Auto-generated method stub
        mMaxxHandle.GetMaxxSenseEnabled(MaxxDefines.MSG_ASYNC);
        mMaxxHandle.GetNotificationBarEnabled(MaxxDefines.MSG_ASYNC);
        //changeSoundmode
       Log.d("MainActivity", "[onServiceConnected]");
    }
    /* MODIFIED-END by chunzhi.sun,BUG-2831181*/

    @Override
    public void onEffectReleased() {
         // TODO Auto-generated method stub
    }

    @Override
    public void onEffectCreated() {
        // TODO Auto-generated method stub
        mMaxxHandle.requestUpdateClientPresetParameters(MaxxDefines.MSG_ASYNC);
        Log.d(Tag, "onEffectCreated after request update client preset parameters");
    }

    @Override
    public void OnTagChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void OnNotificationBarEnableChanged(boolean arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void OnMaxxSenseEnabledChanged(boolean arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void OnParametersChanged(int[] arg0, double[] arg1, int arg2,
             int arg3, int arg4) {
        // TODO Auto-generated method stub
    }

    @Override
    public void OnEnableChanged(boolean arg0) {
    // TODO Auto-generated method stub
     }

    @Override
    public void OnSoundModeChanged(int nSoundMode) {
         // TODO Auto-generated method stub
       //set selected status 
        Log.d(Tag, "onSoundModeChanged soundMode is " + nSoundMode);
        soundModeHasChanged(nSoundMode);
    }

    private void soundModeHasChanged(int nSoundMode) {
        SetSoundMode(nSoundMode);
        mMaxxHandle.PresetSetParameter(MaxxDefines.MSG_ASYNC, MaxxDefines.ALG_MAAP_IPEQ_2_ACTIVE, MaxxDefines.ALG_PARAMETER_IS_ACTIVE, GetOutputMode(), GetSoundMode());

        if (!mShoudSourceOpt) {

        switch (nSoundMode) {
            case MaxxDefines.SOUNDMODE_MUSIC:
                // set different Label
                mMusicPlay.setImageSelected(true);
                mVideoPlay.setImageSelected(false);
                mGeneraMode.setImageSelected(false);
                break;
            case MaxxDefines.SOUNDMODE_MOVIE:
                mVideoPlay.setImageSelected(true);
                mMusicPlay.setImageSelected(false);
                mGeneraMode.setImageSelected(false);
                break;
            case MaxxDefines.SOUNDMODE_GAMING:
                 mGeneraMode.setImageSelected(true);
                 mVideoPlay.setImageSelected(false);
                 mMusicPlay.setImageSelected(false);
                 break;

             }
        }
        m_SharedPreferences.edit().putInt(KEY_PREFERENCE_SOUNDMODE, nSoundMode).apply();
    }

    @Override
    public void OnOutputModeChanged(int arg0) {
        // TODO Auto-generated method stub

    }

    public int GetSoundMode() {
        return this.m_nSoundMode;
    }

    public void SetSoundMode(int soundMode) {
        this.m_nSoundMode = soundMode;
    }

    public int GetOutputMode() {
        return this.m_nOutputMode;
    }

    public void SetOutputMode(int outputMode) {
        this.m_nOutputMode = outputMode;
    }

}
