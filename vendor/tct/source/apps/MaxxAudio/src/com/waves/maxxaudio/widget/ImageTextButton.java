/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio.widget;


import com.waves.maxxaudio.R;

import android.R.integer;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImageTextButton extends RelativeLayout{

	private TextView mTitleView;
	private TextView mSummeryView;
	private ImageView mImageView;
	private ImageView mImageViewMode;
	private Context mContext;

	public ImageTextButton(Context context) {
		super(context,null);
		// TODO Auto-generated constructor stub
		mContext = context;
	}

	public ImageTextButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		LayoutInflater.from(context).inflate(R.layout.image_text_button,this,true);
		mTitleView = (TextView) findViewById(R.id.title);
		mSummeryView = (TextView) findViewById(R.id.summery);
        mImageView = (ImageView) findViewById(R.id.selected_icon);
		mImageViewMode = (ImageView) findViewById(R.id.image_mode);
		this.setClickable(true);
		this.setFocusable(true);

	}

	public void setImgResource(int resId) {
		this.mImageView.setImageResource(resId);
    }

    public void setImageSelected(boolean selected) {
        if(selected) {
            this.mImageView.setVisibility(View.VISIBLE);
        } else {
            this.mImageView.setVisibility(View.GONE);
         }
	}

	public void setImageModeResource(int resId) {
		this.mImageViewMode.setImageResource(resId);
	}

	public void setTitleText(int resId) {
		this.mTitleView.setText(resId);
	}

	public void setSummeryText(int resId) {
		this.mSummeryView.setText(resId);
	}

    public void setTextColor(int color) {
        this.mTitleView.setTextColor(color);
    }

}
