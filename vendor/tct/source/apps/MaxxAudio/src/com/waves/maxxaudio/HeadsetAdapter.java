/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

public class HeadsetAdapter extends BaseAdapter{

	String[] mHeadsetTitle;
	LayoutInflater layoutInflater;
	private CheckedTextView mCheckedTextView;
	private CheckedTextView defaultCheckedTextView;
	private CheckedTextView oldCheckedTextView;
	private int mOldPosition = -1;
	private int checkedPosition = -2;
    private int defaultPosition = 0;
	private String TAG = "Headset_settings_adapter";
	public HeadsetAdapter(Context context,String[] strings,int position) {
		super();
		layoutInflater = LayoutInflater.from(context);
		mHeadsetTitle = strings;
		mOldPosition = position;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mHeadsetTitle.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = layoutInflater.inflate(R.layout.headset_listview_adapter, null);
		mCheckedTextView = (CheckedTextView) convertView.findViewById(R.id.checked_title);
		mCheckedTextView.setText(mHeadsetTitle[position]);

		if(checkedPosition == position) {
			mCheckedTextView.setCheckMarkDrawable(R.drawable.icon_checked);
		} else {
			mCheckedTextView.setCheckMarkDrawable(null);
		}

        if (position == defaultPosition && checkedPosition == -2) {
			Log.d(TAG, "getView defaultCheckedTextView =" + defaultCheckedTextView + "checkedPosition = " + checkedPosition);
            if (defaultCheckedTextView == null) {
            	defaultCheckedTextView = mCheckedTextView;
            	defaultCheckedTextView.setCheckMarkDrawable(R.drawable.icon_checked);
            }

        }

		return convertView;
	}


	public void setCheckedPosition(int position) {
		this.checkedPosition = position;
	}

	public void setDefaultHeadsetChoosed(){
        if (mOldPosition == -1){
            mOldPosition = defaultPosition;
        }

       // defaultCheckedTextView.setChecked(true);
        defaultCheckedTextView.setCheckMarkDrawable(R.drawable.icon_checked);
        Log.d(TAG, "setDefaultHeadsetChoosed setChecked true");
        setOldConvertView(defaultCheckedTextView);
    }

	public void setOldConvertView(CheckedTextView headsetView){
        if (oldCheckedTextView !=null ){
            return;
        } else {
        	oldCheckedTextView = headsetView;
        }
    }




}
