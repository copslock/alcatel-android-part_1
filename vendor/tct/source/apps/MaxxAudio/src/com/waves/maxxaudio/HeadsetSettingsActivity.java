/* Copyright (C) 2016 Tcl Corporation Limited */
package com.waves.maxxaudio;

import com.waves.maxxclientbase.MaxxHandle;
import com.waves.maxxutil.MaxxDefines;

import mst.app.MstActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import mst.widget.toolbar.Toolbar;

public class HeadsetSettingsActivity extends MstActivity implements MaxxHandle.ServiceConnectListener{
	private String TAG = "Headset_settings";
	private ListView mHeadsetListView;
	private HeadsetAdapter mHeadsetAdapter;
	private BroadcastReceiver mHeadsetReceiver = null;
	private SharedPreferences m_SharedPreferences;
	protected MaxxHandle mMaxxHandle;

    private int m_nOutputMode = -2;
    private int m_nSoundMode = -2;
	private int oldPosition = -1;
    private int defaultHeadsetValue = 0;
    private int m_AudioGenre = 0;

    int[] headset_outputmode = new int[]{2,2,22,26,20,30,28,24,32};
    private static final String KEY_HEADSET_CHOOSED = null;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
        setMstContentView(R.layout.headset_list_layout);
        setTitle(getString(R.string.headset_settings));
        showBackIcon(true);
		m_SharedPreferences = getSharedPreferences("MaxxAudioPreference",MODE_PRIVATE);
		initMaxxHandle();
		//initHeadsetUi();
		initHeadsetUI();
		registerHeadsetListener();
	}
    @Override
    public void onNavigationClicked(View view) {
        super.onNavigationClicked(view);
        finish();
    }
	private void initMaxxHandle(){
		mMaxxHandle = new MaxxHandle(this);
		mMaxxHandle.SetAlgType(MaxxDefines.ALG_TYPE_MAXXMOBILE2);
		mMaxxHandle.SetServiceConnectListener(this);
       /* MODIFIED-BEGIN by chunzhi.sun, 2016-11-01,BUG-2831181*/
       if (!this.mMaxxHandle.connectService()) {
            mMaxxHandle.connectService();
         }
         /* MODIFIED-END by chunzhi.sun,BUG-2831181*/
	}


	private void initHeadsetUI() {
		mHeadsetListView = (ListView) findViewById(R.id.headset_listview);
		mHeadsetListView.setItemsCanFocus(false);
		mHeadsetListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		String[] mHeadsetNameStrings = getResources().getStringArray(R.array.headset_name_array);
		AudioManager localAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        oldPosition = m_SharedPreferences.getInt(KEY_HEADSET_CHOOSED,-1);

        if ((oldPosition == -1 || oldPosition > 8) && localAudioManager.isWiredHeadsetOn()){
            //if device fota to new version.The position of headset maybe out of the array.
            oldPosition = defaultHeadsetValue;
            m_SharedPreferences.edit().putInt(KEY_HEADSET_CHOOSED, defaultHeadsetValue).apply();
            mMaxxHandle.SetCustomOutputMode(MaxxDefines.MSG_ASYNC, MaxxDefines.OUTPUTMODE_HEADPHONES, headset_outputmode[defaultHeadsetValue], "");
        } else if (oldPosition > 8){
            //if device fota to new version.The position of headset maybe out of the array.
            oldPosition = -1;
            m_SharedPreferences.edit().putInt(KEY_HEADSET_CHOOSED, -1).apply();
        }
        mHeadsetAdapter = new HeadsetAdapter(this, mHeadsetNameStrings,oldPosition);
        mHeadsetAdapter.setCheckedPosition(oldPosition); // MODIFIED by chunzhi.sun, 2016-11-01,BUG-2831181
        mHeadsetListView.setAdapter(mHeadsetAdapter);
        mHeadsetListView.setOnItemClickListener(new ItemClickListener());

	}

	class  ItemClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                        int position,long id) {

            mHeadsetAdapter.setCheckedPosition(position);
            mHeadsetAdapter.notifyDataSetChanged();

            m_SharedPreferences.edit().putInt(KEY_HEADSET_CHOOSED, position).apply();
            mMaxxHandle.SetCustomOutputMode(MaxxDefines.MSG_ASYNC, MaxxDefines.OUTPUTMODE_HEADPHONES, headset_outputmode[position], "");
        }

    }
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (mHeadsetReceiver != null) {
            unregisterReceiver(mHeadsetReceiver);
            mHeadsetReceiver = null;
        }
        this.mMaxxHandle.disconnectService();
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	public int GetSoundMode() {
        return this.m_nSoundMode;
    }

	public void SetSoundMode(int soundMode) {
        this.m_nSoundMode = soundMode;
    }

    public int GetOutputMode() {
        return this.m_nOutputMode;
    }

    public void SetOutputMode(int outputMode) {
        this.m_nOutputMode = outputMode;
    }


	private void clearAllParametersChanged(int oldPosition){
        int[] modeValues = new int[]{MaxxDefines.SOUNDMODE_MUSIC,MaxxDefines.SOUNDMODE_MOVIE,MaxxDefines.SOUNDMODE_GAMING};
        for (int i =0;i<modeValues.length;i++){
            if (checkResetButtonStatus(modeValues[i])){
                Toast.makeText(this, R.string.discard_adjustment_data, Toast.LENGTH_SHORT).show();
                break;
            }
        }
        for (int i =0;i<modeValues.length;i++){
            if (checkResetButtonStatus(modeValues[i])){
                mMaxxHandle.SavePreset(MaxxDefines.MSG_ASYNC);
                saveBooleanToPref(getResetFlagKey(modeValues[i]), false);
                if (MaxxDefines.SOUNDMODE_MUSIC == modeValues[i]){
                    mMaxxHandle.PresetClearGuiParameters(MaxxDefines.MSG_ASYNC, headset_outputmode[oldPosition], MaxxDefines.SOUNDMODE_MUSIC, m_AudioGenre);
                }else{
                    mMaxxHandle.PresetClearGuiParameters(MaxxDefines.MSG_ASYNC, headset_outputmode[oldPosition], modeValues[i], 0);
                }
            }
        }
    }

	private boolean checkResetButtonStatus(int modeValue){
        String key = null;
        try {
            key = getResetFlagKey(modeValue);
            Log.d(TAG, "[checkResetButtonStatus] key=" + key);
            return m_SharedPreferences.getBoolean(key, false);
        } catch (Exception e) {
        	Log.d(TAG, "[checkResetButtonStatus]  read reset value key(" + key + ") Exception.", e);
        }
        return false;
    }

	private String getResetFlagKey(int modeValue) {
        StringBuffer buffer = new StringBuffer("ResetFlag");
        Log.d(TAG, "[getResetFlagKey(soundMode)] GetOutputMode()=" + GetOutputMode());
        if (GetOutputMode() != MaxxDefines.OUTPUTMODE_SPEAKER) {
            // We should Add out put mode into the key to distinguish different out put mode
            // which belong to the same sound mode.
            buffer.append(GetOutputMode());
        }
        if (modeValue == MaxxDefines.SOUNDMODE_MUSIC) {
            //int genre = (int) Math.pow(2, mGenreSpinner.getSelectedItemPosition());
            return buffer.append(modeValue+ "_" + m_AudioGenre).toString();
        }
        return buffer.append(modeValue + "_0").toString();
    }

	/**
    *
    * @param key a String like {@link #GetSoundMode()} + "_" + {@link #m_AudioGenre}.
    *            If the sound mode (like MOVIE) has not {@link #m_AudioGenre}, used 0 instead.
    * @param value a boolean, true means need to show reset button and false means show hide reset button.
    */
   public void saveBooleanToPref(String key, boolean value) {
       try {
           Log.d(TAG, "[saveBooleanToPref] key=" + key + " | value=" + value);
           m_SharedPreferences.edit().putBoolean(key, value).apply();
       } catch (Exception e) {
           Log.d(TAG, "[saveBooleanToPref] save key(" + key + ") value(" + value + ") Exception.", e);
       }
   }

	@Override
	public void OnServiceConnected() {
		// TODO Auto-generated method stub
        Log.d(TAG,"onServiceConnected"); // MODIFIED by chunzhi.sun, 2016-11-01,BUG-2831181
	}

	 /**
     * Registers an intent to listen for ACTION_HEADSET_PLUG
     * notifications. This intent is called to know if the headset
     * was plugged in/out
     */
    public void registerHeadsetListener() {
        if (mHeadsetReceiver == null) {
            mHeadsetReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    Log.d(TAG, "on receive HeadsetListener " + action);
                    if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                        boolean mHeadsetPlugged = (intent.getIntExtra("state", 0) == 1);
                        Log.d(TAG, "ACTION_HEADSET_PLUG Intent received + mHeadsetPlugged = "
                        + mHeadsetPlugged + " oldPosition = " + oldPosition);
                        if (mHeadsetPlugged){
                            if (oldPosition == -1){
                                mHeadsetAdapter.setDefaultHeadsetChoosed();
                            	//mHeadsetAdapter.setCheckedPosition(defaultHeadsetValue);
                            	Log.d(TAG, "on receive headset listener set default");
                                //mHeadsetAdapter.notifyDataSetChanged();
                                oldPosition = defaultHeadsetValue;
                                m_SharedPreferences.edit().putInt(KEY_HEADSET_CHOOSED, defaultHeadsetValue).apply();
                                mMaxxHandle.SetCustomOutputMode(MaxxDefines.MSG_ASYNC, MaxxDefines.OUTPUTMODE_HEADPHONES, headset_outputmode[defaultHeadsetValue], "");
                            }
                        }
                    }
                }

            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_HEADSET_PLUG);
            registerReceiver(mHeadsetReceiver, iFilter);
            Log.d(TAG, "registerHeadsetListener");
        }
    }


}
