# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
#Delete by heng.cao@tcl.com 2015-11-19 for task667586 begin
LOCAL_CERTIFICATE := platform
#Delete by heng.cao@tcl.com 2015-11-19 for task667586 end

LOCAL_STATIC_JAVA_LIBRARIES := \
        guava \
        android-common \
        android-support-v4 \
        android-support-v7-appcompat \
        android-support-v13 \
        android-support-design \
        maxxclientbase \
        maxxutil_client

#LOCAL_SDK_VERSION := current

LOCAL_PRIVILEGED_MODULE := true

#Include res dir from libraries
LOCAL_SRC_FILES := \
        $(call all-java-files-under, src)

LOCAL_RESOURCE_DIR = \
        $(LOCAL_PATH)/res \
        frameworks/support/v7/appcompat/res \
        frameworks/support/v7/recyclerview/res \
        frameworks/support/design/res

LOCAL_AAPT_FLAGS := \
        --auto-add-overlay \
        --extra-packages android.support.v7.appcompat \
        --extra-packages android.support.v7.recyclerview \
        --extra-packages android.support.design

LOCAL_AAPT_FLAGS += -I out/target/common/obj/APPS/mst-framework-res_intermediates/package-export.apk
#add by heng.cao for task 1845428 at 2016-03-22 begin
LOCAL_JAVA_LIBRARIES := tct.feature_query
LOCAL_JAVA_LIBRARIES += mst-framework
#add by heng.cao for task 1845428 at 2016-03-22 end
LOCAL_PACKAGE_NAME := MaxxAudio
#modify by minzhang@tcl.com for defect 1108633 start
#LOCAL_DEX_PREOPT := false
LOCAL_MULTILIB := 32
#modify end

include $(BUILD_PACKAGE)

include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := maxxclientbase:libs/maxxclientbase_release.jar \
                                        maxxutil_client:libs/maxxutil_release.jar
#                                        android-support-design_23.1.0:/libs/android_support_design.jar
#                                        android-support-v7-appcompat_23.1.0:libs/android_support_v7_appcompat.jar \
#                                        android-support-design_23.1.0:/libs/android_support_design.jar

# the follow line is necessary for private libs.
include $(BUILD_MULTI_PREBUILT)


# additionally, build tests in sub-folders in a separate .apk
include $(call all-makefiles-under,$(LOCAL_PATH))
