##############################################
# TCT Telecom Record Tool                    #
# Author: jie.qiu@tcl.com                    #
#                                            #
# ------------------------------------------ #
# History:                                   #
# 2015-12-14 Create                          #
# 2015-12-21 Add inCall and dataOn           #
##############################################


import re, sys

## Define CSV TAGS
tags = ("Time", "CS State", "CS Tech", "LAU Rej", "PS State", "PS Tech", "RAU Rej", \
        "IS inCall", "Is dataOn","IS Manu Select", "EMC only",)

##############################################
# Function getCsState
##############################################
def getCsState(value):
    returnStr = []
    if (value & 0x03) == 0x01:
        returnStr = "In Service"
    elif (value & 0x03) == 0x02:
        returnStr = "Out Of Service"
    elif (value & 0x03) == 0x03:
        returnStr = "Power Off"
    else:
        returnStr = ""

    return returnStr

##############################################
# Function getPsState
##############################################
def getPsState(value):
    returnStr = []
    if (value & 0x0C) == 0x04:
        returnStr = "In Service"
    elif (value & 0x0C) == 0x08:
        returnStr = "Out Of Service"
    elif (value & 0x0C) == 0x0C:
        returnStr = "Power Off"
    else:
        returnStr = ""

    return returnStr

##############################################
# Function getCsTech
##############################################
def getCsTech(value):
    returnStr = []

    if (value & 0x0F00) == 0x0100:
        returnStr = "GPRS"
    elif (value & 0x0F00) == 0x0200:
        returnStr = "EDGE"
    elif (value & 0x0F00) == 0x0300:
        returnStr = "UMTS"
    elif (value & 0x0F00) == 0x0400:
        returnStr = "1xRTT"
    elif (value & 0x0F00) == 0x0500:
        returnStr = "HSDPA"
    elif (value & 0x0F00) == 0x0600:
        returnStr = "HSUPA"
    elif (value & 0x0F00) == 0x0700:
        returnStr = "EVDO"
    elif (value & 0x0F00) == 0x0800:
        returnStr = "EHRPD"
    elif (value & 0x0F00) == 0x0900:
        returnStr = "LTE"
    elif (value & 0x0F00) == 0x0A00:
        returnStr = "HSPA+"
    elif (value & 0x0F00) == 0x0B00:
        returnStr = "GSM"
    elif (value & 0x0F00) == 0x0C00:
        returnStr = "TD-SCDMA"
    elif (value & 0x0F00) == 0x0D00:
        returnStr = "LTE-CA"
    else:
        returnStr = ""

    return returnStr

##############################################
# Function getPsTech
##############################################
def getPsTech(value):
    returnStr = []

    if (value & 0xF000) == 0x1000:
        returnStr = "GPRS"
    elif (value & 0xF000) == 0x2000:
        returnStr = "EDGE"
    elif (value & 0xF000) == 0x3000:
        returnStr = "UMTS"
    elif (value & 0xF000) == 0x4000:
        returnStr = "1xRTT"
    elif (value & 0xF000) == 0x5000:
        returnStr = "HSDPA"
    elif (value & 0xF000) == 0x6000:
        returnStr = "HSUPA"
    elif (value & 0xF000) == 0x7000:
        returnStr = "EVDO"
    elif (value & 0xF000) == 0x8000:
        returnStr = "EHRPD"
    elif (value & 0xF000) == 0x9000:
        returnStr = "LTE"
    elif (value & 0xF000) == 0xA000:
        returnStr = "HSPA+"
    elif (value & 0xF000) == 0xB000:
        returnStr = "GSM"
    elif (value & 0xF000) == 0xC000:
        returnStr = "TD-SCDMA"
    elif (value & 0x0F00) == 0xD000:
        returnStr = "LTE-CA"
    else:
        returnStr = ""

    return returnStr

##############################################
# Function openRecordFile
##############################################
def openRecordFile():
    args = sys.argv
    if len(args) == 2:
        traceFile = open(str(args[1]), 'rb')
        return traceFile
    else:
        print "openRecordFile failed, please check your file name."
        exit

##############################################
# Function getMessagesFromFile
##############################################
def getMessagesFromFile(traceFile):
    u'''Parses file and creates list of messages, each containing list of its entries (3D table)'''
    itemCount = 0
    itemValidSize = 0
    lineNbr = 0
    allMessages = []
    message = []

    while True:
        line = traceFile.readline()
        if line == "":  # read till EOF
            break
        lineNbr = lineNbr + 1;
        lineList = line.split(",")

        for item in range(0,len(lineList)):
            if len(lineList[item]):
                allMessages.append(lineList[item])

    traceFile.close()
    return allMessages

##############################################
# Function processEntry
##############################################
def processEntry(entry, lineNbr):
    values = [''] * len(tags)
    laurau = 0

    entry1 = entry.split("-")

    if len(entry1[0]) == 1: # Means no more records
        return 0

    time = str(eval("0x"+entry1[0]))
    if len(time) == 7:
        time = "0"+time

    withoutTime = entry1[1]

    if len(entry1) == 3:
        laurau = int(eval("0x"+entry1[2]))

    withoutTime = eval("0x"+withoutTime)

    #Set Time
    values[tags.index("Time")] = time[0]+time[1]+"M"+time[2]+time[3]+"D "+time[4]+time[5]+":"+time[6]+time[7]

    #Set CS&PS State
    values[tags.index("CS State")] = getCsState(withoutTime)
    values[tags.index("PS State")] = getPsState(withoutTime)

    #Set CS&PS Tech
    values[tags.index("CS Tech")] = getCsTech(withoutTime)
    values[tags.index("PS Tech")] = getPsTech(withoutTime)

    #Set EMC only
    if (withoutTime & 0x10) > 0:
        values[tags.index("EMC only")] = "YES"
    else:
        values[tags.index("EMC only")] = ""

    #Set IS Manu Select
    if (withoutTime & 0x80) > 0:
        values[tags.index("IS Manu Select")] = "YES"
    else:
        values[tags.index("IS Manu Select")] = ""

    #Set IS inCall
    if (withoutTime & 0x20) > 0:
        values[tags.index("IS inCall")] = "YES"
    else:
        values[tags.index("IS inCall")] = ""

    #Set Is dataOn
    if (withoutTime & 0x40) > 0:
        values[tags.index("Is dataOn")] = "YES"
    else:
        values[tags.index("Is dataOn")] = ""

    #Set LAU
    if (laurau & 0xFF) != 0:
        values[tags.index("LAU Rej")] = str(laurau & 0xFF)

    #Set RAU
    if (laurau & 0xFF00) != 0:
        values[tags.index("RAU Rej")] = str(laurau>>8 & 0xFF)

    return values

##############################################
# Main
##############################################
if __name__ == "__main__":
    outputFile = open("tctTelecomRecord.csv", 'wb')
    outputFile.write(",".join(tags) + "\n")
    traceFile = openRecordFile()

    allMessages = getMessagesFromFile(traceFile)
    allEntries = []
    for eachEntry in range(0, len(allMessages)):
        val = processEntry(allMessages[eachEntry], len(allMessages))
        if val == 0:
            break
        outputFile.write(",".join(val) + "\n")
        print ",".join(val) + "\n"
        outputFile.flush()
    outputFile.close()
    print "Parsed result saved to tctTelecomRecord.csv file."


