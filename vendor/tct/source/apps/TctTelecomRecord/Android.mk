LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := TelecomRecord

LOCAL_CERTIFICATE := platform
#LOCAL_JAVA_LIBRARIES := qcrilhook telephony-common frameworks_ext tct.framework

LOCAL_MODULE_PATH := $(TARGET_OUT_APP_PATH)


include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))
