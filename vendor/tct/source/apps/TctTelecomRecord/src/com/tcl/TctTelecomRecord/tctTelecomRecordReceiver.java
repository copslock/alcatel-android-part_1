/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiu jie                                                         */
/*  Email  :  jie.qiu@tcl.com                                                 */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : The reciever for creating tctTelecomRecordService            */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 12/14/2015|Qiu jie               |                      |create            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.tcl.TctTelecomRecord;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.TctLog;
import android.os.UserHandle;

public class tctTelecomRecordReceiver extends BroadcastReceiver {
    private static String TAG = "tctTelecomRecordReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        //TctLog.d(TAG,"action = " + action);
        if (action.equals("android.intent.action.BOOT_COMPLETED")){
            TctLog.d(TAG,"start TctTelecomRecord");
            Intent myIntent=new Intent(context,tctTelecomRecordService.class);
            context.startServiceAsUser(myIntent, UserHandle.OWNER);
        }
    }
}
