/******************************************************************************/
/*                                                               Date:11/2014 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2014 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Qiu jie                                                         */
/*  Email  :  jie.qiu@tcl.com                                                 */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments : The Service for TctTelecomRecord                               */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 12/14/2015| Qiu jie              |                      |create            */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.tcl.TctTelecomRecord;

import android.net.Uri;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemProperties;
import android.app.ActivityManager;
import android.app.Service;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.TctLog;
import android.content.BroadcastReceiver;
import android.telephony.ServiceState;
import android.content.ContentResolver;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
//
import android.os.Environment;
import java.io.BufferedWriter;
import java.io.BufferedOutputStream;
import java.io.BufferedInputStream;
import java.io.FileWriter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
//

import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.ITelephony;

public class tctTelecomRecordService extends Service {
    private static String TAG = "TctTelecomRecordService";
    private static boolean DBG = true;

    private static Context mContext;
    private TelecomRecordReceiver mReceiver;
    private IntentFilter mIntentFilter;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mReceiver = new TelecomRecordReceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(TelephonyIntents.ACTION_TCT_TELECOM_RECORD_INFO_UPDATED);

        log("onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        registerReceiver(mReceiver, mIntentFilter);

        return START_STICKY;
    }

    /**
     * Broadcast Receiver for TelecomRecordReceiver
     */
    private class TelecomRecordReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String actionStr = intent.getAction();
            if (TelephonyIntents.ACTION_TCT_TELECOM_RECORD_INFO_UPDATED.equals(actionStr)) {
                String tctRecordInfo = intent.getStringExtra("record-string");
                int slotId = intent.getIntExtra("record-slot", -1);
                if (null != tctRecordInfo
                    && slotId >= 0) {
                    recordFile(tctRecordInfo,slotId);
                }
            }
            else  {
                log("TelecomRecordReceiver error ");
            }
        }
    }

    /*
     * Function recordFile
     */
    private void recordFile(String record, int slot) {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmm");
        Date currDate = new Date(System.currentTimeMillis());
        String timeStr = format.format(currDate);
        String hexTimeStr = Integer.toHexString(Integer.valueOf(timeStr).intValue());

        log(" recordFile current time timeStr="+timeStr);
        log(" recordFile hexTimeStr="+hexTimeStr);

        //move storage location to tctpersist, data in tctpersist will not be cleared when user do factory reset
        //File externalStorage = Environment.getExternalStorageDirectory();
        File externalStorage = new File("/tctpersist/telecomRecord/");
        if(!externalStorage.exists()) {
            if(!externalStorage.mkdirs()) {
                log(" recordFile mkdir failed");
                return;
            }
        }

        String recordFileName = "record_slot"+slot+".txt";
        String recordZipFileName = "record_slot"+slot+"_old.zip";
        long fileLen = 0;

        try {
            if (externalStorage.canWrite()) {
                File resultFile = new File(externalStorage, recordFileName);
                resultFile.setWritable(true, false);
                BufferedWriter rsWriter = new BufferedWriter(new FileWriter(resultFile,true));
                log(" Save in: " + resultFile.getAbsolutePath());

                rsWriter.write(hexTimeStr+"-"+record+",");
                rsWriter.flush();
                rsWriter.close();

                fileLen = resultFile.length();

                if (fileLen > 1024 * 500) { //   if > 500K, zip it
                    File zipfile = new File(externalStorage, recordZipFileName);
                    ZipOutputStream zos = null;
                    if (zipfile.exists()) {
                        //delete zip file
                        zipfile.delete();
                        log(" delete zipfile ");
                    }
                    zipfile.createNewFile();
                    zipfile.setWritable(true, false);
                    log(" create zip file ");
                    FileOutputStream os = new FileOutputStream(zipfile);
                    BufferedOutputStream bos = new BufferedOutputStream(os);
                    zos = new ZipOutputStream(bos);
                    zipFile(resultFile, resultFile.getParent(), zos);

                    if (zos != null) {
                        zos.closeEntry();
                        zos.close();
                        resultFile.delete();
                    }
                }
            }
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

    }

    /*
     * Function zipFile
     */
    private void zipFile(File source, String basePath, ZipOutputStream zos)
        throws IOException {
        log(" zipFile, basepath: "+basePath);
        InputStream is = null;
        String pathName;
        byte[] buf = new byte[1024];
        int length = 0;
        try{
            pathName = source.getPath().substring(basePath.length() + 1);
            is = new FileInputStream(source);
            BufferedInputStream bis = new BufferedInputStream(is);
            zos.putNextEntry(new ZipEntry(pathName));
            while ((length = bis.read(buf)) > 0) {
                zos.write(buf, 0, length);
            }
        }finally {
            if(is != null) {
                is.close();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    private static void log(String s) {
        if(DBG) {
            TctLog.d(TAG,s);
        }
    }
}
