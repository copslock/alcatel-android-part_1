package com.tct.magiclock.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider.Category;
import com.tct.magiclock.db.MagiclockProvider.FavoriteView;
import com.tct.magiclock.db.MagiclockProvider.MypicView;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.db.MagiclockProvider.ShownPictureView;
import com.tct.magiclock.model.ItemInfo;
import com.tct.magiclock.util.MGLKUtils;
import com.tct.magiclock.utils.SettingPref;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chunlin.tang on 2015-10-09.
 */
public class MagicLockBaseAdapter extends CursorAdapter {
    private static final String TAG = "BaseAdapter";

    private Map<String, ItemInfo> mDatas = null;
    /**
     * Record the id of gridview's item which has been selected
     */
    private ArrayList<String> mSelectedIds = null;

    public ArrayList<String> getSelectedIdlist() {
        return mSelectedIds;
    }
    /**
     * Constructor that allows control over auto-requery.  It is recommended
     * you not use this, but instead .
     * When using this constructor, {@link #FLAG_REGISTER_CONTENT_OBSERVER}
     * will always be set.
     *
     * @param context     The context
     * @param c           The cursor from which to get the data.
     * @param autoRequery If true the adapter will call requery() on the
     *                    cursor whenever it changes so the most recent
     */
    public MagicLockBaseAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mDatas = new HashMap<String, ItemInfo>();
        mSelectedIds = new ArrayList<String>();
    }

    /**
     * Makes a new view to hold the data pointed to by cursor.
     *
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     *                moved to the correct position.
     * @param parent  The parent to which the new view is attached to
     * @return the newly created view.
     */
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        try {
            View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.magiclock_base_item, parent, false);
            View selectedFlag = view.findViewById(R.id.mfi_selected_flag);
            view.setTag(selectedFlag);
            Log.d(TAG, "****** [newView] create view ******");
            return view;
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }
        return null;
    }

    /**
     * Bind an existing view to the data pointed to by cursor
     *
     * @param view    Existing view, returned earlier by newView
     * @param context Interface to application's global information
     * @param cursor  The cursor from which to get the data. The cursor is already
     */
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        try {
            View selectedView = (View) view.getTag();
            String id = cursor.getString(cursor.getColumnIndexOrThrow(Picture._ID));
            view.setId(Integer.valueOf(id));
            ItemInfo data = mDatas.get(id);
            if (null == data) {
                data = new ItemInfo();
                mDatas.put(id, data);
                data.id = id;
                data.picName = cursor.getString(cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME));
                data.isSelected = mSelectedIds.contains(id);
            }
            if (null == data.picture) {
                data.picture = getDrawble(context, cursor.getBlob(cursor.getColumnIndexOrThrow(Picture.ICON)));
            }

            Log.d(TAG, "[bindView] data.picName=" + data.picName
                    + " | data.isSelected=" + data.isSelected + " | id=" + id);
            selectedView.setVisibility(data.isSelected ? View.VISIBLE : View.GONE);
            selectedView.setTag(data);

            if (null != data.picture) {
                view.setBackground(data.picture);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }

    }

    // PR 1194403 Add by chunlin.tang@tcl.com 2015-12-26 Begin
    // Fix: IllegalStateException: Couldn't read row 2, col 0 from CursorWindow.
    @Override
    public long getItemId(int position) {
        try {
            return super.getItemId(position);
        } catch (Exception e) {
            Log.w(TAG, "[getItemId] Exception: " + e, e);
        }
        return 0;
    }
    // PR 1194403 Add by chunlin.tang@tcl.com 2015-12-26 End

    private Drawable getDrawble(Context context, byte[] picData) {
        if (null == picData || null == context) {
            return  null;
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(picData, 0, picData.length, null);
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    @SuppressWarnings("unchecked")
    public void updateFavorites(Activity context, CharSequence title) {
        deleteOption(context, title, false);
    }

    @SuppressWarnings("unchecked")
    public void deleteMyPictures(Activity context, CharSequence title) {
        deleteOption(context, title, true);
    }

    private void deleteOption(Activity context, CharSequence title, boolean isPicture) {
        final int num = mSelectedIds.size();
        String[] ids = new String[num];
        for (int i = 0; i < num; i++) {
            String key = mSelectedIds.get(i);
            ids[i] = key;
            Log.d(TAG, "[deleteOption] id=" + key);
            mDatas.remove(key);
        }
        mSelectedIds.clear();
        if (isPicture) {
            new DeleteMyPicture(context, title, num).execute(ids);
        } else {
            new DeleteFavoriteTask(context, title, num).execute(ids);
        }
    }

//    public void selectAll(GridView list, MenuItem deleteBtn) {
//        try {
//            setAllCheckBox(list, true);
//            Cursor cursor = this.getCursor();
//            cursor.moveToPosition(-1);
//            mSelectedIds.clear();
//            while (cursor.moveToNext()) {
//                String id = cursor.getString(cursor.getColumnIndexOrThrow(Picture._ID));
//                mSelectedIds.add(id);
//            }
//            deleteBtn.setVisible(mSelectedIds.size() > 0);
//        } catch (Exception e) {
//            Log.e(TAG, "[selectAll] exception: " + e, e);
//        }
//    }

    /**
     * not be sued because there are not selected function
     * @deprecated
     * @param list
     * @param value
     */
    private void setAllCheckBox(GridView list, boolean value) {
        int count = list.getChildCount();
        Log.d(TAG, "[selectAll] childCount=" + count + " | count=" + list.getCount());
        for (int i = 0; i < count; i++) {
            View v= list.getChildAt(i);
            setCheckBox( v.findViewById(R.id.mfi_selected_flag), value);
        }
    }

    private void setCheckBox(View view, boolean isSelected) {
        if (isSelected) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    /**
     * clear all selected item. called by {@link MagicLockBaseActivity#onKeyDown(int, KeyEvent)}.
     * @param list
     */
    public void clearAll(GridView list) {
        if (null == mSelectedIds) {
            return;
        }
        for (String sId : mSelectedIds) {

            mDatas.get(sId).isSelected = false;
            View v = list.findViewById(Integer.valueOf(sId));
            if (null != v) {
                v.findViewById(R.id.mfi_selected_flag).setVisibility(View.GONE);
            } else {
                Log.e(TAG, "[clearAll] v is null");
            }
        }
        mSelectedIds.clear();
    }

    private abstract class BaseAsyncTask extends AsyncTask<String[], Integer, Boolean> {
        Activity mContext = null;
//        ProgressDialog mProgressDialog = null;
        boolean mCancelOperation = false ;

        public BaseAsyncTask(Activity context, CharSequence title, int num) {
            mContext = context;
//            initProgressDialog(title, num);
        }

//        private void initProgressDialog(CharSequence title, int num) {
//            mProgressDialog = new ProgressDialog(mContext);
//            mProgressDialog.setTitle(title);
//            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//
//            mProgressDialog.setMax(num);
//            mProgressDialog.setProgress(0);
//
//            mProgressDialog.setButton(Dialog.BUTTON_POSITIVE, mContext.getString(android.R.string.cancel
//                    ),
//                    new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // TODO Auto-generated method stub
//                            mCancelOperation = true;
//                        }
//                    });
//            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                @Override
//                public void onCancel(DialogInterface dialog) {
//                    // TODO Auto-generated method stub
//                    mCancelOperation = true;
//                }
//            });
//
//
//        }

//        @Override
//        protected void onPreExecute() {
//            try {
//                Log.e(TAG, "[onPreExecute] mProgressDialog: " + mProgressDialog);
//                mProgressDialog.show();
//
//            } catch (Exception e) {
//                Log.e(TAG, "[onPreExecute] exception: " + e, e);
//            }
//        }

//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            try {
//                mProgressDialog.setProgress(values[0]);
//            } catch (Exception e) {
//                Log.e(TAG, "[onProgressUpdate] exception: " + e, e);
//            }
//        }

//        protected void onPostExecute(Boolean result) {
//            try {
////                mProgressDialog.dismiss();
//                mContext.finish();
////                mContext.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//            } catch (Exception e) {
//                Log.e(TAG, "[onPostExecute] exception: " + e, e);
//            }
//        }
    }

    private class DeleteMyPicture extends BaseAsyncTask {

        public DeleteMyPicture(Activity context, CharSequence title, int num) {
            super(context, title, num);
        }


        @Override
        protected Boolean doInBackground(String[]... params) {
            try {
                String[] ids = params[0];
                final int totalNum = ids.length;

                final int NUM = 30; // update 30 each times.

                String selection = null;
                Log.d(TAG, "[doInBackground] totalNum=" + totalNum);
                //if (totalNum >= NUM) {
                selection = getQuestionMarkSelection(totalNum);
                Log.d(TAG, "DeleteMyPicture [doInBackground] selection="+selection);
                //}

                Cursor cursor = mContext.getContentResolver().query(Picture.CONTENT_URI,
                        new String[] {Picture._ID, Picture.PICTURE_NAME, Picture.ICON_PATH},
                        selection, ids, null);
                int pos = 0;
                final int idIndex = cursor.getColumnIndexOrThrow(Picture._ID);
                final int fileIndex = cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME);
//                final int pathIndex = cursor.getColumnIndexOrThrow(Picture.ICON_PATH);
                ArrayList<String> deleteIds = new ArrayList<String>();
                int updatedNum = 0;
                while (cursor.moveToNext()) {
                    // cancel options
                    if (mCancelOperation) {
                        break;
                    }
                    String sId = cursor.getString(idIndex);
                    String path = Constants.getFolderPath(mContext, TAG); // cursor.getString(pathIndex);
                    String name = cursor.getString(fileIndex);
                    pos ++;
//                    this.publishProgress(pos);
                    if (!deleteFile(path+name)) {
                        Log.d(TAG, "DeleteMyPicture=>[doInBackground] " +path+name +" delete fail!");
                        continue;
                    }

                    deleteIds.add(sId);

                    if (pos % NUM == 0 && deleteIds.size() > 0) {
                        // get all shown pic from
                        updatedNum += deleteFiles(deleteIds);
                    }
                }

                // Process the group that favorites number less than NUM.
                final int notUpdateNum = deleteIds.size();
                if (notUpdateNum > 0) {
                    // There are several favorites not be updated. Just save they.
//                    selection = getQuestionMarkSelection(notUpdateNum);
                    updatedNum += deleteFiles(deleteIds);;
//                    this.publishProgress(updatedNum);
                }
                if (updatedNum > 0) {
                    mContext.getContentResolver().notifyChange(MypicView.CONTENT_URI, null);
                    // If my picture is selected, and some pictures has been deleted successfully.
                    // notify lock screen delete this picture's name from the showing queen.
                    if (SettingPref.isMagicOn(mContext)) {
                        notifyLockscreen();
                    }

                }
            } catch (Exception e) {
                Log.e(TAG, "[doInBackground] Exception: " + e, e);
//                e.printStackTrace();
            }
            return null;
        }

        private void notifyLockscreen() {
            Cursor cursor = mContext.getContentResolver().query(Category.CONTENT_URI,
                    new String[] {Category._ID},
                    Category.CAT + "=? AND " + Category.SELECTED + "=?",
                    new String[]{Constants.CAT_MYPIC, Category.FLAG_SELECTED + ""},
                    null);
            if (null != cursor && cursor.getCount() > 0) {
                new MagicLockGetShownPictureTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mContext); // MODIFIED by Zhenhua.Fan, 2016-11-09,BUG-3266668
            }
        }
        private int deleteFiles(ArrayList<String> deleteIds) {
            String deleteSelection = getQuestionMarkSelection(deleteIds.size());
            String[] deleteArr = deleteIds.toArray(new String[0]);
//            Cursor shownCursor = mContext.getContentResolver().query(ShownPictureView.CONTENT_URI,
//                    new String[] {Picture.PICTURE_NAME},
//                    deleteSelection,
//                    deleteArr, null);
            // delete they from db
            int count = mContext.getContentResolver().delete(Picture.CONTENT_URI, deleteSelection, deleteArr);
            deleteIds.clear();
//            if (null == shownCursor || shownCursor.getCount() <= 0) {
//                return count;
//            }
//            ArrayList<String> shownArr = new ArrayList<String>();
//            final int picNameIndex = shownCursor.getColumnIndexOrThrow(Picture.PICTURE_NAME);
//            while (shownCursor.moveToNext()) {
//                shownArr.add(shownCursor.getString(picNameIndex));
//            }
//            shownCursor.close();
//
//            if (count > 0) {
//                // TODO send showArr to keyGuard through broadcast
//                //
//            }
            return count;
        }
        private boolean deleteFile(String picPath) {
            try {
                File file = new File(picPath);
                if (file.canWrite() && file.delete()) {
                    return true;
                }
            } catch (Exception e) {
                Log.e(TAG, "[deleteFile] Exception: " + e, e);
//                e.printStackTrace();
            }
            return false;
        }
    }
    private class DeleteFavoriteTask extends BaseAsyncTask {

        public DeleteFavoriteTask(Activity context, CharSequence title, int num) {
            super(context, title, num);
        }

        @Override
        protected Boolean doInBackground(String[]... params) {
            Boolean ret = false;
            try {
//                ArrayList<String> datas = params[0];
                String[] ids = params[0]; // datas.toArray(new String[0]);
                final int totalNum = ids.length;
                Log.d(TAG, "DeleteFavoriteTask [doInBackground] ids="+Arrays.deepToString(ids));

                ContentValues values = new ContentValues();
                values.put(Picture.FAVORITE, Picture.FLAG_NOT_FAVORITE);
                byte[] icon = null;
                values.put(Picture.ICON, icon);
                final int NUM = 50; // update 50 each times.

                String selection = null;
                final ArrayList<String> whereArgs = new ArrayList<String>();
                Log.d(TAG, "[doInBackground] totalNum=" + totalNum);
                if (totalNum >= NUM) {
                    selection = getQuestionMarkSelection(NUM);
                    Log.d(TAG, "DeleteFavoriteTask [doInBackground] selection="+selection);
                }

                int updatedNum = 0;
                for (int i = 0; i < totalNum; ) {
                    // cancel options
                    if (mCancelOperation) {
                        break;
                    }
                    // Add the favorite's contactId into group.
                    whereArgs.add(String.valueOf(ids[i]));
                    i++;
                    Log.d(TAG, "DeleteFavoriteTask [doInBackground] i="+i+"; i % NUM="+(i % NUM));
                    if (i % NUM == 0) {
                        // update option
                        updatedNum += batchUpdateFavorites(values, selection, whereArgs);
//                        this.publishProgress(updatedNum);
                    }
                }
                // Process the group that favorites number less than NUM.
                final int notUpdateNum = whereArgs.size();
                if (notUpdateNum > 0) {
                    // There are several favorites not be updated. Just save they.
                    selection = getQuestionMarkSelection(notUpdateNum);
                    updatedNum += batchUpdateFavorites(values, selection, whereArgs);
//                    this.publishProgress(updatedNum);
                }
                Log.d(TAG, "[batchUpdateFavorites]updatedNum="+updatedNum);
                if (updatedNum > 0) {
                    ret = true;
                    mContext.getContentResolver().notifyChange(FavoriteView.CONTENT_URI, null);
                }
            } catch (Exception e) {
                Log.e(TAG, "[doInBackground] Exception: " + e, e);
            }
            return ret;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.d(TAG, "[DeleteFavoriteTask RESULT:"+result);
            if(result){
                new MagicLockGetShownPictureTask().execute(mContext);
            }
            super.onPostExecute(result);
        }

        private int batchUpdateFavorites(ContentValues values, String selection, ArrayList<String> whereArgs) {
            String[] curIds = whereArgs.toArray(new String[0]);
            whereArgs.clear();
            int count = mContext.getContentResolver().update(Picture.CONTENT_URI,
                    values, selection, curIds);

            return count;
        }
    }


    /**
     * create and return a selection string.<br>
     * This function used by
     * @param num the number of question mark
     * @return a selection string. i.e. _id IN (?,?,?)
     */
    private String getQuestionMarkSelection(final int num) {
        final StringBuilder whereBuilder = new StringBuilder();
        final String[] questionMarks = new String[num];
        Arrays.fill(questionMarks, "?");
        whereBuilder.append(Picture._ID + " IN (").
                append(TextUtils.join(",", questionMarks)).
                append(")");
        return whereBuilder.toString();
    }
}
