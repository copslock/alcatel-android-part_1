package com.tct.magiclock.service;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.storage.StorageManager;
import android.os.storage.StorageEventListener;
import android.annotation.Nullable;
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.activity.MagicLockGetShownPictureTask;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.util.MGLKUtils;
import com.tct.magiclock.util.PhotoUtils;
import com.tct.magiclock.utils.Cache;
import com.tct.magiclock.utils.SettingPref;
import com.tct.magiclock.utils.ShakeManager;
import com.tct.magiclock.utils.Utils;

import java.io.File;
import java.util.ArrayList;

import android.net.Uri;
import android.content.ContentValues;
import android.content.ContentProviderClient;
import android.os.RemoteException;
import java.lang.IllegalArgumentException;
import com.tct.magiclock.util.DataCollectUtil;
/**


/**
 * Created by chunlin.tang on 12/20/15.
 */
public class BootCompletedService extends Service {
    private static final String TAG = "BCS";
    private Context mContext;
    private Messenger mReply;
    private static final int REQ_CONNECT = 10;
    private static final int RES_CONNECT = 11;
    private static final int REQ_UPDATE = 12;
    private static final int RES_UPDATE = 13;
    private static final int REQ_RECORD = 14;
    private static final int REQ_DELETE = 15;
    private static final int REQ_SENSOR = 16;
    private boolean mPin;

    public static ArrayList<Bundle> mList;
    private int index = -1;

    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    private ContentValues mValuesData;
    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        Cache.getCache().put(SettingPref.CHANGE_INTERNAL, SettingPref.getString(mContext, SettingPref.CHANGE_INTERNAL, "0"));
    }

    /**
     * Return the communication channel to the service.  May return null if
     * clients can not bind to the service.  The returned
     * {@link IBinder} is usually for a complex interface
     * that has been <a href="{@docRoot}guide/components/aidl.html">described using
     * aidl</a>.
     *
     * @param intent The Intent that was used to bind to this service,
     *               as given to {@link Context#bindService
     *               Context.bindService}.  Note that any extras that were included with
     *               the Intent at that point will <em>not</em> be seen here.
     * @return Return an IBinder through which clients can call on to the
     * service.
     */
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "[onBind] intent=" + intent);
        return new Messenger(dataHandler).getBinder();
    }

    private Handler dataHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case REQ_CONNECT:
                    mReply = msg.replyTo;
                case REQ_UPDATE:
                    if (!"0".equals(Cache.getCache().get(SettingPref.CHANGE_INTERNAL))) break;
                case RES_UPDATE:
                    if (mPin || mList == null || mList.isEmpty()) break;
                    Message result = Message.obtain();
                    result.what = RES_UPDATE;
                    index = ++index % mList.size();
                    result.setData(mList.get(index));
                    try {
                        mReply.send(result);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case REQ_RECORD:
                    Bundle data = mList.get(index);
                    boolean like = msg.arg1 == 1;
                    if (data.getBoolean(Constants.KEY_LIKE) != like) {
                        data.putBoolean(Constants.KEY_LIKE, like);
                        updateFavourite(like, data.getString(Constants.KEY_NAME));
                        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                        mValuesData = new ContentValues();
                        mValuesData.put("action","ADD");
                        mValuesData.put(DataCollectUtil.MAGICLOCK_FAVORITETAP_NUM,String.valueOf(1));
                        DataCollectUtil.storeData(mContext, mValuesData);
                        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                    }
                    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                    //tell if it is a pin press
                    Boolean ifPin = msg.arg2==1?true:false;
                    if (mPin != ifPin){
                        mValuesData = new ContentValues();
                        mValuesData.put("action","ADD");
                        mValuesData.put(DataCollectUtil.MAGICLOCK_PINTAP_NUM,String.valueOf(1));
                        DataCollectUtil.storeData(mContext, mValuesData);
                    }
                    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                    mPin = msg.arg2 == 1;
                    if (mPin) {
                        ShakeManager.unregisterSensor();
                    } else {
                        ShakeManager.registerSensor(mContext);
                    }
                    break;
                case REQ_DELETE:
                    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                    mValuesData = new ContentValues();
                    mValuesData.put("action","ADD");
                    mValuesData.put(DataCollectUtil.MAGICLOCK_DELETETAP_NUM,String.valueOf(1));
                    DataCollectUtil.storeData(mContext, mValuesData);
                    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494
                    Bundle item = mList.remove(index);
                    deleteDislikePic(item.getString(Constants.KEY_NAME));
                    mPin = false;
                    dataHandler.sendEmptyMessage(RES_UPDATE);
                    break;
                case REQ_SENSOR:
                    if (msg.arg1 == 1 && !mPin) {
                        ShakeManager.registerSensor(mContext);
                    } else {
                        ShakeManager.unregisterSensor();
                    }
                    break;
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (null == intent) {
            return START_NOT_STICKY;
        }
        Context context = getApplicationContext();
        if(!SettingPref.isMagicOn(context)){
            return START_NOT_STICKY;
        }
        Log.d(TAG, "[onStartCommand] intent=" + intent);
        if (Utils.ACTION_CHANGE.equals(intent.getAction())) {
            dataHandler.sendEmptyMessage(RES_UPDATE);
        }
        return START_NOT_STICKY;
    }

    private void updateFavourite(boolean like, String picName) {
        ContentValues values = new ContentValues();
        values.put(Picture.FAVORITE, like);
        if (like) {
            Bitmap bp = PhotoUtils.centerCropImage(
                    PhotoUtils.getIntFromXml(mContext,R.dimen.magiclock_fav_item_width,171),
                    PhotoUtils.getIntFromXml(mContext,R.dimen.magiclock_fav_item_height,273),
                    Constants.MAGICLOCK_FILE_PATH + picName);
            values.put(Picture.ICON, PhotoUtils.flattenBitmap(bp));
            bp.recycle();
        } else {
            values.putNull(Picture.ICON);
        }

        int count = mContext.getContentResolver().update(Picture.CONTENT_URI,
                values, Picture.PICTURE_NAME + "=?", new String[] {picName});
    }

    private void deleteDislikePic(String picName) {
        String selection = Picture.PICTURE_NAME + "=?";
        int cursor = mContext.getContentResolver().delete(Picture.CONTENT_URI, selection, new String[] {picName});
        try {
            File file = new File(Constants.MAGICLOCK_FILE_PATH + picName);
            if (file.canWrite() && file.delete() && cursor > 0) {
                Log.i(TAG, "delete success");
            }
        } catch (Exception e) {
            Log.e(TAG, "[deletePicFile] Exception: " + e);
        }
    }
}
