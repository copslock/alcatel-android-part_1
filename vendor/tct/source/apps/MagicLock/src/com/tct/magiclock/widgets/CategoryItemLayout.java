package com.tct.magiclock.widgets;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tct.magiclock.R;
import com.tct.magiclock.utils.*;

/**
 * Item layout of category for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class CategoryItemLayout extends RelativeLayout implements View.OnClickListener {
    private TextView mLabel;
    private CheckBox mCheck;
    private OnCheckedChangeListener mOnCheckedChangeListener;
    private int mask;

    public CategoryItemLayout(Context context) {
        super(context);
    }

    public CategoryItemLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        mLabel = (TextView) findViewById(R.id.label);
        mCheck = (CheckBox) findViewById(R.id.check);
        View entrance = findViewById(R.id.local_entrance);

        mCheck.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            if (mOnCheckedChangeListener != null) {
                mOnCheckedChangeListener.onCheckedChange(this, mask, isChecked);
            }
        });

        setOnClickListener(this);
        if (entrance != null) {
            entrance.setOnClickListener(this);
        }
    }

    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        // IMPORTANT!
        // We must Do Nothing because we had already store values
    }

    @Override
    public void onClick(View v) {
        if (v == this) {
            mCheck.setChecked(!mCheck.isChecked());
        } else if (mask == Config.MASK_CATEGORY_LOCAL) {
            Intent intent = new Intent(Utils.ACTION_LOCAL);
            getContext().startActivity(intent);
        } else if (mask == Config.MASK_CATEGORY_FAVORITE) {
            Intent intent = new Intent(Utils.ACTION_FAVORITE);
            getContext().startActivity(intent);
        }

    }

    public boolean isChecked() {
        return mCheck.isChecked();
    }

    public void setChecked(boolean isChecked) {
        mCheck.setChecked(isChecked);
    }

    public void setMaskAndStatus(int mask, int checkedItem) {
        this.mask = mask;
        mCheck.setChecked((checkedItem & mask) != 0);
    }

    public void setText(int resId) {
        mLabel.setText(resId);
    }

    public void setText(String text) {
        mLabel.setText(text);
    }

    public void setCover(int resId) {
        setBackgroundResource(resId);
    }

    public void setCover(String fileName) {
        //
    }

    public void setOnCheckedChangeListener (OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    public interface OnCheckedChangeListener {
        void onCheckedChange(CategoryItemLayout view, int mask, boolean isChecked);
    }

}
