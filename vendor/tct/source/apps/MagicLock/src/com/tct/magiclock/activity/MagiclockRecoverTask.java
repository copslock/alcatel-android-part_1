package com.tct.magiclock.activity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider;
import com.tct.magiclock.util.PhotoUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sdduser on 3/3/16.
 */
public class MagiclockRecoverTask extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "MagiclockRecoverTask";

    public MagiclockRecoverTask() {

    }

    @Override
    protected Void doInBackground(Context... params) {
        Map<String, String> catIds = getSelectedCat(params[0]);
        getSelectedCatPic(params[0], catIds);
        return null;
    }

    private void saveCategoryCover(Context context, String fileName, String cat) {
        try {
            ContentValues values = new ContentValues();
            Bitmap bp = PhotoUtils.centerCropImage(
                    PhotoUtils.getIntFromXml(context, R.dimen.magiclock_fav_item_width, 171),
                    PhotoUtils.getIntFromXml(context, R.dimen.magiclock_fav_item_height, 273),
                    Constants.getFolderPath(context, TAG) + fileName);
            values.put(MagiclockProvider.Category.COVER, PhotoUtils.flattenBitmap(bp));
            bp.recycle();

            int count = context.getContentResolver().update(MagiclockProvider.Category.CONTENT_URI,
                    values, MagiclockProvider.Category.CAT + "=?", new String[]{cat});
//            getContentResolver().notifyChange(MagiclockProvider.Category.CONTENT_URI, null);
            Log.d(TAG, "[saveCategoryCover] count: " + count + " | fileName=" + fileName);
            // if (count > 0) {
            //     ArrayList<String> catList = new ArrayList<String>();
            //     catList.add(cat);
            //     Intent i = new Intent(MagicLockCategoryActivity.ACTION_UPDATE_COVER);
            //     i.putStringArrayListExtra(MagiclockProvider.Category.CAT, catList);
            //     LocalBroadcastManager.getInstance(context).sendBroadcast(i);
            // }
        } catch (Exception e) {
            Log.e(TAG, "[saveCategoryCover] Exception: " + e, e);
        }
    }

    private String getQuestionMarkSelection(final int num, String tag) {
        final StringBuilder whereBuilder = new StringBuilder();
        final String[] questionMarks = new String[num];
        Arrays.fill(questionMarks, "?");
        whereBuilder.append(tag).
                append(TextUtils.join(",", questionMarks)).
                append(")");
        return whereBuilder.toString();
    }

    /**
     * @param context
     */
    private void getSelectedCatPic(Context context, Map<String, String> catIds) {
        if (catIds == null || catIds.size() <= 0) {
            Log.d(TAG, "select category is 0.");
            return;
        }
        Cursor cursor = null;
        Uri uri = MagiclockProvider.Picture.CONTENT_URI;
        String[] projection = new String[]{MagiclockProvider.Picture.CAT_DATA_ID,
                MagiclockProvider.Picture.PICTURE_NAME};

        String selection = MagiclockProvider.Picture.CAT_DATA_ID + getQuestionMarkSelection(catIds.size(), " IN (")
                + ";" + MagiclockProvider.Picture.CAT_DATA_ID;
        cursor = context.getContentResolver().query(uri, projection,
                selection, catIds.keySet().toArray(new String[0]), null);
        Log.d(TAG, "cursor count = " + cursor.getCount());
        if (null != cursor && cursor.getCount() > 0) {
            final int picIndex = cursor.getColumnIndexOrThrow(MagiclockProvider.Picture.PICTURE_NAME);
            int picSelected = cursor.getColumnIndexOrThrow(MagiclockProvider.Picture.CAT_DATA_ID);
            ArrayList<String> tempDatas = new ArrayList<String>();
            cursor.moveToPosition(-1);
            Log.d(TAG, "cursor move -1");
            while (cursor.moveToNext()) {
                String picName = cursor.getString(picIndex);
                String picCatId = cursor.getString(picSelected);
                String catName = catIds.get(picCatId);
                if (null != catName) {
                    saveCategoryCover(context, picName, catName);
                } else {
                    Log.d(TAG, "doInBackground, picCatId= " + picCatId + " not in select category");
                }
            }
        }
    }

    /**
     * get Selected Category id list
     *
     * @param context
     * @return
     */

    private Map getSelectedCat(Context context) {
        Cursor cursor = null;
        ContentResolver resolver = context.getContentResolver();
        Uri uri = MagiclockProvider.Category.CONTENT_URI;
        String selection = null;
        String[] projection = new String[]{
                MagiclockProvider.Category._ID,
                MagiclockProvider.Category.CAT,
                MagiclockProvider.Category.COVER};
        selection = MagiclockProvider.Category.SELECTED + "="
                + MagiclockProvider.Category.FLAG_SELECTED + " AND "
                + MagiclockProvider.Category.CAT + " NOT IN (?,?)";
        Log.d(TAG, "selected cat.");

        cursor = resolver.query(uri, projection, selection, new String[]{Constants.CAT_MYPIC, Constants.CAT_FAVORITE}, null);
        if (null == cursor || cursor.getCount() <= 0) {
            Log.d(TAG, "selected cat =0 | cat == null.");
            return null;
        }
        int idIndex = cursor.getColumnIndexOrThrow(MagiclockProvider.Category._ID);
        final int tagIndex = cursor
                .getColumnIndexOrThrow(MagiclockProvider.Category.CAT);
        final int coverIndex = cursor
                .getColumnIndexOrThrow(MagiclockProvider.Category.COVER);
        Map<String, String> selectedCat = new HashMap<>();
        while (cursor.moveToNext()) {
            String title = cursor.getString(tagIndex);
            int id = cursor.getInt(idIndex);

            if (null == cursor.getBlob(coverIndex)) {
                Log.d(TAG, "selected title is : " + title + "| selected id is :" + id);
                selectedCat.put(String.valueOf(id), title);
            } else {
                Log.d(TAG, "selected category has cover pic.");
            }
        }

        return selectedCat;
    }
}
