package com.tct.magiclock.model;

import android.graphics.drawable.Drawable;

/**
 * Created by chunlin.tang on 8/19/15.
 */
public class ItemInfo {
    public String id = null;
    public Drawable picture = null;
    public String picName = null;
    public String picPath = null;
    /**
     * true means current item has been selected
     */
    public boolean isSelected = false;
    public ItemInfo(){}
    public ItemInfo(String picName, String picPath) {
        this.picName = picName;
        this.picPath = picPath;
    }
}
