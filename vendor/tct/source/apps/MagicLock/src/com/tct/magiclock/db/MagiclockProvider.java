package com.tct.magiclock.db;

import java.util.ArrayList;

import android.content.ContentUris;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.tct.magiclock.Constants;

public class MagiclockProvider extends ContentProvider {

    private static final String TAG = "Provider";
    private static final String DATABASE_NAME = "magiclock.db";
    private static final int DATABASE_VERSION = 2;

    public static final String AUTHORITY = "com.tct.magiclock";
    private DatabaseHelper mOpenHelper;

    public MagiclockProvider() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        int count = db.delete(uri.getPathSegments().get(0), selection, selectionArgs);
        if (count > 0) {
            db.setTransactionSuccessful();
        }
        db.endTransaction();
        return count;
    }

    @Override
    public String getType(Uri uri) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        String table = uri.getPathSegments().get(0);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        if (Picture.TB_NAME.equals(table)) {
            // If insert into Picture table, remove the cat data from values and put cat id into values.
            String tag = values.getAsString(Category.CAT);
            if (TextUtils.isEmpty(tag)) {
                // throw exception that should give a tag.
                throw new IllegalArgumentException("Should put CAT_DATA into values!");
            }
            // get tag id

            Cursor cursor = query(Category.CONTENT_URI, new String[]{Category._ID}, Category.CAT + "=?",
                    new String[]{tag}, null);
            if (null == cursor || cursor.getCount() <= 0) {
                // throw exception that the tag not exists.
                throw new IllegalArgumentException("Do not support the cat tag (" + tag + ")!");
            }
            final int idIndex = cursor.getColumnIndexOrThrow(Category._ID);
            while (cursor.moveToNext()) {
                int tagId = cursor.getInt(idIndex);
                values.remove(Category.CAT);
                values.put(Picture.CAT_DATA_ID, tagId);
            }
        }

        try {
            long rowId = db.insert(table, null, values);
            if (rowId > 0) {
                return ContentUris.withAppendedId(uri, rowId);
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreate() {
    	Log.d(TAG, "MagiclockProvider==>[onCreate] create db named " + DATABASE_NAME);
        mOpenHelper = new DatabaseHelper(getContext(), DATABASE_NAME, null, DATABASE_VERSION);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(uri.getPathSegments().get(0));
        //modify by dongdong.wang@tcl.com 2016-03.04 for defect 1058525 begin
        Cursor result = null;
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        if (null != selection) {
            String[] specialRequire = selection.split(";");
            if (null != specialRequire && specialRequire.length >= 2) {
                result = qb.query(db, projection, specialRequire[0], selectionArgs, specialRequire[1], null, sortOrder);
            } else  {
                Log.d(TAG, "group by == null, have no this special Requirements");
                result = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            }
        }else {
            Log.d(TAG, "selection == null");
            result = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        }
        result.setNotificationUri(getContext().getContentResolver(), uri);
        //modify by dongdong.wang@tcl.com 2016-03.04 for defect 1058525  end
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.update(uri.getPathSegments().get(0), values, selection, selectionArgs);
        return count;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context, String name,
                CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Picture.TB_NAME +
                    "(" + Picture._ID + " INTEGER PRIMARY KEY," +
                    Picture.CAT_DATA_ID + " INTEGER," +
                    Picture.FAVORITE + " INTEGER NOT NULL DEFAULT " + Picture.FLAG_NOT_FAVORITE + "," +
                    Picture.MY_PICTURE + " INTEGER NOT NULL DEFAULT " + Picture.FLAG_NOT_MYPICTURE + "," +
                    Picture.AUTHOR_NAME + " TEXT," +
                    Picture.PICTURE_NAME + " TEXT UNIQUE," +
                    Picture.ICON + " BLOB," +
                    Picture.DOWNLOAD_TIME + " TEXT," +
                    Picture.ICON_PATH + " TEXT);");

            db.execSQL("CREATE TABLE IF NOT EXISTS " + Category.TB_NAME +
                    "(" + Category._ID + " INTEGER PRIMARY KEY," +
                    Category.CAT + " TEXT," +
                    Category.SELECTED +" INTEGER NOT NULL DEFAULT " + Category.FLAG_SELECTED + "," +
                    Category.COVER + " BLOB);");

            createShownPictureView(db);
            createFavoritesView(db);
            createMypicView(db);
            db.execSQL("create view if not exists view_cover as select pic_name, cat_data_id, min(_id) from pictures group by cat_data_id;");
            // save data into Contants table
            try {
                db.beginTransaction();
                Log.d(TAG, "MagiclockProvider==>DatabaseHelper==>[onCreate] insert data! ");
                // The sql insert data into Category table
                String sqlCat = "INSERT INTO " + Category.TB_NAME + " (" + Category._ID + "," + Category.CAT + "," + Category.SELECTED + ") values(";
                // The sql insert data into Cat_Data table
                //String sqlData = "INSERT INTO " + CatData.TB_NAME + " (" + CatData.CAT_ID + "," + CatData.CAT_DATA + ") values(";
                ArrayList<ConstantItem> constantList = getConstantsValueList();
                long pos = 0;
                String insert = null;
                for (ConstantItem item :constantList) {
                    if (pos != item.id) {
                        pos = item.id;
                        insert = sqlCat + "\"" + item.id + "\",\"" + item.cat + "\",\"" + item.selected+ "\");";
                        db.execSQL(insert);
                        Log.d(TAG, item.id +" " +item.cat + " "+item.selected);
                    }
                }
                db.setTransactionSuccessful();
            } catch(Exception e) {
                Log.w(TAG, "MagiclockProvider==>DatabaseHelper==>[onCreate] exception: " + e, e);
            } finally {
                if (null != db) {
                    db.endTransaction();
                }
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion < 2) {
                updateDBto2(db);
                oldVersion = 2;
            }

            Log.d(TAG, "[onUpgrade] oldVersion=" + oldVersion + " | newVersion=" + newVersion);
            if (oldVersion != DATABASE_VERSION) {
                Log.d(TAG, "[onUpgrade] Destroying all old data.");
                db.execSQL("DROP TABLE IF EXISTS " + Picture.TB_NAME);
                db.execSQL("DROP TABLE IF EXISTS " + Category.TB_NAME);
                onCreate(db);
            }
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion,
                int newVersion) {
            // Do nothing.
            if (newVersion <= 1) {
                updateDBto2(db);
            }
        }

        /**
         * Drop the older view and create new.
         * @param db
         */
        private void updateDBto2(SQLiteDatabase db) {
            db.execSQL("DROP VIEW IF EXISTS " + ShownPictureView.VIEW_NAME);
            createShownPictureView(db);
        }
        /**
         * Save all constants info into constants table.
         * @return an array list
         */
        private ArrayList<ConstantItem> getConstantsValueList() {
            ArrayList<ConstantItem> list = new ArrayList<ConstantItem>();
            long id = 1;
            list.add(new ConstantItem(id++, Constants.CAT_MYPIC));

//            ConstantItem favorite = new ConstantItem(id++, Constants.CAT_FAVORITE);
//            favorite.selected = Category.FLAG_NOT_SELECTED;
//            list.add(favorite);
            list.add(new ConstantItem(id++, Constants.CAT_FAVORITE));
            list.add(new ConstantItem(id++, Constants.CAT_NATURE));
            list.add(new ConstantItem(id++,Constants.CAT_URBAN));
            list.add(new ConstantItem(id++, Constants.CAT_MOTORS));
            list.add(new ConstantItem(id++, Constants.CAT_ARCH));
            list.add(new ConstantItem(id++, Constants.CAT_BLACK_WHITE));
            list.add(new ConstantItem(id++, Constants.CAT_SPACE));
            list.add(new ConstantItem(id++, Constants.CAT_PET));
            list.add(new ConstantItem(id++, Constants.CAT_TRAVEL));
            list.add(new ConstantItem(id++, Constants.CAT_FOOD));
            list.add(new ConstantItem(id++, Constants.CAT_ART));
            return list;
        }


        private void createShownPictureView(SQLiteDatabase db) {
            db.execSQL("CREATE VIEW IF NOT EXISTS " + ShownPictureView.VIEW_NAME + " AS SELECT " + Picture.TB_NAME + "." + Picture._ID
                    + "," + Picture.PICTURE_NAME + "," + Picture.CAT_DATA_ID + "," + Picture.FAVORITE
                    + "," + Picture.MY_PICTURE +","+ Picture.AUTHOR_NAME + "," + Category.SELECTED
                    + " FROM " + Picture.TB_NAME + "," + Category.TB_NAME
                    + " WHERE " + Picture.CAT_DATA_ID + "=" + Category.TB_NAME + "." + Category._ID + " AND ("
                          + Category.SELECTED + "=" + Category.FLAG_SELECTED + " OR " + Picture.TB_NAME + "."
                          + Picture._ID + " IN(SELECT " + Picture.TB_NAME + "." + Picture._ID
                            + " FROM " + Picture.TB_NAME + "," + Category.TB_NAME
                            + " WHERE " + Category.CAT + "='" + Constants.CAT_FAVORITE + "' AND "
                                        + Category.SELECTED + "=" + Category.FLAG_SELECTED + " AND "
                                        + Picture.FAVORITE + "=" + Picture.FLAG_FAVORITE + "))"
            );
        }

        private void createFavoritesView(SQLiteDatabase db) {
            db.execSQL("CREATE VIEW IF NOT EXISTS " + FavoriteView.VIEW_NAME
                    + " AS SELECT " + Category.TB_NAME + "." + Category.CAT + ","
                    + Picture.TB_NAME + "." + Picture._ID + "," + Picture.AUTHOR_NAME + "," + Picture.FAVORITE + ","
                    + Picture.PICTURE_NAME + "," + Picture.ICON_PATH + "," + Picture.ICON
                    + " FROM " + Picture.TB_NAME + "," + Category.TB_NAME
                    + " WHERE " + Picture.FAVORITE + "=" + Picture.FLAG_FAVORITE
                    + " AND " + Picture.TB_NAME + "." + Picture.CAT_DATA_ID + "=" + Category.TB_NAME + "." + Category._ID);
        }
        private void createMypicView(SQLiteDatabase db) {
            db.execSQL("CREATE VIEW IF NOT EXISTS " + MypicView.VIEW_NAME
                    + " AS SELECT " + Picture.TB_NAME + "." + Picture._ID + "," + Picture.AUTHOR_NAME + ","
                                    + Picture.PICTURE_NAME + "," + Picture.ICON_PATH + "," + Picture.ICON
                    + " FROM " + Picture.TB_NAME + "," + Category.TB_NAME
                    + " WHERE " + Picture.CAT_DATA_ID + "=" + Category.TB_NAME + "." + Category._ID
                                + " AND " + Category.CAT + "='" + Constants.CAT_MYPIC + "'");
        }
    }


    public static final class Picture {
        public static final String TB_NAME = "Pictures";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TB_NAME);

        public static final String _ID = "_id";
        /**
         * The id from cat_data table
         */
        public static final String CAT_DATA_ID = "cat_data_id";
        /**
         * The name of some one create this picture
         */
        public static final String AUTHOR_NAME = "author";
        
        /**
         * The picture's name
         */
        public static final String PICTURE_NAME = "pic_name";
        /**
         * The time when create the data
         */
        public static final String DOWNLOAD_TIME = "times";
        /**
         * The path where the picture saved.
         */
        public static final String ICON_PATH = "icon_path";
        public static final String FAVORITE = "favorite";

        public static final String ICON = "icon";

        public static final int FLAG_FAVORITE = 1;
        public static final int FLAG_NOT_FAVORITE = 0;

        public static final String MY_PICTURE = "my_picture";
        public static final int FLAG_MYPICTURE = 1;
        public static final int FLAG_NOT_MYPICTURE = 0;
    }
    public static class ConstantItem {
        public long id = 0;
        public String cat = null;
        //public String data = null;
        public int selected = Category.FLAG_SELECTED;
        public ConstantItem(long id, String cat) {
            this.id = id;
            this.cat = cat;
            //this.data = data;
        }
    }
    public static final class Category { // extends com.tct.magiclock.Category{
        public static final String TB_NAME = "Category";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + TB_NAME);

        public static final String _ID = "_id";
        /**
         * save data like CAT_NATURE, CAT_CITY ...
         */
        public static final String CAT = "cat";//category

        public static final String SELECTED = "selected";

        public static final int FLAG_SELECTED = 1;
        public static final int FLAG_NOT_SELECTED = 0;
        public static final String COVER = "cover";
    }

    public static final class FavoriteView {
        public static final String VIEW_NAME = "view_favorites";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + VIEW_NAME);
        public static final String CAT = "cat";
        public static final String PICTURE_NAME = "pic_name";
    }

    public static final class ShownPictureView {
        public static final String VIEW_NAME = "view_shown_picture";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + VIEW_NAME);
    }

    public static final class MypicView {
        public static final String VIEW_NAME = "view_mypicture";
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + VIEW_NAME);
    }
}
