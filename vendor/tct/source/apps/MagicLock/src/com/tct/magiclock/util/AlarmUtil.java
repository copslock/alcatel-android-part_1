package com.tct.magiclock.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class AlarmUtil {
    public static final boolean ALLOW_LOG = true;
    public static final String TAG = "Alarm";

    public static final String MGLK_ACTION = "com.android.mglk.START_BACKGROUND_SERVICE";

    public static final long ONESECOND = 1000;
    public static final long ONEMINUTE = ONESECOND * 60;
    public static final long ONE_HOUR = 60 * ONEMINUTE;
    public static final long TWO_DAY = 24 * 2 * ONE_HOUR;
    public static final long ONE_WEEK = 24 * 7 * ONE_HOUR;

    public final static int NOTIFICATION_ID = 2;

    private Context mContext;
    private AlarmManager mAlarmManager;

    private PendingIntent mRetryCheckIntent;

    public AlarmUtil(Context context) {
        mContext = context;
        Log.d(TAG,"alarm create");
        mAlarmManager = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
    }


    public void startMagicLockAlarm(){
        Intent intent = new Intent();
        intent.setAction(MGLK_ACTION);
        mRetryCheckIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), TWO_DAY , mRetryCheckIntent);
        log("startMagicLockAlarm  Alarm Set to ", System.currentTimeMillis());
    }

    public void cancelMagicLockAlarm(){
        if(mRetryCheckIntent == null){
            Intent intent = new Intent();
            intent.setAction(MGLK_ACTION);
            mRetryCheckIntent = PendingIntent.getService(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        }
        mAlarmManager.cancel(mRetryCheckIntent);

        log("Cancel MagicLockAlarm  Alarm ");
    }

    public void log(String description , Calendar cal){
        long alarmdate = cal.getTimeInMillis();
        log(description,alarmdate);
    }

    public void log(String description , long milliseconds){
        if(ALLOW_LOG){
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(milliseconds);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String alarmtime = sdf.format(cal.getTime());
            Log.d(TAG, description + " : " + alarmtime);
        }
    }

    public void log(String description){
        if(ALLOW_LOG){
            Log.d(TAG, description);
        }
    }

}
