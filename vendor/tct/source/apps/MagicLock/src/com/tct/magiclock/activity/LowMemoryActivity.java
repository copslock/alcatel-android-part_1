package com.tct.magiclock.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import com.tct.magiclock.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by chunlin.tang on 1/28/16 for PR 1048253.
 */
public class LowMemoryActivity extends Activity {
    private static final String TAG = "LowMemoryActivity";
    private static AlertDialog mLowMemoryDialog = null;//modify by dongdong.wang to fix defect 1048253 at 2016-03-02

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        try {
            showLowMemoryDialog(this);
        } catch (Throwable e) {
            Log.w(TAG, "[onCreate] show low memory dialog.", e);
        }
    }

    private void destroyActivity() {
        try {
            LowMemoryActivity.this.finish();
        } catch (Throwable e) {
            Log.w(TAG, "[destroyActivity] finish activity exception.", e);
        }
    }

    private void showLowMemoryDialog(final Context context) {
        if (mLowMemoryDialog != null && mLowMemoryDialog.isShowing()) {
            Log.d(TAG, "[showLowMemoryDialog] mLowMemoryDialog is showing.");
            return;
        }
        //modify by dongdong.wang to fix defect 1048253 at 2016-03-02
        /*
        boolean hasSDCard = Environment.getExternalStorageState(new File("/storage/sdcard1")).equals(Environment.MEDIA_MOUNTED);
        Log.d(TAG, "[showLowMemoryDialog] hasSDCard=" + hasSDCard);
        AlertDialog.Builder  mBuilder = new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Light_Dialog);
        mBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.d(TAG, "[onCancel] cancel the dialog.");
                destroyActivity();
            }
        });
        if (hasSDCard) {
            mBuilder.setTitle(context.getText(com.android.internal.R.string.move_to_sd_dlg_title1))
                    .setMessage(context.getText(com.android.internal.R.string.move_to_sd_dlg_message1))
                    .setPositiveButton(com.android.internal.R.string.move_to_sd_btn_move_files, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent chooseIntent = new Intent();
                            //modify by dongdong.wang to fix defect 1048253 at 2016-02-23 begin
                            chooseIntent.setComponent(new ComponentName("com.tct.onetouchbooster", "com.tct.onetouchbooster.ui.StorageManagerActivity"));
                            //modify by dongdong.wang to fix defect 1048253 at 2016-02-23 end
                            chooseIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(chooseIntent);
                            Log.d(TAG, "Start SelectToMoveActivity End");
                            destroyActivity();
                        }
                    })
                    .setNegativeButton(com.android.internal.R.string.move_to_sd_btn_no_thanks, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Log.d(TAG, "move to SDCard? User selected no thanks.");
                            destroyActivity();
                        }
                    });
        } else {
            ArrayList<CharSequence> mElements = new ArrayList<>();
            mElements.add(context.getText(com.android.internal.R.string.move_to_sd_file_manager));
            mElements.add(context.getText(com.android.internal.R.string.move_to_sd_app_manager));
            CharSequence[] elements = mElements.toArray(new CharSequence[mElements.size()]);
            mBuilder.setTitle(context.getText(com.android.internal.R.string.move_to_sd_dlg_title2))
                    .setItems(elements,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent chooseItent = new Intent();
                                    Log.d(TAG, "User choose which=" + which);
                                    if (which == 0) {
                                        chooseItent.setComponent(new ComponentName("com.jrdcom.filemanager","com.jrdcom.filemanager.activity.FileBrowserActivity"));
                                    } else {
                                        chooseItent.setAction(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS); // ACTION_MANAGE_ALL_APPLICATIONS_SETTINGS);
                                    }
                                    chooseItent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(chooseItent);
                                    destroyActivity();
                                }
                            })
                    .setPositiveButton(com.android.internal.R.string.move_to_sd_btn_no_thanks, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Log.d(TAG, "Hasn't sdcard. User selected no thanks.");
                            destroyActivity();
                        }
                    });
        }*/
        /*AlertDialog.Builder mBuilder = new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Light_Dialog);
        mBuilder.setTitle(context.getText(com.android.internal.R.string.move_to_sd_dlg_title1))
                .setMessage(context.getText(com.android.internal.R.string.move_to_sd_dlg_title2))
                .setPositiveButton(context.getText(R.string.magiclock_low_memory_btn_move_files).toString().toUpperCase(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent chooseIntent = new Intent();
                        //modify by dongdong.wang to fix defect 1048253 at 2016-02-23 begin
                        chooseIntent.setComponent(new ComponentName("com.tct.onetouchbooster", "com.tct.onetouchbooster.ui.StorageManagerActivity"));
                        //modify by dongdong.wang to fix defect 1048253 at 2016-02-23 end
                        chooseIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(chooseIntent);
                        Log.d(TAG, "Start SelectToMoveActivity End");
                        destroyActivity();
                    }
                })
                .setNegativeButton(com.android.internal.R.string.move_to_sd_btn_no_thanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Log.d(TAG, "move to SDCard? User selected no thanks.");
                        destroyActivity();
                    }
                });
        //modify by dongdong.wang to fix defect 1048253 at 2016-03-02
        mLowMemoryDialog = mBuilder.create();
        mLowMemoryDialog.setCanceledOnTouchOutside(true);
        mLowMemoryDialog.show();*/
        Log.d(TAG, "[showLowMemoryDialog] after show mLowMemoryDialog");
    }
}
