package com.tct.magiclock.utils;

import java.util.HashMap;

/**
 * The class {@code Cache} is a util class to store value
 * @author guoqiang.qiu@tcl.com
 * @since  n8996, Solution-2699694
 */

public class Cache {
    private static Cache cache;
    private HashMap<String, Object> pool;

    private Cache() {
        pool = new HashMap<>();
    }

    public static Cache getCache() {
        if (cache == null) {
            cache = new Cache();
        }
        return cache;
    }

    public void put(String key, Object value) {
        pool.put(key, value);
    }

    public Object get(String key) {
        return pool.get(key);
    }
}