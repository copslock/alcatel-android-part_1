package com.tct.magiclock.activity;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;

import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.model.ItemInfo;
import com.tct.magiclock.util.PhotoUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by chunlin.tang on 7/3/15.
 * @deprecated 2015-10-09
 */
public class MagiclockFavoritesAdapter extends CursorAdapter{

    private static final String TAG = "FavoritesAdapter";
    private Context mContext = null;

    public MagiclockFavoritesAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        try {
            View view = ((LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.magiclock_favorites_item, parent, false);
            ImageView img = (ImageView)view.findViewById(R.id.mfi_picture);
            img.setTag(new ItemInfo());
            view.setTag(img);
            Log.d(TAG, "****** [newView] create view ******");
            return view;
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        try {
            ImageView img = (ImageView)view.getTag();
            String id = cursor.getString(cursor.getColumnIndexOrThrow(Picture._ID));
            String picName = cursor.getString(cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME));
            String picPath = cursor.getString(cursor.getColumnIndexOrThrow(Picture.ICON_PATH));
            ItemInfo data = (ItemInfo)img.getTag();
            Log.d(TAG, "[bindView] id=" + id + " | data.id=" + data.id + " | picName=" + picName + " | picPath=" + picPath);
            view.setId(Integer.valueOf(id));
            if (id.equals(data.id)) {
                img.setBackground(data.picture);
            } else {
                data.id = id;
                data.picName = picName;
                data.picPath = picPath;
                img.setTag(data);

                if (cursor.isNull(cursor.getColumnIndexOrThrow(Picture.ICON))) {
                    // Loading picture from storage
                    Log.d(TAG, "[bindView] load picture from storage.");
                    new LoadPictureTask(mContext, img).execute(data);
                } else {
                    Log.d(TAG, "[bindView] load picture from db.");
                    byte[] photoData = cursor.getBlob(cursor.getColumnIndexOrThrow(Picture.ICON));
                    Bitmap bitmap = BitmapFactory.decodeByteArray(photoData, 0, photoData.length, null);
                    data.picture = new BitmapDrawable(context.getResources(), bitmap);
                    img.setBackground(data.picture);
                }
                
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }
//        this.notifyDataSetChanged();
    }

    /**
     *
     */
    private class LoadPictureTask extends AsyncTask<ItemInfo, Object, Drawable> {
        private Context mContext;
        private WeakReference<ImageView> mimageViewReference;

        public LoadPictureTask(Context context, ImageView imageview) {
            this.mContext = context;
            this.mimageViewReference = new WeakReference<ImageView>(imageview);
        }

        @Override
        protected Drawable doInBackground(ItemInfo... datas) {
            try {
                return loadPictureFromStorage(mContext, datas[0]);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e, e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Drawable result) {
            try {
                ImageView imageview = mimageViewReference.get();
                imageview.setBackground(result);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e, e);
            }
        }
        private Drawable loadPictureFromStorage(Context context, ItemInfo data) {
            FileInputStream fis = null;
            try {
                Log.d(TAG, "loadPictureFromStorage");
                int requireWidth = mContext.getResources().getDimensionPixelSize(R.dimen.magiclock_fav_item_width);
                int requireHeight = mContext.getResources().getDimensionPixelSize(R.dimen.magiclock_fav_item_height);
                Bitmap bitmap = PhotoUtils.centerCropImage(requireWidth, requireHeight, data.picPath + data.picName);
                data.picture = new BitmapDrawable(context.getResources(), bitmap);
                return data.picture;
            } catch (Exception e) {
                Log.e(TAG, " Error getLoacalBitmap, e: " + e, e);
            } finally {
                if (null != fis) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException caught while closing stream: " + e, e);
                    }
                }
            }
            return null;
        }
    }

}
