package com.tct.magiclock.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.flickr.Flickr;
import com.android.flickr.REST;
import com.android.flickr.people.PeopleInterface;
import com.android.flickr.people.User;
import com.android.flickr.photos.Photo;
import com.android.flickr.photos.PhotoList;
import com.android.flickr.photos.PhotosInterface;
import com.android.flickr.photos.SearchParameters;
import com.android.flickr.photos.Size;
import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.activity.MagicLockGetShownPictureTask;
import com.tct.magiclock.activity.MagiclockRecoverTask;
import com.tct.magiclock.db.MagiclockProvider;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.db.MagiclockProvider.Category;
import com.tct.magiclock.db.MagiclockProvider.ShownPictureView;
import com.tct.magiclock.util.MGLKUtils;
import com.tct.magiclock.util.PhotoUtils;
import com.tct.magiclock.utils.Utils;
import com.tct.magiclock.utils.SettingPref;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class MagicLockBackService extends Service {
    private static final String TAG = "BS";

    public static final String SERVICE_STATUS = "is_auto_clean";
    public static final String RETRY_TIMES = "retry_times";
    public static final String DOWN_RESUME = "down_resume";

    /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
    /**
     * update picture type key.
     */
    public static final String KEY_UPDATE_TYPE = "UPDATE_TYPE";
    /* MODIFIED-END by dongdong.wang,BUG-1857937*/

    /** MSG */
    private final int MSG_SERVICE_STOP = 1;
    private final int MSG_AUTO_CLEAN = 2;
    private final int MSG_GET_SHOW_PIC = 3;
    private final int MSG_CHECK_STATUS = 4;
    /**Check if Category Changes before stop*/
    private final int MSG_CAT_CHANGE_CHECK = 5;
    private static final int MSG_SHOW_WARN_NOTIFY = 6;//ADD by dongdong.wang@tcl.com 2015-12.16 for defect 1048253
    private final int INVALID_RETRYTIMES = -1;
    private final int RETRY_MAX = 5;
    private final long DOWNLOAD_GAP = 1000 * 60 * 5;// 5 minutes

    private volatile boolean isCatChanged = false;

    private Context mContext;
    private MyHandler mHandler = new MyHandler();

    private DownloadThread mDownLoadThread = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.CATEGORY_CHANGE_NOTIFICATION);
        registerReceiver(mCategoryChangeReceiver, intentFilter);
        Log.d(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int id) {

        new Thread(new DownloadStatusCheck(intent)).start();

        /** restart after killed by system for low memory */
        return START_STICKY;

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "[onDestroy]");
        isCatChanged = false;
        unregisterReceiver(mCategoryChangeReceiver);
        if(null != mDownLoadThread){
            mDownLoadThread.cancelDownload();
            mDownLoadThread = null;
        }
        super.onDestroy();
    }

    private class MyHandler extends Handler {

        @SuppressWarnings("unchecked")
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
            case MSG_SERVICE_STOP:
                Log.d(TAG, "Handler MSG_SERVICE_STOP");
                stopSelf();
                break;
            case MSG_AUTO_CLEAN:
                Log.d(TAG, "Handler MSG_AUTO_CLEAN");
                recoveryDeletePictures();
                break;
            case MSG_GET_SHOW_PIC:
                Log.d(TAG, "Handler MSG_GET_SHOW_PIC");
                new MagicLockGetShownPictureTask(mHandler,
                        MagicLockGetShownPictureTask.MODE_NOT_DOWNLOAD).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mContext); // MODIFIED by Zhenhua.Fan, 2016-11-09,BUG-3282079
                break;
            case MSG_CHECK_STATUS:
                if(null == msg || null == msg.obj){
                    Log.e(TAG, "Handle MSG_CHECK_STATUS, null msg");
                    stopSelf();
                }else{
                    CategoryDownload info = (CategoryDownload)msg.obj;
                    if(null == mDownLoadThread){
                        mDownLoadThread = new DownloadThread(info);
                        mDownLoadThread.start();
                    }else{
                        Log.d(TAG, "!!Download thread is ongoing!!");
                    }
                } // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
                break;
            case MagicLockGetShownPictureTask.MSG_AVALIABLE_FILES:
                Log.d(TAG, "Handler MSG_AVALIABLE_FILES");
                Set<String> set = (Set<String>)msg.obj;
                /** Start background auto clean task */
                new AutoCleanTask().execute(set);
                break;
            case MSG_CAT_CHANGE_CHECK:
                Log.d(TAG,"MSG_CAT_CHANGE_CHECK isCatChanged:"+isCatChanged);
                if(isCatChanged){
                    isCatChanged = false;
                    CategoryDownload catInfo = loadCategoryFromDb();
                    if(null != mDownLoadThread){
                        mDownLoadThread.cancelDownload();
                        mDownLoadThread = null;
                    }
                    mDownLoadThread = new DownloadThread(catInfo);
                    mDownLoadThread.start();
                }else{
                	/* MODIFIED-BEGIN by zhuang.yao, 2016-11-18,BUG-3469244*/
                	if(null != mDownLoadThread && mDownLoadThread.isAlive()){
                		Log.d(TAG,"MSG_CAT_CHANGE_CHECK  downthreadis alive = true");
                        break;
                	}
                	/* MODIFIED-END by zhuang.yao,BUG-3469244*/
                    Log.d(TAG, "No CAT change, MSG_SERVICE_STOP");
                    stopSelf();
                }
                break;
                //modify by dongdong.wang@tcl.com 2015-12.16 for defect 1048253   begin
                case MSG_SHOW_WARN_NOTIFY:
                    Log.d(TAG, "show low memory waring notification.");
                    MGLKUtils.notifyLowMemory(mContext);
                    new MagicLockGetShownPictureTask(mHandler,
                            MagicLockGetShownPictureTask.MODE_NOT_DOWNLOAD).execute(mContext);
                    break;
                //modify by dongdong.wang@tcl.com 2015-12.16 for defect 1048253   end
            default:
                break;
            }
        }
    }


    /**
    * DownloadStatusCheck // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
    * According to intent, fetch current action
    * and check current download status
    */
    private class DownloadStatusCheck implements Runnable{
        private static final int STATUS_DOWN = 1;
        private static final int STATUS_RESUME = 2;
        private static final int STATUS_RESTART = 3;
        private static final int STATUS_ERR = 0;
        private int   mAction;

        DownloadStatusCheck(Intent intent){
            if(intent != null){
                String action = intent.getAction();
                Log.d(TAG, "[DownloadStatusCheck] action:" + action);
                if ("com.android.mglk.START_BACKGROUND_SERVICE".equals(action)){
                    mAction = STATUS_DOWN;
                }else if(Constants.ACTION_NEED_RESUME.equals(action)){
                    mAction = STATUS_RESUME;
                }else{
                    mAction = STATUS_ERR;
                }
            }else{
                mAction = STATUS_RESTART;
            }
        }
        @Override
        public void run() {
            if (Constants.MAGICLOCK_SCREEN_HEIGHT <= 0) {
                Constants.getScreenSize(mContext, TAG);
            }
            ServiceStatus info = null;
            Log.d(TAG, "[DownloadStatusCheck] mAction:" + mAction);
            switch (mAction) {
            case STATUS_DOWN: {
                info = new ServiceStatus(false, 0, false);
                setServiceStatus(info);
                CategoryDownload catInfo = loadCategoryFromDb();
                Message msg = mHandler.obtainMessage(MSG_CHECK_STATUS);
                msg.obj = catInfo;
                mHandler.sendMessage(msg);
            }
                break;
            case STATUS_RESUME:
                info = getServiceStatus();
                if (info.isDownloadResume) {
                    info.isAutoClean = false;
                    info.isDownloadResume = false;
                    Log.d(TAG, "[DownloadStatusCheck]Retry times ="
                            + info.retryTimes);
                    if (info.retryTimes >= RETRY_MAX) {
                        mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
                        return;
                    } else {
                        info.retryTimes++;
                        setServiceStatus(info);
                    }
                    CategoryDownload CatInfo = loadRecoveryCategoryFromDb();
                    Message msg = mHandler.obtainMessage(MSG_CHECK_STATUS);
                    msg.obj = CatInfo;
                    mHandler.sendMessage(msg);
                } else {
                    mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
                    return;
                }
                break;
            case STATUS_RESTART:
                info = getServiceStatus();
                if (info.isAutoClean) {
                    info.retryTimes = 0;
                    setServiceStatus(info);
                    mHandler.sendEmptyMessage(MSG_AUTO_CLEAN);
                    return;
                } else {
                    if (info.retryTimes >= RETRY_MAX) {
                        Log.d(TAG, "[DownloadStatusCheck]Retry times ="
                                + info.retryTimes);
                        mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
                        return;
                    } else {
                        info.retryTimes++;
                        setServiceStatus(info);
                        /** search for unfinished tags */
                        CategoryDownload catInfo = loadRecoveryCategoryFromDb();
                        Message msg = mHandler.obtainMessage(MSG_CHECK_STATUS);
                        msg.obj = catInfo;
                        mHandler.sendMessage(msg);
                    }
                }
                break;
            case STATUS_ERR:
                mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
                break;
            default:
                break;
            }
        } // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
    }

    private BroadcastReceiver mCategoryChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (Constants.CATEGORY_CHANGE_NOTIFICATION.equals(action)) {
                Log.d(TAG, "[mCategoryChangeReceiver]");
                isCatChanged = true;
            }
        }
    };
    //add by dongdong.wang to fix task 1857939 at 2016-04-05 begin
    /**
     * Download item
     */
    private class DownloadItem{
        public String mUrl = null;
        public int    mScaleSize = 1;
        public boolean mNeedScale = false;

        public DownloadItem(){
        }
        public DownloadItem(String url, boolean needScale, int scaleSize) {
            mUrl = url;
            mScaleSize = scaleSize;
            mNeedScale = needScale;
        }
    }
    //moidfy by dongdong.wang to fix task 1857939 at 2016-04-05 end.
    /**
     * DownloadThread
     * @param CategoryDownload tags and image number
     *
     */
    private class DownloadThread extends Thread{
        private ArrayList<String> mTagList;
        private int mPicSum;
        private volatile boolean mCancelNow = false;

        DownloadThread(CategoryDownload info){
            mTagList = new ArrayList<String>(info.tagList); // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
            mPicSum = info.picSum;
        }

        public void cancelDownload() {
            mCancelNow = true;
        }
    //modify by dongdong.wang to fix task 1857939 at 2016-04-05 begin.
        /**
         * fetch photo size according to id, and choose the most suitable source
         * @param photoId
         * @return
         */
        private DownloadItem fetchPhotoSource(PhotosInterface photointerface,
                                        String photoId) {
            //String ret = null;
            DownloadItem ret = new DownloadItem();
            try {
                Collection<Size> Photosize = photointerface.getSizes(photoId);
                Iterator<Size> it = Photosize.iterator();
                while (it.hasNext()) {
                    Size tempIt = it.next();
                    int sourceHeight = tempIt.getHeight();
                    int sourceWidth = tempIt.getWidth();
                    Log.d(
                            TAG,
                            "photo:" + photoId + "height =" + sourceHeight
                                    + ";width =" + sourceWidth + ";label = "
                                    + tempIt.getLabel()/* + "; source:"
                                     + tempIt.getSource()*/);

                    if ((sourceHeight >= Constants.MAGICLOCK_SCREEN_HEIGHT) &&
                            (sourceWidth >= Constants.MAGICLOCK_SCREEN_WIDTH)) {
                        //ret = tempIt.getSource();
                        float widthRatio = (float)(Math.round(sourceWidth*10 / Constants.MAGICLOCK_SCREEN_WIDTH))/10;
                        float heightRatio = (float)(Math.round(sourceHeight*10 / Constants.MAGICLOCK_SCREEN_HEIGHT))/10;
                        float inSampleSize = 1.0f;
                        inSampleSize = Math.min(widthRatio, heightRatio);
                        ret.mUrl = tempIt.getSource();
                        if(inSampleSize >= 2.0f){
                            ret.mNeedScale = true;
                            ret.mScaleSize = (int)inSampleSize;
                            Log.d(TAG,"[fetchPhotoSource] mScaleSize="+ret.mScaleSize);
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "[fetchPhotoSource] Exception:" + e, e);
            }
            Log.d(TAG, "[fetchPhotoSource] Ret:" + ret.mUrl);
            return ret;
        }
            //add by dongdong.wang to fix task 1857939 at 2016-04-05 end.
        @Override
        public void run() {
            if (!checkDownloadConditions()){
                return;
            }
            Log.d(TAG, "[DownloadThread] tags:" + mTagList.toString()
                    + "; picSum =" + mPicSum);
            boolean result = false;
            int curType = 0;//add by dongdong.wang to fix defect 1857937 at 2016-04-19
            try {
                /** load tags */
                REST rest = new REST(Flickr.DEFAULT_API_HOST);
                Flickr f = new Flickr(Constants.API_KEY, Constants.API_SEC, rest);
                PhotosInterface photointerface = f.getPhotosInterface();
                PeopleInterface userinfo = f.getPeopleInterface();
                SearchParameters searchpara = new SearchParameters();
                searchpara.setGroupId(Constants.GROUP_ID);//should use alcatel
//                searchpara.setUserId(Constants.USER_ID);//Only for UE test
                Log.d(TAG, "[run] ***** searchpara.getGroupId()=" + searchpara.getGroupId());
                Set<String> extras = new HashSet<String>();
                extras.add("original_format");
                searchpara.setExtras(extras);
                //add by dongdong.wang to fix defect 1857937 at 2016-04-19 begin
                curType = getUpdateType(mContext);
                Log.d(TAG, "curType = " + curType);
                searchpara.setSort(curType);
                //add by dongdong.wang to fix defect 1857937 at 2016-04-19 end.
                /** search photos*/
                Iterator<String> tagPool = mTagList.iterator();
                int searchNum = mPicSum / (mTagList.size());//Constants.NUM_PIC_TOTAL

                while (!mCancelNow && tagPool.hasNext()) {
                    String searchTag = (String) tagPool.next();
                    String[] st = new String[] { "" };
                    st[0] = searchTag;
                    searchpara.setTags(st);

                    Log.d(TAG, "Current search tag:" + st[0]
                            + "; searchNum =" + searchNum);

                    /**
                     * Here we need to search double of NUM_PIC_TOTAL, in case, all
                     * the pictures have been down loaded when only one tag selected
                     */
                    PhotoList photolist = photointerface.search(searchpara,
                            Constants.NUM_PIC_TOTAL * 2, 1);

                    if (null != photolist) {
                        int searchRetSum = photolist.size();
                        boolean isCoverDownload = false;
                        Log.d(TAG, "searchRetSum sum=" + searchRetSum);

                        if (searchRetSum > 0) {
                            int downIndex = 0;// we only need searchNum pictures

                            for (int index = 0; index < searchRetSum; index++) {
                                if (!checkDownloadConditions()){
                                    return;
                                }
                                Photo phototemp = photolist.get(index);
                                User user = phototemp.getOwner();

                                String photoSec = phototemp.getSecret();
                                String filename = phototemp.getId() + "_" + photoSec;
                                String photoFormat = phototemp.getOriginalFormat();

                                // check if photo exist
                                boolean isNeedDown = shouldDownloadPhoto(filename);
                                Log.d(TAG, "filename:" + filename
                                        + "; isNeedDown=" + isNeedDown // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
                                         +"; photoFormat:" +photoFormat);

                                if (isNeedDown) {
                                    /** fetch photo and save to storage*/
                                    //phototemp.setOriginalFormat(photoFormat);
                                    String photoid = phototemp.getId();
                                    //moidfy by dongdong.wang to fix task 1857939 at 2016-04-05 begin.
                                    DownloadItem photosource = fetchPhotoSource(photointerface, photoid);
                                    if(null == photosource.mUrl){
                                        continue;
                                    }
                                    URL photourl = new URL(photosource.mUrl);
                                    InputStream ins = photourl.openStream();//phototemp.getLargeHAsStream();//getOriginalAsStream();
                                    //saveIsAsImg(ins);

                                    if (null != ins) {
                                        Bitmap bitmap = null;
                                        // PR 1478640 Add try catch by chunlin.tang@tcl.com 2016-01-26 Begin
                                        // Fix: Magic unlock crashed when occurred OutOfMemoryError.
                                        try {
                                            if(photosource.mNeedScale){
                                                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                                                bitmapOptions.inSampleSize = photosource.mScaleSize;
                                                Log.d(TAG,"Download, decode with scale!");
                                                bitmap = BitmapFactory.decodeStream(ins, null, bitmapOptions);
                                            }else {
                                                bitmap = BitmapFactory.decodeStream(ins);
                                            }
                                        //moidfy by dongdong.wang to fix task 1857939 at 2016-04-05 end.
                                        } catch (OutOfMemoryError e) {
                                            Log.e(TAG, "[run] OutOfMemoryError when decodeStream.", e);
                                            continue;
                                        } finally {
                                            closeInputStream(ins);
                                        }
                                        if (null == bitmap) {
                                            Log.e(TAG, "bitmap is null, continue current loop.");
                                            closeInputStream(ins);
                                            continue;
                                        }
                                        // PR 1478640 Add try catch by chunlin.tang@tcl.com 2016-01-26 End
                                        // fetch photographer info
                                        User userInfoRet = userinfo.getInfo(user
                                                .getId());
                                        String authorName = userInfoRet
                                                .getUsername();

                                        //boolean ret = saveDownloadFile(ins,filename, searchTag, authorName);
                                        boolean ret = saveDownloadFile(bitmap, filename, searchTag, authorName);
                                        Log.d(TAG, "saveDownloadFiles. author:"
                                                + authorName + "; ret:" + ret);

                                        if (ret) {
                                            downIndex++;
                                            if(1 == downIndex){
                                                /**Only the first image used for cover*/
                                                saveCategoryCover(filename,searchTag);
                                                isCoverDownload = true;
                                            }
                                            result = true;/**Download number >=1*/
                                            Log.d(TAG, "downIndex:" + downIndex);
                                            mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC); // MODIFIED by Zhenhua.Fan, 2016-11-09,BUG-3282079
                                            if (downIndex >= searchNum) {
                                                break;// goto next tag
                                            }
                                        }
                                    } else {
                                        Log.e(TAG,
                                                "getLargeAsStream get null bitmap!");
                                    }
                                    closeInputStream(ins);
                                }
                            }
                        }
                        if(!isCoverDownload){
                            setFavoriteOrNormalPicAsCover(searchTag);
                        }

                    } else {
                        Log.d(TAG, "No photo found!!");
                    }
                    if (!checkDownloadConditions()){
                        return;
                    }
                }

            } catch (Exception e) {
                Log.e(TAG, "DownloadThread exception:" + e,e);
                if((e instanceof UnknownHostException) || (e instanceof ConnectException)
                        // PR 1453339 Add by chunlin.tang@tcl.com 2016-01-18 Begin
                        // Fix: Didn't try to download because of SSLHandshakeException.
                        || (e instanceof javax.net.ssl.SSLHandshakeException)){
                        // PR 1453339 Add by chunlin.tang@tcl.com 2016-01-18 End
                    /**
                     * Network error, Retry
                     */
                    //Add by dongdong.wang@tcl.com 2016-03.04 for defect 1058525
                    new MagiclockRecoverTask().execute(mContext);
                    ServiceStatus info = getServiceStatus();
                    info.isDownloadResume = true;
                    setServiceStatus(info);
                    if(result){
                        mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                    }else{
                        mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
                    }
                    return;
                }
            }
            if(result){
                saveUpdateType(mContext, curType);//add by dongdong.wang to fix defect 1857937 at 2016-04-19
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
            }else{
                mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
            }
            mCancelNow = false;
            return;

        }

        // PR 1478640 Add by chunlin.tang@tcl.com 2016-01-26 Begin
        private void closeInputStream(InputStream ins) {
            if (null != ins) {
                try {
                    ins.close();
                } catch (IOException e) {
                    Log.w(TAG, "[closeInputStream] close InputStream Exception.", e);
                }
            }
        }
        // PR 1478640 Add by chunlin.tang@tcl.com 2016-01-26 End

        private void setFavoriteOrNormalPicAsCover(String catTag) {
            Cursor cursor = null;
            boolean retFavCover = false;
            Log.d(TAG, "[setFavoriteAsCover]category:" + catTag);
            ContentResolver resolver = mContext.getContentResolver();

            try {
                Uri uri = MagiclockProvider.FavoriteView.CONTENT_URI;
                String selection = MagiclockProvider.FavoriteView.CAT + "='" + catTag + "'";
                String[] projection = new String[]{MagiclockProvider.FavoriteView.PICTURE_NAME};
                cursor = resolver.query(uri, projection, selection, null, null);
                if (null == cursor || cursor.getCount() <= 0) {
                    Log.d(TAG, "[setFavoriteAsCover] no fav!");
                    retFavCover = false;
                }
                String fileName = null;
                final int picIndex = cursor.getColumnIndexOrThrow(MagiclockProvider.FavoriteView.PICTURE_NAME);
                while (cursor.moveToNext()) {
                    fileName = cursor.getString(picIndex);
                    if (null != fileName) {
                        /**get cover*/
                        saveCategoryCover(fileName, catTag);
                        retFavCover = true;
                        break;
                    }
                }

            } catch (Exception e) {
                Log.e(TAG, "[setFavoriteAsCover]Exception2:" + e, e);

            } finally {
                if (null != cursor) {
                    cursor.close();
                    cursor = null;
                }
            }

            if(!retFavCover){
                /**Set favorite as cover fail*/
                boolean isOk = setNormalPicAsCover(catTag);
                Log.d(TAG, "[setFavoriteOrNormalPicAsCover]:"+isOk);
            }

        }

        private boolean setNormalPicAsCover(String catTag){
            boolean ret = false;
            Cursor cursor = null;
            int  catIndex = -1;
            Log.d(TAG, "[setNormalPicAsCover]category:" + catTag);
            ContentResolver resolver = mContext.getContentResolver();

            try {
                Uri uri = Category.CONTENT_URI;
                String selection = Category.CAT + "='" +catTag +"'";
                String[] projection = new String[]{Category._ID};
                cursor = resolver.query(uri, projection, selection, null, null);
                if (null == cursor || cursor.getCount() <= 0) {
                    Log.d(TAG, "[setNormalPicAsCover] error category!");
                    return false;
                }

                final int picIndex = cursor.getColumnIndexOrThrow(Category._ID);
                while (cursor.moveToNext()) {
                    catIndex = cursor.getInt(picIndex);
                    if (-1 != catIndex) {
                        break;
                    }
                }
            } catch (Exception e) {
                Log.e(TAG, "[setNormalPicAsCover]Exception:" + e, e);
                return false;

            } finally {
                if (null != cursor) {
                    cursor.close();
                    cursor = null;
                }
            }

            try {
                Uri uri = Picture.CONTENT_URI;
                Log.d(TAG,"[setNormalPicAsCover]:"+catIndex);
                String selection = Picture.CAT_DATA_ID + "='" +catIndex +"'";
                String[] projection = new String[]{Picture.PICTURE_NAME};
                cursor = resolver.query(uri, projection, selection, null, Picture.DOWNLOAD_TIME);
                if (null == cursor || cursor.getCount() <= 0) {
                    Log.d(TAG, "[setNormalPicAsCover] No Picture!");
                    return false;
                }
                String  picName = null;
                cursor.moveToLast();
                final int picIndex = cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME);
                picName = cursor.getString(picIndex);
                if (null != picName) {
                    Log.d(TAG,"[setNormalPicAsCover] PIC:" +picName);
                    saveCategoryCover(picName, catTag);
                    ret = true;
                }
            } catch (Exception e) {
                Log.e(TAG, "[setNormalPicAsCover]Exception2:" + e, e);
                return false;

            } finally {
                if (null != cursor) {
                    cursor.close();
                    cursor = null;
                }
            }

            return ret;
        }


        private void saveCategoryCover(String fileName, String cat){
            Intent i = new Intent(Utils.ACTION_UPDATE_COVER);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(i);
        }


        /* MODIFIED-BEGIN by dongdong.wang, 2016-05-16,BUG-2116248*/
        /**
         * check download environment in download progress.
         * @return
         */
        private boolean checkDownloadConditions(){
            // MODIFIED-END by dongdong.wang, BUG-2116248
            final String tag = "[checkDownloadConditions]";
            if (mCancelNow){
                Log.e(TAG,tag + "mCancelNow == true");
                return false;
            } else if (isCatChanged){
                Log.e(TAG,tag + "isCatChanged == true");
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                return false;
            } else if (mPicSum <= 0){
                Log.e(TAG,tag + "!!picSum = 0!!");
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                return false;
            } else if (null == mTagList || (mTagList.size() <= 0)) {
                Log.e(TAG,tag + "No cat select");
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                return false;
            } else if (!Constants.checkFileDir(mContext, TAG)){
                Log.e(TAG,tag + "not find download path.");
                ServiceStatus info = getServiceStatus();
                info.isDownloadResume = true;
                setServiceStatus(info);
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                return false;
            } else if (!Utils.isNetworkOkForDownload(mContext)){
                Log.e(TAG,tag + "network error.");
                new MagiclockRecoverTask().execute(mContext);//Add by dongdong.wang@tcl.com 2016-03.04 for defect 1058525
                ServiceStatus info = getServiceStatus();
                info.isDownloadResume = true;
                setServiceStatus(info);
                mHandler.sendEmptyMessage(MSG_GET_SHOW_PIC);
                return false;
            } else if (!MGLKUtils.isRomAvailableSize(mContext)){
                Log.e(TAG,tag + "not enough storage.");
                mHandler.sendEmptyMessage(MSG_SHOW_WARN_NOTIFY);
                return false;
            }

            return true;
        }
        /* MODIFIED-END by dongdong.wang,BUG-2116248*/
    }


    private boolean shouldDownloadPhoto(String picName) {
        Cursor cursor = null;
        try {
            cursor = mContext.getContentResolver().query(
                    MagiclockProvider.Picture.CONTENT_URI,
                    new String[] { MagiclockProvider.Picture._ID },
                    MagiclockProvider.Picture.PICTURE_NAME + "=?",
                    new String[] { picName }, null);
            if (cursor.getCount() > 0) {
                return false;
            }
        } catch (Exception e) {
            Log.e(TAG, "[shouldDownloadPhoto] Exception: " + e);
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return true;

    }

    /**
     * load tags from Database
     */
    private CategoryDownload loadCategoryFromDb() {
        Cursor cursor = null;
        CategoryDownload ret = new CategoryDownload();
        ret.picSum = Constants.NUM_PIC_TOTAL;
        try {
            ContentResolver resolver = mContext.getContentResolver();
            Uri uri = MagiclockProvider.Category.CONTENT_URI;
            String selection = MagiclockProvider.Category.SELECTED + "="
                    + MagiclockProvider.Category.FLAG_SELECTED;
            String[] projection = new String[] { MagiclockProvider.Category.CAT };
            cursor = resolver.query(uri, projection, selection, null, null);
            if (null == cursor || cursor.getCount() <= 0) {
                return null;
            }
            final int tagIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.CAT);
            ret.tagList = new ArrayList<String>();
            while (cursor.moveToNext()) {
                String title = cursor.getString(tagIndex);
                /** MyPic is not a real category*/
                if (Constants.CAT_MYPIC.equals(title)||Constants.CAT_FAVORITE.equals(title)) {
                    Log.d(TAG, "bypass tag: " + title);
                    continue;
                }
                ret.tagList.add(title);
            }
            Log.d(TAG, "loadCategoryFromDb tag:" + ret.tagList);
            return ret;
        } catch (Exception e) {
            Log.e(TAG, "loadCategoryFromDb exception:" + e);
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return null;

    }

    private class TagInfo {
        public int mIndex;
        public String mName;

        public TagInfo() {
            mIndex = 0;
            mName = null;
        }
    }

    /**
     * load tags from Database
     */
    @SuppressLint("SimpleDateFormat")
    private CategoryDownload loadRecoveryCategoryFromDb() {
        Cursor cursor = null;
        Cursor cursorTermp = null;
        CategoryDownload ret = new CategoryDownload();

        ArrayList<TagInfo> tags = new ArrayList<TagInfo>();
        ContentResolver resolver = mContext.getContentResolver();

        try {
            Uri uri = MagiclockProvider.Category.CONTENT_URI;
            String selection = MagiclockProvider.Category.SELECTED + "="
                    + MagiclockProvider.Category.FLAG_SELECTED;
            String[] projection = new String[] {
                    MagiclockProvider.Category._ID,
                    MagiclockProvider.Category.CAT };
            cursor = resolver.query(uri, projection, selection, null, null);
            if (null == cursor || cursor.getCount() <= 0) {
                Log.d(TAG,
                        "[loadRecoveryCategoryFromDb] No selected category!");
                return null;
            }
            final int tagIdIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category._ID);
            final int tagNameIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.CAT);

            while (cursor.moveToNext()) {
                TagInfo tagTemp = new TagInfo();
                tagTemp.mName = cursor.getString(tagNameIndex);
                tagTemp.mIndex = cursor.getInt(tagIdIndex);

                /** MyPic is not a real category*/
                if (Constants.CAT_MYPIC.equals(tagTemp.mName)||Constants.CAT_FAVORITE.equals(tagTemp.mName)) {
                    Log.d(TAG, "bypass tag: " + tagTemp.mName);
                    continue;
                }
                tags.add(tagTemp); // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
            }
        } catch (Exception e) {
            Log.e(TAG, "loadRecoveryCategoryFromDb exception:" + e);
            e.printStackTrace();
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }

        if (null != tags && tags.size() > 0) {
            ret.tagList = new ArrayList<String>();
            Iterator<TagInfo> tagPool = tags.iterator();
            long currentTime = System.currentTimeMillis();
            int numPerTag = Constants.NUM_PIC_TOTAL/tags.size();
            while (tagPool.hasNext()) {
                TagInfo searchTag = (TagInfo) tagPool.next();
                Uri uritemp = MagiclockProvider.Picture.CONTENT_URI;
                String selectionTemp = MagiclockProvider.Picture.CAT_DATA_ID
                        + "=" + searchTag.mIndex;
                String[] projectionTemp = new String[] { MagiclockProvider.Picture.DOWNLOAD_TIME };
                try { // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
                    cursorTermp = resolver.query(uritemp, projectionTemp,
                            selectionTemp, null, null);
                    if (null == cursorTermp || cursorTermp.getCount() <= 0) {
                        /** this category has no data. need to download*/
                        Log.d(TAG,
                                "[loadRecoveryCategoryFromDb] null pic. add recovery tag:"
                                        + searchTag.mName);
                        ret.tagList.add(searchTag.mName);
                    } else {
                        final int timeIndex = cursorTermp
                                .getColumnIndexOrThrow(MagiclockProvider.Picture.DOWNLOAD_TIME);
                        boolean isNeedLoad = true;
                        int picsum = 0;
                        while (cursorTermp.moveToNext()) {
                            long timeIndb = cursorTermp.getLong(timeIndex);
                            Calendar cal = Calendar.getInstance();
                            cal.setTimeInMillis(timeIndb);
                            SimpleDateFormat sdf = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss");
                            String alarmtime = sdf.format(cal.getTime());
                            Log.d(TAG,
                                    "[loadRecoveryCategoryFromDb] tag:"
                                            + searchTag.mName + "; time:"
                                            + alarmtime);
                            if (((currentTime - timeIndb) >= 0)
                                    /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
                                    && ((currentTime - timeIndb) <= DOWNLOAD_GAP)) {
                                picsum ++;
                                /* MODIFIED-END by dongdong.wang,BUG-1857937*/
                                if(picsum >= numPerTag){
                                    isNeedLoad = false;
                                    Log.d(TAG,
                                            "[loadRecoveryCategoryFromDb] tag:"
                                                    + searchTag.mName
                                                    + " No need to load!");
                                    break;
                                }

                            }
                        }
                        if (isNeedLoad) {
                            /** No picture download in 5 minute, need to download*/
                            ret.tagList.add(searchTag.mName);
                            Log.d(TAG,
                                    "[loadRecoveryCategoryFromDb] add recovery tag:"
                                            + searchTag.mName);
                        }

                    }
                } catch (Exception e) {
                    Log.e(TAG, "loadRecoveryCategoryFromDb exception2:"
                            + e);
                    e.printStackTrace();
                } finally {
                    if (null != cursorTermp) {
                        cursorTermp.close();
                        cursorTermp = null;
                    }
                }

            }
            ret.picSum = numPerTag * (ret.tagList.size());
            return ret;

        }

        return null;

    }


    /**
     * // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
     * @param bm
     * @param fileName
     * @param tag
     *            save to db
     */
    private boolean saveDownloadFile(Bitmap bm, String fileName, String tag,
            String authorName) {
        if(null == bm){
            Log.e(TAG, "[saveDownloadFile] error:Null input!");
            return false;
        }
        Constants.checkFileDir(this, TAG);
        boolean ret = false;

        Bitmap cropbm = null;
        BufferedOutputStream bos = null;
        try {
            File file = new File(Constants.getFolderPath(this, TAG) + fileName);
            Log.d(TAG, "saveDownloadFile:" + file);

            bos = new BufferedOutputStream(new FileOutputStream(file));
            /**
             * Crop and save
             */
            int requireWidth = Constants.MAGICLOCK_SCREEN_WIDTH;
            int requireHeight = Constants.MAGICLOCK_SCREEN_HEIGHT; // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937

            cropbm = PhotoUtils.centerCropImageBitmap(requireWidth, requireHeight, bm);
            PhotoUtils.addCreditInfo(mContext, "by " + authorName, requireWidth, requireHeight, cropbm);
            cropbm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            bos.close();
            cropbm.recycle();
            //moidfy by dongdong.wang to fix task 1857939 at 2016-04-05 begin.
            if(!bm.isRecycled()) {
                bm.recycle();
                Log.d(TAG, "saveDownloadFile recycle bm");
            }
            //moidfy by dongdong.wang to fix task 1857939 at 2016-04-05 end.
            ContentResolver resolver = getContentResolver();
            ContentValues values = new ContentValues();

            values.put(MagiclockProvider.Category.CAT, tag);
            values.put(MagiclockProvider.Picture.AUTHOR_NAME, authorName);
            values.put(MagiclockProvider.Picture.PICTURE_NAME, fileName);
            values.put(MagiclockProvider.Picture.DOWNLOAD_TIME,
                    System.currentTimeMillis());
            values.put(MagiclockProvider.Picture.ICON_PATH,
                    Constants.getFolderPath(this, TAG));
            Uri result = resolver.insert(MagiclockProvider.Picture.CONTENT_URI,
                    values);
            if (null != result) {
                ret = true;
            }

        } catch (Exception e) {
            Log.e(TAG, "saveDownloadFile exception:" + e,e);
            return false;
        // PR 1478640 Add by chunlin.tang@tcl.com 2016-01-26 Begin
        // Fix: Magic unlock crashed when occurred OutOfMemoryError.
        } catch (OutOfMemoryError e) {
            Log.w(TAG, "[saveDownloadFile] can't save " + fileName + " because of OutOfMemoryError.", e);
            if (null != bm && bm.isRecycled() == false) {
                bm.recycle();
            }
            if (null != cropbm && cropbm.isRecycled() == false) {
                cropbm.recycle();
            }
            if (null != bos) {
                try {
                    bos.close();
                } catch (Exception ex) {

                }
            }
            return false;
        // PR 1478640 Add by chunlin.tang@tcl.com 2016-01-26 End
        }
        Log.d(TAG, "saveDownloadFile ret = " + ret);
        return ret;

    }

    /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
    /**
     * get update type to set search params.
     * @param context
     * @return
     */
    public int getUpdateType(Context context){
        SharedPreferences sp = context.getSharedPreferences(Constants.PREFS_MGLK_NAME, Context.MODE_PRIVATE);
        int type = sp.getInt(KEY_UPDATE_TYPE, 0);
        return type;
    }

    /**
     * Record search type to xml.
     * @param context
     * @param currentType
     */
    public void saveUpdateType(Context context,int currentType){
        if (currentType >= 5){
            currentType = 0;
        }else {
            currentType = currentType + 1;
        }
        SharedPreferences sp = context.getSharedPreferences(Constants.PREFS_MGLK_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putInt(KEY_UPDATE_TYPE, currentType);
        ed.commit();
    }
    /* MODIFIED-END by dongdong.wang,BUG-1857937*/

    /**
     * setAutoCleanStatus Store the status
     */
    private synchronized void setServiceStatus(ServiceStatus info) {
        Log.d(TAG, "[setAutoCleanStatus]:"+info);
        SharedPreferences sp = mContext.getSharedPreferences(Constants.PREFS_MGLK_NAME,
                Context.MODE_PRIVATE);
        Editor editor = sp.edit();
        editor.putBoolean(SERVICE_STATUS, info.isAutoClean);
        editor.putBoolean(DOWN_RESUME, info.isDownloadResume);
        if(INVALID_RETRYTIMES != info.retryTimes){
            editor.putInt(RETRY_TIMES, info.retryTimes);
        }
        editor.commit();
    }


    /**
     * getServiceStatus Get the status
     * return: ServiceStatus
     */
    private synchronized ServiceStatus getServiceStatus() {
        ServiceStatus ret = new ServiceStatus();
        SharedPreferences sp = mContext.getSharedPreferences(Constants.PREFS_MGLK_NAME,
                Context.MODE_PRIVATE);
        ret.isAutoClean = sp.getBoolean(SERVICE_STATUS, false);
        ret.retryTimes = sp.getInt(RETRY_TIMES, 0);
        ret.isDownloadResume = sp.getBoolean(DOWN_RESUME, false);
        Log.d(TAG, "[getAutoCleanStatus]:" + ret);
        return ret;
    }


    private String getQuestionMarkSelection(final int num, String tag) {
        final StringBuilder whereBuilder = new StringBuilder();
        final String[] questionMarks = new String[num];
        Arrays.fill(questionMarks, "?");
        whereBuilder.append(tag).
                append(TextUtils.join(",", questionMarks)).
                append(")");
        return whereBuilder.toString();
    }

    public class CategoryDownload{
        public int picSum = 0;
        public ArrayList <String> tagList = null;
    }

    /**
     * ServiceStatus
     * @isAutoClean Service killed when do auto clean
     * @retryTimes  If service is killed when do download, can retry 3 times
     */
    public class ServiceStatus{
        public boolean isAutoClean = false;
        public int      retryTimes = 0;
        public boolean isDownloadResume = false;
        public ServiceStatus(){

        }
        public ServiceStatus(boolean autoclean, int times){
            isAutoClean = autoclean;
            retryTimes = times;
        }
        public ServiceStatus(boolean autoclean, int times, boolean needResume){
            isAutoClean = autoclean;
            retryTimes = times;
            isDownloadResume = needResume;
        }
        @Override
        public String toString(){
            return "Parameter [isAutoClean=" + isAutoClean +
                    ", retryTimes=" + retryTimes + ", isDownloadResume="+isDownloadResume+"]";
        }
    }

    /**
     // MODIFIED-BEGIN by dongdong.wang, 2016-04-20, BUG-1857937
     * Auto clean files in background. Include:
     *            1.Pictures under unselected categories except favorite and My picture
     *            2.Pictures not managed by database
     *            3.Pictures under selected categories keep max NUM_PIC_TOTAL,
     // MODIFIED-END by dongdong.wang, BUG-1857937
     *                       delete old Pictures evenly for every category
     *            4.Delete useless data in database
     *
     * @param Set<String> Pictures currently displayed on key-guard
     * @param Void
     * @param Void
     */
    private class AutoCleanTask extends AsyncTask<Set<String>, Void, Void>{

        @Override
        protected Void doInBackground(Set<String>... params) {
            try{
                ServiceStatus info = getServiceStatus();
                info.isAutoClean = true;
                setServiceStatus(info);

                deletePictures(params[0]);
                deleteCategoryCovers();

                info.isAutoClean = false;
                setServiceStatus(info);
            }catch(Exception e){
                Log.e(TAG, "AutoCleanTask Exception:"+e, e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            //mHandler.sendEmptyMessage(MSG_SERVICE_STOP);
            mHandler.sendEmptyMessage(MSG_CAT_CHANGE_CHECK);
        }



    }

    /**
     * Delete all the unselected categories' cover
     */
    private void deleteCategoryCovers() {
        Cursor cursor= null;
        try {

            ContentResolver resolver = mContext.getContentResolver();
            Uri uri = MagiclockProvider.Category.CONTENT_URI;
            String selection = MagiclockProvider.Category.SELECTED + "="
                    + MagiclockProvider.Category.FLAG_NOT_SELECTED
                    +" AND " + Category.CAT +"<>'" + Constants.CAT_MYPIC+"'"
                    +" AND " + Category.CAT +"<>'" + Constants.CAT_FAVORITE+"'";
            String[] projection = new String[] {
                    MagiclockProvider.Category.CAT,
                    MagiclockProvider.Category.COVER };
            cursor = resolver.query(uri, projection, selection, null, null);
            if (null == cursor || cursor.getCount() <= 0) {
                Log.d(TAG,"no unselected category!");
                return;
            }
            final int tagIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.CAT);
            final int coverIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.COVER);

            ArrayList<String> catList = new ArrayList<String>();
            while (cursor.moveToNext()) {
                String title = cursor.getString(tagIndex);
                if (null != cursor.getBlob(coverIndex)) {
                    catList.add(title); // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
                }
            }
            Log.d(TAG, "deleteCategoryCovers cat:" + catList.toString());
            if(null == catList || catList.size() <= 0){
                Log.d(TAG, "No category cover to delete!");
                return;
            }

            ContentValues values = new ContentValues();
            values.putNull(Category.COVER);

            int count = mContext.getContentResolver().update(
                    Category.CONTENT_URI,
                    values,
                    getQuestionMarkSelection(catList.size(), Category.CAT
                            + " IN("),
                    (String[]) catList.toArray(new String[catList.size()]));
            // if (count > 0) {
            //     Intent i = new Intent(
            //             MagicLockCategoryActivity.ACTION_UPDATE_COVER);
            //     i.putStringArrayListExtra(Category.CAT, catList);
            //     LocalBroadcastManager.getInstance(this).sendBroadcast(i);
            // }

        } catch (Exception e) {
            Log.e(TAG, "[deleteCovers] Exception: " + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
    }

    private void deletePictures(Set<String> datas) {
        long startTime = System.currentTimeMillis();
        String[] names = null;
        String selection = null;
        Log.d(TAG, "[" +
                "] startTime: " + startTime);

        if(null != datas && datas.size() >0) {
            names = datas.toArray(new String[0]);
            /** Get All need to be deleted pictures name and path from DB.
             *  then, delete these files and delete they from DB.
             */
            selection = getQuestionMarkSelection(names.length,
                    Picture.PICTURE_NAME + " NOT IN(")
                    + " AND "
                    + Picture.MY_PICTURE + "=" + Picture.FLAG_NOT_MYPICTURE
                    + " AND "
                    + Picture.FAVORITE + "=" + Picture.FLAG_NOT_FAVORITE;
        }else{
            selection = Picture.FAVORITE + "="
                    + Picture.FLAG_NOT_FAVORITE + " AND "
                    + Picture.MY_PICTURE + "=" + Picture.FLAG_NOT_MYPICTURE;
        }
        Cursor cursor = null;
        ArrayList<String> tempDatas = new ArrayList<String>();
        try {
            cursor = getContentResolver().query(
                    Picture.CONTENT_URI,
                    new String[] { Picture._ID, Picture.PICTURE_NAME,
                            Picture.ICON_PATH }, selection, names, null);
            final int idIndex = cursor.getColumnIndexOrThrow(Picture._ID);
            final int nameIndex = cursor
                    .getColumnIndexOrThrow(Picture.PICTURE_NAME);
            final int pathIndex = cursor
                    .getColumnIndexOrThrow(Picture.ICON_PATH);

            if (null != cursor && cursor.getCount() >0) {
                while (cursor.moveToNext()) {
                    String name = cursor.getString(nameIndex);
                    String path = cursor.getString(pathIndex);
                    File file = new File(path, name);
                    file.delete();
                    tempDatas.add(cursor.getString(idIndex));
                }
                if (tempDatas.size() > 0) {
                    getContentResolver().delete(
                            Picture.CONTENT_URI,
                            getQuestionMarkSelection(tempDatas.size(),
                                    Picture._ID + " IN("),
                            tempDatas.toArray(new String[0]));
                } else {
                    Log.d(TAG,"[deletePictures] There are not picture need to be deleted!");
                }
            } else {
                Log.d(TAG, "[deletePictures] 01 cursor is null!");
            }
            tempDatas.clear();
        } catch (Exception e) {
            Log.e(TAG, "[deletePictures] Exception:" + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
        /** Get all pictures' name from db and save they into tempDatas.
         *  Get all files from flickr_test directory.
         *  Then, delete all the files that not into tempDatas.
         */
        try {
            cursor = getContentResolver()
                    .query(Picture.CONTENT_URI,
                            new String[] { Picture.PICTURE_NAME }, null,
                            null, null);
            final int nameIdex2 = cursor
                    .getColumnIndexOrThrow(Picture.PICTURE_NAME);
            if (null != cursor && cursor.getCount()>0) {
                while (cursor.moveToNext()) {
                    tempDatas.add(cursor.getString(nameIdex2));
                }
            }else{
                Log.d(TAG, "[deletePictures] 02 cursor is null!");
            }
        } catch (Exception e) {
            Log.e(TAG, "deletePictures Exception:" + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
        if (tempDatas.size() > 0) {
            File directory = new File(Constants.getFolderPath(this, TAG));
            File[] filesDelete = directory.listFiles();
            /* MODIFIED-BEGIN by dongdong.wang, 2016-05-19,BUG-2116248*/
            if (null == filesDelete){
                Log.d(TAG, "[deletePictures] null == filesDelete");
                return;
            }
            /* MODIFIED-END by dongdong.wang,BUG-2116248*/
            for (File file : filesDelete) {
                String fileName = file.getName();
                if (!tempDatas.contains(fileName)) {
                    file.delete();
                } else {
                    tempDatas.remove(fileName);
                }
            }
        } else {
            Log.d(TAG, "[deletePictures] Get nothing from db!");
        }
        Log.d(TAG,"[deletePictures] Total Time used:"+ (System.currentTimeMillis() - startTime));
    }


    private void recoveryDeletePictures() {
        new Thread() {
            public void run() {
                Cursor cursor = null;
                ServiceStatus info = new ServiceStatus(true,INVALID_RETRYTIMES);
                setServiceStatus(info);
                try {

                    /** Get 30 pictures that recently down-loaded*/
                    String selection = Picture.FAVORITE + "="
                            + Picture.FLAG_NOT_MYPICTURE + " AND "
                            + Picture.MY_PICTURE + "="
                            + Picture.FLAG_NOT_MYPICTURE;
                    String[] projection = new String[] { Picture.PICTURE_NAME };
                    cursor = getContentResolver().query(
                            ShownPictureView.CONTENT_URI, projection,
                            selection, null, null);
                    Set<String> names = new HashSet<String>();
                    if (null != cursor) {
                        int count = cursor.getCount();
                        if (count > Constants.NUM_PIC_TOTAL) {
                            cursor.moveToPosition(count
                                    - Constants.NUM_PIC_TOTAL - 1);
                        }
                        addPicturesToSet(names, cursor);
                    }
                    /** Get all favorites and my pictures*/
                    selection = Picture.FAVORITE + "=" + Picture.FLAG_FAVORITE
                            + " OR " + Picture.MY_PICTURE + "="
                            + Picture.FLAG_MYPICTURE;
                    cursor = getContentResolver().query(
                            ShownPictureView.CONTENT_URI, projection,
                            selection, null, null);
                    addPicturesToSet(names, cursor);
                    deletePictures(names);
                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e, e);
                } finally {
                    info.isAutoClean = false;
                    info.retryTimes = INVALID_RETRYTIMES;
                    setServiceStatus(info);
                    if (null != cursor) {
                        cursor.close();
                    }
                }
            }
        }.start();

    }

    private void addPicturesToSet(Set<String> names, Cursor cursor) {
        try {
            if (cursor.getCount() <= 0) {
                Log.d(TAG,"cursor.getCount() <= 0");
                return ;
            }
            final int picIndex = cursor.getColumnIndexOrThrow(Picture.PICTURE_NAME);
            while (cursor.moveToNext()) {
                String picName = cursor.getString(picIndex);
                names.add(picName);
                Log.d(TAG,"[addPicturesToSet] picName=" + picName);
            }
        } catch (Exception e) {
            Log.w(TAG, "[addPicturesToSet] Exception: " + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
    }

}
