package com.tct.magiclock.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.util.Calendar;

import com.tct.magiclock.service.BootCompletedService;

/**
 * The class {@code Utils} is a util class for WallShuffle
 * @author guoqiang.qiu@tcl.com
 * @since  n8996, Solution-2699694
 */

public class Utils {
    private static final long HOUR = 3_600_000L;
    private static final int ALARM_TIME = 8;

    public static final String ACTION_CHANGE = "com.tct.magiclock.action.CHANGE";
    public static final String ACTION_UPDATE = "com.tct.magiclock.action.UPDATE";
    public static final String ACTION_LOCAL = "com.tct.magiclock.action.LOCAL";
    public static final String ACTION_FAVORITE = "android.intent.action.SET_FAVORITE_WALLPAPER";
    public static final String ACTION_UPDATE_COVER = "com.tct.magiclock.action.UPDATE_COVER";

    public static final int INTERVAL_EACH = 0;
    public static final int INTERVAL_HOUR = 1;
    public static final int INTERVAL_HALF = 12;
    public static final int INTERVAL_DAY = 24;
    
    /**
     * Check if network is OK
     * @return true if can connect to network
     */
    public static boolean isNetworkOkForDownload(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnected()) {
            return false;
        }
        return netInfo.getType() == ConnectivityManager.TYPE_WIFI
            || (!SettingPref.getBoolean(context, SettingPref.WIFI, true)
                && netInfo.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Set an alarm to change wallpaper
     * @param interval Change interval
     * @return true if can connect to network
     */  
    public static void setIntervalAlarm(Context context, int interval) {
        if (interval == INTERVAL_EACH) return;//No need to set alarm to update wallpaper
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, getStartTime(interval), interval * HOUR, getChangePendingIntent(context));
    }

    public static void cancelIntervalAlarm(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        PendingIntent intent = getChangePendingIntent(context);
        intent.cancel();
        am.cancel(intent);
    }

    static PendingIntent getChangePendingIntent(Context context) {
        Intent intent = new Intent(ACTION_CHANGE);
        return PendingIntent.getService(context, 0, intent, 0);
    }

    private static long getStartTime(int interval) {
        long time = System.currentTimeMillis();
        if (interval == INTERVAL_HOUR) {
            return time + HOUR;
        }

        final int HOUR_TYPE = interval == INTERVAL_DAY ? Calendar.HOUR_OF_DAY : Calendar.HOUR;

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);

        c.set(HOUR_TYPE, ALARM_TIME);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        if (c.getTimeInMillis() - time < 0) { //Contract update time has expired
            c.set(HOUR_TYPE, ALARM_TIME + interval);
        }
        Log.d("Qiu", "Time->" + c.getTime());

        return c.getTimeInMillis();
    }
}