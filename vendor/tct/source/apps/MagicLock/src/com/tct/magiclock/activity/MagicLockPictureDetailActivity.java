package com.tct.magiclock.activity;

import android.app.Activity;
/* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider;
/* MODIFIED-END by dongdong.wang,BUG-1857937*/
import com.tct.magiclock.util.PhotoUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

/**
 * Created by chunlin.tang on 8/19/15.
 */
public class MagicLockPictureDetailActivity extends Activity implements View.OnClickListener{
    //add by dongdong.wang to fix defect 1857937 at 2016-04-19 begin
    private static final String TAG = "PictureDetail";
    public static final String KEY_IMG_PATH = "imagesPath";
    public static final String KEY_PIC_NAME="picName";
    private View mPictureInfo = null;
    private ImageView mPictureDetail = null;
//    private View mLoading = null;

    /**
     * The status bar and navigation have been hided.
     */
    public static final int STATUS_FULL_SCREEN = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
    /**
     * The status bar and navigation have been shown.
     */
    public static final int STATUS_NOT_FULL_SCREEN = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.magiclock_picture_detail_activity);

        try {
//            mLoading = findViewById(R.id.mpda_loading_progress);
            mPictureDetail = (ImageView)findViewById(R.id.mpda_picture_detail);
            /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
            mPictureInfo = findViewById(R.id.magiclock_info);
            mPictureDetail.setOnClickListener(this);
            setPicInfoVisibility(false);
            /* MODIFIED-END by dongdong.wang,BUG-1857937*/
            loadingPicture(getIntent());
        } catch (Exception e) {
            Log.w(TAG, "[onCreate] init picture exception!", e);
        }
    }

    private void loadingPicture(Intent intent) {
        /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
        //String picturePath = intent.getStringExtra(KEY_IMG_PATH);
        String pictureName = intent.getStringExtra(KEY_PIC_NAME);
        new LoadPictureTask(this, mPictureDetail).execute(pictureName);
        /* MODIFIED-END by dongdong.wang,BUG-1857937*/
    }

    public void hideSystemUI(View view) {
        try {
            view.setSystemUiVisibility(STATUS_FULL_SCREEN);
        } catch (Exception e) {
            Log.e(TAG, "[hideSystemUI] view=" + view, e);
        }

    }


    public void showSystemUI(View view) {
        try {
            view.setSystemUiVisibility(STATUS_NOT_FULL_SCREEN);
        } catch (Exception e) {
            Log.e(TAG, "[showSystemUI] view=" + view, e);
        }
    }

    private void opStatusBarAndNav(View view) {
        if (isFullScreen(view)) {
            showSystemUI(view);
        } else {
            hideSystemUI(view);
        }
    }

    private boolean isFullScreen(View view) {
        try {
            Log.d(TAG, "[isFullScreen] getSystemUiVisibility=" + view.getSystemUiVisibility());
            return (view.getSystemUiVisibility() == STATUS_FULL_SCREEN);
        } catch (Exception e) {
            Log.e(TAG, "[isFullScreen] view=" + view, e);
        }
        return true;
    }
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.in_from_right, R.anim.out_from_left);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.mpda_picture_detail:
                opStatusBarAndNav(view);
                break;
        }
    }

    /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
    /**
     * set picture info visible.
     * @param isVisible
     */
    private void setPicInfoVisibility(boolean isVisible){
        mPictureInfo.setVisibility(isVisible ? (View.VISIBLE) : (View.GONE));
    }

    public class LoadPictureTask extends AsyncTask<String, Object, Bitmap> {
        private Context mContext;
        private WeakReference<ImageView> mimageViewReference;
        private String mImageAuthor = null;
        /* MODIFIED-END by dongdong.wang,BUG-1857937*/

        public LoadPictureTask(Context context, ImageView imageview) {
            this.mContext = context;
            this.mimageViewReference = new WeakReference<ImageView>(imageview);
        }
        @Override
        protected Bitmap doInBackground(String... datas) {
            if (Constants.MAGICLOCK_SCREEN_HEIGHT <= 0) {
                Constants.getScreenSize(mContext, TAG);
            }
            try {
                mImageAuthor = null; // MODIFIED by dongdong.wang, 2016-04-20,BUG-1857937
                return loadPictureFromStorage(mContext, datas[0]);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e,e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            try {
                ImageView imageview = mimageViewReference.get();
                imageview.setImageBitmap(result);
                imageview.setVisibility(View.VISIBLE);
//                imageview.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.in_from_left));
//                mLoading.setVisibility(View.GONE);
                /* MODIFIED-BEGIN by dongdong.wang, 2016-04-20,BUG-1857937*/
                if(null != mImageAuthor){
                    TextView magiclockName = (TextView)findViewById(R.id.magiclock_name);
                    String strDis = "@ by " + mImageAuthor + " ";
                    magiclockName.setText(strDis);
                    setPicInfoVisibility(true);
                }
            } catch (Exception e) {
                Log.e(TAG, "[onPostExecute] result=" + result, e);
            }
        }

        /**
         *  load picture info for db sql.
         * @param context
         * @param fileName
         * @return
         */
        private String loadAuthorInfo(Context context, String fileName){
            ContentResolver cr = context.getContentResolver();
            String[] projection = new String[]{MagiclockProvider.Picture.AUTHOR_NAME};
            String selection = MagiclockProvider.Picture.PICTURE_NAME+"='"+ fileName+"'";
            Cursor cursor = null;
            String ret = null;

            try{
                cursor = cr.query(MagiclockProvider.FavoriteView.CONTENT_URI,
                        projection, selection,null,null);
                if(null != cursor && cursor.getCount() >0){
                    final int index = cursor.getColumnIndexOrThrow(MagiclockProvider.Picture.AUTHOR_NAME);
                    while (cursor.moveToNext()) {
                        ret = cursor.getString(index);
                    }
                }else{
                    Log.e(TAG, "loadAuthorInfo NULL cursor");
                }

            }catch (Exception e){
                Log.e(TAG, "loadAuthorInfo "+e,e);
            }finally {
                if(cursor != null){
                    cursor.close();
                    cursor = null;
                }
            }

            return ret;
        }
        private Bitmap loadPictureFromStorage(Context context, String filePath) {
            FileInputStream fis = null;
            String filePathTemp = Constants.getFolderPath(context, TAG) +filePath;
            try {

                int requireWidth = Constants.MAGICLOCK_SCREEN_WIDTH;
                int requireHeight = Constants.MAGICLOCK_SCREEN_HEIGHT;
                mImageAuthor = loadAuthorInfo(context, filePath);
                Log.d(TAG, "loadPictureFromStorage file = " + filePathTemp +" | authot = "+mImageAuthor);
                //add by dongdong.wang to fix defect 1857937 at 2016-04-19 end.
               return PhotoUtils.centerCropImage(requireWidth, requireHeight, filePathTemp);
               /* MODIFIED-END by dongdong.wang,BUG-1857937*/
            } catch (Exception e) {
                Log.e(TAG, " Error getLoacalBitmap, e: " + e, e);
            } finally {
                if (null != fis) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        Log.e(TAG, "IOException caught while closing stream: " + e, e);
                    }
                }
            }
            return null;
        }
    }
}
