package com.tct.magiclock;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.android.settingslib.drawer.SettingsDrawerActivity;

import android.content.ContentValues;
import com.tct.magiclock.util.DataCollectUtil;
import android.util.Log;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
/**
 * Setting entry activity for Wall Shuffle
 * Created by TCTNB(Guoqiang.Qiu), Solution-2699694
 */

public class WallShuffleSettingsActivity extends SettingsDrawerActivity implements
        PreferenceFragment.OnPreferenceStartFragmentCallback {
    private static final String EXTRA_SHOW_FRAGMENT = ":settings:show_fragment";
    private static final String EXTRA_SHOW_FRAGMENT_ARGUMENTS = ":settings:show_fragment_args";
    private static final String EXTRA_SHOW_FRAGMENT_TITLE = ":settings:show_fragment_title";

    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    //for get boolean from intent, defined in super class
    public static final String EXTRA_SHOW_MENU = "show_drawer_menu";
    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        final String initialFragmentName = intent.getStringExtra(EXTRA_SHOW_FRAGMENT);

        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        if (intent != null && intent.getBooleanExtra(EXTRA_SHOW_MENU, false)) {
            ContentValues mValuesData = new ContentValues();
            mValuesData.put("action","ADD");
            mValuesData.put(DataCollectUtil.MAGICLOCK_SETTINGENTER_NUM,String.valueOf(1));
            DataCollectUtil.storeData((Context)this, mValuesData);
        }else{
            ContentValues mValuesData = new ContentValues();
            mValuesData.put("action","ADD");
            mValuesData.put(DataCollectUtil.MAGICLOCK_NOTSETTINGENTER_NUM,String.valueOf(1));
            DataCollectUtil.storeData((Context)this, mValuesData);
        }
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

        if (savedInstanceState == null) {
            if (initialFragmentName != null) {
                setTitleFromIntent(intent);
                Bundle initialArguments = intent.getBundleExtra(EXTRA_SHOW_FRAGMENT_ARGUMENTS);
                CharSequence initialTitle = intent.getCharSequenceExtra(EXTRA_SHOW_FRAGMENT_TITLE);
                switchToFragment(initialFragmentName, initialArguments, initialTitle);
            } else {
                switchToFragment(WallShuffleSettings.class.getName(), null, null);
            }
        } else {
            setTitleFromIntent(intent);
        }
        android.app.ActionBar mActionBar = getActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(initialFragmentName != null);
            mActionBar.setHomeButtonEnabled(true);
        }
    }

    //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
    @Override
    protected void onStart(){
        super.onStart();
	SharedPreferences preferences = this.getSharedPreferences("data_collection",0);
        SharedPreferences.Editor editor = preferences.edit();
	int resumeTime = (int) (System.currentTimeMillis()/1000);
	editor.putInt("setting_start_time", resumeTime).commit();
    }

    @Override
    protected void onStop(){
        super.onStop();
	SharedPreferences preferences = this.getSharedPreferences("data_collection",0);
        int pauseTime = (int) (System.currentTimeMillis()/1000);
	int resumeTime = preferences.getInt("setting_start_time", 0);
	if (resumeTime != 0){
	    int settingInternal = pauseTime - resumeTime;
	    ContentValues mValuesData = new ContentValues();
            mValuesData.put("action","AVERAGE");
            mValuesData.put(DataCollectUtil.MAGICLOCK_MAINSETTING_DUR,String.valueOf(settingInternal));
            DataCollectUtil.storeData((Context)this, mValuesData);
	}
    }
    //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-3304494

    @Override
    public boolean onPreferenceStartFragment(PreferenceFragment caller, Preference pref) {
        startActivity(onBuildStartFragmentIntent(this, pref.getFragment(), pref.getExtras(), pref.getTitle()));
        return true;
    }

    @Override
    public boolean onNavigateUp() {
        finish();
        return true;
    }

    private void setTitleFromIntent(Intent intent) {
        final String initialTitle = intent.getStringExtra(EXTRA_SHOW_FRAGMENT_TITLE);
        setTitle(initialTitle);
    }

    private void switchToFragment(String fragmentName, Bundle args, CharSequence title) {
        Fragment fragment = Fragment.instantiate(this, fragmentName, args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        if (title != null) {
            transaction.setBreadCrumbTitle(title);
        }
        transaction.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
    }

    private Intent onBuildStartFragmentIntent(Context context, String fragmentName,
                                              Bundle args, CharSequence title) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClass(context, WallShuffleSettingsActivity.class);
        intent.putExtra(EXTRA_SHOW_FRAGMENT, fragmentName);
        intent.putExtra(EXTRA_SHOW_FRAGMENT_ARGUMENTS, args);
        intent.putExtra(EXTRA_SHOW_FRAGMENT_TITLE, title);
        return intent;
    }
}
