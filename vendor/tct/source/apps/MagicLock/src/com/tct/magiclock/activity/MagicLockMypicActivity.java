package com.tct.magiclock.activity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.tct.magiclock.Constants;
import com.tct.magiclock.R;
import com.tct.magiclock.db.MagiclockProvider;
import com.tct.magiclock.db.MagiclockProvider.Category;
import com.tct.magiclock.db.MagiclockProvider.MypicView;
import com.tct.magiclock.db.MagiclockProvider.Picture;
import com.tct.magiclock.util.MGLKUtils;
import com.tct.magiclock.util.PhotoUtils;
import com.tct.magiclock.utils.SettingPref;

import android.content.ContentValues;
import com.tct.magiclock.util.DataCollectUtil;

public class MagicLockMypicActivity extends MagicLockBaseActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TAG = "MypicActivity";

    private static final int REQUEST_CODE_PHOTO_PICKED_WITH_DATA = 1001;
    private static final int REQUEST_CROP_PHOTO = 1002;

    private Uri mTempPhotoUri;
    private Uri mCroppedPhotoUri;

    private Context mContext;

    /**
     * Add my picture button.
     */
    private View mAddBtn = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;

        mAddBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startPickFromGalleryActivity();
            }
        });
//        findViewById(R.id.amm_delete_mypicture).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // start MagicLockPictureDeleteActivity
//                Intent intent = new Intent(MagicLockMypicActivity.this, MagicLockPictureDeleteActivity.class);
//                intent.putExtra(MagicLockPictureDeleteActivity.ACTION_KEY, MagicLockPictureDeleteActivity.ACTION_OP_MYPICURES);
////                intent.putExtra(MagicLockPictureDeleteActivity.SELECTED_ID_KEY, view.getId());
////                intent.putExtra(MagicLockPictureDeleteActivity.SELECTED_POS_KEY, position);
//                startActivity(intent);
//            }
//        });

        mNoContentTips = (TextView)findViewById(R.id.amm_no_mypicture);
        // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.02 Begin
        // Set text to loading because the loading option maybe coast a long time.
        // so,we should give user a message before the picture showing.
        mNoContentTips.setText(R.string.magiclock_loading);
        // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.02 End
        mNoContentTips.setVisibility(View.VISIBLE);
        getLoaderManager().initLoader(0, null, this);

        if (Constants.MAGICLOCK_SCREEN_HEIGHT <= 0) {
            Constants.getScreenSize(mContext, TAG);
        }



        //[FEATURE]-ADD-BEGIN by TCTNB(YITING.LU), 2016-10-26, Task-3304494
        ContentValues mValuesData = new ContentValues();
        mValuesData.put("action","ADD");
        mValuesData.put(DataCollectUtil.MAGICLOCK_MYPICTURE_NUM,String.valueOf(1));
        DataCollectUtil.storeData(this, mValuesData);
        //[FEATURE]-ADD-END by TCTNB(YITING.LU), 2016-10-26, Task-


    }

    @Override
    protected void setContentView(){
        setContentView(R.layout.activity_magiclock_mypic);
        mGridView = (GridView) findViewById(R.id.amm_gridview);
        mAddBtn = findViewById(R.id.amm_add_mypicture);
    }
    /**
     * change the visibility of {@link #mAddBtn}
     * @param value is {@link android.view.View#VISIBLE} or {@link android.view.View#GONE}
     */
    @Override
    protected void setAddBtnVisibility(int value) {
        mAddBtn.setVisibility(value);
    }

    @Override
    protected void doDeleteOptions(){
        mAdapter.deleteMyPictures(this, getTitle());
    }

    @Override
    protected void setTitle() {
        setTitle(R.string.magiclock_cat_mypic);
        if (null != mNoContentTips) {
            mNoContentTips.setText(R.string.magiclock_no_my_picture);
        }
    }

    /**
     *
     * @return a boolean, false means can add new picture into my picture. true means can not.
     */
    private boolean checkMyPictureNum() {
        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(Picture.CONTENT_URI, new String[] {Picture._ID},
                    Picture.MY_PICTURE + "=" + Picture.FLAG_MYPICTURE, null, null);
            if (cursor.getCount() >= Constants.NUM_PIC_TOTAL) {
                return true;
            }
        } catch (Exception e) {
            Log.e(TAG, "[checkMyPictureNum] Exception: " + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
            }
        }
        return false;
    }

    private void startPickFromGalleryActivity() {
        if (checkMyPictureNum()) {
            Toast.makeText(this, R.string.magiclock_mypicture_limit, Toast.LENGTH_SHORT).show();
            return;
        }

        mTempPhotoUri = PhotoUtils.generateTempImageUri(mContext);
        mCroppedPhotoUri = PhotoUtils.generateTempCroppedImageUri(mContext);
        final Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
        intent.setType("image/*");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mTempPhotoUri);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION |
                Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setClipData(ClipData.newRawUri(MediaStore.EXTRA_OUTPUT, mTempPhotoUri));
        startActivityForResult(intent, REQUEST_CODE_PHOTO_PICKED_WITH_DATA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode:" + requestCode + "; resultCode=" + resultCode);
        if (Activity.RESULT_OK != resultCode) {
            Log.e(TAG, "onActivityResult. result not OK!");
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_PHOTO_PICKED_WITH_DATA:
                final Uri uri;
                if (null != data && null != data.getData()) {
                    try {
                        uri = data.getData();
                        Log.d(TAG, "onActivityResult. uri=" + uri + "; mTempPhotoUri=" + mTempPhotoUri + "; mCroppedPhotoUri=" + mCroppedPhotoUri);
                        // PR 1201257 Add by chunlin.tang@tcl.com 2016.01.06 Begin
                        // Fix: Magic unlock stopped after Google Photos Crash.
                        if (null == mCroppedPhotoUri) {
                            mCroppedPhotoUri = PhotoUtils.generateTempCroppedImageUri(mContext);
                            Log.d(TAG, "onActivityResult.new  mCroppedPhotoUri=" + mCroppedPhotoUri);
                        }
                        // PR 1201257 Add by chunlin.tang@tcl.com 2016.01.06 End

                        PhotoUtils.savePhotoFromUriToUri(mContext, uri, mTempPhotoUri, false);
                        //start crop photo
                        Intent intent = new Intent("com.android.camera.action.CROP");
                        intent.setDataAndType(mTempPhotoUri, "image/*");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, mCroppedPhotoUri);
                        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION |
                                Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        intent.setClipData(ClipData.newRawUri(MediaStore.EXTRA_OUTPUT, mCroppedPhotoUri));
                        PhotoUtils.addPhotoPickerExtras(intent, mCroppedPhotoUri);
                        int width = Constants.MAGICLOCK_SCREEN_WIDTH;
                        int height = Constants.MAGICLOCK_SCREEN_HEIGHT;
                        Log.d(TAG, "width=" + width + "; height=" + height);
                        PhotoUtils.addCropExtras(intent, width, height);
                        startActivityForResult(intent, REQUEST_CROP_PHOTO);
                    } catch (Exception e) {
                        Log.e(TAG, "PICKED WITH DATA exception:" + e, e);
                    }

                }
                break;
            case REQUEST_CROP_PHOTO:
                final Uri uri_temp;
                if (null != data && null != data.getData()) {
                    uri_temp = data.getData();
                } else {
                    uri_temp = mCroppedPhotoUri;
                }
                try {
                    mContext.getContentResolver().delete(mTempPhotoUri, null, null);
                    mTempPhotoUri = null;
                    loadPicFromStorage(uri_temp);
                } catch (Exception e) {
                    Log.e(TAG, "REQUEST_CROP_PHOTO exception" + e);
                    e.printStackTrace();
                }
        }

    }

    private void loadPicFromStorage(Uri uri) {
        if (uri != null) {
            loadImageTask loadPicTask = new loadImageTask();
            loadPicTask.execute(uri);
        } else {
            cannotLoadImage();
        }
    }

    protected void cannotLoadImage() {
        CharSequence text = getString(R.string.magiclock_cannot_load_image);
        Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        toast.show();
    }


    /**
     * Instantiate and return a new Loader for the given ID.
     *
     * @param id   The ID whose loader is to be created.
     * @param args Any arguments supplied by the caller.
     * @return Return a new Loader instance that is ready to start loading.
     */
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,MypicView.CONTENT_URI,
                new String[] {Picture._ID, Picture.PICTURE_NAME, Picture.ICON_PATH, Picture.ICON},
                null, null, null);
    }
    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        try {
            if (data.getCount() > 0) {
                if (null == mNoContentTips)
                    mNoContentTips = (TextView) findViewById(R.id.amm_no_mypicture);
                mNoContentTips.setVisibility(View.GONE);
            } else {
                if (null == mNoContentTips)
                    mNoContentTips = (TextView) findViewById(R.id.amm_no_mypicture);
                mNoContentTips.setVisibility(View.VISIBLE);
            }
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.02 Begin
            // Because we set the text to loading at onCreate(), so, we should
            // change the text to no my picture when the data load finished.
            mNoContentTips.setText(R.string.magiclock_no_my_picture);
            // PR 1006422 Add by chunlin.tang@tcl.com 2015.12.02 End
        } catch (Exception e) {
            Log.e(TAG, "[onLoadFinished] Exception: " + e, e);
        }
        try {
            data.setNotificationUri(getContentResolver(), MypicView.CONTENT_URI);
            mAdapter.changeCursor(data);
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e, e);
        }
    }
    /**
     * Called when a previously created loader is being reset, and thus
     * making its data unavailable.  The application should at this point
     * remove any references it has to the Loader's data.
     *
     * @param loader The Loader that is being reset.
     */
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        try {
            mAdapter.changeCursor(null);
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
            e.printStackTrace();
        }
    }



    private class loadImageTask extends AsyncTask<Uri, Void, String> {

        @Override
        protected String doInBackground(Uri... params) {
            Uri uri = params[0];
            String ret = null;

            try {
                final Bitmap bitmap = PhotoUtils.getBitmapFromUri(mContext, uri);
                String path = uri.toString();
                int index = path.lastIndexOf("/");
                String fileName = path.substring(index + 1);
                boolean isOk = savePicInfo(bitmap, fileName);
                bitmap.recycle();
                if (isOk) {
                    ret = fileName;
                }

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                Log.e(TAG, "[loadImageTask] exception:" + e);
                e.printStackTrace();
            }

            return ret;
        }

        @Override
        protected void onPostExecute(String fileName) {
            Log.d(TAG, "onPostExecute, path:" + fileName);
            if (null != fileName && null != mContext) {
                try {
                    mContext.getContentResolver().notifyChange(MagiclockProvider.MypicView.CONTENT_URI, null);
                    mContext.getContentResolver().delete(mCroppedPhotoUri, null, null);
                    mCroppedPhotoUri = null;
                } catch (Exception e) {
                    Log.e(TAG, "[loadImageTask] onPostExecute exception:" + e);
                    e.printStackTrace();
                }
            }

        }
    }

    private void notifyKeyGuardChange(String fileName) {
        if(!SettingPref.isMagicOn(mContext)){
            return;
        }
        /** Check if My Picture is selected*/
        String selection = Category.CAT + " = '" + Constants.CAT_MYPIC + "'";
        String[] projection = new String[] { Category.SELECTED };
        Uri uri = Category.CONTENT_URI;
        Cursor cursor = null;
        int isSelected = 0;
        try {
            cursor = mContext.getContentResolver().query(uri, projection,
                    selection, null, null);
            while (cursor.moveToNext()) {
                final int index = cursor.getColumnIndexOrThrow(Category.SELECTED);
                Log.d(TAG, "index="+index);
                isSelected = cursor.getInt(index);
                Log.d(TAG, "is Mypic Selected = "+isSelected);
            }
        } catch (Exception e) {
            Log.e(TAG, "[notifyKeyGuardChange]" + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
        if (1 == isSelected) {
            Log.d(TAG, "notifyKeyGuardChange!");
            /* MODIFIED-BEGIN by Zhenhua.Fan, 2016-11-09,BUG-3266668*/
            new MagicLockGetShownPictureTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,mContext);
//            ArrayList<String> list = new ArrayList<String>();
//            Bundle bd = new Bundle();
//            list.add(Constants.FLAG_MYPIC);
//            list.add("0");
//            bd.putStringArrayList(fileName, list);
//            Intent intent = new Intent(Constants.ACTION_DATACHANGED);
//            intent.putExtra(Constants.KEY_TAG, Constants.TAG_ADD);
//            intent.putExtra(Constants.KEY_DATA, bd);
//            sendBroadcast(intent);
/* MODIFIED-END by Zhenhua.Fan,BUG-3266668*/
        }
    }

    /**
     *
     * @param bm
     * @param fileName
     */
    private boolean savePicInfo(Bitmap bm, String fileName)
    {
        Constants.checkFileDir(this, TAG);
        boolean ret = false;
        try {
            File file= new File(Constants.getFolderPath(this, TAG)+fileName);
            Log.d(TAG,"savePicInfo:"+file);

            BufferedOutputStream bos;
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            bos.flush();
            bos.close();
            Log.d(TAG, "[savePicInfo] set readable result: " + file.setReadable(true, false));

            ContentResolver resolver = getContentResolver();
            ContentValues values = new ContentValues();

            values.put(MagiclockProvider.Category.CAT, Constants.CAT_MYPIC);
            values.put(MagiclockProvider.Picture.AUTHOR_NAME, Constants.FLAG_MYPIC);
            values.put(MagiclockProvider.Picture.PICTURE_NAME, fileName);
            values.put(MagiclockProvider.Picture.DOWNLOAD_TIME, System.currentTimeMillis());
            values.put(MagiclockProvider.Picture.ICON_PATH, Constants.getFolderPath(this, TAG));
            values.put(Picture.MY_PICTURE, Picture.FLAG_MYPICTURE);
            Bitmap bp = PhotoUtils.centerCropImage(
                    PhotoUtils.getIntFromXml(mContext, R.dimen.magiclock_fav_item_width, 171),
                    PhotoUtils.getIntFromXml(mContext, R.dimen.magiclock_fav_item_height, 273),
                    Constants.getFolderPath(this, TAG)+fileName);
            values.put(Picture.ICON, PhotoUtils.flattenBitmap(bp));

            Uri result = resolver.insert(MagiclockProvider.Picture.CONTENT_URI, values);
            if(null != result){
                ret = true;
                notifyKeyGuardChange(fileName);
            }

        } catch (Exception e) {
            Log.e(TAG, "[savePicInfo]Exception:" + e, e);
            return false;
        }
        Log.d(TAG,"testSaveFile ret = "+ret);
        return ret;

    }
}
