package com.tct.magiclock.activity;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.tct.magiclock.Constants;
import com.tct.magiclock.db.MagiclockProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by dongdong.wang on 12/13/15.
 */
public class CleanPictureTask extends AsyncTask<Void, Void, Void> {
    private static final String TAG = "CleanPictureTask";
    private Context mContext;
    public CleanPictureTask(Context context){
        this.mContext = context;
    }

    public CleanPictureTask(){}


    @Override
    protected Void doInBackground(Void... params) {
        Cursor cursor = null;
        if (mContext == null){
            Log.d(TAG, "mContext = " + mContext);
            return null;
        }
        try {
            Uri uri = MagiclockProvider.Picture.CONTENT_URI;
            String selection = MagiclockProvider.Picture.FAVORITE + "="
                    + MagiclockProvider.Picture.FLAG_NOT_FAVORITE + " AND "
                    + MagiclockProvider.Picture.MY_PICTURE + "=" + MagiclockProvider.Picture.FLAG_NOT_MYPICTURE;

            cursor = mContext.getContentResolver().query(uri,new String[] {MagiclockProvider.Picture._ID,
                            MagiclockProvider.Picture.PICTURE_NAME},
                    selection, null, null);
            Log.d(TAG, "cursor count = " +cursor.getCount());
            if (null != cursor && cursor.getCount() > 0) {
                final int picIndex = cursor.getColumnIndexOrThrow(MagiclockProvider.Picture.PICTURE_NAME);
                final int idIndex = cursor.getColumnIndexOrThrow(MagiclockProvider.Picture._ID);
                ArrayList<String> tempDatas = new ArrayList<String>();
                cursor.moveToPosition(-1);
                Log.d(TAG, "cursor move -1" );
                while (cursor.moveToNext()) {
                    String picName = cursor.getString(picIndex);
                    Log.d(TAG, "doInBackground, filename= " + picName);
                    File file = new File(Constants.getFolderPath(mContext, TAG), picName);
                    file.delete();
                    tempDatas.add(cursor.getString(idIndex));
                }
                if (tempDatas.size() > 0) {
                    mContext.getContentResolver().delete(
                            MagiclockProvider.Picture.CONTENT_URI,
                            getQuestionMarkSelection(tempDatas.size(),
                                    MagiclockProvider.Picture._ID + " IN("),
                            tempDatas.toArray(new String[0]));


                } else {
                    Log.d(TAG, "[CleanPictureTask] cursor is null!");
                }
            }

        }catch (Exception e) {
            Log.e(TAG, "[CleanPictureTask] Exception:", e);
        }
        return null;
    }

    private String getQuestionMarkSelection(final int num, String tag) {
        final StringBuilder whereBuilder = new StringBuilder();
        final String[] questionMarks = new String[num];
        Arrays.fill(questionMarks, "?");
        whereBuilder.append(tag).
                append(TextUtils.join(",", questionMarks)).
                append(")");
        return whereBuilder.toString();
    }
    @Override
    protected void onPostExecute(Void result) {
        new MagicLockDeleteCoverTask(
                MagicLockDeleteCoverTask.FLAG_DELETE_ALL).execute(mContext);
    }
}
