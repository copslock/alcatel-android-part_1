package com.tct.magiclock.utils;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.tct.magiclock.R;

/**
 * The class {@code Utils} is a switch bar util class for WallShuffle
 * @author guoqiang.qiu@tcl.com
 * @since  n8996, Solution-2699694
 */

public class SwitchBarHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private View mSwitchBar;
    private Switch mSwitch;
    private TextView mText;
    private OnCheckedChangeListener mListener;
    private boolean mChecked;

    public SwitchBarHolder(View view) {
        mSwitchBar = view;
        mSwitch = (Switch) mSwitchBar.findViewById(android.R.id.switch_widget);
        mText = (TextView) mSwitchBar.findViewById(R.id.switch_text);
        mSwitchBar.setOnClickListener(this);
        mSwitch.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        setChecked(!mChecked);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        setChecked(b);
    }

    public void setChecked(boolean checked) {
        if (mChecked != checked) {
            mChecked = checked;
            mText.setText(mChecked ? R.string.switch_status_on : R.string.switch_status_off);
            mSwitch.setChecked(mChecked);
            if (mListener != null) {
                mListener.onCheckedChanged(mChecked);
            }
        }
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mListener = listener;
    }

    public interface OnCheckedChangeListener {
        void onCheckedChanged(boolean status);
    }
}