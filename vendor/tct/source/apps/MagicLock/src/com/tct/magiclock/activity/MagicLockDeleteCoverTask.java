package com.tct.magiclock.activity;

import java.util.ArrayList;
import java.util.Arrays;

import com.tct.magiclock.Constants;
import com.tct.magiclock.db.MagiclockProvider;
import com.tct.magiclock.db.MagiclockProvider.Category;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

/**
 * 
 * @author xiaohui.niu@tcl.com 2015/08/26
 *
 */


public class MagicLockDeleteCoverTask  extends AsyncTask<Context, Void, Void> {
    private static final String TAG = "DeleteCover";
    private Handler mHandler;
    private int mDeleteMode = 0;
    public static final int FLAG_DELETE_ALL = 1;
    public static final int FLAG_DELETE_UNSELECTED = 2;

   
    public MagicLockDeleteCoverTask(){
        
    }
    
    public MagicLockDeleteCoverTask(int deleteFlag){
        mDeleteMode = deleteFlag;
    }
    
    public MagicLockDeleteCoverTask(Handler handler){
        mHandler = handler;
    }
    @Override
    protected Void doInBackground(Context... params) {
        deleteCategoryCovers(params[0]);
        return null;
    }
    
    /**
     * Delete categories' covers
     */
    private void deleteCategoryCovers(Context mContext) {
        Cursor cursor = null;
        try {

            ContentResolver resolver = mContext.getContentResolver();
            Uri uri = MagiclockProvider.Category.CONTENT_URI;
            String selection = null;
            if(FLAG_DELETE_UNSELECTED == mDeleteMode){
                selection = MagiclockProvider.Category.SELECTED + "="
                        + MagiclockProvider.Category.FLAG_NOT_SELECTED;
                Log.d(TAG, "Delete unselected.");
            }else if(FLAG_DELETE_ALL == mDeleteMode){
                Log.d(TAG, "Delete all.");
            }
            String[] projection = new String[] {
                    MagiclockProvider.Category.CAT,
                    MagiclockProvider.Category.COVER };
            cursor = resolver.query(uri, projection, selection, null, null);
            if (null == cursor || cursor.getCount() <= 0) {
                return;
            }
            final int tagIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.CAT);
            final int coverIndex = cursor
                    .getColumnIndexOrThrow(MagiclockProvider.Category.COVER);

            ArrayList<String> catList = new ArrayList<String>();
            while (cursor.moveToNext()) {
                String title = cursor.getString(tagIndex);
                /** MyPic is not a real category */
                if (Constants.CAT_MYPIC.equals(title)
                        || Constants.CAT_FAVORITE.equals(title)) {
                    Log.d(TAG, "bypass tag: " + title);
                    continue;
                }
                if (null != cursor.getBlob(coverIndex)) {
                    catList.add(title);                    
                }
            }
            
            if(null == catList || catList.size() <=0){
                Log.d(TAG, "[deleteCategoryCovers] no need.");
                return;
            }
            Log.d(TAG, "[deleteCategoryCovers] cat:" + catList.toString());

            ContentValues values = new ContentValues();
            values.putNull(Category.COVER);

            int count = mContext.getContentResolver().update(
                    Category.CONTENT_URI,
                    values,
                    getQuestionMarkSelection(catList.size(), Category.CAT
                            + " IN("),
                    (String[]) catList.toArray(new String[catList.size()]));
            if (count > 0) {
//                Intent i = new Intent(
//                        MagicLockCategoryActivity.ACTION_UPDATE_COVER);
//                i.putStringArrayListExtra(Category.CAT, catList);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(i);
            }

        } catch (Exception e) {
            Log.e(TAG, "[deleteCovers] Exception: " + e, e);
        } finally {
            if (null != cursor) {
                cursor.close();
                cursor = null;
            }
        }
    }
    
    private String getQuestionMarkSelection(final int num, String tag) {
        final StringBuilder whereBuilder = new StringBuilder();
        final String[] questionMarks = new String[num];
        Arrays.fill(questionMarks, "?");
        whereBuilder.append(tag).
                append(TextUtils.join(",", questionMarks)).
                append(")");
        return whereBuilder.toString();
    }
    
   
}
