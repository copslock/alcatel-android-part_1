LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := VLife
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := vlife.apk
LOCAL_MODULE_CLASS := APPS
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_PRIVILEGED_MODULE := false
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libvlife_media
LOCAL_SRC_FILES := lib64/libvlife_media.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib64
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libvlife_openglutil
LOCAL_SRC_FILES := lib64/libvlife_openglutil.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib64
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libvlife_render
LOCAL_SRC_FILES := lib64/libvlife_render.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_PATH := $(TARGET_OUT)/lib64
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)
