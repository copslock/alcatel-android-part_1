/* Copyright (C) 2016 Tcl Corporation Limited */
/*
 * Copyright (C) 2007-2008 Esmertec AG.
 * Copyright (C) 2007-2008 The Android Open Source Project
 * Copyright (C) 2010-2012, The Linux Foundation. All rights reserved.
 * Not a Contribution, Apache license notifications and license are retained
 * for attribution purposes only
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/******************************************************************************/
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 11/12/2013|       ji.li          |        FR-551269     |auto register for cucc */
/* ----------|----------------------|----------------------|----------------- */
/* 01/29/2016|     Dandan.Fang      |      PR1530879       |[Telecom][CHINA]- */
/*           |                      |                      |SIXM-04001 [Mand  */
/*           |                      |                      |atory]auto register*/
/*           |                      |                      |function          */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.android.tct.autoregister;

import static android.content.Intent.ACTION_BOOT_COMPLETED;
import static android.provider.Telephony.Sms.Intents.SMS_RECEIVED_ACTION;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.zip.CRC32;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SqliteWrapper;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Telephony.Sms;
import android.provider.Telephony.Sms.Inbox;
import android.provider.Telephony.Sms.Intents;
import android.provider.Telephony.Sms.Outbox;
//import android.telephony.MSimSmsManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.VoLteServiceState;
import android.text.TextUtils;
import android.util.Log;
import android.util.Slog;
import android.util.TctLog;
import android.widget.Toast;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.android.internal.telephony.uicc.IccUtils;
import com.google.android.mms.MmsException;

//[FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
import com.qualcomm.qcrilhook.IQcRilHook;
import com.qualcomm.qcrilhook.QcRilHook;

import android.os.AsyncResult;

//[FEATURE]-Add-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom

//[FEATURE]-Add-BEGIN by TCTNB.(ji.li),01/11/2014, FR584793
import android.provider.Settings;
//[FEATURE]-Add-END by TCTNB.(ji.li),01/11/2014, FR584793

//[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,12/21/2015,Task1177186,
//SMS and data auto-registeration features for China Telecom
import android.telephony.SubscriptionInfo;
//[BUGFIX]-Add-END by TCTNB.yongzhen.wang
import android.provider.Settings; // MODIFIED by qili.zhang, 2016-05-07,BUG-2094734


/**
 * This service essentially plays the role of a "worker thread", allowing us to
 * store incoming messages to the database, update notifications, etc. without
 * blocking the main thread that SmsRegisterReceiver runs on.
 */
public class SmsRegisterService extends Service {
    private static final String TAG = "SmsRegisterService";

    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;
    private boolean mSending;

    public static final String MESSAGE_SENT_ACTION = "com.android.mms.transaction.MESSAGE_SENT";

    // Indicates next message can be picked up and sent out.
    public static final String EXTRA_MESSAGE_SENT_SEND_NEXT = "SendNextMsg";

    private int mResultCode;

    static final String SHARED_PREFS_REGISTER = "com.android.autoregister_preferences_register";
    static final String OPERATOR_id = "com.android.tct.autoregister.uri";

    private static final String AUTO_REGISTER_RETRY_ALARM_ACTION = "com.android.tct.autoregister.AUTO_REGISTER_RETRY_ALARM";
    private Uri mOPERATORUri = Uri.parse(OPERATOR_id);
    private static int mRetryCount = 0;//[FEATURE]-Mod by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
    private SharedPreferences mSharePreference = null;
    private static boolean SENDER_OPENED = true;
    private String CTCC_SERVER_NUMBER = "10655459";
    private static TelephonyManager mTelephonyManager = null;
    private static PhoneStateListener mMSimPhoneStateListener = null;

    private static final short unicom_port = 0x6838;

    private final int CMCC_TYPE = 0x0001;// China mobile 1
    private final int CUCC_TYPE = 0x0002;// China unicom 2
    private final int CTCC_TYPE = 0x0003;// China telecom 3

    private final String CARRIER_MOBILE = "46000";
    private final String CARRIER_UNICOM = "46001";
    private final String CARRIER_TELECOM = "46003";
    //[FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
    private final String CARRIER_TELECOM_EX = "46011";
    private final String CARRIER_TELECOM_EX2 = "46005";
    private final String CARRIER_TELECOM_EX3 = "46009";

  //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
  //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private static int subIdOfCdmaService = SubscriptionManager.INVALID_SUBSCRIPTION_ID;
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    QcRilHook mQcRilOemHook = null;
    private static final byte CT_SUCCESS_PROTOCOLVERSION = 1;
    private static final byte SUCCESS_COMMANDTYPE = 4;
    private static final int TELESERVICE_REGISTER = 0xfded;
    private static boolean SWITCH_MOBILE = false;

    /* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094734*/
    public static final String CONFIG_CURRENT_CDMA_IMSI= "config_current_cdma_imsi";
    public static final String CONFIG_CURRENT_CDMA_PHONEID= "config_current_cdma_phoneid";
    // check whether card has changed
    boolean isSwitchCard(Context context) {
        if (subIdOfCdmaService != SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
            //String cardImsi = mTelephonyManager.getSubscriberId(subIdOfCdmaService);
            String cardImsi=getSimIMSI();
            /* MODIFIED-END by qili.zhang,BUG-2094734*/
            SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_REGISTER,
                    Context.MODE_PRIVATE);
            String imsiFromStorage = sp.getString("IMSI", "");
            Log.e(TAG, "OPERATOR isSwitchCard cardImsi=" + cardImsi + " imsiFromStorage="  + imsiFromStorage);
            if (!imsiFromStorage.equals(cardImsi))
                return true;
        }
        // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto
        // register telecom
        Log.d(TAG, "isSwitchCard mQcRilOemHook=" + mQcRilOemHook);
        if (mQcRilOemHook != null) {
            //[BUGFIX]-Add-BEGIN by TCTNB.yongzhen.wang,12/21/2015,Task1177186,
            //SMS and data auto-registeration features for China Telecom
            AsyncResult result = mQcRilOemHook
                    .sendQcRilHookMsg(IQcRilHook.QCRIL_EVT_HOOK_OEM_FETCH_RUIM_CARD_CHANGE_FLAG);
            //[BUGFIX]-Add-END by TCTNB.yongzhen.wang
            Log.e(TAG, "AsyncResult ex=" + result.exception);
            if (result.exception == null) {
               byte[] data = (byte[]) result.result;
               Log.e(TAG, "AsyncResult result----=" + IccUtils.bytesToHexString(data));
               if (data[0] == 0x01) {
                  Log.e(TAG, "AsyncResult 111ex=" + result.exception);
                  return true;
               } else {
                  Log.e(TAG, "AsyncResult000 ex=" + result.exception);
               }
            }
        } else {
            return SWITCH_MOBILE;
        }
        // [FEATURE]-Add-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto
        // register telecom

        return false;
    }

    private boolean hasRegistered() {
        String register_status = mSharePreference.getString("REGISTER", "0");
        Log.e(TAG, "hasRegistered register_status=" + register_status);// [FEATURE]-Add
                                                                          // by
                                                                          // TCTNB.(Ji.Li),08/27/2013,
                                                                          // FR-503474,auto
                                                                          // register
                                                                          // OPERATOR
        return register_status.equals("1");
    }

    // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
    private void responseRegistMessage(byte[] pdu) {
        if (checkRegistMessage(pdu)) {
            // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),04/26/2014,PR-603958,regisger success flag
            Settings.Global.putString(this.getContentResolver(),"settings_auto_register_feedback","Got feedback");
            // [FEATURE]-Add-END by TCTNB.(Ji.Li),04/26/2014,PR-603958,regisger success flag
            Log.i(TAG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );
            Log.i(TAG, "|Telecom register success on imsi: " + getSimIMSI()+"|");
            Log.i(TAG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" );
            SharedPreferences.Editor editor = mSharePreference.edit();
            editor.putString("REGISTER", "1");
            editor.commit();// [BUGFIX]-Mod by TCTNB.(Ji.Li),08/21/2013,
                            // FR-509650,auto register telecom
            onDestroy();
        }else{
            //service response with error message, retry again.
            if (getOperatorType() == CTCC_TYPE) {
                Log.e(TAG, "------ handleSmsSent fail: mRetryCount=" + mRetryCount);
                if(mRetryCount < 3 && false == hasRegistered()){
                    cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 2);

                    // Set the timeout alarm,once the operation time out,just launch the retry operation.
                    long alarmTimer = 2 * 60 * 1000 + SystemClock.elapsedRealtime();
                    setAlarm(this, alarmTimer, AUTO_REGISTER_RETRY_ALARM_ACTION, 2);
                }else{
                    Log.i(TAG, "Retry times morn than 3, or registered successful ,then stop autoRegisterService");
                    onDestroy();
                }
            }
        }

    }

    private boolean checkRegistMessage(byte[] pdu) {
        // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),08/01/2013, FR-492997,auto
        // register telecom
        if (pdu == null)
            return false;
        // [FEATURE]-Add-END by TCTNB.(Ji.Li),08/01/2013, FR-492997,auto register telecom
        ByteArrayInputStream input = new ByteArrayInputStream(pdu);
        DataInputStream dis = new DataInputStream(input);

        try {
            byte protocolVersion = dis.readByte();
            Log.e(TAG, "Telecom protocolVersion : " + protocolVersion);

            byte commandType = dis.readByte();
            Log.e(TAG, "Telecom commandType : " + commandType);

            if (isChinaOPERATOR()) {
                if (CT_SUCCESS_PROTOCOLVERSION == protocolVersion
                        && SUCCESS_COMMANDTYPE == commandType) {
                    // [DEBUG]-Modify-BEGIN by TCTNB Jianze.Dai for
                    // PR-515468,auto register telecom 08/28/2013
                    // The lastest register operation finished normally,just
                    // cancel the timeout alarm.
                    cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 0);
                    cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 1);
                    // [DEBUG]-Modify-END by TCTNB Jianze.Dai for PR-515468,auto
                    // register telecom 08/28/2013
                    //[FEATURE]-Add-BEGIN by TCTNB.(ji.li),01/11/2014, FR584793
                    cancelAlarm(this,AUTO_REGISTER_RETRY_ALARM_ACTION,2);
                    //[FEATURE]-Add-END by TCTNB.(ji.li),01/11/2014, FR584793
                    return true;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public byte[] getRegistMessageInfo() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(output);
        int i = 0;
        byte message[] = getAllMessages();
        final CRC32 crc = new CRC32();
        crc.update(message);
        String crcStr = Long.toHexString(crc.getValue());
        char crcChars[] = crcStr.toCharArray();
        int crcLengh = crcChars.length;
        try {
            dos.write(message);
            while (i < 8) {
                int temp = i - (8 - crcLengh);
                if (temp >= 0) {
                    dos.writeByte((int) crcChars[temp]);
                } else {
                    // [BUGFIX]-Mod-BEGIN by TCTNB.(Ji.Li),04/09/2014,640931
                    dos.writeByte(0x30);
                    // [BUGFIX]-Mod-END by TCTNB.(Ji.Li),04/09/2014,640931
                }
                i++;
            }

            return output.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (dos != null) {
                    dos.close();
                    dos = null;
                }
                if (output != null) {
                    output.close();
                    output = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getPhoneMEID() {

        // [BUGFIX]-Mod-BEGIN by TCTNB.(Ji.Li),02/24/2014, FR-605841,auto register telecom
        String phone_meid = "0000";
        phone_meid = Settings.System.getString(SmsRegisterService.this.getContentResolver(),"phone_meid");
        Log.i(TAG, "phone_meid="+phone_meid);
        return phone_meid;
        // [BUGFIX]-Mod-END by TCTNB.(Ji.Li),02/24/2014, FR-605841,auto register telecom
    }

    private String getPhoneIMEI() {
        TelephonyManager tm = TelephonyManager.getDefault();
        return tm.getDeviceId();
    }


    private String getSimIMSI() {
        // read from sim card
/* MODIFIED-BEGIN by qili.zhang, 2016-05-07,BUG-2094734*/
//        if (subIdOfCdmaService != SubscriptionManager.INVALID_SUBSCRIPTION_ID){
//            return mTelephonyManager.getSubscriberId(subIdOfCdmaService);
//        } else return "0";

        if (subIdOfCdmaService != SubscriptionManager.INVALID_SUBSCRIPTION_ID){

             String mImsi=null;
             mImsi=Settings.Global.getString( getApplicationContext().getContentResolver(),CONFIG_CURRENT_CDMA_IMSI);
             int PhoneId=Settings.Global.getInt( getApplicationContext().getContentResolver(),CONFIG_CURRENT_CDMA_PHONEID,-1);
             int PhoneIdOfCdmaService=SubscriptionManager.getPhoneId(subIdOfCdmaService);
             Log.i(TAG," SMSRegister: imsi "+mImsi +" phoneid " +PhoneId+"PhoneIdOfCdmaService  "+SubscriptionManager.getPhoneId(subIdOfCdmaService) );
             if(PhoneId!=-1&&PhoneId==PhoneIdOfCdmaService){
                  return mImsi;
             }else{
                  return "0";
             }
         } else return "0";
         /* MODIFIED-END by qili.zhang,BUG-2094734*/

    }

    private String getSoftwareVersion() {
        // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,autoregister telecom
        // register OPERATOR
        int operator_type = getOperatorType();
        String message = null;
        switch (operator_type) {
            case CUCC_TYPE:
                return Build.VERSION.INCREMENTAL;
            case CMCC_TYPE:
                message = null;
                break;
            case CTCC_TYPE:
                 //[BUGFIX]-ADD-Begin by qili.zhang for defect 1664041 03/07/2015
                 //[Fix the version from SystemProperties ,not need get it from SDM:def_autoregister_version_fixed]
                 /*
                String version = getApplicationContext().getResources().getString(
                        R.string.def_autoregister_version_fixed);//SystemProperties.get("ro.tct.boot.ver","1C33");
                //[BUGFIX]-ADD-Begin by qili.zhang for defect 1664041 03/07/2015
                */
                /* MODIFIED-BEGIN by qili.zhang, 2016-05-10,BUG-2094734*/
                String version = SystemProperties.get("ro.def.ct.software.version");
                //String version=SystemProperties.get("ro.tct.boot.ver").substring(1, 5);
                /* MODIFIED-END by qili.zhang,BUG-2094734*/
                //[BUGFIX]-ADD-END by qili.zhang
                Log.i(TAG, "CTCC_TYPE getSoftwareVersion version="+version);
//                if (version.length()>=5) {
//                    version = version.substring(1,5);
//                }
                return version;
            default:
                message = null;
        }
        return message;
        // [FEATURE]-Add-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
        // register OPERATOR
    }

    private String translateOperatorType(int type) {
        switch (type) {
            case CUCC_TYPE:
                return "CUCC_TYPE";
            case CMCC_TYPE:
                return "CMCC_TYPE";
            case CTCC_TYPE:
                return "CTCC_TYPE";
            default:
                return "Unknown error code";
        }
    }
//[FEATURE]-Mod-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
    private byte[] getAllMessages() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(output);
        String productname = "TCL-i806";
        if (!SystemProperties.get("ro.product.model").startsWith("ONE TOUCH")) {
            productname = SystemProperties.get("ro.product.model").replace(' ', '-');
        }
        try {
            String message = null;
            int operator_type = getOperatorType();
            switch (operator_type) {
                case CUCC_TYPE:
                    message = "IMEI:" + getPhoneIMEI() + '/' + getSimIMSI() + '/' + productname
                            + '/' + getSoftwareVersion();
                    byte data[] = message.getBytes();
                    dos.write(data);
                    Log.e(TAG, " OperatorType= " + translateOperatorType(operator_type));
                    Log.e(TAG, "---OPERATOR getAllMessages message=" + message);

                    Log.e(TAG, "data Length : " + data.length);
                    break;
                case CMCC_TYPE:
                    message = null;
                    break;
                case CTCC_TYPE:
                    dos.writeByte(2);
                    dos.writeByte(3);
                    message = "<a1><b1>" + productname + "</b1>" + // device
                                                                   // model
                            "<b2>" + getPhoneMEID() + "</b2>" + // MEID
                            "<b3>" + getSimIMSI() + "</b3>" + // IMSI
                            "<b4>" + getSoftwareVersion() + "</b4>" +

                            "</a1>";
                    Log.e(TAG, "---Telecom getAllMessages message=" + message);
                    byte data1[] = message.getBytes();
                    dos.writeByte(data1.length);
                    dos.writeByte(0);
                    dos.write(data1);
                    Log.e(TAG, "data Length : " + data1.length);
                    break;
                default:
                    message = null;
            }
//[FEATURE]-Mod-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
            return output.toByteArray();
        } catch (Exception e) {
            return null;
        } finally {
            try {
                if (dos != null) {
                    dos.close();
                    dos = null;
                }
                if (output != null) {
                    output.close();
                    output = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private int getOperatorType() {
        int operator_type = getApplicationContext().getResources().getInteger(
                com.android.internal.R.integer.def_sms_autoregister_operator);
        if ((operator_type >= 0) && (operator_type <= 3))
            return operator_type;
        return 0;
    }

    // check whether it's a carrier need auto registration message.
    boolean isChinaOPERATOR() {
        String numeric;
        try {
            numeric = null;

             Log.d(TAG, "subIdOfCdmaService="+subIdOfCdmaService);
             if (subIdOfCdmaService != SubscriptionManager.INVALID_SUBSCRIPTION_ID){
                numeric= mTelephonyManager.getSimOperator(subIdOfCdmaService);
             }else{
                return false;
             }

            Log.d(TAG, "uim carrier: " + numeric + " getOperatorType=" + getOperatorType());
            if ((CARRIER_UNICOM.equals(numeric)) && (getOperatorType() == CUCC_TYPE)) {
                return true;
            }
            if ((CARRIER_MOBILE.equals(numeric)) && (getOperatorType() == CMCC_TYPE)) {
                return true;
            }
            //[FEATURE]-Mod-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
            if ((numeric != null) && (numeric.equals(CARRIER_TELECOM)
                    || numeric.equals(CARRIER_TELECOM_EX)
                    || numeric.equals(CARRIER_TELECOM_EX2)
                    || numeric.equals(CARRIER_TELECOM_EX3)
                    || numeric.startsWith("455"))
                    && (getOperatorType() == CTCC_TYPE)) {
                Log.i(TAG, "CTCC Uim card success!");
                return true;
            } else {
                if (null == numeric || numeric.startsWith("0000")) {
                    // [FEATURE]-Mod-BEGIN by TCTNB.(Ji.Li),08/27/2013,
                    // FR-503474,auto register OPERATOR
                    // Cancel the past re-check alarm at first.
                    cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 1);

                    // Set the alarm to re-check the MCC MNC after 1 minutes
                    long alarmTimer = 1 * 60 * 1000 + SystemClock.elapsedRealtime();
                    if (getOperatorType() != 0) {
                        setAlarm(this, alarmTimer, AUTO_REGISTER_RETRY_ALARM_ACTION, 1);
                    }
                }
                // [FEATURE]-Mod-END by TCTNB.(Ji.Li),08/27/2013, FR-503474,auto
                // register OPERATOR
                Log.i(TAG, "Not correct sim card return");
                return false;
            }
            //[FEATURE]-Mod-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
        } catch (Exception e) {
            Log.d(TAG, "carrier:error ");
            return false;
        }
    }

    private boolean isFactoryMode() {
        return SystemProperties.get("dev.tct.MMITest").equalsIgnoreCase("true");
    }

    private int getRetryCount() {
        String register_status = mSharePreference.getString("RETRY_COUNT", "0");
        return Integer.parseInt(register_status);
    }

    private void setRetryCount(int count) {
        SharedPreferences.Editor editor = mSharePreference.edit();
        editor.putString("RETRY_COUNT", "" + count);
        editor.commit();
    }

    private void setRegFlagAfterSmsSentSuccessful() {
        SharedPreferences.Editor editor = mSharePreference.edit();
        editor.putString("REGISTER", "0");// [FEATURE]-Mod by
                                          // TCTNB.(Ji.Li),08/27/2013,
                                          // FR-503474,auto register OPERATOR
        editor.putString("IMSI", getSimIMSI());
        editor.commit();// [FEATURE]-Mod by TCTNB.(Ji.Li),08/27/2013,
                        // FR-503474,auto register OPERATOR
    }
//[FEATURE]-Mod-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
    private void sendOPERATORRegister(Context context) throws Exception {
        Log.e(TAG, "---sendOPERATORRegister getOperatorType()=" + getOperatorType());

        if (getOperatorType() == 0)
            return;
        byte[] data = null;

        String[] destAddr = null;
        short operator_port = 0x0000;
        String scAddr = null;
        if (isChinaOPERATOR()) {
            if (getOperatorType() == CUCC_TYPE) {
                data = getAllMessages();

                if (data == null) {
                    // Don't try to send an empty message.
                    throw new Exception("Null message body or dest.");
                }
                operator_port = unicom_port;
                if (!getApplicationContext().getResources().getBoolean(
                        R.bool.def_autoregister_unicom_address_flag)) {
                    destAddr = new String[] {
                        "10655464",
                    };
                } else {
                    destAddr = new String[] {
                        "10655459",
                    };
                }
            } else if (getOperatorType() == CMCC_TYPE) {
                // todo
                operator_port = 0x0000;
            } else if (getOperatorType() == CTCC_TYPE) {
                data = getRegistMessageInfo();

                if (data == null) {
                    // Don't try to send an empty message.
                    throw new Exception("Null message body or dest.");
                }
                destAddr = new String[] {
                    "10659401",
                };
                operator_port = 0x0000;
            } else
                return;
        } else {
            return;
        }
//[FEATURE]-Mod-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
        Log.e(TAG, "phone:" + destAddr[0] + " ; SC=" + scAddr + " operator_port="
                + operator_port);
        Log.e(TAG, "destAddr[0] =" + destAddr[0]);

        SmsManager smsManager = SmsManager.getDefault();
        Log.i(TAG, "subIdOfCdmaService="+subIdOfCdmaService);
        if (subIdOfCdmaService != SubscriptionManager.INVALID_SUBSCRIPTION_ID) {
             smsManager = SmsManager.getSmsManagerForSubscriptionId(subIdOfCdmaService);
        }
        //[BUGFIX]-Add-END by TCTNB.yongzhen.wang

        for (int i = 0; i < destAddr.length; i++) {

            Intent intent = new Intent(SmsRegisterService.MESSAGE_SENT_ACTION, mOPERATORUri,
                    context, SmsRegisterReceiver.class);

            PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

            try {
                // [BUGFIX]-Add-BEGIN by TCTNB.(Ji.Li),04/09/2014,640931
                Log.e(TAG, "data = "+IccUtils.bytesToHexString(data));
                // [BUGFIX]-Add-END by TCTNB.(Ji.Li),04/09/2014,640931
                // check on http://114.247.0.117:7001/aidmcuweb/
                // account:18606872872
                // pw:dm@123
                smsManager.sendDataMessage(destAddr[i], scAddr, operator_port, data, sentIntent,
                        null);
                // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),04/26/2014,PR-603958,regisger success flag
                Settings.Global.putLong(this.getContentResolver(),"settings_auto_register_time",System.currentTimeMillis());
                Settings.Global.putString(this.getContentResolver(),"settings_auto_register_ack","Unsuccessful");
                Settings.Global.putString(this.getContentResolver(),"settings_auto_register_feedback","Not Got Feedback");
            } catch (Exception ex) {
                throw new Exception("sendRegisterMessage: caught " + ex + " sendDataMessage");
            }
        }

    }

    /**
     * set alarm
     *
     * @param: Context context: Service context long time: Alarm setting String
     *         action:The action string when the alarm reached int reqcode: 0:
     *         The last register operation timeout; 1: The last register
     *         operation didn't get the right MNC MCC
     * @return: none
     */
    private static void setAlarm(Context context, long time, String action, int reqCode) {
        Log.e(TAG, "setAlarm() action = " + action + ", reqCode = " + reqCode);

        PendingIntent pi;
        AlarmManager am;
        Intent intentAlarm = new Intent(action);
        intentAlarm.putExtra("messageID", reqCode);
        pi = PendingIntent.getBroadcast(context, reqCode, intentAlarm,
                PendingIntent.FLAG_CANCEL_CURRENT);
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, time, pi);
    }

    /**
     * cancel alarm
     *
     * @param: Context context:Service context String action:The action string
     *         when the alarm reached int reqcode:0: The last register operation
     *         timeout; 1: The last register operation didn't get the right MNC
     *         MCC
     * @return: none
     */
    private static void cancelAlarm(Context context, String action, int reqCode) {
        TctLog.e(TAG, "cancelAlarm() action = " + action + ", reqCode = " + reqCode);

        PendingIntent pi;
        AlarmManager am;
        pi = PendingIntent.getBroadcast(context, reqCode, new Intent(action),
                PendingIntent.FLAG_CANCEL_CURRENT);
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pi);
    }

    private static String translateResultCode(int resultCode) {
        switch (resultCode) {
            case Activity.RESULT_OK:
                return "Activity.RESULT_OK";
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                return "SmsManager.RESULT_ERROR_GENERIC_FAILURE";
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                return "SmsManager.RESULT_ERROR_RADIO_OFF";
            case SmsManager.RESULT_ERROR_NULL_PDU:
                return "SmsManager.RESULT_ERROR_NULL_PDU";
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                return "SmsManager.RESULT_ERROR_NO_SERVICE";
            case SmsManager.RESULT_ERROR_LIMIT_EXCEEDED:
                return "SmsManager.RESULT_ERROR_LIMIT_EXCEEDED";
            case SmsManager.RESULT_ERROR_FDN_CHECK_FAILURE:
                return "SmsManager.RESULT_ERROR_FDN_CHECK_FAILURE";
            default:
                return "Unknown error code";
        }
    }

    // [FEATURE]-Mod-END by TCTNB.(Ji.Li),08/29/2013, FR-503474,auto register
    // OPERATOR

    @Override
    public void onCreate() {

        // Start up the thread running the service. Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.
        HandlerThread thread = new HandlerThread(TAG, Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();
        // [FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),08/12/2013, FR-503474,auto
        // register OPERATOR
        Log.e(TAG, "--- SmsRegisterService onCreate mSending=" + mSending);
        mSharePreference = this.getSharedPreferences(SHARED_PREFS_REGISTER, Context.MODE_PRIVATE);
        mTelephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        // [FEATURE]-Add-END by TCTNB.(Ji.Li),08/12/2013, FR-503474,auto
        // register OPERATOR
        try {
            if (mQcRilOemHook == null) {
                mQcRilOemHook = new QcRilHook(this, null);// [FEATURE]-Add by
                                                          // TCTNB.(Ji.Li),12/18/2013,
                                                          // FR-557607 ,auto
                                                          // register telecom
            }
        } catch (Exception e) {

        }
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Temporarily removed for this duplicate message track down.

        mResultCode = intent != null ? intent.getIntExtra("result", 0) : 0;

        if (mResultCode != 0) {
            Log.e(TAG, "onStart: #" + startId + " mResultCode: " + mResultCode + " = "
                    + translateResultCode(mResultCode));
        }

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.obj = intent;
        mServiceHandler.sendMessage(msg);
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
        SmsRegisterReceiver.noAllowRegistered();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        /**
         * Handle incoming transaction requests. The incoming requests are
         * initiated by the MMSC Server or by the MMS Client itself.
         */
        @Override
        public void handleMessage(Message msg) {
            int serviceId = msg.arg1;
            Intent intent = (Intent) msg.obj;

            TctLog.e(TAG, "handleMessage serviceId: " + serviceId + " intent: " + intent);

            if (intent != null) {
                String action = intent.getAction();

                int error = intent.getIntExtra("errorCode", 0);

                Log.e(TAG, "handleMessage action: " + action + " error: " + error);

                if (MESSAGE_SENT_ACTION.equals(intent.getAction())) {
                    handleSmsSent(intent, error);
                } else if ("android.provider.Telephony.SMS_REGISTER_RECEIVED".equals(action)) {//[FEATURE]-Mod by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
                    handleSmsReceived(intent, error);
                } else if (TelephonyIntents.ACTION_SERVICE_STATE_CHANGED.equals(action)) {
                    //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
                    //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
                    subIdOfCdmaService = intent.getIntExtra("subId_cdma_in_service", SubscriptionManager.INVALID_SUBSCRIPTION_ID);

                    Log.i(TAG,"Do auto cdma register in sub=: " + subIdOfCdmaService);
                    handleCdmaAutoRegister();
                    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
                } else if (AUTO_REGISTER_RETRY_ALARM_ACTION.equals(action)) {
                        try {
                            mRetryCount = mRetryCount + 1;
                            sendOPERATORRegister(SmsRegisterService.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
                // [FEATURE]-Add-END by TCTNB.(Ji.Li),08/27/2013, FR-503474,auto
                // register OPERATOR
            }
        }
    }

  //[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
  //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
    private void handleCdmaAutoRegister(){
        if (SENDER_OPENED) {
            SENDER_OPENED = false;
            Log.d(TAG, "handleCdmaAutoRegister -- isSwitchCard(SmsRegisterService.this)= " + isSwitchCard(SmsRegisterService.this));
            if ((!hasRegistered()) || (isSwitchCard(SmsRegisterService.this))) {
                if (isFactoryMode()) {
                    return;
                }
                try {
                    mRetryCount = 0;
                    sendOPERATORRegister(SmsRegisterService.this);
                } catch (Exception e) {
                        e.printStackTrace();
                }
            }else{
                onDestroy();
            }
        }
    }
    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang

    private void handleSmsSent(Intent intent, int error) {
        Uri uri = intent.getData();
        mSending = false;
        boolean sendNextMsg = intent.getBooleanExtra(EXTRA_MESSAGE_SENT_SEND_NEXT, false);

        TctLog.e(TAG, "handleSmsSent uri: " + uri + " sendNextMsg: " + sendNextMsg
                + " mResultCode: " + mResultCode + " = " + translateResultCode(mResultCode)
                + " error: " + error);

        if (uri != null) {
            Log.e(TAG, "---OPERATOR handleSmsSent uri.toString()=" + uri.toString());
            if (uri.toString().equals(OPERATOR_id)) {
                Log.e(TAG, "------OPERATOR handleSmsSent mResultCode=" + mResultCode);
                if (mResultCode == Activity.RESULT_OK) {
                    // write the identifer read the imsi from uim and save to mobile
                    Settings.Global.putString(this.getContentResolver(),"settings_auto_register_ack","Successful");
                    if (getOperatorType() == CUCC_TYPE) {
                        Log.e(TAG, "------ handleSmsSent " + OPERATOR_id);
                        cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 0);
                        SharedPreferences.Editor editor = mSharePreference.edit();
                        editor.putString("REGISTER", "1");
                        editor.commit();
                    }//[BUGFIX]-Add-BEGIN by TCTNB.Dandan.Fang,01/29/2016,PR1530879,
                  //[Telecom][CHINA]SIXM-04001 [Mandatory] auto register function
                    else if(getOperatorType() == CTCC_TYPE){
                        setRegFlagAfterSmsSentSuccessful();
                    }
                    //[BUGFIX]-Add-END by TCTNB.Dandan.Fang
                } else {
                    if (getOperatorType() == CTCC_TYPE) {
                        Log.e(TAG, "------ handleSmsSent fail: mRetryCount=" + mRetryCount);
                        if(mRetryCount < 3 && false == hasRegistered()){
                            cancelAlarm(this, AUTO_REGISTER_RETRY_ALARM_ACTION, 2);

                            // Set the timeout alarm,once the operation time out,just launch the retry operation.
                            long alarmTimer = 2 * 60 * 1000 + SystemClock.elapsedRealtime();
                            setAlarm(this, alarmTimer, AUTO_REGISTER_RETRY_ALARM_ACTION, 2);
                        }else{
                            Log.i(TAG, "Retry times morn than 3, or registered successful ,then stop autoRegisterService");
                            onDestroy();
                        }
                    }
                }
                return;
            }
        }
    }

    private void handleSmsReceived(Intent intent, int error) {
        SmsMessage[] msgs = Intents.getMessagesFromIntent(intent);
        String format = intent.getStringExtra("format");
        //[FEATURE]-Add-BEGIN by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom
        TctLog.e(TAG, "--Telecom handleSmsReceived format=" + format);
        if ((format != null) && format.equals(SmsMessage.FORMAT_3GPP2)) {
            if (msgs.length == 1) {
                try {
                    if ((msgs[0] != null) && (msgs[0].mWrappedSmsMessage != null)
                            && (msgs[0].mWrappedSmsMessage.getPdu() != null)) {

                        ByteArrayInputStream bais = new ByteArrayInputStream(
                                msgs[0].mWrappedSmsMessage.getPdu());
                        DataInputStream dis = new DataInputStream(bais);
                        int type = dis.readInt();
                        int teleserviceid = dis.readInt();
                        Log.e(TAG, "--Telecom handleSmsReceived teleserviceid=" + teleserviceid);
                        if (teleserviceid == TELESERVICE_REGISTER) {
                            responseRegistMessage(msgs[0].mWrappedSmsMessage.getUserData());
                            dis.close();
                            return;
                        }
                        dis.close();
                    }
                } catch (IOException ex) {
                    throw new RuntimeException(
                            "createFromPdu: conversion from byte array to object failed: " + ex, ex);
                }

            }
        }
        //[FEATURE]-Add-END by TCTNB.(Ji.Li),12/18/2013, FR-557607 ,auto register telecom

    }
}
