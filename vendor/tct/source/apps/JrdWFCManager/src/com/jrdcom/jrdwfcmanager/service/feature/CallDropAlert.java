/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.os.Message;
import com.jrdcom.jrdwfcmanager.service.FeatureProcessHandler;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class CallDropAlert extends _FeatureImplementor {

    public CallDropAlert(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean skipWhenWFCDisabled() {
        return true;
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.IMPL_IMSOP_HO_FAIL
                || what == IMPL_MSG.IMPL_CHECK_CALL_DROP_ALERT
                || what == IMPL_MSG.IMPL_IMSOP_HO_SUCCESS
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        final SystemRFStatus systemRFStatus = mHolder.getSystemRFStatus();
        FeatureProcessHandler pHandler = mHolder.getProcessHandler();
        switch (what) {
            case IMPL_IMSOP_HO_FAIL:
                systemRFStatus.setOnCallDropPopupEnabled(true);
            case IMPL_CHECK_CALL_DROP_ALERT:
                if (systemRFStatus.canOnCallDropPopup()) {
                    systemRFStatus.increasePopupTimes();
                    notifyStateEvent(STATE_EVENT.CALL_DROP_ALERT);
                }
                break;
            case IMPL_IMSOP_HO_SUCCESS:
                pHandler.removeMessages(IMPL_MSG.IMPL_IMSOP_HO_FAIL);
                pHandler.removeMessages(IMPL_MSG.IMPL_CHECK_CALL_DROP_ALERT);
                systemRFStatus.setOnCallDropPopupEnabled(false);
                break;
        }
        return true;
    }
}