package com.jrdcom.jrdwfcmanager.service;

import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.state.CellPref;
import com.jrdcom.jrdwfcmanager.service.state.Disabled;
import com.jrdcom.jrdwfcmanager.service.state.Initial;
import com.jrdcom.jrdwfcmanager.service.state.InvalidSim;
import com.jrdcom.jrdwfcmanager.service.state.OnCall;
import com.jrdcom.jrdwfcmanager.service.state.PoorWifiSignal;
import com.jrdcom.jrdwfcmanager.service.state.RegError;
import com.jrdcom.jrdwfcmanager.service.state.Registered;
import com.jrdcom.jrdwfcmanager.service.state.Registering;
import com.jrdcom.jrdwfcmanager.service.state.WifiDisconnected;
import com.jrdcom.jrdwfcmanager.service.state.WifiOff;
import com.jrdcom.jrdwfcmanager.service.state._State;
import java.util.HashMap;

public class WFCStateProxy extends _State {
    private _State mPreState;
    private _State mCurrentState;
    private HashMap<Class<? extends _State>, _State> mStates;

    public WFCStateProxy(IComponentHolder holder) {
        super(holder);
        mStates = new HashMap<Class<? extends _State>, _State>();
        mStates.put(Initial.class, new Initial(holder));
        mStates.put(Disabled.class, new Disabled(holder));
        mStates.put(Registering.class, new Registering(holder));
        mStates.put(RegError.class, new RegError(holder));
        mStates.put(CellPref.class, new CellPref(holder));
        mStates.put(Registered.class, new Registered(holder));
        mStates.put(InvalidSim.class, new InvalidSim(holder));
        mStates.put(WifiOff.class, new WifiOff(holder));
        mStates.put(WifiDisconnected.class, new WifiDisconnected(holder));
        mStates.put(OnCall.class, new OnCall(holder));
        mStates.put(PoorWifiSignal.class, new PoorWifiSignal(holder));
    }

    public _State getCurrentState() {
        return mCurrentState;
    }

    public _State getPreState() {
        return mPreState;
    }

    public _State obtainState(Class<? extends _State> clazz) {
        _State state = mStates.get(clazz);
        if (state == null) {
            throw new RuntimeException("Invalid State in mStates HashMap -- " + clazz);
        }
        return state;
    }

    public void onStateEvent(EventData eData) {
        if (eData == null) {
            throw new RuntimeException("WFCStateProxy onStateEvent, eData is null!");
        }

        if (mCurrentState == null && eData.event != STATE_EVENT.INITIAL) {
            Logger.loge("WFCStateProxy onStateEvent return, mCurrentState is null, event: " + eData.event);
            return;
        }

        STATE_EVENT event = eData.event;
        _State nextState = null;
        switch (eData.event) {
            case INITIAL:
                nextState = obtainState(Initial.class);
                mPreState = mCurrentState = nextState;
                break;
            case LOCALE_CHANGED:
                nextState = onSystemLocaleChanged(eData);
                break;
            case CALL_DROP_ALERT:
                nextState = onCallDropAlert(eData);
                break;
            case ON_CALL:
                nextState = onOnCall(eData);
                break;
            case OFF_CALL:
                nextState = onOffCall(eData);
                break;
            case WIFI_ON:
                nextState = onWifiOn(eData);
                break;
            case WIFI_OFF:
                nextState = onWifiOff(eData);
                break;
            case WIFI_CONNECTED:
                nextState = onWifiConnected(eData);
                break;
            case WIFI_DISCONNECTED:
                nextState = onWifiDisconnected(eData);
                break;
            case POOR_WIFI_SIGNAL:
                nextState = onPoorWifiSignal(eData);
                break;
            case AIRPLANE_ON:
                nextState = onAirplaneOn(eData);
                break;
            case AIRPLANE_OFF:
                nextState = onAirplaneOff(eData);
                break;
            case AUTO_CHECK:
            case REGISTERING:
                nextState = onRegistering(eData);
                break;
            case REGISTERED:
                nextState = onRegistered(eData);
                break;
            case REGERROR:
                nextState = onRegerror(eData);
                break;
            case INVALID_SIM:
                nextState = onInvalidSim(eData);
                break;
            case ENABLED:
                nextState = onEnabled(eData);
                break;
            case DISABLED:
                nextState = onDisabled(eData);
                break;
            case WIFI_PREF:
                nextState = onWifiPref(eData);
                break;
            case WIFI_ONLY:
                nextState = onWifiOnly(eData);
                break;
            case CELL_PREF:
                nextState = onCellPref(eData);
                break;
            case NO_CELL_DIAL_TIP:
            case NO_CELL_MSG_TIP:
            case NO_CELL_DIALER_ENTERED_NOTIFY:
            case NO_CELL_MMS_ENTERED_NOTIFY:
                onNoCellEvents(eData);
                break;
            default:
                nextState = mCurrentState;
        }

        if (nextState == null) {
            Logger.logd("WFCStateProxy ... " + event + " ..., do nothing on " + mCurrentState);
        } else if (nextState != mCurrentState) {
            Logger.logd("WFCStateProxy >>> " + event + " >>>, change state from " + mCurrentState + " --> " + nextState);
            mPreState = mCurrentState;
            mCurrentState = nextState;

            eData.eventHandleState = mPreState;
            mPreState.onStateDeactivated(eData);
            mCurrentState.onStateActivated(eData);
        } else {
            Logger.logd("WFCStateProxy === " + event + " ===, refresh on " + mCurrentState);

            eData.eventHandleState = mCurrentState;
            mCurrentState.onStateActivated(eData);
        }
    }

    // _IStateImpl {
    @Override
    public _State onSystemLocaleChanged(EventData eData) {
        return mCurrentState.onSystemLocaleChanged(eData);
    }

    @Override
    public _State onCallDropAlert(_State.EventData eData) {
        return mCurrentState.onCallDropAlert(eData);
    }

    @Override
    public _State onOnCall(_State.EventData eData) {
        return mCurrentState.onOnCall(eData);
    }

    @Override
    public _State onOffCall(_State.EventData eData) {
        return mCurrentState.onOffCall(eData);
    }

    @Override
    public _State onWifiOn(_State.EventData eData) {
        // showPopup(DIALOG_WIFION); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        return mCurrentState.onWifiOn(eData);
    }

    @Override
    public _State onWifiOff(_State.EventData eData) {
        return mCurrentState.onWifiOff(eData);
    }

    @Override
    public _State onWifiConnected(_State.EventData eData) {
        return mCurrentState.onWifiConnected(eData);
    }

    @Override
    public _State onWifiDisconnected(_State.EventData eData) {
        return mCurrentState.onWifiDisconnected(eData);
    }

    @Override
    public _State onPoorWifiSignal(EventData eData) {
        return mCurrentState.onPoorWifiSignal(eData);
    }

    @Override
    public _State onAirplaneOn(_State.EventData eData) {
        return mCurrentState.onAirplaneOn(eData);
    }

    @Override
    public _State onAirplaneOff(_State.EventData eData) {
        return mCurrentState.onAirplaneOff(eData);
    }

    @Override
    public _State onRegistering(_State.EventData eData) {
        return mCurrentState.onRegistering(eData);
    }

    @Override
    public _State onRegistered(_State.EventData eData) {
        return mCurrentState.onRegistered(eData);
    }

    @Override
    public _State onRegerror(_State.EventData eData) {
        return mCurrentState.onRegerror(eData);
    }

    @Override
    public _State onInvalidSim(EventData eData) {
        return mCurrentState.onInvalidSim(eData);
    }

    @Override
    public _State onEnabled(_State.EventData eData) {
        return mCurrentState.onEnabled(eData);
    }

    @Override
    public _State onDisabled(_State.EventData eData) {
        return mCurrentState.onDisabled(eData);
    }

    @Override
    public _State onWifiPref(_State.EventData eData) {
        return mCurrentState.onWifiPref(eData);
    }

    @Override
    public _State onWifiOnly(_State.EventData eData) {
        return mCurrentState.onWifiOnly(eData);
    }

    @Override
    public _State onCellPref(_State.EventData eData) {
        return mCurrentState.onCellPref(eData);
    }

    @Override
    public _State onNoCellEvents(_State.EventData eData) {
        return mCurrentState.onNoCellEvents(eData);
    }
    // } _IStateImpl
}
