package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class Initial extends _State {
    public Initial(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public _State onOnCall(EventData eData) {
        return getNextState(eData, OnCall.class);
    }

    @Override
    public _State onOffCall(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }

    @Override
    public _State onDisabled(EventData eData) {
        return getNextState(eData, Disabled.class);
    }

    @Override
    public _State onCellPref(EventData eData) {
        return getNextState(eData, CellPref.class);
    }
}
