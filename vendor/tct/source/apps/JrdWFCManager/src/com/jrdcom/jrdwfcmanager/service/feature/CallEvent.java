/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.os.Message;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CALL_EMERGENCY;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CALL_ENDED;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CALL_STARTED;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_CALL_ACTIVATED;
import static com.jrdcom.jrdwfcmanager.common.WFCUtils.IMPL_MSG.IMPL_CHECK_RADIO_POWER;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class CallEvent extends _FeatureImplementor {

    public CallEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean skipWhenWFCDisabled() {
        return true;
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_CALL_STARTED
                || what == IMPL_CALL_ENDED
                || what == IMPL_CALL_EMERGENCY
                || what == IMPL_CHECK_CALL_ACTIVATED
                ;
    }

    /*
     2.7 On-call icon – Req ID 54112
     2.12 Call icon in the dialer is the WFC icon – Req ID 58402
     2.13 Marquee message appears in dialer – Req ID 58403
     */
    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        SystemRFStatus sRFStatus = mHolder.getSystemRFStatus();
        boolean isEmergencyCall = sRFStatus.isEmergencyCall();
        boolean isRegistered = SystemRFStatus.isWFCRegistered();

        switch (what) {
            case IMPL_CALL_STARTED:
                sRFStatus.freezeEMCCallState(false);
                if (isRegistered) {
                    sRFStatus.initOnCallID();
                    notifyStateEvent(STATE_EVENT.ON_CALL);
                }
                break;
            case IMPL_CALL_ENDED:
                if (isRegistered) {
                    notifyStateEvent(STATE_EVENT.OFF_CALL);
                    sRFStatus.clearOnCallID();
                }

                if (isEmergencyCall) {
                    mHolder.getProcessHandler().sendMessage(IMPL_CHECK_RADIO_POWER);
                }

                sRFStatus.setIsEmergencyCall(false);
                sRFStatus.setIsCallActivated(false);
                break;
            case IMPL_CHECK_CALL_ACTIVATED:
                sRFStatus.setIsCallActivated(true);
                break;
            case IMPL_CALL_EMERGENCY:
                sRFStatus.setIsEmergencyCall(true);
                sRFStatus.freezeEMCCallState(true);
                break;
        }
        return true;
    }
}