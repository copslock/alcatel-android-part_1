package com.jrdcom.jrdwfcmanager.service;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.database.ContentObserver;
import android.net.Uri;

import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public class WFCContentObserver extends ContentObserver {
    private static final int OB_WFC_ENABLED = 0;
    private static final int OB_WFC_PREFERRED = 1;
    private static final int OB_WFC_NOTIFY_LOW_SIGNAL_SEND_MSG = 2;
    private static final int OB_WFC_NOTIFY_DIALER_ENTERED = 3;
    private static final int OB_WFC_NOTIFY_MMS_ENTERED = 4;

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    private static final Uri URI_WFC_IMS_ENABLED = Uri.withAppendedPath(android.provider.Settings.Global.CONTENT_URI,
            android.provider.Settings.Global.WFC_IMS_ENABLED);
    private static final Uri URI_WFC_IMS_MODE = Uri.withAppendedPath(android.provider.Settings.Global.CONTENT_URI,
            android.provider.Settings.Global.WFC_IMS_MODE);

    private static UriMatcher mMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        String authority = JrdIMSSettings.CONTENT_URI.getAuthority();
        String path = JrdIMSSettings.CONTENT_URI.getPath();
        mMatcher.addURI(authority, path + "/" + WFCUtils.CONTENT_NOTIFY_LOW_SIGNAL_SEND_MSG, OB_WFC_NOTIFY_LOW_SIGNAL_SEND_MSG);
        mMatcher.addURI(authority, path + "/" + WFCUtils.CONTENT_NOTIFY_DIALER_ENTERED, OB_WFC_NOTIFY_DIALER_ENTERED);
        mMatcher.addURI(authority, path + "/" + WFCUtils.CONTENT_NOTIFY_MMS_ENTERED, OB_WFC_NOTIFY_MMS_ENTERED);

        mMatcher.addURI(URI_WFC_IMS_ENABLED.getAuthority(), URI_WFC_IMS_ENABLED.getPath(), OB_WFC_ENABLED);
        mMatcher.addURI(URI_WFC_IMS_MODE.getAuthority(), URI_WFC_IMS_MODE.getPath(), OB_WFC_PREFERRED);
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
    }

    private IComponentHolder mHolder;

    public WFCContentObserver(IComponentHolder holder) {
        super(holder.getFeatureUiHandler());
        mHolder = holder;
    }

    public void init() {
        ContentResolver cr = mHolder.getContext().getContentResolver();
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        cr.registerContentObserver(Uri.withAppendedPath(JrdIMSSettings.CONTENT_URI,
                WFCUtils.CONTENT_NOTIFY_LOW_SIGNAL_SEND_MSG), true, this);
        cr.registerContentObserver(Uri.withAppendedPath(JrdIMSSettings.CONTENT_URI,
                WFCUtils.CONTENT_NOTIFY_DIALER_ENTERED), true, this);
        cr.registerContentObserver(Uri.withAppendedPath(JrdIMSSettings.CONTENT_URI,
                WFCUtils.CONTENT_NOTIFY_MMS_ENTERED), true, this);

        cr.registerContentObserver(URI_WFC_IMS_ENABLED, true, this);
        cr.registerContentObserver(URI_WFC_IMS_MODE, true, this);
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
    }

    public void onStop() {
        mHolder.getContext().getContentResolver().unregisterContentObserver(this);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        Logger.logd("WFCContentObserver onChange uri: " + uri);
        FeatureProcessHandler processHandler = mHolder.getProcessHandler();
        switch (mMatcher.match(uri)) {
            case OB_WFC_ENABLED:
                processHandler.sendMessage(WFCUtils.IMPL_MSG.WFC_ENABLED_CHANGED);
                break;
            case OB_WFC_PREFERRED:
                processHandler.sendMessage(WFCUtils.IMPL_MSG.WFC_PREFERENCE_CHANGED);
                break;
            case OB_WFC_NOTIFY_LOW_SIGNAL_SEND_MSG:
                processHandler.sendMessage(WFCUtils.IMPL_MSG.DIALOG_NO_CELL_SEND_MSG_TIPS);
                break;
            case OB_WFC_NOTIFY_DIALER_ENTERED:
                processHandler.sendMessage(WFCUtils.IMPL_MSG.NOTIFY_NO_CELL_DIALER_ENTERED);
                break;
            case OB_WFC_NOTIFY_MMS_ENTERED:
                processHandler.sendMessage(WFCUtils.IMPL_MSG.NOTIFY_NO_CELL_MMS_ENTERED);
                break;
            default:
                Logger.loge("WFCContentObserver onChange invalid uri: " + uri);
        }
    }
}
