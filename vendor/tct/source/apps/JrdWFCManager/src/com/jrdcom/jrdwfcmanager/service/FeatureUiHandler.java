/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.ui.DialogManagerActivity;
import com.jrdcom.jrdwfcmanager.ui.NotificationManager;
import com.jrdcom.jrdwfcmanager.ui.SoundManager;

public class FeatureUiHandler extends Handler {
    private Context mContext;
    private NotificationManager mNotificationManager;
    private SoundManager mSoundManager;

    public FeatureUiHandler(Context context) {
        mContext = context;
        mNotificationManager = new NotificationManager();
        mSoundManager = new SoundManager();
    }

    @Override
    public void handleMessage(Message msg) {
        if (JrdWFCManager.isMiniVersion()) {
            Logger.logd("FeatureUiHandler handleMessage, In mini mode, skip all UI event");
            return;
        }

        if (msg.obj != null && msg.obj instanceof UiData) {
            UiData data = (UiData) msg.obj;

            // check popup
            if ((msg.what & WFCUtils.UI_HANDLER_BIT_POPUP) != 0) {
                DialogManagerActivity.show(data.dialog);
            }

            // check notification
            if ((msg.what & WFCUtils.UI_HANDLER_BIT_NOTIFICATION) != 0) {
                mNotificationManager.show(data.notification);
            }

            // check right side icon
            if ((msg.what & WFCUtils.UI_HANDLER_BIT_WFC_RIGHT_SIDE_ICON) != 0) {
                // updateWFCRightIcon(data.showWFCRightIcon);
            }
        }

        // check beep
        if ((msg.what & WFCUtils.UI_HANDLER_BIT_BEEP) != 0) {
            mSoundManager.beep();
        }
    }

    private void updateWFCRightIcon(boolean show) {
        Logger.logd("updateWFCRightIcon, show: " + show);
        Intent i = new Intent(WFCUtils.ACTION_STATUS_BAR_ICON);
        i.putExtra(WFCUtils.SYSTEM_PROPERTIES_WFC_REGISTERED, show);
        mContext.sendBroadcast(i);
    }

    public static class UiData {
        public DialogManagerActivity.DialogPack dialog;
        public NotificationManager.NotificationPack notification;
        public boolean showWFCRightIcon;
    }
}