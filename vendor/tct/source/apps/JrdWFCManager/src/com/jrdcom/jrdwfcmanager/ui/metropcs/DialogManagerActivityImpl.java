/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui.metropcs;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.internal.app.AlertController;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

public class DialogManagerActivityImpl extends com.jrdcom.jrdwfcmanager.ui.tmo.DialogManagerActivityImpl {
    public DialogManagerActivityImpl(Object activity) {
        super(activity);
    }

    @Override
    public String getMessageBody(int type) {
        return mActivity.getResources().getStringArray(R.array.wfc_dialog_msg_metropcs)[type];
    }

    @Override
    public void onCreateDialog(AlertController.AlertParams p, int type) {
        switch (type) {
            case WFCUtils.DIALOG_E911:
                TextView e911_textview = (TextView) p.mView.findViewById(R.id.topmsg);
                e911_textview.setText(R.string.missing_e911_msg_metropcs);
                break;
            case WFCUtils.DIALOG_FIRST_WFC:
                replaceImsReadyIcon(p.mView);
                break;
            case WFCUtils.DIALOG_OOBE_WIFI_CONNECTED:
                replaceImsReadyIcon(p.mView);
                break;
            default:
                super.onCreateDialog(p, type);
        }
    }

    private void replaceImsReadyIcon(View view) {
        ImageView icon = (ImageView) view.findViewById(R.id.icon);
        icon.setImageResource(R.drawable.ims_ready_icon);
    }
}