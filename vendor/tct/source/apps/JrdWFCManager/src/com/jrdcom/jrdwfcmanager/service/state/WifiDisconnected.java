package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class WifiDisconnected extends _State {
    public WifiDisconnected(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(NOT_CONNECTED_TO_WIFI, null);
    }

    @Override
    public _State onOnCall(EventData eData) {
        return null;
    }

    @Override
    public _State onOffCall(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiConnected(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getNextState(eData, Registered.class);
    }
}
