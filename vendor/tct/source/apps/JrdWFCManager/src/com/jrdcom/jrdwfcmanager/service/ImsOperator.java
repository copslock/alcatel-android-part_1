/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.content.Context;
import android.os.Message;

import com.android.ims.ImsConfig;
import com.android.ims.ImsManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

import java.util.concurrent.atomic.AtomicBoolean;

public class ImsOperator implements IImsOperator {
    private static final String TAG = ImsOperator.class.getSimpleName();
    /**
     * For accessing the IMS related service.
     * Internal use only.
     */
    private static final String IMS_SERVICE = "ims";
    private AtomicBoolean mBusy;
    private Message mPendingMessage;

    private IComponentHolder mHolder;

    public ImsOperator(IComponentHolder holder) {
        mHolder = holder;
        mBusy = new AtomicBoolean(false);
    }

    /**
     * Binds the IMS service to make/receive the call.
     */
    @Override
    public void init() {
    }

    @Override
    public boolean isBusyFor(Message msg) {
        boolean busy = mBusy.get() && mPendingMessage != msg;
        if (busy) {
            Logger.logw("Busying for message " + mPendingMessage);
        }
        return busy;
    }

    @Override
    public void setWifiCallingPreference(WFCUtils.IMPL_MSG msg, int wifiCallingStatus, int wifiCallingPreference) {
        if (mBusy.get()) {
            Logger.loge("setWifiCallingPreference(): ImsOperator is busy now.");
            return;
        }

        mBusy.set(true);
        mPendingMessage = mHolder.getProcessHandler().obtainMessage(msg.ordinal());
        Context context = mHolder.getContext();
        ImsManager.setWfcMode(context, wifiCallingPreference); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        ImsManager.setWfcSetting(context, wifiCallingStatus == ImsConfig.FeatureValueConstants.ON);

        FeatureProcessHandler.WFCStatusData data = new FeatureProcessHandler.WFCStatusData();
        sendPendingMessage(data);
    }

    private void sendPendingMessage(FeatureProcessHandler.WFCStatusData data) {
        if (mPendingMessage != null) {
            mPendingMessage.obj = data;
            mPendingMessage.sendToTarget();
        } else {
            Logger.loge("sendPendingMessage(), mPendingMessage is null.");
        }

        mPendingMessage = null;
        mBusy.set(false);
    }
}
