/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui.tmo;

import com.android.internal.app.AlertController;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.ui.DialogManagerActivity;

public class DialogManagerActivityImpl implements DialogManagerActivity.IImplementor {
    protected DialogManagerActivity mActivity;

    public DialogManagerActivityImpl(Object activity) {
        mActivity = (DialogManagerActivity) activity;
    }

    @Override
    public String getMessageBody(int type) {
        return mActivity.getResources().getStringArray(R.array.wfc_dialog_msg)[type];
    }

    @Override
    public void onCreateDialog(AlertController.AlertParams p, int type) {
    }
}