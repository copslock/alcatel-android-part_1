package com.jrdcom.jrdwfcmanager.service.state;

import android.text.TextUtils;
import com.android.ims.ImsReasonInfo;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class InvalidSim extends _State {
    private ImsReasonInfo mImsReasonInfo;

    public InvalidSim(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        try {
            ImsReasonInfo imsInfo = eData.imsReasonInfo;
            String extraMsg = "";

            if (imsInfo == null
                    && mImsReasonInfo != null
                    && mHolder.getWFCStateProxy().getCurrentState().getClass() == this.getClass()) {
                extraMsg = mImsReasonInfo.getExtraMessage();
            } else {
                extraMsg = imsInfo.getExtraMessage();
            }

            updateWFCSettingsStateInfo(ERROR_CODE, extraMsg);
            showNotification(NOTIFICATION_REG_ERROR, false, eData.imsReasonInfo);

            if (imsInfo != null && !TextUtils.isEmpty(imsInfo.getExtraMessage())) {
                // {101, 10100, 403, REG09 - Missing 911 Address}
                if (!mHolder.getSystemRFStatus().is911PopupDisabled()
                        && imsInfo.getExtraCode() == 403
                        && imsInfo.getExtraMessage().toLowerCase().contains("missing 911 address")) {
                    showPopup(DIALOG_E911);
                    mHolder.getSystemRFStatus().set911PopupDisabled(true);
                }
                mImsReasonInfo = imsInfo;
            }
        } catch (Exception e) {
            Logger.loge("*** " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }
}
