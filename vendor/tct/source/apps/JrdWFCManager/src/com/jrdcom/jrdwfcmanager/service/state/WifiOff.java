package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class WifiOff extends _State {
    public WifiOff(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(WIFI_OFF, null);
    }

    @Override
    public _State onOnCall(EventData eData) {
        return null;
    }

    @Override
    public _State onOffCall(EventData eData) {
        return null;
    }

    @Override
    public _State onWifiOn(EventData eData) {
        return getNextState(eData, WifiDisconnected.class);
    }

    @Override
    public _State onWifiConnected(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }
}
