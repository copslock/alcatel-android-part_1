package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.service.IComponentHolder;

public class PoorWifiSignal extends _State {
    public PoorWifiSignal(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        clearNotification();
        updateWFCSettingsStateInfo(POOR_WIFI_SIGNAL, null);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }

    @Override
    public _State onEnabled(EventData eData) {
        return getNextState(eData, Registering.class);
    }
}
