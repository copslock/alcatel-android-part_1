/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import android.provider.Settings;

import com.android.ims.ImsReasonInfo;
import com.android.ims.JrdIMSSettings;
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/

public class NotificationManager implements WFCUtils {
    private static final int ID_NOTIFICATION = 50000;

    private Context mContext;
    private android.app.NotificationManager mNotificationManager;
    private int mShowingType = INVALID;

    public static NotificationPack getNotificationPack(int type) {
        return getNotificationPack(type, null);
    }

    public static NotificationPack getNotificationPack(int type, ImsReasonInfo info) {
        Context context = JrdWFCManager.getContext();
        NotificationPack pack = new NotificationPack();
        pack.type = type;

        switch (type) {
            case NOTIFICATION_DISSMISS:
                pack.dissmiss = true;
                break;
            case NOTIFICATION_REG_ERROR:
                pack.isNoClear = false;
                pack.isSound = false;
                pack.isVibrate = false;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_error;
                pack.title = context.getText(R.string.ims_error);
                pack.text = info.getExtraMessage();
                pack.notificationBgColor = JrdWFCManager.getContext().getColor(R.color.error_icon); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
                break;
            case NOTIFICATION_REGISTERED:
                pack.isVibrate = false;
                pack.isSound = false;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_registered;
                pack.title = context.getText(R.string.ims_ready_new);
                pack.text = context.getText(R.string.ims_ready_content);
                break;
            case NOTIFICATION_ON_CALL:
                pack.isSound = false;
                pack.isVibrate = false;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_on_call;
                pack.title = context.getText(R.string.ims_ready_new);
                pack.text = context.getText(R.string.ims_ready_content);
                break;
            case NOTIFICATION_CALL_END:
                pack.isSound = false;
                pack.isVibrate = false;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_registered;
                pack.title = context.getText(R.string.ims_ready_new);
                pack.text = context.getText(R.string.ims_ready_content);
                break;
            case NOTIFICATION_NO_CELL_DIAL:
                pack.isNoClear = false;
                pack.isSound = false;
                pack.isVibrate = false;
                pack.isAutoCancel = true;
                pack.isForced = true;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_info;
                pack.title = context.getText(R.string.wifi_enabled_home_title);
                pack.text = context.getText(R.string.wifi_enabled_home_msg);

                pack.intent.setAction(Settings.ACTION_WIFI_SETTINGS); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
                break;
            case NOTIFICATION_NO_CELL_SEND_MSG:
                pack.isNoClear = false;
                pack.isSound = false;
                pack.isVibrate = false;
                pack.isAutoCancel = true;
                pack.isForced = true;
                pack.notificationIconResId = R.drawable.stat_sys_wfc_info;
                pack.title = context.getText(R.string.wifi_enabled_mms_title);
                pack.text = context.getText(R.string.wifi_enabled_mms_msg);

                /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                pack.intent.setAction(Settings.ACTION_WIFI_SETTINGS);
                break;
            default:
                throw new RuntimeException("NotificationManager, Unknow Notification Type.");
        }

        if (!SystemRFStatus.isFeatureOn(com.android.internal.R.bool.jrd_ims_feature_left_status_bar_icon)) {
            pack.dissmiss = true;
        }
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/

        return pack;
    }

    public NotificationManager() {
        mContext = JrdWFCManager.getContext();
        mNotificationManager = (android.app.NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void show(NotificationPack pack) {
        if (pack == null) {
            Logger.loge("NotificationManager@show, pack is null.");
            return;
        }

        if (pack.dissmiss) {
            Logger.logd("NotificationManager@show, dissmiss current notification");
            mNotificationManager.cancel(ID_NOTIFICATION);
            mShowingType = INVALID;
            return;
        }

        if (!pack.isForced && mShowingType == pack.type) {
            Logger.logd("NotificationManager@show, skip show notification type: " + pack.type);
            return;
        }

        mShowingType = pack.type;
        Logger.logd("NotificationManager@show, show notification type: " + pack.type);

        final Notification notification = new Notification();
        notification.when = System.currentTimeMillis();
        int iconResId = pack.notificationIconResId;
        if (iconResId != 0) {
            notification.icon = iconResId;
        }

        if (pack.isSound) {
            notification.defaults |= Notification.DEFAULT_SOUND;
        }
        if (pack.isVibrate) {
            notification.defaults |= Notification.DEFAULT_VIBRATE;
        }
        if (pack.isNoClear) {
            notification.flags |= Notification.FLAG_NO_CLEAR;
        }
        if (pack.isAutoCancel) {
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
        }
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        if (pack.notificationBgColor != 0) {
            notification.color = pack.notificationBgColor;
        }
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/

        PendingIntent contentIntent = PendingIntent.getActivity(mContext, ID_NOTIFICATION, pack.intent, 0);
        notification.setLatestEventInfo(mContext, pack.title, pack.text, contentIntent);
        mNotificationManager.notify(ID_NOTIFICATION, notification);
    }

    public static class NotificationPack {
        public int type = INVALID;

        public boolean dissmiss = false;
        public boolean isNoClear = true;
        public boolean isSound = true;
        public boolean isVibrate = true;
        public boolean isAutoCancel = false;
        public boolean isForced = false;
        public int notificationBgColor; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        public int notificationIconResId;
        public CharSequence title = "";
        public CharSequence text = "";
        public Intent intent;

        NotificationPack() {
            intent = new Intent();
            /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
            intent.setAction(Settings.ACTION_WIRELESS_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        }
    }
}
