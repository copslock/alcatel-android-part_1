/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.os.Message;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;


public class GAPPEvent extends _FeatureImplementor {

    public GAPPEvent(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.DIALOG_NO_CELL_DIAL_TIPS
                || what == IMPL_MSG.DIALOG_NO_CELL_SEND_MSG_TIPS
                || what == IMPL_MSG.NOTIFY_NO_CELL_DIALER_ENTERED
                || what == IMPL_MSG.NOTIFY_NO_CELL_MMS_ENTERED
                ;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        if (!mHolder.getSystemRFStatus().isSimCardOK()) {
            Logger.loge("GAPPEvent, won't show dialog because Sim card is not OK.");
            return true;
        }

        if (!SystemRFStatus.isWFCEnabled()) {
            Logger.loge("GAPPEvent, won't show dialog because WFC is disabled.");
            return true;
        }

        if (SystemRFStatus.isAirplaneModeOn()) {
            Logger.loge("GAPPEvent, won't show dialog because in airplane mode.");
            return true;
        }

        if (!mHolder.getSystemRFStatus().isLowSignal()) {
            Logger.loge("GAPPEvent, won't show dialog because cell signal is OK.");
            return true;
        }

        if (!SystemRFStatus.isWifiEnabled()) {
            Logger.loge("GAPPEvent, won't show dialog because wifi is disabled.");
            return true;
        }

        if (SystemRFStatus.isWifiConnected()) {
            Logger.loge("GAPPEvent, won't show dialog because has connected AP.");
            return true;
        }

        switch (what) {
            case DIALOG_NO_CELL_DIAL_TIPS:
                notifyStateEvent(STATE_EVENT.NO_CELL_DIAL_TIP);
                break;
            case DIALOG_NO_CELL_SEND_MSG_TIPS:
                notifyStateEvent(STATE_EVENT.NO_CELL_MSG_TIP);
                break;
            case NOTIFY_NO_CELL_DIALER_ENTERED:
                notifyStateEvent(STATE_EVENT.NO_CELL_DIALER_ENTERED_NOTIFY);
                break;
            case NOTIFY_NO_CELL_MMS_ENTERED:
                notifyStateEvent(STATE_EVENT.NO_CELL_MMS_ENTERED_NOTIFY);
                break;
        }
        return true;
    }
}