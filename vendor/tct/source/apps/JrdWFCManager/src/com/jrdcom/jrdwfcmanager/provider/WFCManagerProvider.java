/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.android.ims.JrdIMSSettings;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/


public class WFCManagerProvider extends ContentProvider {
    private String TAG = "WFCManagerProvider";

    protected IDataSource mSharePrefSource;
    protected IDataSource mMemSource;
    protected IDataSource mNotify;

    @Override
    public boolean onCreate() {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        final Context context = getContext();
        mSharePrefSource = new DBSharePreferences(context);
        mMemSource = new DBMemory(context);
        mNotify = new DBNotify(context);
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        return true;
    }

    @Override
    public String getType(Uri uri) {
        //To get the MIME type corresponding to a content URI, call ContentResolver.getType().
        return mSharePrefSource.getType(uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri result = null;
        String key = values.getAsString(JrdIMSSettings.NAME_CV_KEY); // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
        if (key.startsWith("@")) {
            result = mMemSource.insert(uri, values);
        } else if (key.startsWith("__")) {
            result = mNotify.insert(uri, values);
        } else {
            result = mSharePrefSource.insert(uri, values);
        }
        return result;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new RuntimeException("WFCManagerProvider not support delete!");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new RuntimeException("WFCManagerProvider not support update!");
    }

    @Override
    public Cursor query(Uri uri, String[] select, String selection, String[] selectionArgs, String sort) {
        if (selectionArgs == null || selectionArgs.length < 1) {
            throw new RuntimeException("WFCManagerProvider query() invalid selectionArgs!");
        }

        Cursor result = null;
        if (selectionArgs[0].startsWith("@")) {
            result = mMemSource.query(uri, select, selection, selectionArgs, sort);
        } else if (selectionArgs[0].startsWith("__")) {
            // do nothing
        } else {
            result = mSharePrefSource.query(uri, select, selection, selectionArgs, sort);
        }
        return result;
    }
}
