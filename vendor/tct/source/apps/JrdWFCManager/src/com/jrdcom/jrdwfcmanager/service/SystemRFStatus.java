/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.CellInfo;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import com.android.ims.ImsConfig;
import com.android.ims.JrdIMSSettings;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/
import com.jrdcom.jrdwfcmanager.JrdWFCManager;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.common.WFCUtils;

import java.util.List;

public class SystemRFStatus {
    public static final int INVALID_VALUE = Integer.MAX_VALUE;
    public static final int ON_CALL_DROP_TIMES_REQUIRED = 3;
    public static final int ON_CALL_DROP_FREEZED_DURATION = 30 * 60 * 1000;

    private static Context sContext = JrdWFCManager.getContext();

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    public SystemRFStatus() {
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    public static boolean isAirplaneModeOn() {
        return Settings.System.getInt(JrdWFCManager.getContext().getContentResolver(),
                Settings.System.AIRPLANE_MODE_ON, 0) != 0;
    }

    /********************************
     * Sim state
     ********************************/
    private static final int SIM_CHECK_FREZZED_DELAY = 15 * 1000;
    private boolean mIsSimCardOK = true;
    private long simCheckUnfrezzedTimePoint;

    public boolean isSimCardOK() {
        if (isWFCRegistered()) {
            mIsSimCardOK = true;
        }
        return mIsSimCardOK;
    }

    public void setSimCardOK(boolean isOK) {
        mIsSimCardOK = isOK;
    }

    public void setSimCheckUnfrezzedTimePoint() {
        simCheckUnfrezzedTimePoint = System.currentTimeMillis() + SIM_CHECK_FREZZED_DELAY;
    }

    public boolean isSimCheckingFrezzed() {
        return System.currentTimeMillis() <= simCheckUnfrezzedTimePoint;
    }

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    public boolean isSIMPlmnChanged() {
        String imsi = ((TelephonyManager) sContext.getSystemService(Context.TELEPHONY_SERVICE)).getSubscriberId();
        if (imsi == null) {
            Logger.logd("isSIMPlmnChanged(), imsi is null!");
            return false;
        }

        imsi = imsi.substring(0, 6);
        String savedImsi = JrdIMSSettings.getString(sContext, WFCUtils.CONTENT_SIM_CARD_PLMN, null);
        if (imsi.equals(savedImsi)) {
            Logger.logd("isSIMPlmnChanged(), imsi[" + imsi + "] is the same");
            return false;
        }

        JrdIMSSettings.put(sContext, WFCUtils.CONTENT_SIM_CARD_PLMN, imsi);
        return true;
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    // for holding missing 911 address state
    private boolean mIsMissing911Address;
    private boolean mDisable911Popup;

    public boolean isMissing911Address() {
        return mIsMissing911Address;
    }

    public void setIsMissing911Address(boolean missed) {
        mIsMissing911Address = missed;
    }

    public boolean is911PopupDisabled() {
        return mDisable911Popup;
    }

    public void set911PopupDisabled(boolean disable) {
        mDisable911Popup = disable;
    }

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    public static String getICCID() {
        return ((TelephonyManager) sContext.getSystemService(Context.TELEPHONY_SERVICE)).getSimSerialNumber();
    }

    public static String getSavedImsi() {
        return JrdIMSSettings.getString(sContext, WFCUtils.CONTENT_SIM_CARD_PLMN, null);
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/

    /********************************
     * Cell RAT infos
     ********************************/
    private int mCellServiceState = INVALID_VALUE;
    private int mCellSignalStrengthLevel = INVALID_VALUE;

    public void setCellServiceState(int state) {
        mCellServiceState = state;
    }

    public void setCellSignalStrengthLevel(int level) {
        mCellSignalStrengthLevel = level;
    }

    public boolean isLowSignal() {
        boolean lowsignal = false;
        if (mCellServiceState == ServiceState.STATE_OUT_OF_SERVICE ||
                mCellServiceState == ServiceState.STATE_POWER_OFF) {
            lowsignal = true;
        }

        if (mCellSignalStrengthLevel == SignalStrength.SIGNAL_STRENGTH_NONE_OR_UNKNOWN) {
            lowsignal = true;
        }

        return lowsignal;
    }

    public static void setIsLowSignal(boolean state) {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        if (JrdIMSSettings.getBoolean(sContext, WFCUtils.CONTENT_IS_LOW_SIGNAL, false) != state) {
            JrdIMSSettings.put(sContext, WFCUtils.CONTENT_IS_LOW_SIGNAL, state);
            /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        }
    }

    private boolean hasRegisteredCellNetwork() {
        boolean hasRegisterdCell = false;
        TelephonyManager telephonyManager = (TelephonyManager) sContext.getSystemService(Context.TELEPHONY_SERVICE);
        List<CellInfo> list = telephonyManager.getAllCellInfo();
        if (list != null && list.size() > 0) {
            for (CellInfo ci : list) {
                if (ci.isRegistered()) {
                    hasRegisterdCell = true;
                    break;
                }
            }
        }
        return hasRegisterdCell;
    }

    /********************************
     * Wifi RAT infos
     ********************************/
    private int mWifiRssi = INVALID_VALUE;

    public int getWifiRssi() {
        return mWifiRssi;
    }

    public void setWifiRssi(int rssi) {
        mWifiRssi = rssi;
    }

    public static boolean isWifiEnabled() {
        boolean isEnabled = false;
        try {
            WifiManager wifiManager = (WifiManager) sContext.getSystemService(Context.WIFI_SERVICE);
            isEnabled = wifiManager.isWifiEnabled();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isEnabled;
    }

    public static boolean isWifiConnected() {
        boolean isConnected = false;
        try {
            ConnectivityManager manager = (ConnectivityManager) sContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo.State wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState();
            isConnected = wifi == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isConnected;
    }

    public static String getAPSSID() {
        String name = "";
        try {
            WifiManager wifiManager = (WifiManager) sContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            name = wInfo.getSSID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return name;
    }

    public static boolean hasSavedAP() {
        boolean hasSavedAP = false;
        try {
            WifiManager wifiManager = (WifiManager) sContext.getSystemService(Context.WIFI_SERVICE);
            List<WifiConfiguration> existingConfigs = wifiManager.getConfiguredNetworks();
            hasSavedAP = existingConfigs == null ? false : !existingConfigs.isEmpty();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasSavedAP;
    }

    /********************************
     * WFC state
     ********************************/
    public static boolean isWFCEnabled() {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        int wfcEnabled = android.provider.Settings.Global.getInt(JrdWFCManager.getContext().getContentResolver(),
                android.provider.Settings.Global.WFC_IMS_ENABLED,
                ImsConfig.FeatureValueConstants.OFF);
        return wfcEnabled == 1;
    }

    public static int getWFCPref() {
        return android.provider.Settings.Global.getInt(JrdWFCManager.getContext().getContentResolver(),
                android.provider.Settings.Global.WFC_IMS_MODE,
                ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED);
    }

    public static void setWFCSettingsState(String state) {
        JrdIMSSettings.put(sContext, WFCUtils.CONTENT_SETTINGS_STATE, state);
    }

    public static boolean isWFCRegistered() {
        TelephonyManager tm = TelephonyManager.getDefault();
        return tm.isImsRegistered() && tm.isWifiCallingAvailable();
    }

    public static void setWFCRegState(boolean registered) {
        if (SystemRFStatus.isWFCRegistered() != registered) {
            JrdIMSSettings.put(sContext, WFCUtils.CONTENT_IS_WFC_REGISTERED, registered);
            /* MODIFIED-END by Yuan Yili,BUG-2419568*/
        }
    }

    /********************************
     * On call state
     ********************************/
    private long mOnCallID;
    private boolean mIsOnCallDropPopupEnabled;
    private long mOnCallLastDropPopupTimePoint;
    private int mOnCallDropPopupTimes;

    public void initOnCallID() {
        mIsOnCallDropPopupEnabled = true;
        mOnCallLastDropPopupTimePoint = System.currentTimeMillis();
        mOnCallDropPopupTimes = 0;
        mOnCallID = mOnCallLastDropPopupTimePoint;
    }

    public void clearOnCallID() {
        mIsOnCallDropPopupEnabled = false;
        mOnCallLastDropPopupTimePoint = 0;
        mOnCallDropPopupTimes = 0;
        mOnCallID = -1;
    }

    public void increasePopupTimes() {
        mOnCallDropPopupTimes++;
    }

    public void setOnCallDropPopupEnabled(boolean enabled) {
        mIsOnCallDropPopupEnabled = enabled;
    }

    public boolean canOnCallDropPopup() {
        boolean canPopup = false;
        long currentTP = System.currentTimeMillis();
        Logger.logd("canOnCallDropPopup," +
                "  mOnCallID: " + mOnCallID +
                "  mIsOnCallDropPopupEnabled: " + mIsOnCallDropPopupEnabled +
                "  mOnCallDropPopupTimes: " + mOnCallDropPopupTimes +
                "  elapse: " + (currentTP - mOnCallLastDropPopupTimePoint));
        if (mOnCallID > 0 && mIsOnCallDropPopupEnabled) {
            if (mOnCallDropPopupTimes < ON_CALL_DROP_TIMES_REQUIRED) {
                canPopup = true;
            } else if (currentTP - mOnCallLastDropPopupTimePoint > ON_CALL_DROP_FREEZED_DURATION) {
                mOnCallLastDropPopupTimePoint = currentTP;
                mOnCallDropPopupTimes = 0;
                canPopup = true;
            }
        }
        return canPopup;
    }

    private boolean isWFCEmergencyCall = false;
    private boolean isEmcFreezed = false;

    public void setIsEmergencyCall(boolean emc) {
        if (!isEmcFreezed) {
            isWFCEmergencyCall = emc;
        }
    }

    public boolean isEmergencyCall() {
        return isWFCEmergencyCall;
    }

    public void freezeEMCCallState(boolean freezed) {
        isEmcFreezed = freezed;
    }

    private boolean isCallActivated = false;

    public void setIsCallActivated(boolean activated) {
        isCallActivated = activated;
    }

    public boolean isCallActivated() {
        return isCallActivated;
    }

    public static boolean isVolteEnabled() {
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        int volteEnabled = android.provider.Settings.Global.getInt(JrdWFCManager.getContext().getContentResolver(),
                android.provider.Settings.Global.ENHANCED_4G_MODE_ENABLED,
                ImsConfig.FeatureValueConstants.OFF);
        return volteEnabled == 1;
    }

    /********************************
     * On call state
     ********************************/
    public static boolean isFeatureOn(int feature) {
        boolean isFeatureOn = JrdIMSSettings.isFeatureOn(JrdWFCManager.getContext(), feature);
        return isFeatureOn;
        /* MODIFIED-END by Yuan Yili,BUG-2419568*/
    }


    @Override
    public String toString() {
        return "isAirplaneModeOn(): " + isAirplaneModeOn() +
                "\nisSimCardOK(): " + isSimCardOK() +
                "\nisLowSignal(): " + isLowSignal() +
                "\ngetWifiRssi(): " + getWifiRssi() +
                "\nisWifiEnabled(): " + isWifiEnabled() +
                "\nisWifiConnected(): " + isWifiConnected() +
                "\nisWFCEnabled(): " + isWFCEnabled() +
                "\ngetWFCPref(): " + getWFCPref() +
                "\nisWFCRegistered(): " + isWFCRegistered();
    }
}
