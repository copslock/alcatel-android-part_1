/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager;

import android.app.ActivityManager; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import android.app.Application;
import android.content.Context;
import android.os.SystemProperties;
import android.widget.Toast;

import com.jrdcom.jrdwfcmanager.common.WFCUtils;

import java.util.HashMap;
import java.util.Map;

public class JrdWFCManager extends Application {
    private String TAG = "JrdWFCManager";

    public static final String SYSTEM_PROPERTIES_OPERATOR = "ro.jrd.wfc.manager.operator";

    public static final String OPERATOR_GLOBAL = "0";
    public static final String OPERATOR_TMO = "1";
    public static final String OPERATOR_METROPCS = "2";
    public static final String OPERATOR_TRACFONE = "3";

    private static Context mContext;
    private static final long mAppStartTimeStamp = System.currentTimeMillis();
    private static Map<String, String> mCarrier = new HashMap<String, String>();

    public JrdWFCManager() {
        mCarrier.put(OPERATOR_GLOBAL, "global");
        mCarrier.put(OPERATOR_TMO, "tmo");
        mCarrier.put(OPERATOR_METROPCS, "metropcs");
        mCarrier.put(OPERATOR_TRACFONE, "tracfone");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }

    public static void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static String getCarrierName() {
        return mCarrier.get(SystemProperties.get(SYSTEM_PROPERTIES_OPERATOR, OPERATOR_TMO));
    }

    public static long getAppLivedSeconds() {
        return (System.currentTimeMillis() - mAppStartTimeStamp) / 1000;
    }

    public static boolean isMiniVersion() {
        return SystemProperties.getBoolean(WFCUtils.FLAG_MINI_VERSION, false);
    }

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    public static String getRunningActivityName() {
        ActivityManager activityManager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        String runningActivity = activityManager.getRunningTasks(1).get(0).topActivity.getClassName();
        return runningActivity;
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/
}
