package com.jrdcom.jrdwfcmanager.service.state;

import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;

public class OnCall extends _State {
    public OnCall(IComponentHolder holder) {
        super(holder);
    }

    @Override
    public void onStateActivated(EventData eData) {
        if (!mHolder.getSystemRFStatus().isEmergencyCall()) {
            showNotification(NOTIFICATION_ON_CALL);
        } else {
            Logger.logd("WFC icon not turn green on emergency call.");
        }
    }

    @Override
    public _State onOffCall(EventData eData) {
        return getNextState(eData, Registered.class);
    }

    @Override
    public _State onRegistering(EventData eData) {
        return getNextState(eData, Registering.class);
    }

    @Override
    public _State onRegistered(EventData eData) {
        _State state = null;
        if (SystemRFStatus.isWFCRegistered()) {
            state = getHighPriorityState(eData);
        } else {
            state = getNextState(eData, Registered.class);
        }
        return state;
    }

    @Override
    public _State onRegerror(EventData eData) {
        return getNextState(eData, RegError.class);
    }
}
