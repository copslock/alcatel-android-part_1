/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.service.feature;

import android.content.Context;
import android.os.Message;
import android.os.SystemProperties;

/* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
import com.android.ims.JrdIMSSettings;
import com.jrdcom.jrdwfcmanager.common.Logger;
import com.jrdcom.jrdwfcmanager.service.IComponentHolder;
import com.jrdcom.jrdwfcmanager.service.SystemRFStatus;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
/* MODIFIED-END by Yuan Yili,BUG-2419568*/

public class FirstRunCheck extends _FeatureImplementor {
    private static final int SET_DEFAULT_VALUE = 0;

    private int mCheckActionType = INVALID;

    public FirstRunCheck(IComponentHolder h) {
        super(h);
    }

    @Override
    protected boolean eatMsg(IMPL_MSG what) {
        return what == IMPL_MSG.FIRST_RUN_CHECK
                || what == IMPL_MSG.__FIRST_RUN_CHECK;
    }

    @Override
    protected boolean handleMessage(IMPL_MSG what, Message msg) {
        String platformVer = SystemProperties.get(SYSTEM_PROPERTY_PLATFORM_VERSION);
        /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
        final Context context = mHolder.getContext();
        int defEnabledVal = context.getResources().getInteger(com.android.internal.R.integer.jrd_ims_config_vowifi_enabled_default);
        int defPreference = context.getResources().getInteger(com.android.internal.R.integer.jrd_ims_config_vowifi_preference_default);

        switch (what) {
            case FIRST_RUN_CHECK:
                if (!JrdIMSSettings.getBoolean(context, CONTENT_DEFAULT_PROFILE_SET, false)) {
                    // set wfc default value to QMI
                    mHolder.getImsOperator().setWifiCallingPreference(IMPL_MSG.__FIRST_RUN_CHECK, defEnabledVal, defPreference);

                    mCheckActionType = SET_DEFAULT_VALUE;
                } else {
                    mHolder.getProcessHandler().sendMessage(IMPL_MSG.__FIRST_RUN_CHECK);
                }
                break;
            case __FIRST_RUN_CHECK:
                switch (mCheckActionType) {
                    case SET_DEFAULT_VALUE:
                        JrdIMSSettings.put(context, CONTENT_IS_WFC_REGISTERED, false);
                        JrdIMSSettings.put(context, CONTENT_DEFAULT_PROFILE_SET, true);
                        break;
                }

                Logger.logd("--------------- FirstRunCheck ---------------");
                try {
                    String macAddr = getMacAddress();

                    String imsi = mHolder.getSystemRFStatus().getSavedImsi();
                    if (imsi != null) {
                        imsi = imsi.substring(0, 6) + "???";
                    }

                    String iccid = SystemRFStatus.getICCID();
                    if (iccid != null) {
                        iccid = iccid.substring(0, 7) + "???";
                    }

                    Logger.logd("Platform Ver:   " + platformVer);
                    Logger.logd("MAC Address:    " + macAddr);
                    Logger.logd("Saved IMSI:     " + imsi);
                    Logger.logd("SIM ICCID:      " + iccid);
                    Logger.logd("isAirplaneMode: " + SystemRFStatus.isAirplaneModeOn());

                    Logger.logd("Check type:     " + getChkType(mCheckActionType));
                    Logger.logd("WFC Enabled:    " + SystemRFStatus.isWFCEnabled());
                    Logger.logd("WFC Preference: " + getPrefString(SystemRFStatus.getWFCPref()));
                    Logger.logd("WFC Registered: " + JrdIMSSettings.getBoolean(context, CONTENT_IS_WFC_REGISTERED, false));
                } catch (Exception e) {
                    Logger.loge(e.toString());
                }
                /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                Logger.logd("---------------------------------------------");

                notifyStateEvent(STATE_EVENT.INITIAL);
                mHolder.getFeatureUiHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        mHolder.onApplicationReady();
                    }
                });
                break;
        }
        return true;
    }

    private String getChkType(int type) {
        switch (type) {
            case SET_DEFAULT_VALUE:
                return "SET_DEFAULT_VALUE";
        }
        return "NORMAL"; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
    }

    private String getPrefString(int pref) {
        switch (pref) {
            case IMS_WIFI_PREF:
                return "IMS_WIFI_PREF";
            case IMS_WIFI_ONLY:
                return "IMS_WIFI_ONLY";
            case IMS_CELL_PREF:
                return "IMS_CELL_PREF";
        }
        return "ERROR";
    }

    /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
    private String getMacAddress() {
        String macAddr = null;
        try {
            Runtime runtime = Runtime.getRuntime();
            java.lang.Process proc = runtime.exec("cat /sys/class/net/wlan0/address");
            InputStream inputstream = proc.getInputStream();
            InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
            BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
            macAddr = bufferedreader.readLine().toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return macAddr;
    }
    /* MODIFIED-END by Yuan Yili,BUG-2419568*/
}
