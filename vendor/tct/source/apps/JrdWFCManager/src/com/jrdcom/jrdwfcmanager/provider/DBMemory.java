/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import com.android.ims.JrdIMSSettings; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
import com.jrdcom.jrdwfcmanager.common.Logger;
import java.lang.reflect.Field;

public class DBMemory implements IDataSource {
    // ------ columns ------
    private boolean isRegistered;
    private boolean isLowSignal;
    private boolean isOnCall;
    private String wfcSettingsState;
    // ---------------------

    private Context mContext;

    public DBMemory(Context context) {
        mContext = context;
    }

    @Override
    public String getType(Uri uri) {
        return JrdIMSSettings.MIME_TYPE_SETTINGS_ITEM; // MODIFIED by Yuan Yili, 2016-06-28,BUG-2419568
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri insertUri = null;
        if (values != null && values.size() >= 2) {
            try {
                /* MODIFIED-BEGIN by Yuan Yili, 2016-06-28,BUG-2419568*/
                String key = values.getAsString(JrdIMSSettings.NAME_CV_KEY);
                Object value = values.get(JrdIMSSettings.NAME_CV_VALUE);
                /* MODIFIED-END by Yuan Yili,BUG-2419568*/
                String fieldName = key.substring(1);
                Field column = getClass().getDeclaredField(fieldName);
                column.setAccessible(true);
                Class<?> type = column.getType();
                if (type.equals(String.class)) {
                    column.set(this, String.valueOf(value));
                } else {
                    column.set(this, value);
                }

                insertUri = Uri.withAppendedPath(uri, key);
                Logger.logd("DBMemory insert: " + key + "=" + value);
                notifyChange(insertUri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new RuntimeException("DBMemory not support delete!");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new RuntimeException("DBMemory not support update!");
    }

    @Override
    public Cursor query(Uri uri, String[] select, String selection, String[] selectionArgs, String sort) {
        if (selectionArgs == null || selectionArgs.length < 1) {
            throw new RuntimeException("query(), selectionArgs is empty.");
        }

        MatrixCursor cursor = null;
        String key = selectionArgs[0];
        Object value = null;
        try {
            String fieldName = key.substring(1);
            Field column = getClass().getDeclaredField(fieldName);
            Class<?> type = column.getType();
            if (type.equals(boolean.class)) {
                value = column.getBoolean(this) ? 1 : 0;
            } else if (type.equals(long.class)) {
                value = column.getLong(this);
            } else if (type.equals(float.class)) {
                value = column.getFloat(this);
            } else if (type.equals(int.class)) {
                value = column.getInt(this);
            } else if (type.equals(String.class)) {
                value = column.get(this);
            }

            String[] columnNames = new String[]{key};
            Object[] values = new Object[]{value};

            cursor = new MatrixCursor(columnNames);
            cursor.addRow(values);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Logger.logd("DBMemory query: " + key + "=" + value);
        return cursor;
    }

    /**
     * @param uri
     */
    private void notifyChange(Uri uri) {
        Logger.logd("DBMemory@notifyChange, notify uri: " + uri);
        mContext.getContentResolver().notifyChange(uri, null);
    }
}
