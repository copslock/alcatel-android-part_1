/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui;

import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.SystemClock;

public class SoundManager {
    private ToneGenerator mToneGenerator;

    public SoundManager() {
        try {
            mToneGenerator = new ToneGenerator(AudioManager.STREAM_VOICE_CALL, 85);
        } catch (RuntimeException e) {
            mToneGenerator = null;
        }
    }

    public void beep() {
        beep(ToneGenerator.TONE_PROP_BEEP2, 300);
    }

    private void beep(final int toneId, final int duration) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if ((mToneGenerator != null)) {
                    mToneGenerator.startTone(toneId);
                    SystemClock.sleep(duration);
                    mToneGenerator.stopTone();
                }
            }
        }).start();
    }
}