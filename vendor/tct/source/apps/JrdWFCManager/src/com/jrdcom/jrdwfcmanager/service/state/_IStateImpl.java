package com.jrdcom.jrdwfcmanager.service.state;

public interface _IStateImpl {
    _State onSystemLocaleChanged(_State.EventData eData);

    _State onCallDropAlert(_State.EventData eData);

    _State onOnCall(_State.EventData eData);

    _State onOffCall(_State.EventData eData);

    _State onWifiOn(_State.EventData eData);

    _State onWifiOff(_State.EventData eData);

    _State onWifiConnected(_State.EventData eData);

    _State onWifiDisconnected(_State.EventData eData);

    _State onPoorWifiSignal(_State.EventData eData);

    _State onAirplaneOn(_State.EventData eData);

    _State onAirplaneOff(_State.EventData eData);

    _State onRegistering(_State.EventData eData);

    _State onRegistered(_State.EventData eData);

    _State onRegerror(_State.EventData eData);

    _State onInvalidSim(_State.EventData eData);

    _State onEnabled(_State.EventData eData);

    _State onDisabled(_State.EventData eData);

    _State onWifiPref(_State.EventData eData);

    _State onWifiOnly(_State.EventData eData);

    _State onCellPref(_State.EventData eData);

    _State onNoCellEvents(_State.EventData eData);
}
