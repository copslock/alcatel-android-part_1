/** =========================================================================================== */
/**                                                                             Date:2015-06-15 */
/**                                      PRESENTATION                                           */
/**                                                                                             */
/**             Copyright 2015 TCL Communication Technology Holdings Limited.                   */
/**                                                                                             */
/**  This material is company confidential, cannot be reproduced in any form without the        */
/**  written permission of TCL Communication Technology Holdings Limited.                       */
/**                                                                                             */
/** ------------------------------------------------------------------------------------------- */
/**  Author :  bob.shen                                                                         */
/**  Email  :  bob.shen@tcl.com                                                                 */
/**  Role   :                                                                                   */
/**  Reference documents : 80-NU412-1.sv.pdf                                                    */
/** ------------------------------------------------------------------------------------------- */
/**  Comments :                                                                                 */
/**  Labels   :                                                                                 */
/** ------------------------------------------------------------------------------------------- */
/**     Modifications on Features list / Changes Request / Problems Report                      */
/** ------------------------------------------------------------------------------------------- */
/**    date    |        author        |         Key          |     comment                      */
/** -----------|----------------------|----------------------|--------------------------------- */
/** 2015-06-15 | bob.shen             | FR978429             | Create JrdWFCManager application */
/** ------------------------------------------------------------------------------------------- */
/** =========================================================================================== */
package com.jrdcom.jrdwfcmanager.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.jrdcom.jrdwfcmanager.R;
import com.jrdcom.jrdwfcmanager.common.ImplementorFactory;

public class Tutorial extends Activity implements View.OnClickListener {
    private static final String TAG = "Tutorial";

    private String[] contents;
    private TextView mUpperContent, mLowerContent;
    private Button mNextBtn, mDoneBtn;
    private ImageView mIcon;

    private Tutorial.IImplementor mImplementor;

    public Tutorial() {
        mImplementor = (Tutorial.IImplementor) ImplementorFactory.getImplementor(this.getClass(), this);
        if (mImplementor == null) {
            throw new RuntimeException("Tutorial's implementor is null.");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial);
        initTutorialView();

        if (savedInstanceState != null && savedInstanceState.getInt("page", -1) == 1) {
            setNextContent();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("page", (mNextBtn.getVisibility() == View.VISIBLE) ? 0 : 1);
    }

    private void initTutorialView() {
        mUpperContent = (TextView) findViewById(R.id.upper_content);
        mLowerContent = (TextView) findViewById(R.id.lower_content);
        mIcon = (ImageView) findViewById(R.id.icon);
        mNextBtn = (Button) findViewById(R.id.next_btn);
        mDoneBtn = (Button) findViewById(R.id.done_btn);
        contents = mImplementor.getContents();
        mImplementor.initActionBar(mIcon);
        mUpperContent.setText(contents[0]);
        mNextBtn.setOnClickListener(this);
        mDoneBtn.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == mNextBtn) {
            setNextContent();
        } else if (v == mDoneBtn) {
            finish();
        }
    }

    void setNextContent() {
        mUpperContent.setText(contents[1]);
        mLowerContent.setVisibility(View.VISIBLE);
        mIcon.setVisibility(View.VISIBLE);
        mIcon.setImageResource(R.drawable.ims_ready_icon);
        mNextBtn.setVisibility(View.GONE);
        mDoneBtn.setVisibility(View.VISIBLE);
    }


    /**
     * inner classes
     */
    public interface IImplementor extends ImplementorFactory.IImplementor {
        String[] getContents();

        void initActionBar(ImageView icon);
    }

}