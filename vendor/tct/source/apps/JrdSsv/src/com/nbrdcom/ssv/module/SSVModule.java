/**************************************************************************************************/
/*                                                                                 Date : 05/2011 */
/*                            PRESENTATION                                                        */
/*              Copyright (c) 2011 TCL Communications(Ningbo) R&D Center.                         */
/**************************************************************************************************/
/*                                                                                                */
/*    This material is company confidential, cannot be reproduced in any                          */
/*    form without the written permission of TCL Communications(Ningbo) R&D Center.               */
/*                                                                                                */
/*================================================================================================*/
/*   Author :  SHI Haojun                                                                         */
/*   Role   :  SSV                                                                                */
/*   Reference documents :                                                                        */
/*================================================================================================*/
/* Comments :                                                                                     */
/*     file    : SSVModule.java                                                                   */
/*     Labels  :                                                                                  */
/*================================================================================================*/
/* Modifications   (month/day/year)                                                               */
/*================================================================================================*/
/* date    |  author       |FeatureID                    |modification                            */
/*=========|===============|=============================|========================================*/
/*05/06/11 | SHI Haojun    |bug[211283]                  |Other modules must impl this interface  */
/*================================================================================================*/
/* Problems Report(PR/CR)                                                                         */
/*================================================================================================*/
/* date    | author        | PR #                        |                                        */
/*=========|===============|=============================|========================================*/
/*=========|===============|=============================|========================================*/
/*         |               |                             |                                        */
/*================================================================================================*/
package com.nbrdcom.ssv.module;

import java.util.Iterator;
import java.util.Map.Entry;

import android.content.Context;

/**
 * Every module must implement this interface. When new modules are extended
 * into SSV, you must implement this interface through your class. Implement
 * Rule: The class must be named as SSV[ModuleName]Module(e.g. SSVBrowserModule)
 * when it implements this interface and be positioned in the same package as
 * SSVModule, and also a public constructor without argument was mandatory
 * because SSV will reflect the class to finish its configuration.
 */
public interface SSVModule {
    /**
     * Configure its own module by implement this method. SSV will invoke this
     * method to finish the module configuration
     */
    public boolean configure(Context ssv);

    /**
     * SSV module builder
     */
    static class Builder {
        public static SSVModule create(String name) {
            SSVModule module = null;
            if (name != null && name.length() != 0) {
                try {
                    Class<?> moduleclass = Class
                            .forName("com.nbrdcom.ssv.module.SSV" + name
                                    + "Module");
                    module = (SSVModule) moduleclass.newInstance();
                } catch (IllegalAccessException e) {
                    module = null;
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    module = null;
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    module = null;
                    e.printStackTrace();
                }
            }
            return module;
        }
    }

    /**
     * Own module data
     */
    interface Data {
        int getInt(String key, int defValue);

        float getFloat(String key, float defValue);

        String getString(String key, String defValue);

        boolean getBoolean(String key, boolean defValue);

        Record[] getRecords(String key);

        boolean containsKey(String key);

        Variant get(String key);

        Iterator<Entry<String, Variant>> getAll();

        /**
         * Record data type
         */
        interface Record {
            int count();

            String getKey(int index, String defValue);

            String getValue(int index, String defValue);

            String getValue(String key, String defValue);
        }

        /**
         * all data will packaged to Variant
         */
        interface Variant {
            static final int TYPE_KNOWN = -1;
            static final int TYPE_INT = 0;
            static final int TYPE_FLOAT = 1;
            static final int TYPE_STRING = 2;
            static final int TYPE_BOOL = 3;
            static final int TYPE_RECORDS = 4;

            int getType();

            int toInt();

            float toFloat();

            String toString();

            boolean toBool();

            Record[] toRecords();
        }
    }
}
