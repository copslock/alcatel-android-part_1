package com.nbrdcom.ssv.module;

import java.io.File;
import java.io.IOException;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;

import com.jrdcom.ssv.SsvService;
import com.nbrdcom.ssv.SSVXmlParser;

public class SSVRingtoneModule implements SSVModule {
    private static final String TAG = "SSVRingtoneModule";
    public static final String MODULE = "Ringtone";
    public static final String ITEM = "audio_path";
    private String mCanonicalPath;
    private Handler mHandler = null;
    private Context mContext = null;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Uri uri = intent.getData();
            if(!uri.toString().startsWith("file:///system/media")) {
                // wait for internal storage is ready
                return;
            }

            // do configure
            // get uri of the file
            Uri contentUri = MediaStore.Audio.Media.getContentUriForPath(mCanonicalPath);
            ContentResolver resolver = context.getContentResolver();
            Uri ringtoneUri = null;
            String[] cols = new String[] { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA };
            String where = MediaStore.Audio.Media.DATA + "=?";
            String[] whereArgs = new String[] { mCanonicalPath };
            Cursor cursor = resolver.query(contentUri, cols, where, whereArgs, null);
            if (cursor != null && cursor.getCount() == 1) {
                // found media
                cursor.moveToFirst();
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
                // got it
                ringtoneUri = ContentUris.withAppendedId(contentUri, id);
                Log.v(TAG, "ringtone uri = " + ringtoneUri.toString());
                cursor.close();
            } else {
                // not found ?
                Log.e(TAG, "media not found ???");
                moduleFinished(false);
            }
            if (ringtoneUri != null) {
                // set as ringtone
                Settings.System.putString(resolver, Settings.System.RINGTONE,
                        ringtoneUri.toString());
                // update the IS_RINGTONE flag
                ContentValues values = new ContentValues();
                values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
                int rows = resolver.update(ringtoneUri, values, null, null);
                // TODO remove the old one (if it's not a standard ringtone)???
            }

            moduleFinished(true);
        }
    };

    private void moduleFinished(boolean success) {
        // unregister receiver
        mContext.unregisterReceiver(mReceiver);

        // send message to service to checkFinished()
        if (mHandler == null) {
            return;
        }

        Message msg = Message.obtain();
        if (success) {
            msg.what = SsvService.CONFIG_SUCCEED_MSG;
        } else {
            msg.what = SsvService.CONFIG_ERROR_MSG;
        }
        msg.obj = MODULE;
        mHandler.sendMessage(msg);
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    public boolean configure(Context context) {
        mContext = context;
        SSVModule.Data data = SSVXmlParser.retrieveParser().getModuleData(MODULE);
        if (data != null) {
            if (data.containsKey(ITEM)) {
                // get the file
                // TODO the default value should get from somewhere ???
                String filePath = data.getString(ITEM, "/system/media/audio/ringtones/Phone.ogg");
                File newRingtoneFile = new File(filePath);
                if (!newRingtoneFile.exists()) {
                    Log.e(TAG, "file not found: " + filePath);
                    return false;
                }
                mCanonicalPath = filePath;
                try {
                    // use canonical path to query db
                    mCanonicalPath = newRingtoneFile.getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "error on getCanonicalPath()");
                    return false;
                }

                // register a receiver for ACTION_MEDIA_SCANNER_FINISHED
                // configure will be finished in receiver
                IntentFilter filter = new IntentFilter(Intent.ACTION_MEDIA_SCANNER_FINISHED);
                filter.addDataScheme("file");
                context.registerReceiver(mReceiver, filter);
            }
        }

        return true;
    }

}
