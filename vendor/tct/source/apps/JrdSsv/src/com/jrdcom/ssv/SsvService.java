package com.jrdcom.ssv;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.Log;

import com.nbrdcom.ssv.SSVXmlParser;
import com.nbrdcom.ssv.module.SSVModule;
import com.nbrdcom.ssv.module.SSVRingtoneModule;
//Added by rifang.zhang.hz for PR580027 begin
import android.content.ContentResolver;
import android.database.ContentObserver;
import android.provider.Settings;

//Added by rifang.zhang.hz for PR580027 end

public class SsvService extends Service {
    private static final String TAG = "Jrd_SsvService";
    private String mSaveFile = "save_mccmncspn";

    private Context mContext = null;
    private SsvUtil mUtil;
    private int mTransition = SsvManager.TRANSITION_NONE;
    private int mDesiredState = SsvManager.STATE_ORIG;
    private boolean mSaveMccMnc = false;
    private static int mModuleTotal = 0;

    // Configuration succeed.
    public static final int CONFIG_SUCCEED_MSG = 1;
    // Error occur in one module configuration.
    public static final int CONFIG_ERROR_MSG = 2;
    // Finish to invoke all modules.
    public static final int CONFIG_FINISH_MSG = 3;

    // added by rifang.zhang.hz for PR580027 begin
    private Object mSettingsLock = new Object();
    private ContentResolver mContentResolver;
    private SettingsObserver mSettingsObserver;

    // added by rifang.zhang.hz for PR580027 end
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        mUtil = SsvUtil.getInstance();
        if (mUtil == null) {
            mUtil = SsvUtil.init(mContext);
        }

        // added by rifang.zhang.hz for PR580027 begin
        mContentResolver = this.getContentResolver();
        mSettingsObserver = new SettingsObserver();
        // Added by rifang.zhang.hz for PR580027 end
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            mTransition = intent.getIntExtra(SsvUtil.TRANSITION,
                    SsvManager.TRANSITION_NONE);
            try {
                mOnStartThread.start();
            } catch (IllegalThreadStateException ex) {
                Log.e(TAG, "thread has been started before.");
            }
        }
        return START_STICKY;
    }

    /**
     * Doing long-running operations, kick off a new thread to perform
     * configuration.
     */
    private final Thread mOnStartThread = new Thread(new Runnable() {
        public void run() {
            switch (mTransition) {
            case SsvManager.TRANSITION_ORIG_TO_DEF:
                mDesiredState = SsvManager.STATE_DEF;
                mSaveMccMnc = false;
                defaultConfiguration();
                break;

            // case SsvManager.TRANSITION_ORIG_TO_OP:
            // Modified by rifang.zhang.hz for PR565756 begin
            // case SsvManager.TRANSITION_DEF_TO_OP:
            // updateRintone();
            // case SsvManager.TRANSITION_ORIG_TO_SUB:
            // case SsvManager.TRANSITION_DEF_TO_OP:
            // Modified by rifang.zhang.hz for PR565756 end
            // case SsvManager.TRANSITION_DEF_TO_SUB:
            // mDesiredState = SsvManager.STATE_OP;
            // mSaveMccMnc = true;
            // operatorConfiguration();
            // break;

            case SsvManager.TRANSITION_ORIG_TO_OP:
            case SsvManager.TRANSITION_DEF_TO_OP:
                // Added by rifang.zhang.hz for PR580027 begig
                Settings.System.putInt(mContext.getContentResolver(),
                        "ringtoneSave", 1);
                // Added by rifang.zhang.hz for PR580027 end
                mDesiredState = SsvManager.STATE_OP;
                mSaveMccMnc = true;
                operatorConfiguration();
                break;

            case SsvManager.TRANSITION_ORIG_TO_SUB:
            case SsvManager.TRANSITION_DEF_TO_SUB:
                //add by peng.kuang
                Settings.System.putInt(mContext.getContentResolver(),
                        "ringtoneSave", 1);
                //add end by peng.kuang
                mDesiredState = SsvManager.STATE_SUB;
                mSaveMccMnc = true;
                operatorConfiguration();
                break;
            default:
                break;
            }
        }
    });

    private void defaultConfiguration() {

        // clear mccmnc
        mUtil.setPrevMccMnc("");
        // clear mccmnc for language customization
        mUtil.setLangMccMnc("");
        // set current state
        mUtil.setCurrentState(mDesiredState);
        // update mccmnc configuration
        mUtil.updateMccMncConfiguration(mUtil.getPrevMccMnc());

        // update ringtone
        // updateRintone();
        // need not to update language
        try {
            ssvApkFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void operatorConfiguration() {
        // we don't use module to configure ssv anymore

        // save mccmnc
        mUtil.setPrevMccMnc(mUtil.getSimMccMnc());
        // set current state
        mUtil.setCurrentState(mDesiredState);
        // update mccmnc configuration
        mUtil.updateMccMncConfiguration(mUtil.getPrevMccMnc());

        mUtil.setPreSimSpn(mUtil.getSimSpn());

        // update ringtone
        // updateRintone();
        // update language
        //updateLanguage();

        try {
            ssvApkFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void languageConfiguration() {
        // need not to update prev mcc
        // need not to update current state
        // update mccmnc configuration
        mUtil.updateMccMncConfiguration(mUtil.getPrevMccMnc());

        // need not to update ringtone
        // update language
        updateLanguage();

        try {
            ssvApkFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLanguage() {
        String langMccMnc = mUtil.getSimMccMnc();

        Log.d(TAG, "..........updateSystemConfigSSVLanguage: mccmnc="
                + langMccMnc);
        mUtil.setLangMccMnc(langMccMnc);
        // mContext.getResources().updateSystemConfigSSVLanguage(langMccMnc);
    }

    private void updateRintone() {
        // ringtone new solution
        Log.d(TAG, "start ringtone service");
        Intent i = new Intent(mContext, SsvRingtoneService.class);
        i.putExtra("userConfirmed", true);
        mContext.startService(i);
    }

    private void ssvApkFinished() throws IOException {
        Log.v(TAG, "=========SsvService Finished=========");
        Log.v(TAG, "set the properties for bootanimation");
        String mccmnc = mUtil.getSimMccMnc();
        String spn = mUtil.getSimSpn();
        if (!mccmnc.equals(" ") && !spn.equals(" ")) {
            String inforStr = mccmnc.substring(0, 3) + " "
                    + mccmnc.substring(3, mccmnc.length()) + " "
                    + mUtil.getSimSpn();
            SystemProperties.set("persist.sys.pre.mcccmncspn", inforStr);
        } else {
            SystemProperties.set("persist.sys.pre.mcccmncspn", "00 00 00");
        }
//Delete by yali.wang for PR715519
      //  SystemProperties.set("ctl.start", "updatelogo");

        Log.v(TAG, "stop SsvService by itself");
        SsvService.this.sendBroadcast(new Intent(
                SsvActivity.ACTION_JRD_SSV_SERVICE_FINISHED));
        SsvService.this.stopSelf();

        // Added the function to reboot begin
        Log.v(TAG, "reboot the system");
        Intent reboot = new Intent(Intent.ACTION_REBOOT);
        reboot.putExtra("nowait", 1);
        reboot.putExtra("interval", 1);
        reboot.putExtra("window", 0);
        sendBroadcast(reboot);
        // Added the function to reboot end

    }

    /**
     * perform configuration.
     */
    private void doConfig(String xmlFile) {
        if (xmlFile != null) {
            try {
                SSVXmlParser.parse(mContext, xmlFile, mListener);
            } catch (Exception e) {
                Log.e(TAG, "Exception" + e.getMessage());
            }
            sendConfigFinishedMsg(mModuleTotal);
        }
    }

    /**
     * Send CONFIG_FINISH_MSG when all modules invoked.
     */
    private void sendConfigFinishedMsg(int totalModule) {
        Message msg = Message.obtain();
        msg.what = CONFIG_FINISH_MSG;
        msg.obj = totalModule;
        mHandler.sendMessage(msg);
    }

    /**
     * Listen the xml when one module parse finished.
     */
    final SSVXmlParser.Listener mListener = new SSVXmlParser.Listener() {
        public void singleModuleParsed(String moduleName) {
            if (moduleName != null) {
                SSVModule module = SSVModule.Builder.create(moduleName);
                if (module != null) {
                    mModuleTotal++;
                    new SsvModuleStarter(module, moduleName).start();
                }
            }
        }
    };

    /**
     * Doing long-running operations, kick off a new thread to perform
     * configuration.
     */
    private class SsvModuleStarter extends Thread {
        private SSVModule mModule = null;
        private String mName = null;

        SsvModuleStarter(SSVModule module, String name) {
            mModule = module;
            mName = name;
        }

        public void run() {
            if (mModule instanceof SSVRingtoneModule) {
                // ringtone module has a delay
                SSVRingtoneModule module = (SSVRingtoneModule) mModule;
                module.setHandler(mHandler);
                if (module.configure(mContext)) {
                    // delay configure, don't send message here
                } else {
                    // config exactly failed, send CONFIG_ERROR_MSG
                    Message msg = Message.obtain();
                    msg.what = CONFIG_ERROR_MSG;
                    msg.obj = mName;
                    mHandler.sendMessage(msg);
                }
            } else {
                Message msg = Message.obtain();
                try {
                    if (mModule.configure(mContext)) {
                        msg.what = CONFIG_SUCCEED_MSG;
                    } else {
                        msg.what = CONFIG_ERROR_MSG;
                    }
                } catch (Exception e) {
                    msg.what = CONFIG_ERROR_MSG;
                    Log.e(TAG,
                            "Exception in mModule.configure(mContext); mName="
                                    + mName + e.getMessage());
                }
                msg.obj = mName;
                mHandler.sendMessage(msg);
            }
        }
    }

    /**
     * handle the message after one module configuration finished.
     */
    final Handler mHandler = new Handler() {
        private int mTotal = -1;
        private int mCount = 0;
        private boolean hasError = false;

        public void handleMessage(Message msg) {
            String moduleName = null;
            switch (msg.what) {
            case CONFIG_SUCCEED_MSG:
                mCount++;
                moduleName = (String) msg.obj;
                Log.v(TAG, "Module:" + moduleName + " CONFIG_SUCCEED_MSG");
                break;
            case CONFIG_ERROR_MSG:
                mCount++;
                moduleName = (String) msg.obj;
                hasError = true;
                Log.e(TAG, "Module:" + moduleName + " CONFIG_ERROR_MSG");
                break;
            case CONFIG_FINISH_MSG:
                mTotal = (Integer) msg.obj;
                Log.v(TAG, "CONFIG_FINISH_MSG: mTotal=" + mTotal);
                break;
            }
            checkFinished();
        }

        private void checkFinished() {
            Log.v(TAG, "checkFinished()...mTotal=" + mTotal + ", mCount="
                    + mCount);
            if (mTotal != -1 && mTotal <= mCount) {
                if (!hasError) {
                    // config successfully
                    if (mSaveMccMnc) {
                        // save mccmnc
                        mUtil.setPrevMccMnc(mUtil.getSimMccMnc());
                    } else {
                        // clear the mccmnc
                        mUtil.setPrevMccMnc("");
                    }
                    // set current state
                    mUtil.setCurrentState(mDesiredState);
                }
                // update mccmnc configuration
                mUtil.updateMccMncConfiguration(mUtil.getPrevMccMnc());

                Log.v(TAG, "=========Finished=========");
                SsvService.this.stopSelf();
            }
        }
    };

    // added by rifang.zhang.hz for PR580027 begin
    private class SettingsObserver extends ContentObserver {

        SettingsObserver() {
            super(new Handler());
            mContentResolver.registerContentObserver(
                    Settings.System.getUriFor("ssvringtone"), false, this);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            synchronized (mSettingsLock) {
                Log.d("zfang", "change ^^^^^^");
                int ssvringtone = Settings.System.getInt(
                        mContext.getContentResolver(), "ssvringtone", 0);
                if (1 == ssvringtone) {
                    try {
                        ssvApkFinished();
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        Settings.System.putInt(mContext.getContentResolver(),
                                "ssvringtone", 0);
                    }
                }
            }
        }
    }
    // added by rifang.zhang.hz for PR580027 end
}
