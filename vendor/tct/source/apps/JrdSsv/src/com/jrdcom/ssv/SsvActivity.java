/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 03/30/2015|     xian.wang        |       PR-960639      |[SSV]Length of significant*/
/*           |                      |                      |part of number for */
/*           |                      |                      |search in PHB is abnormal*/
/* ========================================================================== */
/* 05/08/2015|     jun.hou          |       PR-986880      |[SSV - AMX] To im-*/
/*           |                      |                      |plement configura-*/
/*           |                      |                      |tion in "Date &   */
/*           |                      |                      |time" according   */
/*           |                      |                      |customer requested*/
/* ========================================================================== */
/* 05/21/2015|     xian.wang        |       PR-1001171     |[Telefonica][SSV]*/
/*           |                      |                      |data roaming status */
/*           |                      |                      |is wrong for Peru*/
/* ----------|----------------------|----------------------|----------------- */
/* 07/29/2015|     xian.wang        |       PR-1051873     |[CLARO-SSV]Create */
/*           |                      |                      |a code to check   */
/*           |                      |                      |SSV version       */
/* ----------|----------------------|----------------------|----------------- */
/* 08/26/2015|     haibin.yu        |       FR-1067379     |SSV Design / Pre  */
/*           |                      |                      |-study For Idol4  */
/*           |                      |                      |                  */
/* ========================================================================== */
package com.jrdcom.ssv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SsvManager;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.telephony.PhoneNumberUtils;
import android.app.StatusBarManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.content.res.Resources;
import android.net.Uri;

import com.android.internal.telephony.MccTable;


import android.app.AlarmManager;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration;

import android.text.TextUtils;
import android.provider.Settings;
import android.provider.MediaStore;
import android.util.TctLog;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;


public class SsvActivity extends Activity {
    private static final String TAG = "SsvActivity";
    private PackageManager pm;
    private SsvManager ssv;
    private AppWidgetManager awm;
    private SsvUtil util;
    private int transition;
    private int mGetCardCount = 0;
    public static final String ACTION_JRD_SSV_SERVICE_FINISHED = "jrd.ssv.service_finished";
    public static final String ACTION_SIM_LOCK_DISMISSED = "com.jrdcom.ssv.sim_lock_dismissed";
    public static final int MSG_LOAD_RESOURCE_NONE = 0;
    public static final int MSG_LOAD_RESOURCE_NO_DIALOG = 1;
    public static final int MSG_LOAD_RESOURCE_DIALOG_BUTTON_YES = 2;
    public static final int MSG_LOAD_RESOURCE_DIALOG_BUTTON_NO = 3;
    public static final int MSG_DISPLAY_LOAD_DIALOG = 4;
    public static final int MSG_DISMISS_LOAD_DIALOG = 5;
    public static final int MSG_TEF_REBOOT_BUTTON_YES = 7;
    public static final int MSG_TEF_REBOOT_BUTTON_NO = 8;
    public static final int MSG_GMS_REBOOT_BUTTON_YES = MSG_TEF_REBOOT_BUTTON_NO + 1;
    public static final int MSG_FOC_REBOOT_BUTTON_YES = 10;
    public static final int SIM_STATE_READY = 5;
    private static final String MCCMNC_KEY = "persist.sys.mcc.mnc";
    private static final String LANG_MCCMNC_KEY = "persist.sys.lang.mccmnc";
    private static final String SPN_KEY = "persist.sys.sim.spn";
    private static final String IS_MCCMNC_CHANGE = "persist.sys.is.mcc.mnc.change";
    private static final String OPERATOR_SIM = "ro.ssv.operator.choose";
    private static final String PHONEBOOK_MIN_MATCH="persist.sys.phone.minmatch.len";
    private static final String ACTION_LOAD_RESOURCES="com.jrdcom.ssv.action.LOAD_RESOURCES";
    private static final String LANG_COUNTRY_KEY="persist.sys.lang.country";
    private String mCurrentMccMnc = "";
    private StatusBarManager mStatusBarManager ;
    public static String mOperatorSim = "";
    private HashMap<String, Integer> mapMCCStatus = new HashMap<String, Integer>();
    protected static final String SCREEN_OFF = "screen_off";
    private static final String HIDELOCK_FROM_SSVACTIVITY = "jrdssv.ssvactivity.hidelock";
    private static final String SHOWLOCK_FROM_SSVACTIVITY = "jrdssv.ssvactivity.showlock";

    private static final int DPM_CONFIG_MASK_FASTDORMANCY = 0x00000001;
    private static final int DPM_CONFIG_MASK_CT = 0x00000002;
    boolean ssvEnabled = false;

    private static final String HOURS_12 = "12";
    private static final String HOURS_24 = "24";
    private LoadSsvResource mLoadSsvResource = new LoadSsvResource(this);

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "main:  receive: action = " + action);
            if(ACTION_SIM_LOCK_DISMISSED.equals(action)||ACTION_JRD_SSV_SERVICE_FINISHED.equals(action)){
                ssvActivityDone();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter filter = new IntentFilter(ACTION_JRD_SSV_SERVICE_FINISHED);
        filter.addAction(ACTION_SIM_LOCK_DISMISSED);
        registerReceiver(mReceiver, filter);
        ssvEnabled = SystemProperties.getBoolean("ro.ssv.enabled", false);
        Log.d(TAG, "main:  ssvactivity started, the ssvenable=" + ssvEnabled);

        if (!ssvEnabled) {
            Log.d(TAG, "main:  ssvactivity is disable, return out");
            ssvActivityDone();
            return;
        }

        mOperatorSim = SystemProperties.get(OPERATOR_SIM, "TMO");
        Log.d(TAG, "details:   mOperatorSim=" + mOperatorSim);

        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        // init SsvUtil
        util = SsvUtil.init(this);
        pm = getPackageManager();
        awm = AppWidgetManager.getInstance(SsvActivity.this);
        ssv = SsvManager.getInstance();
        if(mStatusBarManager == null) {
            mStatusBarManager = (StatusBarManager) getApplication().getSystemService(Context.STATUS_BAR_SERVICE);
            hideNaviBar();
            //[BUGFIX]-Mod-BEGIN by TSNJ,tong.lu,1/31/2016,defect-1427530 -->
            SystemProperties.set("persist.sys.hideNaviBar", "true");
            //[BUGFIX]-Mod-END by TSNJ,tong.lu,1/31/2016,defect-1427530 -->
        }
        util.setSsvKeyGuardState(true);

        if (mOperatorSim.equals("TMO")) {
            viewInitTMO();
        }else if (mOperatorSim.equals("TEF")||mOperatorSim.equals("AMV")||mOperatorSim.equals("GMS")|| "FOC".equals(mOperatorSim)){
            viewInitTelefonica();
        } else {
            viewInitTelefonica();
        }
        getActionBar().setDisplayShowHomeEnabled(false);
        getActionBar().setDisplayUseLogoEnabled(false);
    }

    private void hideNaviBar() {
        int state = StatusBarManager.DISABLE_NONE;
        state |= StatusBarManager.DISABLE_EXPAND;
        state |= StatusBarManager.DISABLE_HOME;
        state |= StatusBarManager.DISABLE_RECENT;
        state |= StatusBarManager.DISABLE_BACK;
        mStatusBarManager.disable(state);
    }
    private void setLanguageFromMcc() {
        String mccmnc = "";
        int mcc = 0;
        mccmnc = ssv.getSimMccMnc();
        Log.d(TAG,"setLanguageFromMcc mccmnc == " + mccmnc);
        if (!mccmnc.equals("")) {
            try{
                mcc = Integer.parseInt(mccmnc.substring(0, 3));
            }catch (Exception e){
                Log.v(TAG, "Set AutoTimeZone Exception ");
                mcc = 111;
            }
        }
        String language = MccTable.defaultLanguageForMcc(mcc);
        String country = MccTable.countryCodeForMcc(mcc);
        if (language == null||country == "") {
            language = "en";
            country ="US";
        }
        SystemProperties.set("persist.sys.language", language);
        SystemProperties.set("persist.sys.country", country);
        Log.e(TAG, "setLanguageFromMcc,set language=" + language + ",country=" + country);
    }

    private void setWifiHotspotName(Context context, String strCurMccmnc) {
        if (mOperatorSim.equals("TEF")) {
            String hotspotEx = "70403,71030,70604,71204,71402";
            Log.d(TAG, "setWifiHotspotName strCurMccmnc : " + strCurMccmnc + ", hotspotEx = " + hotspotEx);
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiConfiguration config = wifiManager.getWifiApConfiguration();
            String[] hotspotExMccmnc = hotspotEx.split(",");
            if (hotspotExMccmnc != null && hotspotExMccmnc.length != 0) {
                for (int i = 0; i < hotspotExMccmnc.length; i++) {
                    if (hotspotExMccmnc[i].equals(strCurMccmnc)) {
                        config.SSID = getString(R.string.tef_wifi_hotspot_name);
                        wifiManager.setWifiApConfiguration(config);
                        Log.d(TAG,"setWifiHotspotName hotspotExMccmnc[i]= " +hotspotExMccmnc[i] +",hotspot_name=" +
                                getString(R.string.tef_wifi_hotspot_name) + ", config.SSID= " + config.SSID);
                    }
                }
            }
        }
    }
    private void setIsMccMncChange(boolean changestate) {
        String lastmccmnc = "";
        String isneedchange = "";
        lastmccmnc = SystemProperties.get(MCCMNC_KEY, "");
        if (changestate) {
            if (mCurrentMccMnc.equals(lastmccmnc)) {
                isneedchange = "false";
            } else {
                isneedchange = "true";
            }
        }
        if (!changestate) {
            isneedchange = "false";
        }
        Log.d(TAG,"setIsMccMncChange== "+ isneedchange + ", currentmccmnc== " +mCurrentMccMnc);
        SystemProperties.set(IS_MCCMNC_CHANGE, isneedchange);
    }

    private boolean checkIsNeedChange(boolean changestate) {
        String lastmccmnc = "";
        boolean isneedchange = false;
        lastmccmnc = SystemProperties.get(MCCMNC_KEY, "");
        Log.d(TAG, "checkIsNeedChange, mCurrentMccMnc==" + mCurrentMccMnc);
        if (changestate) {
            if (!mCurrentMccMnc.equals("") && mCurrentMccMnc.equals(lastmccmnc)) {
                isneedchange = false;
            } else {
                isneedchange = true;
            }
        }
        if (!changestate) {
            isneedchange = false;
        }
        return isneedchange;
    }

    private void viewInitTelefonica() {
        String nowmccmnc = ssv.getSimMccMnc();
        String everypremccmnc = ssv.getEveryPrevMccMnc();
        String noBtnState = "";
        Log.d(TAG, "nowmccmnc==========" + nowmccmnc);
        Log.d(TAG, "everypremccmnc==========" + everypremccmnc);

        if (!everypremccmnc.equals(nowmccmnc)) {
            util.setButtonNoNum(LoadSsvResource.VALUE_BTN_CLICK_NONE);
        }
        setContentView(R.layout.main);

        TextView body = (TextView) findViewById(R.id.body);
        String text = "";
        body.setTextSize(16);
            text = getString(R.string.notify_amv_firstboot);
        body.setText(text);

        Button yesButton = (Button) findViewById(R.id.btn_yes);
        yesButton.setText(getString(R.string.button_reboot));
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ("GMS".equalsIgnoreCase(mOperatorSim)) {
                    loadResource(MSG_GMS_REBOOT_BUTTON_YES);
                } else if ("FOC".equalsIgnoreCase(mOperatorSim)) {
                    loadResource(MSG_FOC_REBOOT_BUTTON_YES);
                } else {
                    loadResource(MSG_TEF_REBOOT_BUTTON_YES);
                }
            }
        });

        noBtnState = util.getButtonNoNum();
        Button noButton = (Button) findViewById(R.id.btn_no);
        noButton.setVisibility(View.GONE);
    }

    private void viewInitTMO() {
        transition = util.getTransition();

        setContentView(R.layout.main);

        TextView body = (TextView) findViewById(R.id.body);
        String text = "";
        String operateMcc = util.getSimMccMnc().substring(0,3);
                Log.d(TAG , "operateMcc =  " + operateMcc);
                if ((!operateMcc.equals("")) && operateMcc.equals("202")) {
                    text = getString(R.string.notify_cosmote_firstboot);
                } else {
                    text = getString(R.string.notify_firstboot);
                }
                body.setText(text);
                Button yesButton = (Button) findViewById(R.id.btn_yes);
                yesButton.setText(getString(R.string.next));
                yesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        loadResource(MSG_LOAD_RESOURCE_DIALOG_BUTTON_YES);
                    }
                });
                Button noButton = (Button) findViewById(R.id.btn_no);
                noButton.setVisibility(View.GONE);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // do not response BACK
        return;
    }

    private void ssvActivityDone() {
        Log.v(TAG, "=========SsvActivity Finished=========");
        if (ssvEnabled) {
            Message msg = Message.obtain();
            msg.what = MSG_DISMISS_LOAD_DIALOG;
            if (mLoadSsvResource.hasMessages(MSG_DISMISS_LOAD_DIALOG)) {
                mLoadSsvResource.removeMessages(MSG_DISMISS_LOAD_DIALOG);
            }
            mLoadSsvResource.sendMessage(msg);
        }
        if (ssvEnabled) {
            Log.d(TAG,"ssv.getSimMccMnc() ===== " + ssv.getSimMccMnc());
            ssv.setEveryPrevMccMnc(ssv.getSimMccMnc());//set the value of every_mccmnc
        }
        setAcitvityComponentNameDisable();
        if (util != null) {
            util.setSsvKeyGuardState(false);
        }
        if (ssvEnabled && (mStatusBarManager !=null )) {
            mStatusBarManager.disable(StatusBarManager.DISABLE_NONE);
        }
        finish();
    }

    //Add SSV control for Fastdormancy feature
    /*
    private void setDPMFeature(Context context) {
        String property =  SystemProperties.get("ro.ssv.enabled", "false");
        String  operatorChoose =  SystemProperties.get("ro.ssv.operator.choose", "");
        int dpmFeature = SystemProperties.getInt("persist.dpm.feature", 0);
        boolean isEnableFastDormancy = false;
        int dpmFeatureChanged = dpmFeature;
        Log.v(TAG, "FastDormancy, operator: " + operatorChoose);
        if (property.equals("true") &&  "AMV".equals(operatorChoose)) {
             String dormancyPerso = context.getResources().getString(com.android.internal.R.string.def_tctfw_ssv_fastDormancy);
             String mccmnc = SystemProperties.get("persist.sys.lang.mccmnc", "");
             Log.v(TAG, "FastDormancy, mccmnc:" + mccmnc);
             if (!TextUtils.isEmpty(mccmnc)) {
                if (!TextUtils.isEmpty(dormancyPerso)) {
                    String[] dormancyItems = dormancyPerso.split(",");
                    for (String item : dormancyItems) {
                        final String[] value = item.split("-");
                        if (value[0].equals(mccmnc)) {
                            Log.v(TAG, "FastDormancy, value[0]:" + value[0] + " value[1]:" + value[1]);
                            if ("1".equals(value[1])) {
                                 Log.v(TAG, "FastDormancy, isEnableFastDormancy true");
                                 isEnableFastDormancy = true;
                            } else {
                                 Log.v(TAG, "FastDormancy, isEnableFastDormancy false");
                                 isEnableFastDormancy = false;
                            }
                            break;
                        }
                    }
                }
             }
         }
         if(isEnableFastDormancy ) {
             Log.v(TAG,"FD required");
             if ((dpmFeature & DPM_CONFIG_MASK_FASTDORMANCY) == 0) {
                 dpmFeatureChanged |=  DPM_CONFIG_MASK_FASTDORMANCY;
             }
             if ((dpmFeature & DPM_CONFIG_MASK_CT) == 0) {
                 dpmFeatureChanged |=  DPM_CONFIG_MASK_CT;
             }
         }
         else {
             Log.v(TAG,"FD not required");
             if ( (dpmFeature & DPM_CONFIG_MASK_FASTDORMANCY) == DPM_CONFIG_MASK_FASTDORMANCY ) {
                  dpmFeatureChanged &= ~DPM_CONFIG_MASK_FASTDORMANCY;
             }
             if ( (dpmFeature & DPM_CONFIG_MASK_CT ) == DPM_CONFIG_MASK_CT) {
                  dpmFeatureChanged &= ~DPM_CONFIG_MASK_CT;
             }
         }

         Log.v(TAG,"dpmFeatureChanged = " + dpmFeatureChanged);
         if ( dpmFeatureChanged != dpmFeature) {
             Log.v(TAG,"modify perist.dpm.feature to " + dpmFeatureChanged);
             dpmFeature = dpmFeatureChanged;
             SystemProperties.set("persist.dpm.feature", String.valueOf(dpmFeature));
         }
    }
    */
    private void loadResource(int message) {
        Message msg = Message.obtain();
        msg.what = MSG_DISPLAY_LOAD_DIALOG;
        msg.arg1 = message;
        if (mLoadSsvResource.hasMessages(MSG_DISPLAY_LOAD_DIALOG)) {
            mLoadSsvResource.removeMessages(MSG_DISPLAY_LOAD_DIALOG);
        }
        mLoadSsvResource.sendMessage(msg);
    }

    private void setAcitvityComponentNameDisable(){
        ComponentName name = new ComponentName(SsvActivity.this, SsvActivity.class);
        try {
            pm.setComponentEnabledSetting(name,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        } catch (Exception e) {
            Log.e(TAG, "SSV,,,,set enabled settings....Exception:" + e.getMessage());
        }
    }

    class LoadSsvResource extends Handler {
        private Context mcontext;
        private ProgressDialog dialog;
        private static final String VALUE_BTN_CLICK_NONE = "nobuttonclick_none";
        private static final String VALUE_BTN_CLICK_FRIST = "nobuttonclick_frist";
        private static final String VALUE_BTN_CLICK_SECOND = "nobuttonclick_second";

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_LOAD_RESOURCE_NONE:
                // nothing to do
                // do not start ssv service, but update the configuration with
                // old mccmnc
                // util.updateMccMncConfiguration(util.getPrevMccMnc());
                ssvActivityDone();
                break;

            case MSG_LOAD_RESOURCE_NO_DIALOG:
                util.setNotifySimNextTime(true);
                // util.setNotifyLangNextTime(true);
                ssvActivityDone();
                break;

            case MSG_LOAD_RESOURCE_DIALOG_BUTTON_YES:
                // start service to do configuration
                util.setSwitchEnable(true);
                mCurrentMccMnc = util.getSimMccMnc();
                String mccspn = "";
                mccspn = mCurrentMccMnc.substring(0, 3) + SystemProperties.get("persist.sys.sim.spn","");
                Log.d(TAG,"mccspn == " + mccspn);
                SystemProperties.set(LANG_MCCMNC_KEY, mccspn);
                util.setLangMccMnc(mCurrentMccMnc);
                SystemProperties.set("persist.sys.tmossv.off", "true");
                Intent i = new Intent(SsvActivity.this, SsvService.class);
                i.putExtra(SsvUtil.TRANSITION, transition);
                startService(i);
                util.setNotifySimNextTime(false);
                //setWifiDefaultState();
                break;
            case MSG_GMS_REBOOT_BUTTON_YES:
                mCurrentMccMnc = util.getSimMccMnc();
                util.initSsvSettings();
                util.setSwitchEnable(true);
                util.setLangMccMnc(mCurrentMccMnc);
                util.setCurrentState(SsvManager.STATE_OP);
                SystemProperties.set(LANG_MCCMNC_KEY, mCurrentMccMnc);
                ssvActivityDone();
                // start to reboot
                Log.v(TAG, "reboot the system because MSG_GMS_REBOOT_BUTTON_YES");
                rebootTheDevices();
                break;
            case MSG_TEF_REBOOT_BUTTON_YES:
                //[BUGFIX]-Mod-BEGIN by TSNJ,tong.lu,2/1/2016,defect-1427530
                SystemProperties.set("persist.sys.hideNaviBar", "false");
                //[BUGFIX]-Mod-END by TSNJ,tong.lu,2/1/2016,defect-1427530
                mCurrentMccMnc = util.getSimMccMnc();
                util.initSsvSettings();
                util.setSwitchEnable(true);
                util.setLangMccMnc(mCurrentMccMnc);
                util.setCurrentState(SsvManager.STATE_OP);
                deleteMediaProvider();
                SystemProperties.set(LANG_MCCMNC_KEY, mCurrentMccMnc);
                setMccmncForBootAnimationValue(mCurrentMccMnc); //BUGFIX-ADD-BEGIN BY NJTS.WEI.HUANG 2015-01-25 FOR 1424663
                //setCountryNameByMcc();
                try {
                    Settings.System.putString(getContentResolver(),"ssvNeedChangedTone","true");
                    Settings.System.putString(getContentResolver(),"ssvNeedChangedConfigs","true");
                    Settings.System.putString(getContentResolver(),"ssvNeedChangedVoiceMail","true");
                } catch (Exception e)
                {
                    Log.e(TAG,"SSV set ssvNeedChangedTone error.");
                }

                //[BUGFIX]-Mod-BEGIN by TSNJ,kaibang.liu,1-20-2015, Defect: 1275762
                int provisioned = Settings.Global.getInt(getContentResolver(), Settings.Global.DEVICE_PROVISIONED, 0);
                int setupComplete = Settings.Secure.getInt(getContentResolver(), Settings.Secure.USER_SETUP_COMPLETE, 0);
                if (provisioned == 1 && setupComplete == 1) {
                    //setLanguageFromMcc();
                }
                //[BUGFIX]-Mod-END by TSNJ,kaibang.liu

                //SetRomaing(mcontext);
                //SetTimeStatus();
                //SetDeaultValAMV();
                //setWifiHotspotName(mcontext, mCurrentMccMnc);
                //setTimeZoneByMccmnc(mCurrentMccMnc);
                //setTimeFormatByMccmnc(mCurrentMccMnc);
                //setDPMFeature(mcontext);

                //setWifiDefaultState();
                Log.d(TAG, "case MSG_REBOOT_BUTTON_YES" + mCurrentMccMnc);
                // disable ourself (will be enabled again before AMS starting HOME)
                setAcitvityComponentNameDisable();

                ssvActivityDone();
                // start to reboot
                rebootTheDevices();
                break;

            case MSG_TEF_REBOOT_BUTTON_NO:
                util.setSsvKeyGuardState(false);
                setBtnNoState();
                ssvActivityDone();

            case MSG_LOAD_RESOURCE_DIALOG_BUTTON_NO:
                // do not start ssv service, but update the configuration with
                // old mccmnc
                util.setNotifySimNextTime(true);
                util.setCurrentState(SsvManager.STATE_DEF);
                util.setSwitchEnable(false);
                ssvActivityDone();
                break;

            case MSG_DISPLAY_LOAD_DIALOG:
                // show switch dialog
                String dialogTitle = mcontext.getResources().getString(
                        R.string.dialog_title);
                String dialogMessage = mcontext.getResources().getString(
                        R.string.dialog_message);
                dialog = ProgressDialog.show(mcontext, dialogTitle,
                        dialogMessage, true, false);

                Message msg1 = Message.obtain();
                Log.d(TAG, "msg.arg1=====" + msg.arg1);
                msg1.what = msg.arg1;
                if (mLoadSsvResource.hasMessages(msg1.what)) {
                    mLoadSsvResource.removeMessages(msg1.what);
                }
                mLoadSsvResource.sendMessageDelayed(msg1, 500);
                break;

            case MSG_DISMISS_LOAD_DIALOG:
                if (dialog != null) {
                    dialog.dismiss();
                    dialog = null;
                }
                break;
            case MSG_FOC_REBOOT_BUTTON_YES:
                ssv.setIsFirstRun(true);
                String mMccAlias = ssv.getFocMccAlias();
                Log.d(TAG, "details:  MSG_FOC_REBOOT_BUTTON_YES" + "     setProp persist.sys.lang.mccmnc:" + mMccAlias);
                util.initSsvSettings();//delete old the ssv.xml file
                util.setSwitchEnable(true);
                SystemProperties.set(LANG_MCCMNC_KEY, mMccAlias);
                util.setLangMccMnc(mMccAlias);
                util.setCurrentState(SsvManager.STATE_OP);
                ssvActivityDone();
                rebootTheDevices();
                break;
            }
        }

        public LoadSsvResource(Context context) {
            mcontext = context;
        }

        public void setBtnNoState() {
            String prevbtnnostate = "";
            prevbtnnostate = util.getButtonNoNum();
            if(prevbtnnostate.equals(VALUE_BTN_CLICK_NONE)){
                util.setButtonNoNum(VALUE_BTN_CLICK_FRIST);
            }else if(prevbtnnostate.equals(VALUE_BTN_CLICK_FRIST)){
                util.setButtonNoNum(VALUE_BTN_CLICK_SECOND);
            }
        }

    }

    private void rebootTheDevices() {
        Log.v(TAG, "reboot the system");
        Intent reboot = new Intent(Intent.ACTION_REBOOT);
        reboot.putExtra("nowait", 1);
        reboot.putExtra("interval", 1);
        reboot.putExtra("window", 0);
        sendBroadcast(reboot);
    }

    private void setTimeOrTimeZone(boolean autoTimeEnabled, boolean autoZoneEnabled){
       Settings.Global.putInt(getContentResolver(), Settings.Global.AUTO_TIME,
                autoTimeEnabled ? 1 : 0);
       Settings.Global.putInt(
                getContentResolver(), Settings.Global.AUTO_TIME_ZONE, autoZoneEnabled ? 1 : 0);
    }
    //[BUGFIX]-Mod-END by TSNJ,kaibang.liu,
    //[BUG-FIX]-ADD-BEGIN by xiaowei.yang-nb,12/03/2015,Task-1035092
    private void deleteMediaProvider()
    {
        String premccmnc = SystemProperties.get("persist.sys.lang.mccmnc","");
        if(!"".equals(premccmnc)) {
            getContentResolver().delete(MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
                    MediaStore.Audio.Media.DATA + " like ?", new String[]{"%" + premccmnc + "%"});
            TctLog.d(TAG, "delteMediaProvider success!");
        }
    }
    //[BUG-FIX]-ADD-END by xiaowei.yang-nb,12/03/2015,Task-1035092

    //BUGFIX-ADD-BEGIN BY NJTS.WEI.HUANG 2015-01-25 FOR 1424663
    private void setMccmncForBootAnimationValue(String mccmnc){
        String bootSsvMccMnc = "ssv_boot_mccmnc";
        boolean isSetSuccess = setBootValue(bootSsvMccMnc,mccmnc);
        if(isSetSuccess){
            Log.d(TAG,"setBootValue success!  mccmnc = "+ mccmnc);
        }else{
            Log.d(TAG,"setBootValue failed!  mccmnc = "+ mccmnc);
        }
    }

    private boolean setBootValue(String name , String value){
        File boot_value= new File("/cache/boot_value/");
        if(!boot_value.exists() && !boot_value.isDirectory()){
            boot_value.mkdir();
        }
        try {
            FileWriter fileWriter = new FileWriter("/cache/boot_value/"+name);
            BufferedWriter buffuerWriter = new BufferedWriter(fileWriter);
            buffuerWriter.write(value);
            buffuerWriter.flush();
            buffuerWriter.close();
            fileWriter.close();
        } catch (Exception e) {
            Log.e(TAG, "setBootValue failed:" + e);
            return false;
        }
        return true;
    }
    //BUGFIX-ADD-END BY NJTS.WEI.HUANG 2015-01-25
}
