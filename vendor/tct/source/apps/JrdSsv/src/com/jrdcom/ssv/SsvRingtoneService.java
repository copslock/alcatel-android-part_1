package com.jrdcom.ssv;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
//Added by rifang.zhang.hz for PR565756/FR580027 begin
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.os.SsvManager;

//Added by rifang.zhang.hz for PR565756/FR580027 end

public class SsvRingtoneService extends Service {
    public static final String PROP_INTERNAL_MEDIA_READY = "service.internal_media.ready";
    private static final String TAG = "SsvRingtoneService";

    private boolean mIsUserConfirmed = false;

    private static final int MSG_CHECK = 1;
    private static final int DELAY_MS = 500;
    // Added by rifang.zhang.hz for PR565756 end
    private Context mContext = null;
    // Added by rifang.zhang.hz for PR565756 end
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_CHECK:
                Log.d(TAG, "handle message MSG_CHECK...");
                boolean isInternalMediaReady = SystemProperties.getBoolean(
                        PROP_INTERNAL_MEDIA_READY, false);
                if (isInternalMediaReady) {
                    Log.d(TAG, "    ready to update ringtone");
                    updateRingtone();
                    // exit
                    SsvRingtoneService.this.stopSelf();
                } else {
                    Log.d(TAG,
                            "    internal media is not ready yet, check later");
                    mHandler.removeMessages(MSG_CHECK);
                    mHandler.sendEmptyMessageDelayed(MSG_CHECK, DELAY_MS);
                }
                break;
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Added by rifang.zhang.hz for PR565756 begin
        mContext = this.getApplicationContext();
        // Added by rifang.zhang.hz for PR565756 end
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        synchronized (this) {
            Log.d(TAG, "onStartCommand()");
            if (!mIsUserConfirmed) {
                mIsUserConfirmed = intent.getBooleanExtra("userConfirmed",
                        false);
            }

            boolean startedByMediaReady = intent.getBooleanExtra("mediaReady",
                    false);
            if (mIsUserConfirmed) {
                // check if we can update ringtone now
                Log.d(TAG, "mIsUserConfirmed true");
                check();
            } else if (startedByMediaReady) {
                // service started by receiver when media ready, but user had
                // not confirmed to update ringtone, stop service in this case
                stopSelf();
            }
        }
        return START_STICKY;
    }

    private void check() {
        boolean isInternalMediaReady = SystemProperties.getBoolean(
                PROP_INTERNAL_MEDIA_READY, false);
        if (isInternalMediaReady) {
            Log.d(TAG, "    1 ready to update ringtone");
            updateRingtone();
            // Added by rifang.zhang.hz for PR565756 begin
            Intent intent = new Intent("Intent.Action.Ssv.UpdateRingtone");
            mContext.sendBroadcast(intent);
            // Added by rifang.zhang.hz for PR565756 begin
            // exit after update ringtone
            stopSelf();
        } else {
            // Added by rifang.zhang.hz for PR565756 begin
            Log.d(TAG, "send check msg");
            if (mHandler.hasMessages(MSG_CHECK)) {
                mHandler.removeMessages(MSG_CHECK);
            }
            mHandler.sendEmptyMessageDelayed(MSG_CHECK, DELAY_MS);
            // Added by rifang.zhang.hz for PR565756 end
        }
    }

    // Modified by rifang.zhang.hz for PR565756/FR580027 begin
    private void updateRingtone() {
        Log.d(TAG, "updateRingtone()");
        // do update ringtone

        // get ringtone file
        /*
         * String fileName = "Buoy.ogg"; String path =
         * Environment.getCustpackDirectory() + "/JRD_custres/audio/ringtones/"
         * + fileName; Log.d(TAG, "ringtone path=" + path);
         *
         * // get uri of the file Uri ringtoneUri = getRingtoneUri(path);
         * Log.d(TAG, "ringtoneUri=" + ringtoneUri.toString()); if (ringtoneUri
         * != null) { // set as ringtone ContentResolver resolver =
         * getContentResolver(); Settings.System.putString(resolver,
         * Settings.System.RINGTONE, ringtoneUri.toString()); // update the
         * IS_RINGTONE flag ContentValues values = new ContentValues();
         * values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
         * resolver.update(ringtoneUri, values, null, null); }
         */

        String fileName = null;
        String path = null;
        ContentResolver resolver = getContentResolver();
        Resources res = this.getApplicationContext().getResources();
        int id = 0;
//        id = com.android.internal.R.string.def_ssv_ringtone_voice_call;
//        doUpdateRingtone(id, Settings.System.RINGTONE,
//                RingtoneManager.TYPE_RINGTONE);
//        id = com.android.internal.R.string.def_ssv_ringtone_video_call;
//        doUpdateRingtone(id, Settings.System.RINGTONE,
//                RingtoneManager.TYPE_RINGTONE);
        // doUpdateRingtone(id, Settings.System.VIDEO_CALL,
        // RingtoneManager.TYPE_RINGTONE);

        //id = com.android.internal.R.string.def_ssv_ringtone2_voice_call;
        //doUpdateRingtone(id, Settings.System.RINGTONE,
        //        RingtoneManager.TYPE_RINGTONE);
        // doUpdateRingtone(id, Settings.System.RINGTONE2,
        // RingtoneManager.TYPE_RINGTONE);
        //id = com.android.internal.R.string.def_ssv_ringtone2_video_call;
        //doUpdateRingtone(id, Settings.System.RINGTONE,
        //        RingtoneManager.TYPE_RINGTONE);
        // doUpdateRingtone(id, Settings.System.VIDEO_CALL2,
        // RingtoneManager.TYPE_RINGTONE);

//        id = com.android.internal.R.string.def_ssv_ringtone_notification;
//        doUpdateRingtone(id, Settings.System.NOTIFICATION_SOUND,
//                RingtoneManager.TYPE_NOTIFICATION);
//
//        id = com.android.internal.R.string.def_ssv_notify_download;
//        doUpdateRingtone(id, "downloadringtone",
//                RingtoneManager.TYPE_NOTIFICATION);
//
//        id = com.android.internal.R.string.def_ssv_config_alarm;
//        doUpdateRingtone(id, "alarmringtone", RingtoneManager.TYPE_ALARM);
    }

    // Modified by rifang.zhang.hz for PR565756/FR580027 end

    private Uri getRingtoneUri(String path) {
        Uri ringtoneUri = null;

        Uri contentUri = MediaStore.Audio.Media.getContentUriForPath(path);
        ContentResolver resolver = getContentResolver();
        String[] cols = new String[] { MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.DATA };
        String where = MediaStore.Audio.Media.DATA + "=?";
        String[] whereArgs = new String[] { path };
        Cursor cursor = resolver
                .query(contentUri, cols, where, whereArgs, null);

        if (cursor != null) {
            if (cursor.getCount() == 0) {
                Log.e(TAG, "ringtone not found(count == 0)");
                cursor.close();
            } else {
                // got it
                cursor.moveToFirst();
                int id = cursor.getInt(cursor
                        .getColumnIndex(MediaStore.Audio.Media._ID));
                ringtoneUri = ContentUris.withAppendedId(contentUri, id);
                cursor.close();
            }
        } else {
            // not found ?
            Log.e(TAG, "ringtone not found(cursor is null)");
        }

        return ringtoneUri;
    }

    // Added by rifang.zhang.hz for PR565756/FR580027 begin
    void doUpdateRingtone(int id, String settingsName, int type) {
        String fileName = null;
        String path = null;
        String ringtonePath = null;
        ContentResolver resolver = getContentResolver();
        Resources res = this.getApplicationContext().getResources();
        Uri ringtoneUri = null;

        SsvManager mSsvManager = (SsvManager) mContext.getSystemService("ssv");
        int transition = mSsvManager.getTransition();
        int state = SsvManager.STATE_ORIG;
        switch (transition) {
        case SsvManager.TRANSITION_ORIG_TO_OP:
        case SsvManager.TRANSITION_DEF_TO_OP:
            Settings.System.putString(resolver, settingsName + "Name",
                    res.getString(id));
            Settings.System.putInt(mContext.getContentResolver(),
                    "ringtoneSave", 1);
            path = "460998";
            break;
        case SsvManager.TRANSITION_ORIG_TO_SUB:
        case SsvManager.TRANSITION_DEF_TO_SUB:
            if (!(settingsName.equals("downloadringtone") || settingsName
                    .equals("alarmringtone"))) {
                Log.d(TAG, "change to sub only set download ringtone");
                return;
            }
            Settings.System.putString(resolver, settingsName + "Name",
                    res.getString(id));
            break;
        default:
            break;
        }

        fileName = res.getString(id);
        if (type == RingtoneManager.TYPE_RINGTONE) {
            if (path == null) {
                //ringtonePath = Environment.getCustpackDirectory()
                //        + "/JRD_custres/audio/ringtones/" + fileName;
            } else {
                //ringtonePath = Environment.getCustpackDirectory()
                //        + "/JRD_custres/audio/ringtones/460998/" + fileName;
            }
        } else if (type == RingtoneManager.TYPE_NOTIFICATION) {
            if (path == null) {
                //ringtonePath = Environment.getCustpackDirectory()
                //        + "/JRD_custres/audio/notifications/" + fileName;
            } else {
                //ringtonePath = Environment.getCustpackDirectory()
                //        + "/JRD_custres/audio/notifications/460998/" + fileName;
            }
        } else if (type == RingtoneManager.TYPE_ALARM) {
            //ringtonePath = Environment.getCustpackDirectory()
            //        + "/JRD_custres/audio/alarms/" + fileName;
        }
        Log.d(TAG, "fileName = " + fileName + ", path = " + path
                + ", ringtonePath = " + ringtonePath);

        ringtoneUri = getUri(ringtonePath);
        if (ringtoneUri != null) {
            Log.d(TAG, "ringtoneUri=" + ringtoneUri.toString()
                    + ", fileName = " + fileName + ", ringtonePath = "
                    + ringtonePath);
            Settings.System.putString(resolver, settingsName,
                    ringtoneUri.toString());
            // update the IS_RINGTONE flag
            ContentValues values = new ContentValues();
            if (type == RingtoneManager.TYPE_RINGTONE) {
                values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
            } else if (type == RingtoneManager.TYPE_NOTIFICATION) {
                values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
            } else if (type == RingtoneManager.TYPE_ALARM) {
                values.put(MediaStore.Audio.Media.IS_ALARM, true);
            }
            resolver.update(ringtoneUri, values, null, null);
        } else {
            Log.d(TAG, "ringtoneUri is null, do nothing");
        }
    }

    private Uri getUri(String path) {
        String uriString;
        Cursor internalCursor = getInternalRingtones(path);
        if (internalCursor != null) {
            if (internalCursor.getCount() > 0) {
                internalCursor.moveToFirst();
                uriString = MediaStore.Audio.Media.INTERNAL_CONTENT_URI
                        + "/"
                        + internalCursor.getString(internalCursor
                                .getColumnIndex("_id"));
                internalCursor.close();
                return Uri.parse(uriString);
            }
            internalCursor.close();
        }

        Cursor mediaCursor = getMediaRingtones(path);
        if (mediaCursor != null) {
            if (mediaCursor.getCount() > 0) {
                mediaCursor.moveToFirst();
                uriString = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                        + "/"
                        + mediaCursor.getString(mediaCursor
                                .getColumnIndex("_id"));
                mediaCursor.close();
                return Uri.parse(uriString);
            }
            mediaCursor.close();
        }
        return null;
    }

    private Cursor getInternalRingtones(String path) {
        return mContext.getContentResolver().query(
                MediaStore.Audio.Media.INTERNAL_CONTENT_URI,
                new String[] {
                        MediaStore.Audio.Media._ID,
                        MediaStore.Audio.Media.DATA,
                        "\"" + MediaStore.Audio.Media.INTERNAL_CONTENT_URI
                                + "\"", MediaStore.Audio.Media.TITLE_KEY },
                "_data=?", new String[] { path },
                MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }

    private Cursor getMediaRingtones(String path) {
        final String status = Environment.getExternalStorageState();

        return (status.equals(Environment.MEDIA_MOUNTED) || status
                .equals(Environment.MEDIA_MOUNTED_READ_ONLY)) ? mContext
                .getContentResolver().query(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        new String[] { MediaStore.Audio.Media._ID,
                                MediaStore.Audio.Media.DATA,
                                MediaStore.Audio.Media.TITLE_KEY }, "_data=?",
                        new String[] { path },
                        MediaStore.Audio.Media.DEFAULT_SORT_ORDER) : null;
    }
    // Added by rifang.zhang.hz for PR565756/FR580027 end
}
