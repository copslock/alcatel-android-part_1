/******************************************************************************/
/*                                                               Date:10/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  Yongzhen.wang                                                   */
/*  Email  :  null                                                            */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     : vendor/tct/source/apps/TctLogFilter                            */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 10/17/2016|    Yongzhen.wang     |    defect-3014708    |[Telecom][Intern- */
/*           |                      |                      |al]Remove invalid */
/*           |                      |                      | logs information */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/
package com.tct.LogFilter;


import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.provider.Settings;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;

public class LogFilterReceiver extends BroadcastReceiver {
    private static final String TAG = "LogFilterReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive " + intent);

        String action = intent.getAction();
        String host = intent.getData() != null ? intent.getData().getHost() : null;

        //log filter,*#*#7457#*#*
        if (TelephonyIntents.SECRET_CODE_ACTION.equals(action) && "7457".equals(host)) {
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setClassName("com.tct.LogFilter","com.tct.LogFilter.LogFilterActivity");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
        //[BUGFIX]-Add-BEGIN by TCTNB.Xijun.Zhang,11/14/2016,Defect-3408797
        //Secret code for network setting test mode
        else if (TelephonyIntents.SECRET_CODE_ACTION.equals(action) && "789".equals(host)) {
            if (Settings.System.getInt(context.getContentResolver(), "network_settings_test_mode_flag", 0) == 0) {
                Settings.System.putInt(context.getContentResolver(), "network_settings_test_mode_flag", 1);
            } else {
                Settings.System.putInt(context.getContentResolver(), "network_settings_test_mode_flag", 0);
            }
        }
        //[BUGFIX]-Add-END by TCTNB.Xijun.Zhang,11/14/2016,Defect-3408797
    }
}
