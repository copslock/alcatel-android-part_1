/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/14/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.privacymode;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.Service;
import android.app.ActivityManager.RecentTaskInfo;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.CallLog;
import android.provider.Settings;
import tct.util.privacymode.ITctPrivacyModeService;
import tct.util.privacymode.ITctPrivacyModeResult;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.hardware.FileLock;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.zhong.myclient.contentprovider.DatabaseOperate;

import android.provider.MediaStore;

public class PrivacyModeService extends Service {
    private static final String TAG = "PrivacyModeService";

    private int RESULT_OK = 0;
    private int RESULT_FAILED = 1;

    private int OWNER_MODE = 0;
    private int PRIVACY_MODE = 1;

    private IBinder mBinder = new ServiceStub(this);

    private Handler mHandler = new Handler();
    private Context mContext;

    static class ServiceStub extends ITctPrivacyModeService.Stub {
        private PrivacyModeService mService;

        protected ServiceStub(PrivacyModeService service) {
            mService = service;
        }

        @Override
        public void enterPrivacyMode(boolean isPrivacy, IBinder resultListener) {
            mService.enterPrivacyMode(isPrivacy, ITctPrivacyModeResult.Stub.asInterface(resultListener));
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    protected void enterPrivacyMode(final boolean isPrivacy, final ITctPrivacyModeResult resultListener) {
        mContext = this.getApplicationContext();
        //Log.i(TAG, "enterPrivacyMode()");
        mHandler.post(new Runnable(){
            @Override
            public void run() {
                int result = enterPrivacyMode(isPrivacy);
                //Settings.System.putInt(getContentResolver(), Settings.System.CURRENT_PHONE_MODE, isPrivacy ? 1 : 0);
                //int result = RESULT_OK;
                setResult(resultListener, result);
            }});
    }

    protected void setResult(ITctPrivacyModeResult resultListener, int result) {
        //Log.i(TAG, "setResult()# result: " + result);
        if (resultListener != null) {
            try {
                Log.i(TAG, "feedback result result = " + result);
                resultListener.onResult(result);
            } catch (RemoteException e) {
                Log.e(TAG, "feedback result failed");
            }
        }
    }

    protected int enterPrivacyMode(boolean isPrivacy) {
        int result = RESULT_OK;
        ContentResolver resolver = this.getContentResolver();
        int preMode = Settings.System.getInt(resolver, Settings.System.CURRENT_PHONE_MODE, 0);
        int newMode = isPrivacy ? 1 : 0;
        Log.i("ccxccx", "enterPrivacyMode start preMode = " + preMode + ", newMode = " + newMode);
        Long start = System.currentTimeMillis();
        FileLock fl = new FileLock();
        if (preMode != newMode) {
            if (!isPrivacy && tctLauncherIsHomeDefault()) {
                //backup tct launcher workspace
                try {
                    DatabaseOperate.backup(mContext);
                } catch (Exception e) {
                    //Log.i("ccxccx", " back up error e = " + e);
                }
            }
            Cursor c = resolver.query(PrivacyModeProvider.CONTENT_URI, new String[]{PrivacyModeProvider.PACKAGE_NAME}, null, null, null);
            if (c != null) {
                try {
                    PackageManager pm = mContext.getPackageManager();
                    NotificationManager nm =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    while (c.moveToNext()) {
                        //Log.i("ccxccx", "enterPrivacyMode name = " + c.getString(0) + ", isPrivacy = " + isPrivacy);
                        try {
                            if (isPrivacy) {
                                pm.setApplicationEnabledSetting(c.getString(0), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, 0);
                            } else {
                                pm.setApplicationEnabledSetting(c.getString(0), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 0);
                                nm.cancelApplicationNotification(c.getString(0));
                            }
                        } catch (Exception e) {
                            Log.i("ccxccx", "enterPrivacyMode disable enable error = " + e);
                        }
                    }
                } catch (Exception e) {
                    result = RESULT_FAILED;
                    Log.i("ccxccx", "enterPrivacyMode name = " + c.getString(0) + ", failed = " + e);
                } finally {
                    c.close();
                }
                Log.i("ccxccx", "enable/disable app cost time " + (System.currentTimeMillis() - start));
            }

            if (newMode == 0) {
                Settings.System.putLong(resolver, Settings.System.SWITCH_TO_NORMAL_MODE_TIMESTAMP, System.currentTimeMillis());
                clearRecentApps(mContext);
            } else {
                //private mode, show private files
                fl.unHideAnyFile();
            }
            resolver.notifyChange(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null);
            resolver.notifyChange(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null);
            resolver.notifyChange(CallLog.CONTENT_URI, null);
            Settings.System.putInt(resolver, Settings.System.CURRENT_PHONE_MODE, newMode);
            //send mode change broadcast
            Intent intent = new Intent("android.intent.actions.CURRENT_MODE_CHANGED");
            intent.putExtra("current_mode", newMode);
            sendBroadcast(intent);
        }
        if (newMode == 0) {
            //normal mode, hide private files, filelock will unHideAnyFile when reboot phone
            fl.hideLockFile();
        }
        Log.e("ccxccx", "enterPrivacyMode end result = " + ((result == RESULT_OK) ? "success" : "failed") + ", time = " + (System.currentTimeMillis() - start));
        return result;
    }

    private void clearRecentApps(Context context) {
        long startTime = System.currentTimeMillis();
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        PackageManager pm = mContext.getPackageManager();

        final List<ActivityManager.RecentTaskInfo> recentTasks = am.getRecentTasks(21,
                ActivityManager.RECENT_IGNORE_UNAVAILABLE);
        //long start2 = System.currentTimeMillis();
        //Log.v("ccxccx", "time1 " + (start2 - startTime));
        ActivityInfo homeInfo = new Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME).resolveActivityInfo(pm, 0);

        //Log.d("ccxccx", "clearRecentApps size " + recentTasks.size());
        for (RecentTaskInfo recentTaskInfo : recentTasks) {
            if (recentTaskInfo.baseIntent == null)
                continue;

            Intent intent = new Intent(recentTaskInfo.baseIntent);
            if (recentTaskInfo.origActivity != null) {
                intent.setComponent(recentTaskInfo.origActivity);
            }
            String packageName = intent.getComponent().getPackageName();
            //Log.i("ccxccx", "packageName " + packageName + ", action = " + intent.getAction());

            // Don't kill the current home activity.
            if (isCurrentHomeActivity(intent.getComponent(), homeInfo)) {
                continue;
            }

            boolean re = am.removeTask(recentTaskInfo.persistentId);
            //Log.i("ccxccx", "rm packageName " + packageName + ", re = " + re);
        }
        Log.w("ccxccx", "clear app cost time = " + (System.currentTimeMillis() - startTime));
    }

    private boolean isCurrentHomeActivity(ComponentName component,
            ActivityInfo homeInfo) {
        if (homeInfo == null) {
            final PackageManager pm = mContext.getPackageManager();
            homeInfo = new Intent(Intent.ACTION_MAIN).addCategory(
                    Intent.CATEGORY_HOME).resolveActivityInfo(pm, 0);
        }
        return homeInfo != null
                && homeInfo.packageName.equals(component.getPackageName())
                && homeInfo.name.equals(component.getClassName());
    }

    private boolean tctLauncherIsHomeDefault() {
        String tct_package = "com.tct.launcher";
        ArrayList<ResolveInfo> homeActivities = new ArrayList<ResolveInfo>();
        PackageManager pm = mContext.getPackageManager();
        ComponentName def = pm.getHomeActivities(homeActivities);

        boolean isDefault = false;
        if (def != null) {
            isDefault = tct_package.equals(def.getPackageName());
        } else if (homeActivities.size() == 1) {
            ResolveInfo info = homeActivities.get(0);
            if (info.activityInfo != null) {
                isDefault = tct_package.equals(info.activityInfo.packageName);
            }
        }

        return isDefault;
    }
}
