/******************************************************************************/
/*                                                               Date:03/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 03/07/2016|     caixia.chen      |     task 1427498     |访客模式              */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.privacymode;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import com.tct.privacymode.photo.ContactPhotoManager;
import com.tct.privacymode.photo.ContactPhotoManager.DefaultImageRequest;

import android.app.ActionBar;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.RawContacts;
import android.provider.Settings.System;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.Button;

public class AddPrivacyContact extends ListActivity implements OnClickListener, OnQueryTextListener {
    private static final int QUERY_TOKEN = 628;
    private boolean alreadyUpdate = false;

    static final String[] CONTACTS_SUMMARY_PROJECTION = new String[] {
        Contacts._ID, // 0
        Contacts.DISPLAY_NAME_PRIMARY, // 1
        Contacts.DISPLAY_NAME_ALTERNATIVE, // 2
        Contacts.PHOTO_ID, // 3
        Contacts.LOOKUP_KEY, // 4
        Contacts.IS_PRIVATE //5
    };

    private static final int CONTACT_ID_COLUMN_INDEX = 0;
    private static final int DISPLAY_NAME_PRIMARY_COLUMN_INDEX = 1;
    private static final int DISPLAY_NAME_ALTERNATIVE_COLUMN_INDEX = 2;
    private static final int PHOTO_ID_COLUMN_INDEX = 3;
    private static final int LOOKUP_KEY_COLUMN_INDEX = 4;
    private static final int IS_PRIVATE_COLUMN_INDEX = 5;

    private ContactItemListAdapter mAdapter;
    private QueryHandler mQueryHandler;
    private Context mContext;
    private TextView mSelectAll;
    private TextView mTitle;
    private ContentResolver mResolver;
    private View mEmptyView;
    public ProgressDialog mProgressDialog;

    private ArrayList<String> mChoiceSet = new ArrayList<String>();
    private ContentObserver mModeObserver;
    private boolean mShouldDestroy = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        setContentView(R.layout.add_privacy_contact);

        setActionBar();

        mEmptyView = findViewById(R.id.no_contact);

        mAdapter = new ContactItemListAdapter(this);
        mQueryHandler = new QueryHandler(this);
        mContext = getApplicationContext();

        Button saveBtn = (Button) findViewById(R.id.save_button);
        saveBtn.setOnClickListener(this);

//        mModeObserver = new OwnerAndPrivacyModeObserver();
//        getContentResolver().registerContentObserver(System.getUriFor(System.CURRENT_PHONE_MODE), true, mModeObserver);

        getListView().setAdapter(mAdapter);

        startQuery();
    }

    @Override
    public void onDestroy() {
        mQueryHandler.removeCallbacksAndMessages(QUERY_TOKEN);
        if (mAdapter.getCursor() != null) {
            mAdapter.getCursor().close();
        }
//        try {
//            if (mModeObserver != null) {
//                getContentResolver().unregisterContentObserver(mModeObserver);
//                mModeObserver = null;
//            }
//        } catch (Exception e) {
//        }
        super.onDestroy();
    }

    public void startQuery() {
        Uri uri = Contacts.CONTENT_URI;
        /* MODIFIED-BEGIN by caixia.chen, 2016-04-27,BUG-1997456*/
        String sel = Contacts.IS_PRIVATE + "=0" + " AND " + RawContacts.ACCOUNT_TYPE + "!='com.android.sim'";
        mQueryHandler.startQuery(QUERY_TOKEN, null, uri, CONTACTS_SUMMARY_PROJECTION, sel,
                null, RawContacts.SORT_KEY_PRIMARY);
                /* MODIFIED-END by caixia.chen,BUG-1997456*/
    }


    private class QueryHandler extends AsyncQueryHandler {
        protected WeakReference<AddPrivacyContact> mActivity;

        public QueryHandler(Context context) {
            super(context.getContentResolver());
            mActivity = new WeakReference<AddPrivacyContact>(
                    (AddPrivacyContact) context);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            if (token != QUERY_TOKEN) {
                return;
            }
            // In the case of low memory, the WeakReference object may be
            // recycled.
            if (mActivity == null || mActivity.get() == null) {
                mActivity = new WeakReference<AddPrivacyContact>(
                        AddPrivacyContact.this);
            }
            final AddPrivacyContact activity = mActivity.get();
            int num = 0;
            if (cursor != null) {
                num = cursor.getCount();
            }
            if (num == 0) {
                showEmptyView();
                return;
            }
            activity.mAdapter.changeCursor(cursor);
            showListView();
        }
    }

    private final class ContactItemListAdapter extends CursorAdapter {
        Context mContext;
        protected LayoutInflater mInflater;
        private ContactPhotoManager mContactPhotoManager;

        public ContactItemListAdapter(Context context) {
            super(context, null, false);

            mContext = context;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mContactPhotoManager = ContactPhotoManager.getInstance(mContext);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            long contactId= cursor.getLong(CONTACT_ID_COLUMN_INDEX);
            String lookupKey = cursor.getString(LOOKUP_KEY_COLUMN_INDEX);
            String name = cursor.getString(DISPLAY_NAME_PRIMARY_COLUMN_INDEX);

            view.setTag(Long.toString(contactId));
            ((TextView) view.findViewById(R.id.contact_name))
                    .setText(name == null ? mContext.getText(R.string.missing_name) : name);

            long photoId = 0;
            if (!cursor.isNull(PHOTO_ID_COLUMN_INDEX)) {
                photoId = cursor.getLong(PHOTO_ID_COLUMN_INDEX);
            }

            ImageView photo = ((ImageView) view.findViewById(R.id.contact_photo));
            photo.setVisibility(View.VISIBLE);

            DefaultImageRequest request = null;
            if (photoId == 0) {
                request = new DefaultImageRequest(name, lookupKey, true);
            }

            mContactPhotoManager.loadThumbnail(photo, photoId, false, true, request);

            CheckBox checkBox = (CheckBox) view.findViewById(R.id.pick_contact_check);
            if (mChoiceSet.contains(Long.toString(contactId))) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View v = mInflater
                        .inflate(R.layout.add_privacy_contact_item, parent, false);
            return v;
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        CheckBox checkBox = (CheckBox) v.findViewById(R.id.pick_contact_check);
        boolean isChecked = !checkBox.isChecked();
        checkBox.setChecked(isChecked);
        if (isChecked) {
            mChoiceSet.add((String) v.getTag());
        } else {
            mChoiceSet.remove((String) v.getTag());
        }
        Cursor cursor = mAdapter.getCursor();
        boolean isSelected = mChoiceSet.size() != cursor.getCount();
        //mSelectAll.setText(isSelected ? R.string.menu_select_all : R.string.menu_select_none);
        updateTitle();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_button:
                onDone();
                break;
        }
    }

//    public void onSave() {
//        if (mChoiceSet.size() == 0) {
//            setResult(RESULT_CANCELED);
//            finish();
//        } else {
//            mProgressDialog = new ProgressDialog(this);
//            mProgressDialog.setIndeterminate(true);
//            mProgressDialog.setCancelable(false);
//            mProgressDialog.show();
//            new SaveThread().run();
//        }
//    }
//
//    public class SaveThread extends Thread {
//        public SaveThread() {
//            super();
//        }
//
//        public void run() {
//            ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
//
//            ContentValues values = new ContentValues(1);
//            values.put(Contacts.IS_PRIVATE, 1);
//            String CONTACT_KEY_IN = Contacts._ID + " IN (";
//            StringBuilder sel = new StringBuilder(CONTACT_KEY_IN);
//            boolean first = true;
//            for (int i = 0; i < mChoiceSet.size(); i++) {
//                if (!first) {
//                    sel.append(',');
//                } else {
//                    first = false;
//                }
//                sel.append(mChoiceSet.get(i));
//            }
//            sel.append(')');
//
//            ops.add(ContentProviderOperation.newUpdate(Contacts.CONTENT_URI)
//                    .withSelection(sel.toString(), null)
//                    .withValues(values)
//                    .build());
//
//            if (ops.size() > 0) {
//                try {
//                    mContext.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            mProgressDialog.dismiss();
//            setResult(RESULT_OK);
//            finish();
//        }
//    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        doFilter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        doFilter(newText);
        return false;
    }

    public void doFilter(String s) {
        if (TextUtils.isEmpty(s)) {
            startQuery();
            return;
        }

        Uri uri = Uri.withAppendedPath(Contacts.CONTENT_FILTER_URI, Uri.encode(s.toString()));
        mQueryHandler.startQuery(QUERY_TOKEN, null, uri, CONTACTS_SUMMARY_PROJECTION, null,
                null, RawContacts.SORT_KEY_PRIMARY);
    }

    private void selectAll() {
        Cursor cursor = mAdapter.getCursor();
        if (cursor == null) {
            return;
        }

        boolean isSelected = mChoiceSet.size() != cursor.getCount();
        mChoiceSet.clear();

        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            long contactId = cursor.getLong(CONTACT_ID_COLUMN_INDEX);

            if (isSelected) {
                mChoiceSet.add(Long.toString(contactId));
            } else {
                mChoiceSet.remove(Long.toString(contactId));
            }
        }

        int count = getListView().getChildCount();
        for (int i = 0; i < count; i++) {
            View v = getListView().getChildAt(i);
            CheckBox checkBox = (CheckBox) v.findViewById(R.id.pick_contact_check);
            checkBox.setChecked(isSelected);
        }
//        mSelectAll.setText(!isSelected ? R.string.menu_select_all : R.string.menu_select_none);
        updateTitle();
    }

    public void onDone() {
        if (mChoiceSet.size() == 0) {
            finish();
            return;
        }
        ContentValues values = new ContentValues(1);
        values.put(Contacts.IS_PRIVATE, 1);
        String CONTACT_KEY_IN = Contacts._ID + " IN (";
        StringBuilder sel = new StringBuilder(CONTACT_KEY_IN);
        boolean first = true;
        for (int i = 0; i < mChoiceSet.size(); i++) {
            if (!first) {
                sel.append(',');
            } else {
                first = false;
            }
            sel.append(mChoiceSet.get(i));
        }
        sel.append(')');
        this.getContentResolver().update(Contacts.CONTENT_URI, values, sel.toString(), null);
        setResult(RESULT_OK);
        finish();
    }

    private void setActionBar() {
//        ActionBar actionBar = getActionBar();
//        actionBar.setHomeButtonEnabled(false);
//        actionBar.setDisplayShowCustomEnabled(true);
//        actionBar.setCustomView(R.layout.multi_select_actionbar);
//        mTitle = (TextView) findViewById(R.id.title);
//        mSelectAll = (TextView) findViewById(R.id.select_all);
//
//        mSelectAll.setOnClickListener(new android.view.View.OnClickListener() {
//            public void onClick(View arg0) {
//                selectAll();
//            }
//        });
    }

    private void updateTitle() {
//        if (mChoiceSet.size() == 0) {
//            mTitle.setText(getString(R.string.none_selected));
//        } else {
//            mTitle.setText(getString(R.string.selected_num, mChoiceSet.size()));
//        }
    }

    public void showEmptyView() {
        mEmptyView.setVisibility(View.VISIBLE);
        getListView().setVisibility(View.GONE);
    }

    public void showListView() {
        mEmptyView.setVisibility(View.GONE);
        getListView().setVisibility(View.VISIBLE);
    }

    //destroy screen when phone switch to privacy mode
    private class OwnerAndPrivacyModeObserver extends ContentObserver {
        public OwnerAndPrivacyModeObserver() {
            super(new Handler());
        }

        @Override
        public void onChange(boolean selfChange) {
            if (System.getInt(getContentResolver(), System.CURRENT_PHONE_MODE, 0) == 1) {
                mShouldDestroy = true;
            }
        }
    }

    protected void onResume() {
        super.onResume();
//        if (mShouldDestroy || (System.getInt(getContentResolver(), System.CURRENT_PHONE_MODE, 0) == 0)) {
//            finish();
//        }
    }
}
