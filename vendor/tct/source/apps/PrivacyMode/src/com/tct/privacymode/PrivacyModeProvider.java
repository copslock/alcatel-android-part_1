/******************************************************************************/
/*                                                               Date:09/2016 */
/*                                PRESENTATION                                */
/*                                                                            */
/*       Copyright 2016 TCL Communication Technology Holdings Limited.        */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/*  Author :  caixia.chen                                                     */
/*  Email  :                                                                  */
/*  Role   :                                                                  */
/*  Reference documents :                                                     */
/* -------------------------------------------------------------------------- */
/*  Comments :                                                                */
/*  File     :                                                                */
/*  Labels   :                                                                */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/*     Modifications on Features list / Changes Request / Problems Report     */
/* -------------------------------------------------------------------------- */
/*    date   |        author        |         Key          |     comment      */
/* ----------|----------------------|----------------------|----------------- */
/* 09/14/2016|     caixia.chen      |     task 2854067     |Private mode      */
/* ----------|----------------------|----------------------|----------------- */
/******************************************************************************/

package com.tct.privacymode;

import java.util.List;

import android.app.backup.BackupManager;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Binder;
import android.os.Process;
import android.provider.BaseColumns;
import android.provider.UserDictionary;
import android.provider.UserDictionary.Words;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.textservice.SpellCheckerInfo;
import android.view.textservice.TextServicesManager;

/**
 * Provides access to a database of user defined words. Each item has a word and a frequency.
 */
public class PrivacyModeProvider extends ContentProvider {

    private static final String AUTHORITY = "com.tct.privacymode.provider";

    private static final String TAG = "PrivacyModeProvider";

    private static final String DATABASE_NAME = "privacy_mode.db";
    private static final int DATABASE_VERSION = 1;

    private static final String PRIVACYAPP_TABLE_NAME = "privacy_apps";

    private static ArrayMap<String, String> sProjectionMap;

    private static final UriMatcher sUriMatcher;

    private static final int APPS = 1;

    private static final int APPS_ID = 2;

    public static final String ID = "_id";
    public static final String PACKAGE_NAME = "package_name";

    public static final Uri CONTENT_URI =
            Uri.parse("content://" + AUTHORITY + "/privacy_apps");

    static {
        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(AUTHORITY, "privacy_apps", APPS);
        sUriMatcher.addURI(AUTHORITY, "privacy_apps/#", APPS_ID);

        sProjectionMap = new ArrayMap<>();
        sProjectionMap.put(ID, ID);
        sProjectionMap.put(PACKAGE_NAME, PACKAGE_NAME);
    }

    private BackupManager mBackupManager;

    /**
     * This class helps open, create, and upgrade the database file.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + PRIVACYAPP_TABLE_NAME + " ("
                    + ID + " INTEGER PRIMARY KEY,"
                    + PACKAGE_NAME + " TEXT"
                    + ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + PRIVACYAPP_TABLE_NAME);
            onCreate(db);
        }
    }

    private DatabaseHelper mOpenHelper;

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        //mBackupManager = new BackupManager(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
            String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        switch (sUriMatcher.match(uri)) {
            case APPS:
                qb.setTables(PRIVACYAPP_TABLE_NAME);
                qb.setProjectionMap(sProjectionMap);
                break;

            case APPS_ID:
                qb.setTables(PRIVACYAPP_TABLE_NAME);
                qb.setProjectionMap(sProjectionMap);
                qb.appendWhere("_id" + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        // If no sort order is specified use the default
        String orderBy;
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = ID;
        } else {
            orderBy = sortOrder;
        }

        // Get the database and run the query
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, orderBy);

        // Tell the cursor what uri to watch, so it knows when its source data changes
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case APPS:
                return "vnd.android.cursor.dir/privacyapps";

            case APPS_ID:
                return "vnd.android.cursor.item/privacyapps";

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        // Validate the requested uri
        if (sUriMatcher.match(uri) != APPS) {
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;
        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        if (!values.containsKey(PACKAGE_NAME)) {
            throw new SQLException("package name must be specified");
        }

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        long rowId = db.insert(PRIVACYAPP_TABLE_NAME, null, values);
        if (rowId > 0) {
            Uri app_uri = ContentUris.withAppendedId(CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(app_uri, null);
            //mBackupManager.dataChanged();
            return app_uri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
            case APPS:
                count = db.delete(PRIVACYAPP_TABLE_NAME, where, whereArgs);
                break;

            case APPS_ID:
                String id = uri.getPathSegments().get(1);
                count = db.delete(PRIVACYAPP_TABLE_NAME, ID + "=" + id
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        //mBackupManager.dataChanged();
        return count;
    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count;
        switch (sUriMatcher.match(uri)) {
            case APPS:
                count = db.update(PRIVACYAPP_TABLE_NAME, values, where, whereArgs);
                break;

            case APPS_ID:
                String id = uri.getPathSegments().get(1);
                count = db.update(PRIVACYAPP_TABLE_NAME, values, ID + "=" + id
                        + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        //mBackupManager.dataChanged();
        return count;
    }
}
