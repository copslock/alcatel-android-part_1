
package com.tct.securitysdkinit;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

import com.xdja.safekeyservice.jarv2.SecuritySDKManager;

public class InitService extends JobService {

    private static final String TAG = "InitService";

    private CheckConnectionTask sTask = null;
    private ConnectivityManager cm;
    private SecuritySDKManager securitySDKManager;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "start job");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean inited = preferences.getBoolean("initState", false);
        tryInitSDK();
        jobFinished(params, !inited);
        Log.i(TAG, "need  needsReschedule:" + !inited);
        return !inited;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(TAG, "stop job");
        return false;
    }

    private void tryInitSDK() {
        if (cm == null) {
            cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        }
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.getState() == NetworkInfo.State.CONNECTED) {
            if (sTask == null || sTask.getStatus() == AsyncTask.Status.FINISHED) {
                sTask = new CheckConnectionTask();
                sTask.execute(InitService.this);
                return;
            } else {
                sTask.cancel(true);
                sTask = new CheckConnectionTask();
                sTask.execute(InitService.this);
                return;
            }
        } else {
            Log.i(TAG, "NetworkInfo: " + info);
        }
    }

    class CheckConnectionTask extends AsyncTask<JobService, Void, Integer> {

        private JobService mService;
        private int initCode = -1;

        @Override
        protected Integer doInBackground(JobService... service) {
            mService = service[0];
            return connectAndGetResponseCode(0);
        }

        @Override
        protected void onPostExecute(Integer paramInteger) {
            Log.w(TAG, "connect getResponseCode: " + paramInteger);
            if (paramInteger == 204) {
                // TODO: init Secure SDK here.
                Log.d(TAG, "init Secure SDK!!!!");
                Log.d(TAG, "init here!!");
                if (securitySDKManager == null) {
                    securitySDKManager = SecuritySDKManager.getInstance();
                }
                int challengeCode = -1;
                String challenge = null;
                try {
                    JSONObject challengeJSON = securitySDKManager.getChallenge(mService);
                    challengeCode = challengeJSON.getInt("ret_code");
                    Log.i(TAG, "challengeCode:" + challengeCode);
                    if (challengeCode == 0) {
                        challenge = challengeJSON.getJSONObject("result").getString("challenge");
                    } else {
                        Log.e(TAG, "error get challenge" + challengeJSON);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "get JSON error:", e);
                    return;
                } catch (Exception e) {
                    Log.e(TAG, "getChallenge error", e);
                    return;
                }
                /* MODIFIED-BEGIN by Wang Xiongke, 2016-10-26,BUG-3057767*/
                try {
                    securitySDKManager.init(InitService.this,
                        challenge,
                        new SecuritySDKManager.InitCallBack() {
                            @Override
                            public void onInitComplete(JSONObject result) {
                                try {
                                    initCode = result.getInt("ret_code");
                                    if (initCode == 0) {
                                        Log.i(TAG, "Secure SDK init success");
                                    } else {
                                        Log.w(TAG, "init failed :" + result);
                                    }
                                } catch (JSONException e) {
                                    Log.e(TAG, "error get init result:", e);
                                }
                            }
                        });
                } catch (Exception e) {
                    Log.d(TAG, "init sdk error", e);
                }
                /* MODIFIED-END by Wang Xiongke,BUG-3057767*/
                boolean result = (initCode == 0);
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mService);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("initState", result);
                editor.commit();
                if (result) {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            PackageManager pm = mService.getPackageManager();
                            pm.setApplicationEnabledSetting(mService.getPackageName(),
                                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 0);
                        }
                    });
                }
            }
        }

        private Integer connectAndGetResponseCode(int paramInt) {
            Log.i(TAG, "connecting to walled garden server. retries=" + paramInt);
            HttpURLConnection localHttpURLConnection = null;
            try {
                URL localURL = new URL("http://www.qualcomm.cn/generate_204");
                localHttpURLConnection = (HttpURLConnection)localURL.openConnection();
                localHttpURLConnection.setInstanceFollowRedirects(false);
                localHttpURLConnection.setConnectTimeout(10000);
                localHttpURLConnection.setReadTimeout(10000);
                localHttpURLConnection.setUseCaches(false);
                int i = localHttpURLConnection.getResponseCode();
                Integer localInteger = Integer.valueOf(i);
                Log.d(TAG, "get response cdoe:" + localInteger);
                return localInteger;
            } catch (java.io.IOException ioe) {
                Log.w(TAG, "IOException e=" + ioe, ioe);
                while (true){
                    if (localHttpURLConnection != null)
                        localHttpURLConnection.disconnect();
                    if (!isNetworkConnected() || (paramInt >= 3)) {
                        Log.i(TAG, "stop connect: connected ? "+ isNetworkConnected() + " retries:" + paramInt);
                        break;
                    }
                    Log.i(TAG, "try to connect again");
                    return connectAndGetResponseCode(paramInt + 1);
                }
            } finally {
                if (localHttpURLConnection != null)
                    localHttpURLConnection.disconnect();
            }
            return Integer.valueOf(0);
        }

        private boolean isNetworkConnected() {
            ConnectivityManager cm = (ConnectivityManager) mService.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            return info == null ? false : info.getState() == NetworkInfo.State.CONNECTED;
        }
    }

}
