/********************************************************************************
 **
 **    Copyright 2015 Waves Audio Ltd. All Rights Reserved.
 **
 ********************************************************************************
 **
 **  The source code, information and material ("Material") contained herein
 **  is owned by Waves Audio Ltd.(“Waves”), and title
 **  to such Material remains with Waves.
 **  The Material contains proprietary information of Waves. The Material is protected by worldwide copyright laws and treaty
 **  provisions. Recipient shall use the Materials solely for the purpose of its implementation of MaxxAudio into the Material.
 **  No part of the Material may be copied, reproduced, modified,
 **  published, uploaded, posted, transmitted, distributed or disclosed in any way
 **  without Waves's prior express written permission. No license under any patent,
 **  copyright or other intellectual property rights in the Material is granted to
 **  or conferred upon you, either expressly, by implication, inducement, estoppel
 **  or otherwise. Any license under such intellectual property rights must
 **  be express and approved by Waves in writing.
 **
 **  Unless otherwise agreed by Waves in writing, you may not remove or alter this
 **  notice or any other notice embedded in the Materials”
 **
 ********************************************************************************
 **  *Third-party brands and names are the property of their respective owners.

 ** The provisions of this notice shall not derogate from any of Waves' rights under the Non Disclosure Agreement between Waves and Recipient or under any Service Level Agreement ("SLA"), regardless of when such SLA is signed between Waves and Recipient.
 */

package com.waves.maxxservice;

import android.net.Uri;

/**
 * Contract class for controlling Waves effect
 */
public final class WavesFXContract {

    public static final String CONFIG = "config";

    // column names
    public static final String COLUMN_NAME_ENABLE = "enable";
    public static final String COLUMN_NAME_SOUND_MODE = "sound_mode";

    // column indexes
    public static final int COLUMN_INDEX_ENABLE = 0;
    public static final int COLUMN_INDEX_SOUND_MODE = 1;

    // definition for COLUMN_NAME_ENABLE
    public static final int WavesFXState_DISABLED = 0;
    public static final int WavesFXState_ENABLE = 1;
    public static final int WavesFXState_CURRENT = -1;

    public static final int SOUND_MODE_MUSIC = 2;
    public static final int SOUND_MODE_MOVIES = 3;
    public static final int SOUND_MODE_GAMING = 4;
    public static final int SOUND_MODE_CURRENT = -1;


    private static final Uri BASE_URI = Uri
            .parse("content://com.waves.maxxaudio.WavesFXProvider/");

    public static final Uri CONFIG_URI = Uri.withAppendedPath(BASE_URI, CONFIG);

    public static final String[] ALL_COLUMNS = {COLUMN_NAME_ENABLE, COLUMN_NAME_SOUND_MODE};
}
