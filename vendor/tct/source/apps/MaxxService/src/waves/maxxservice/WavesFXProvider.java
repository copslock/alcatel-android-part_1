/********************************************************************************
 **
 **    Copyright 2015 Waves Audio Ltd. All Rights Reserved.
 **
 ********************************************************************************
 **
 **  The source code, information and material ("Material") contained herein
 **  is owned by Waves Audio Ltd.(“Waves”), and title
 **  to such Material remains with Waves.
 **  The Material contains proprietary information of Waves. The Material is protected by worldwide copyright laws and treaty
 **  provisions. Recipient shall use the Materials solely for the purpose of its implementation of MaxxAudio into the Material.
 **  No part of the Material may be copied, reproduced, modified,
 **  published, uploaded, posted, transmitted, distributed or disclosed in any way
 **  without Waves's prior express written permission. No license under any patent,
 **  copyright or other intellectual property rights in the Material is granted to
 **  or conferred upon you, either expressly, by implication, inducement, estoppel
 **  or otherwise. Any license under such intellectual property rights must
 **  be express and approved by Waves in writing.
 **
 **  Unless otherwise agreed by Waves in writing, you may not remove or alter this
 **  notice or any other notice embedded in the Materials”
 **
 ********************************************************************************
 **  *Third-party brands and names are the property of their respective owners.

 ** The provisions of this notice shall not derogate from any of Waves' rights under the Non Disclosure Agreement between Waves and Recipient or under any Service Level Agreement ("SLA"), regardless of when such SLA is signed between Waves and Recipient.
 */

package com.waves.maxxservice;

import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.IBinder;

public class WavesFXProvider extends ContentProvider {

    private MaxxService m_Service = null;

    private ServiceConnection m_ServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            m_Service = ((MaxxService.MaxxServiceBinder)service).getService();
            m_Service.SetProvider(WavesFXProvider.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            m_Service = null;
        }
    };

    @Override
    public boolean onCreate() {
        Intent intent = new Intent(getContext(), MaxxService.class);
        intent.putExtra(MaxxService.EXTRA_FROM_PROVIDER, true);
        return getContext().bindService(intent, m_ServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (uri.getLastPathSegment().equals(WavesFXContract.CONFIG))
        {
            Integer enable = m_Service.IsEnabled() ? WavesFXContract.WavesFXState_ENABLE : WavesFXContract.WavesFXState_DISABLED;
            Integer soundMode = m_Service.GetSoundMode();
            MatrixCursor cursor = new MatrixCursor(WavesFXContract.ALL_COLUMNS);
            cursor.addRow(new Object[] { enable, soundMode });

            return  cursor;
        }
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public synchronized int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (uri.getLastPathSegment().equals(WavesFXContract.CONFIG))
        {
            Integer soundMode = values.getAsInteger(WavesFXContract.COLUMN_NAME_SOUND_MODE);
            if (null != soundMode && WavesFXContract.SOUND_MODE_CURRENT != soundMode)
            {
                m_Service.SetSoundMode(soundMode);
            }

            Integer enable = values.getAsInteger(WavesFXContract.COLUMN_NAME_ENABLE);
            if (null != enable && WavesFXContract.WavesFXState_CURRENT != enable)
            {
                m_Service.SetEnabled(WavesFXContract.WavesFXState_ENABLE == enable);
            }

            return 1;
        }
        return 0;
    }

    public void NotifyConfigObservers()
    {
        getContext().getContentResolver().notifyChange(WavesFXContract.CONFIG_URI, null);
    }
}
