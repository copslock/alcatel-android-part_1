ifeq ($(TCT_TARGET_WAVES_EFFECT), true)
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := MaxxService
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_SRC_FILES := $(call all-java-files-under, src)

LOCAL_STATIC_JAVA_LIBRARIES := \
        android-support-v4 \
        android-support-v7-appcompat \
        android-support-v7-mediarouter \
        maxxservicebase \
        maxxutilservice

LOCAL_RESOURCE_DIR = \
        $(LOCAL_PATH)/res \
        frameworks/support/v7/appcompat/res \
        frameworks/support/v7/mediarouter/res \

LOCAL_AAPT_FLAGS := \
        --auto-add-overlay \
        --extra-packages android.support.v7.appcompat \
        --extra-packages android.support.v7.mediarouter \

LOCAL_MULTILIB := 32

LOCAL_PREBUILT_JNI_LIBS := \
	libs/arm/libmaxxeffectwrapper.so \
	libs/arm/libosl-maxxaudio-itf.so
	
include $(BUILD_PACKAGE)

include $(CLEAR_VARS)

LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := \
		maxxservicebase:libs/maxxservicebase.jar \
        maxxutilservice:libs/maxxutil.jar 

include $(BUILD_MULTI_PREBUILT)
endif
