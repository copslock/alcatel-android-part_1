#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <inttypes.h>

#ifdef  DVSC_PRINT_DEBUG_INFO
#define DEBUGINFO printf
#else
#define DEBUGINFO(...)
#endif

typedef struct {
    uintptr_t pAddr;
    int isWrite;
    int fd;
    void *mapBase;
    void *vAddr;
    size_t mapSize;
} MapInfo;

// read len bytes from address. len must be 1, 2, or 4.
static unsigned memRead(void* address, int len) {

    unsigned value = 0;
    switch (len) {
        case 1:
            value = *((volatile unsigned char*) address);
            break;
        case 2:
            value = *((volatile unsigned short*) address);
            break;
        case 4:
            value = *((volatile unsigned*) address);
            break;
        default:
            break;
    }

    DEBUGINFO("read address:%p value:0x%0*x\n", address, len*2, value);
    return value;
}

// write len bytes value to address. len must be 1, 2, or 4.
static void memWrite(void* address, int len, unsigned value) {

    switch (len) {
        case 1:
            *((volatile unsigned char*) address) = value;
            break;
        case 2:
            *((volatile unsigned short*) address) = value;
            break;
        case 4:
            *((volatile unsigned*) address) = value;
            break;
        default:
            break;
    }

    DEBUGINFO("write address:%p value:0x%0*x\n", address, len*2, value);
}

//map one page of memory starting from mapInfo->pAddr
static void* mapMemPage(MapInfo* mapInfo) {

    mapInfo->fd = -1;

    mapInfo->fd = open("/dev/mem", (mapInfo->isWrite ? O_RDWR : O_RDONLY) | O_SYNC);
    if (-1 == mapInfo->fd) {
        perror("failed open /dev/mem");
        return MAP_FAILED;
    }

    mapInfo->mapSize = getpagesize();

    size_t pageMask = mapInfo->mapSize - 1;
    off64_t offsetInPage = mapInfo->pAddr & pageMask;

    mapInfo->mapBase = mmap64(0, mapInfo->mapSize,
            (mapInfo->isWrite ? PROT_WRITE : PROT_READ) | PROT_READ, MAP_SHARED, mapInfo->fd,
            (off64_t)(mapInfo->pAddr & (~pageMask)));
    if (MAP_FAILED == mapInfo->mapBase) {
        perror("failed map");
        close(mapInfo->fd);
        mapInfo->fd = -1;
        return MAP_FAILED;
    }
    mapInfo->vAddr = (char*) mapInfo->mapBase + offsetInPage;
    return mapInfo->vAddr;
}

// unmap the memory at mapInfo->mapBase of length mapInfo->mapSize
// close the file
static void unmapMem(MapInfo* mapInfo) {

    if (-1 == mapInfo->fd) {
        return;
    }

    if (MAP_FAILED != mapInfo->mapBase) {
        if (-1 == munmap(mapInfo->mapBase, mapInfo->mapSize)) {
            perror("munmap failed");
        }
    }

    close(mapInfo->fd);
    mapInfo->fd = -1;
}

static int dvscMemWrite(lua_State *L) {
    uintptr_t pAddr = luaL_checknumber(L, 1);
    unsigned value = luaL_checknumber(L, 2);
    int len = luaL_checkint(L, 3);
    int result = -1;
    MapInfo mapInfo;

    mapInfo.pAddr = pAddr;
    mapInfo.isWrite = 1;

    DEBUGINFO("dvscMemWrite %" PRIxPTR " value %#x len %d\n", mapInfo.pAddr, value, len);
    if (MAP_FAILED != mapMemPage(&mapInfo)) {
        memWrite(mapInfo.vAddr, len, value);
        result = 0;
    }

    unmapMem(&mapInfo);
    lua_pushinteger(L, result);
    return 1;
}

static int dvscMemRead(lua_State *L) {
    uintptr_t pAddr = luaL_checknumber(L, 1);
    int len = luaL_checkint(L, 2);
    int result = -1;
    unsigned value = 0;
    MapInfo mapInfo;

    mapInfo.pAddr = pAddr;
    mapInfo.isWrite = 0;

    DEBUGINFO("dvscMemRead %" PRIxPTR " %d\n", mapInfo.pAddr, len);
    if (MAP_FAILED != mapMemPage(&mapInfo)) {
        value = memRead(mapInfo.vAddr, len);
        result = 0;
    }

    unmapMem(&mapInfo);

    lua_pushinteger(L, value);
    lua_pushinteger(L, result);

    return 2;
}

int dvscEntry(lua_State *L) {
    lua_register(L, "dvscMemRead", dvscMemRead);
    lua_register(L, "dvscMemWrite", dvscMemWrite);
    return 0;
}

#ifdef DVSC_FEATURE_TEST
#include <stdlib.h>

static uintptr_t gPhyAddress = 0;
static int gLen = 4;
static unsigned gValue = 0;
static int gWrite = 0;

static int getOptions(int argc, char* argv[]) {
    int rc = 0;
    int cmdArgs;

    if(argc < 2) {
        fprintf(stderr, "Need Address");
        return -1;
    }

    gPhyAddress = (uintptr_t)strtoull(argv[1], 0, 16);

    while ((cmdArgs = getopt(argc-1, argv+1, "l:v:h")) != -1) {
        switch (cmdArgs) {
            case 'l':
            gLen = atoi(optarg);
            if (gLen != 1 && gLen != 2 && gLen != 4 && gLen != 8) {
                fprintf(stderr, "length of %s is not a supported data unit length\n", optarg);
                rc = -2;
            }
            break;

            case 'v':
            gValue = (unsigned) (strtoull(optarg, 0, 16));
            gWrite = 1;
            break;

            case 'h':
            rc = -3;
            break;

            default:
            fprintf(stderr, "wrong argument\n");
            rc = -4;
            break;
        }
    }

    if (rc ) {
        printf("%s -a<address> -v<value>  -l<1|2|4|8> where: \n", argv[0]);
        printf("-l<1|2|4|8>  the data unit length to read. Default is 4  \n");
        printf("-a<address>  the physical address, in hex\n");
        printf("-v<value>  the value to write, in hex\n");
        return rc;
    }
    else {
        DEBUGINFO("address:%#" PRIxPTR " length:%d value:%#x\n",
                gPhyAddress, gLen, gValue);
    }

    return rc;
}

int main(int argc, char* argv[]) {

    if (getOptions(argc, argv)) {
        return -1;
    }

    MapInfo mapInfo;
    mapInfo.pAddr = gPhyAddress;
    mapInfo.isWrite = gWrite;
    int result = -1;

    if (MAP_FAILED != mapMemPage(&mapInfo)) {
        if (mapInfo.isWrite) {
            memWrite(mapInfo.vAddr, gLen, gValue);
        } else {
            memRead(mapInfo.vAddr, gLen);
        }
        result = 0;
    }

    unmapMem(&mapInfo);

    return result;
}

#endif
