LOCAL_PATH := $(call my-dir)
# The foreach block in this file is a simplified version of auto-prebuilt-boilerplate function from
# build/core/multi_prebuilt.mk. I do not use that function (and BUILD_MULTI_PREBUILT) because it
# is not as simple as what I have below and it does not allow me to set LOCAL_MODULE_PATH.

dvs_script_file_list:= $(shell cd $(LOCAL_PATH)/scripts; echo *.sh *.lua)
$(foreach t, $(dvs_script_file_list), \
  $(eval include $(CLEAR_VARS)) \
  $(eval LOCAL_MODULE_TAGS   := eng debug) \
  $(eval LOCAL_MODULE        := $(t)) \
  $(eval LOCAL_SRC_FILES     := scripts/$(LOCAL_MODULE)) \
  $(eval LOCAL_MODULE_CLASS  := EXECUTABLES) \
  $(eval LOCAL_MODULE_PATH   := $(TARGET_OUT_OPTIONAL_EXECUTABLES)) \
  $(eval include $(BUILD_PREBUILT)) \
)

#should not need the following line. etc/dvs should be created automatically when using make
#$(shell mkdir -p $(PRODUCT_OUT)/system/etc/dvs/)
#also, we can simply copy files as shown below. However using make is better.
#$(shell cp $(LOCAL_PATH)/extras/*.rd $(PRODUCT_OUT)/system/etc/dvs/)

dvs_rd_file_list:= $(shell cd $(LOCAL_PATH)/extras; echo *.rd)
$(foreach t, $(dvs_rd_file_list), \
  $(eval include $(CLEAR_VARS)) \
  $(eval LOCAL_MODULE_TAGS   := eng debug) \
  $(eval LOCAL_MODULE        := $(t)) \
  $(eval LOCAL_SRC_FILES     := extras/$(LOCAL_MODULE)) \
  $(eval LOCAL_MODULE_CLASS  := ETC) \
  $(eval LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/etc/dvs) \
  $(eval include $(BUILD_PREBUILT)) \
)

dvs_scripts_file_list:=
dvs_rd_file_list:=

include $(call first-makefiles-under,$(LOCAL_PATH))
