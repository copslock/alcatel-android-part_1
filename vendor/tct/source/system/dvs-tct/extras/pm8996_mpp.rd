#register description file for PM8994-PM8996 MPP

g;pmic_mpp

r;mpp_revision1;0
    f;dig_minor;0;7

r;mpp_revision2;1
    f;dig_major;0;7

r;mpp_revision3;2
    f;ana_minor;0;7

r;mpp_revision4;3
    f;ana_major;0;7

r;mpp_perph_type;4
    f;type;0;7
        v;mpp;0x11

r;mpp_perph_subtype;5
    f;subtype;0;7
        v;mpp_4ch_sink;0x3
        v;mpp_4ch_aout;0x5
        v;mpp_4ch_aout_sink;0x7
        v;mpp_8ch_sink;0xb
        v;mpp_8ch_aout;0xd
        v;mpp_8ch_aout_sink;0xf

r;mpp_status1;8
    f;mpp_ok;7;7;r;enabled
        v;disabled;0;d
        v;enabled;1;e
    f;mpp_val;0;0;r;input
        v;mpp_input_low;0;low
        v;mpp_input_high;1;high

r;mpp_int_rt_sts;0x10
    f;mpp_in_sts;0;0
        v;int_rt_status_low;0
        v;int_rt_status_high;1

r;mpp_int_set_type;0x11
    f;mpp_in_type;0;0;rw;it
        v;level;0x0;level
        v;edge;0x1;edge

r;mpp_int_polarity_high;0x12
    f;mpp_in_high;0;0;rw;high_trigger
        v;high_trigger_disabled;0x0;d
        v;high_trigger_enabled;0x1;e

r;mpp_int_polarity_low;0x13
    f;mpp_in_low;0;0;rw;il
        v;low_trigger_disabled;0;d
        v;low_trigger_enabled;1;e

r;mpp_int_latched_clr;0x14
    f;mpp_in_latched_clr;0;0;w

r;mpp_int_en_set;0x15
    f;mpp_in_en_set;0;0;rw;ies
        v;int_disabled;0;d
        v;int_enabled;1;e

r;mpp_int_en_clr;0x16
    f;mpp_in_en_clr;0;0;rw;iec
        v;int_disabled;0;d
        v;int_enabled;1;e

r;mpp_int_latched_sts;0x18
    f;mpp_in_latched_sts;0;0
        v;no_int_latched;0
        v;interrupt_latched;1

r;mpp_int_pending_sts;0x19
    f;mpp_in_pending_sts;0;0
        v;no_int_pending;0
        v;interrupt_pending;1

r;mpp_int_mid_sel;0x1a
    f;int_mid_sel;0;1;rw;ims
        v;mid0;0;m0
        v;mid1;1;m1
        v;mid2;2;m2
        v;mid3;3;m3

r;mpp_int_priority;0x1b
    f;int_priority;0;0;rw;ip
        v;sr;0
        v;a;1

r;mpp_mode_ctl;0x40
    f;mode;4;6;rw;m
        v;digital_input;0;di
        v;digital_output;1;do
        v;digital_in_and_out;2;dio
        v;bidirectional;3;bi
        v;analog_in;4;ai
        v;analog_out;5;ao
        v;current_sink;6;cs
    f;en_and_source_sel;0;3;rw;src
        v;low;0;l
        v;high;1;h
        v;paired;2;p
        v;not_paired;3;np
        v;dtest1;8;d1
        v;not_dtest1;9;nd1
        v;dtest2;10;d2
        v;not_dtest2;11;nd2
        v;dtest3;12;d3
        v;not_dtest3;13;nd3
        v;dtest4;14;d4
        v;not_dtest4;15;nd4

r;mpp_dig_vin_ctl;0x41
    f;voltage_sel;0;2;rw;v
        v;vin0;0;v0
        v;vin1;1;v1
        v;vin2;2;v2
        v;vin3;3;v3

r;mpp_dig_pull_ctl;0x42
    f;pullup_sel;0;2;rw;p
        v;r600ohm;0;600
        v;open;1;open
        v;r10kohm;2;10k
        v;r30kohm;3;30k

r;mpp_dig_in_ctl;0x43
    f;dtest1;0;0;rw;dt1
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest2;1;1;rw;dt2
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest3;2;2;rw;dt3
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e
    f;dtest4;3;3;rw;dt4
        v;dtest_disabled;0;d
        v;dtest_enabled;1;e

r;mpp_en_ctl;0x46
    f;perph_en;7;7;rw;e
        v;mpp_disabled;0;d
        v;mpp_enabled;1;e

r;mpp_ana_out_ctl;0x48
    f;ref_sel;0;2;rw;ref
        v;vref_1v25;0;1v25

r;mpp_ana_in_ctl;0x4a
    f;route_sel;0;2;rw;rs
        v;hkadc5;0;adc5
        v;hkadc6;1;adc6
        v;hkadc7;2;adc7
        v;hkadc8;3;adc8

r;mpp_sink_ctl;0x4c
    f;current_sel;0;2;rw;cs
        v;current_5ma;0;5ma
        v;current_10ma;1;10ma
        v;current_15ma;2;15ma
        v;current_20ma;3;20ma
        v;current_25ma;4;25ma
        v;current_30ma;5;30ma
        v;current_35ma;6;35ma
        v;current_40ma;7;40ma