# register description file for PM8994-PM8996 SMPS

g;pmic_hf_smps

r;s_ctrl_perph_subtype;5
    f;subtype;7;0;r;type
        v;hf;8;hf

r;s_ctrl_status;8
    f;vreg_ok;7;7
        v;vreg_ok_true;1;true
        v;vreg_ok_false;0;false
    f;ils;5;5
        v;ilegal_limit_stop_true;1;true
        v;ilegal_limit_stop_false;0;false
    f;ul_voltage;4;4
        v;ul_int_true;1;true
        v;ul_int_false;0;false
    f;ll_voltage;3;3
        v;ll_int_true;1;true
        v;ll_int_false;0;false
    f;ps_true;2;2
        v;ps_true;1;true
        v;ps_false;0;false
    f;npm_true;1;1
        v;npm_vregok_true;1;true
        v;npm_vregok_false;0;false
    f;stepper_done;0;0
        v;stepper_done_true;1;true
        v;stepper_done_false;0;false

r;s_ctrl_voltage_ctl1;0x40
    f;range;0;0;
        v;range_hv_true;1;high
        v;range_hv_false;0;low

r;s_ctrl_voltage_ctl2;0x41
    f;v_set;6;0;rw;vs

r;s_ctrl_pfm_ctl;0x44
    f;pfm_volt_ctl;7;7;rw;vboost
        v;pfm_volt_boost_true;1;true
        v;pfm_volt_boost_false;0;false
    f;pfm_iboost;6;6;rw;iboost
        v;pfm_iboost_true;1;true
        v;pfm_vboost_false;0;false
    f;pfm_type_i;5;5;rw;type
        v;pfm_legacy;1;legacy
        v;pfm_advanced;0;advanced
    f;pfm_comp_hyst;4;4;rw;hyst
        v;pfm_hyst_6mv;1;6mv
        v;prm_hyst_3mv;0;3mv
    f;pfm_comp_pls_fltr;3;3;rw;fltr
        v;pfm_comp_pls_fltr_250ms;1;250ns
        v;pfm_comp_pls_fltr_100ns;0;100ns
    f;pfm_ipllim_ctrl;2;2;rw;comp
        v;pfm_iplim_ctrl_true;1;e
        v;pfm_iplim_ctrl_false;0;d
    f;pfm_iplim_dly;1;0;rw;delay
        v;pfm_iplim_ctrl_600ns;3;600ns
        v;pfm_iplim_ctrl_300ns;2;300ns
        v;pfm_iplim_ctrl_150ns;1;150ns
        v;pfm_iplim_ctrl_75ns;0;75ns

r;s_ctrl_mode_ctl;0x45
    f;pwm;7;7;rw
        v;pwm_force;1;f
        v;pwm_no_force;0;l;nf
    f;auto_mode;6;6;rw;auto
        v;auto_true;1;true
        v;auto_false;0;false
    f;follow_pmic_awake;4;4;rw;follow
        v;follow_pmic_awake_true;1;true
        v;follow_pmic_awake_false;0;false
    f;follow_hwen3;3;3;rw;h3
        v;follow_hwen3_true;1;true
        v;follow_hwen3_false;0;false
    f;follow_hwen2;2;2;rw;h2
        v;follow_hwen2_true;1;true
        v;follow_hwen2_false;0;false
    f;follow_hwen1;1;1;rw;h1
        v;follow_hwen1_true;1;true
        v;follow_hwen1_false;0;false
    f;follow_hwen0;0;0;rw;h0
        v;follow_hwen0_true;1;true
        v;follow_hwen0_false;0;false

r;s_ctrl_en_ctl;0x46
    f;perph_en;7;7;rw;buck_en
        v;buck_enable_true;1;true
        v;buck_disable_false;0;false
    f;follow_hwe3;3;3;rw;f_h3
        v;follow_hwe3_true;1;true
        v;follow_hwe3_false;0;false
    f;follow_hwe2;2;2;rw;f_h2
        v;follow_hwe2_true;1;true
        v;follow_hwe2_false;0;false
    f;follow_hwe1;1;1;rw;f_h1
        v;follow_hwe1_true;1;true
        v;follow_hwe1_false;0;false
    f;follow_hwe0;0;0;rw;f_h0
        v;follow_hwe0_true;1;true
        v;follow_hwe0_false;0;false

r;s_ctrl_pd_ctl;0x48
    f;pd_en;7;7;rw;st_pd
        v;pd_enable_true;1;true
        v;pd_enable_false;0;false

r;s_ctrl_ul_ll_ctrl;0x68
    f;ul_int_en;7;7;rw;u_stop
        v;ul_int_en_true;1;e
        v;ul_int_en_false;0;d
    f;ll_int_en;6;6;rw;l_stop
        v;ll_int_en_true;1;e
        v;ll_int_en_false;0;d

r;s_ctrl_ul_voltage;0x69
    f;v_set_ul;6;0;rw;ul_set

r;s_ctrl_ll_voltage;0x6b
    f;v_set_ll;6;0;rw;ll_set

# this starts the S_PS section
# nothing of value to read from this section

# this starts the S_FREQ section
r;s_freq_clk_enable;0x246
    f;en_clk_int;7;7;rw;clk
        v;force_en_enable;1;e
        v;force_en_disable;0;d
    f;follow_clk_sx_req;0;0;rw;fol_clk
        v;follow_clk_req_enabled;1;e
        v;follow_clk_req_disabled;0;d

r;s_freq_clk_div;0x250
    f;clk_div;3;0;rw;frq
        v;freq_1m2hz;15;1.2m
        v;freq_1m3hz;14;1.3m
        v;freq_1m4hz;13;1.4m
        v;freq_1m5hz;12;1.5m
        v;freq_1m6hz;11;1.6m
        v;freq_1m7hz;10;1.7m
        v;freq_1m9hz;9;1.9m
        v;freq_2m1hz;8;2.1m
        v;freq_2m4hz;7;2.4m
        v;freq_2m7hz;6;2.7m
        v;freq_3m2hz;5;3.2m
        v;freq_3m8hz;4;3.8m
        v;freq_4m8hz;3;4.8m
        v;freq_6m4hz;2;6.4m
        v;freq_9m6hz;1;9.6m
        v;freq_9m6hz_0;0;ns-9.6m

r;s_freq_clk_phase;0x251
    f;clk_phase;3;0;rw;dly

r;s_freq_gang_ctl1;0x2c0
    f;frq_gang_leader_pid;7;0;rw;f_gl_pid

r;s_freq_gang_ctl2;0x2c1
    f;frq_gang_en;7;7;rw;f_gang_en
        v;ganging_enabled;1;e
        v;ganging_disabled;0;d