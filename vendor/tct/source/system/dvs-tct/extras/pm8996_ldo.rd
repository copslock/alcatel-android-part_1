#register description file for PM8994-PM8996 LDO

g;pmic_ldo

r;ldo_status1;8
    f;vreg_ok;7;7
        v;ldo_voltage_ok;1;ok
        v;ldo_voltage_low;0;low
    f;ils_detected;5;5
        v;upper_limit_setting_err;1;err
        v;upper_limit_setting_ok;0;ok
    f;ul_voltage_detected;4;4
        v;voltage_level_setting_overlimit;1;over
        v;voltage_level_setting_ok;0;ok
    f;ll_voltage_detected;3;3
        v;voltage_level_setting_underlimit;1;under
        v;voltage_level_setting_ok;0;ok
    f;npm_true;1;1
        v;npm_voltage_ok;1;ok
        v;not_npm_or_voltage_not_ok;0;nok
    f;stepper_done;0;0
        v;stepper_done;1;yes
        v;stepper_not_done;0;no

r;ldo_status2;9
    f;softstart_done;7;7
        v;softstart_done;1;yes
        v;softstart_not_done;0;no
    f;over_current_detected;6;6
        v;over_current_detected;1;yes
        v;no_over_current_detected;0;no
    f;vreg_on;5;5
        v;ldo_on;1;on
        v;ldo_off;0;off
    f;ldo_range_ext;0;0
        v;ldo_range_extended;1;yes
        v;ldo_range_not_extended;0;no

r;ldo_status3;0x0A
    f;ldo_range_sel;7;7
        v;ldo_range_selected;1;yes
        v;ldo_range_not_selected;0;no
#the following field is strange, it takes 7 bits with only 2 allowed values!
    f;ldo_vset;0;6
        v;ldo_program_selected;1;yes
        v;ldo_program_not_selected;0;no

r;ldo_voltage_ctl1;0x40
    f;range;2;0;rw;vr
        v;m0.375s12.5;0;r0
        v;m0.375s12.5_;1;r1
        v;m0.75s12.5;2;r2
        v;m1.5s25;3;r3
        v;m1.75s50;4;r4


r;ldo_voltage_ctl2;0x41
    f;vset;0;5;rw;vs

r;ldo_mode_ctl2;0x45
    f;npm;7;7;rw
        v;forced_npm;1;true
        v;forced_npm_false;0;false
    f;bypass_act;6;6;rw;ba
        v;bypass_act_true;1;true
        v;bypass_act_false;0;false
    f;bypass_en;5;5;rw;be
        v;by_pass_enabled;1;e
        v;by_pass_disabled;0;d
    f;follow_pmic_awake;4;4;rw;fpa
        v;follow_pmic_avake_true;1;true
        v;follow_pmic_avake_false;0;false
    f;npm_follow_hw_en3;3;3;rw;ne3
        v;npm_follow_hw_en3_true;1;true
        v;npm_follow_hw_en3_false;0;false
    f;npm_follow_hw_en2;2;2;rw;ne2
        v;npm_follow_hw_en2_true;1;true
        v;npm_follow_hw_en2_false;0;false
    f;npm_follow_hw_en1;1;1;rw;ne1
        v;npm_follow_hw_en1_true;1;true
        v;npm_follow_hw_en2_false;0;false
    f;npm_follow_hw_en0;0;0;rw;ne0
        v;npm_follow_hw_en0_true;1;true
        v;npm_follow_hw_en0_false;0;false

r;ldo_en_ctl;0x46
    f;en_ldo_int;7;7;rw;e
        v;en_ldo_int_true;1;true
        v;en_ldo_int_false;0;false
    f;follow_hw_en3;3;3;rw;fe3
        v;follow_hw_en3_true;1;true
        v;follow_hw_en3_false;0;false
    f;follow_hw_en2;2;2;rw;fe2
        v;follow_hw_en2_true;1;true
        v;follow_hw_en2_false;0;false
    f;follow_hw_en1;1;1;rw;fe1
        v;follow_hw_en1_true;1;true
        v;follow_hw_en2_false;0;false
    f;follow_hw_en0;0;0;rw;fe0
        v;follow_hw_en0_true;1;true
        v;follow_hw_en0_false;0;false

r;ldo_pd_ctl;0x48
    f;pulldn_en;7;7;rw;pd
        v;pulldn_enabled;1;e
        v;pulldn_disabled;0;d

r;ldo_current_lim_ctl;0x4a
    f;current_lim_en;7;7;rw;cle
        v;current_lim_enabled;1;e
        v;current_lim_disabled;0;d
    f;current_lim_testmode_en;5;5;rw;clte
        v;current_lim_testmode_enabled;1;e
        v;current_lim_testmode_disabled;0;d

r;ldo_soft_start_ctl;0x4c
    f;ldo_soft_start_en;7;7;rw;ste
        v;ldo_soft_start_enabled;1;e
        v;ldo_soft_start_disabled;0;d

r;ldo_config_ctl;0x52
    f;clamp_ctrl;6;7;rw
    f;clamp_en;5;5;rw
        v;clamp_enabled;1;e
        v;clamp_disabled;0;d
    f;cascade;4;4;rw
        v;cascade_true;1;true
        v;cascade_false;0;false
    f;act_bypass_buff_en;3;3;rw
        v;act_bypass_buff_enabled;1;e
        v;act_bypass_buff_disabled;0;d

r;ldo_vs_ctl;0x61
    f;vs_en;7;7;rw
        v;vs_enabled;1;e
        v;vs_disabled;0;d
    f;vs_delay;0;2;rw
        v;delay_1_2560;7
        v;delay_1_1280;6
        v;delay_1_640;5
        v;delay_1_320;4
        v;delay_1_160;3
        v;delay_1_80;2
        v;delay_1_40;1
        v;delay_1_20;0

r;ldo_sec_access;0xd0
    f;sec_unlock;0;7;rw
        v;sec_unlock;0xa5;unlock
        v;sec_lock;0;lock
