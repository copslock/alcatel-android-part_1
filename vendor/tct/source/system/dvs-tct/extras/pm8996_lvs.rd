#register description file for PM8994-PM8996 LVS

g;pmic_lvs

r;lvs_perph_sbutype;5
    f;subtype;7;0
        v;otg;17
        v;hdmi;16
        v;mv500;9
        v;mv300;8
        v;lv300;2
        v;lv100;1

r;lvs_status1;8
    f;vreg_ok;7;7
        v;vs_ok;1;ok
        v;vs_not_ok;0;nok
    f;overcurrent;5;5
        v;oc_event_occurred;1;oc
        v;no_oc_event;0;noc
    f;npm_true;1;1
        v;npm;1
        v;lpm;0

r;lvs_int_rt_sts;0x10
    f;overcurrent_rt_sts;2;2;r;oc
        v;int_rt_status_high;1;h
        v;int_rt_status_low;0;l
    f;vreg_ok_rt_sts;0;0;r;ok
        v;int_rt_status_high;1;h
        v;int_rt_status_low;0;l

r;lvs_mode_ctl1;0x45
    f;npm;7;7;rw
        v;force_npm;1;true
        v;no_force;0;false
    f;auto;6;6;rw
        v;auto_enabled;1;e
        v;no_auto;0;d
    f;follow_pmic_awake;4;4;rw;fpa
        v;follow_sleep_b;1;true
        v;no_follow;0;false

r;lvs_en_ctl1;0x46
    f;perph_en;7;7;rw;e
        v;force_on;1;e
        v;no_force;0;d
    f;follow_hwen3;3;3;rw;h3
        v;follow_hwen;1;e
        v;no_follow;0;d
    f;follow_hwen2;2;2;rw;h2
        v;follow_hwen;1;e
        v;no_follow;0;d
    f;follow_hwen1;1;1;rw;h1
        v;follow_hwen;1;e
        v;no_follow;0;d
    f;follow_hwen0;0;0;rw;h0
        v;follow_hwen;1;e
        v;no_follow;0;d

r;lvs_pd_ctl;0x48
    f;pd_en;7;7;rw;pd
        v;pd_enable;1;e
        v;pd_disable;0;d

r;lvs_current_lim_ctl;0x4A
    f;ocp_override;0;0;rw;ocp
        v;stay_enabled_on_ocp;1;e
        v;disable_on_ocp;0;d

r;lvs_soft_start_ctl;0x4C
# this bit is confusing ...
    f;soft_start_en;7;7;rw;soft_st
        v;softstart_enabled;1;e
        v;softstart_disabled;0;d
    f;soft_start_sel;1;0;rw;soft_st_sel
        v;ss_pd_750na;3;750na
        v;ss_pd_550na;2;550na
        v;ss_pd_250na;1;250na
        v;ss_pd_50na;0;50na