# register description file for PM8994-PM8996 clocks

g;pmic_bb_rf_clock

r;clk_status;8
    f;clk_ok;7;7
        v;clk_on;1;on
        v;clk_off;0;off

r;clk_edge_ctl1;0x43
    f;out_edge;3;0;rw;edge

r;clk_drv_ctl1;0x44
    f;out_drv;1;0;rw;d
        v;one_x;0;1x
        v;two_x;1;2x
        v;three_x;2;3x
        v;four_x;3;4x

r;clk_en_ctl;0x46
    f;clk_en;7;7;rw;e
        v;clk_force_en;1;fe
        v;clk_not_force;0;nf
    f;pc_polarity;1;1;rw;p
        v;pos_pincontrol_polarity;0;p
        v;neg_pincontrol_polarity;1;n
    f;follow_pc_en;0;0;rw;fpe
        v;not_follow_pin;0;no
        v;follow_pin;1;yes

g;pmic_div_clock
r;clk_status;8
    f;clk_ok;7;7
        v;divider_on;1;on
        v;divider_off;0;off

r;clk_div_ctl1;0x43
    f;div_factor;2;0;rw;div
        v;xo_div1_0;0;d1_0
        v;xo_div1;1;d1
        v;xo_div2;2;d2
        v;xo_div4;3;d4
        v;xo_div8;4;d8
        v;xo_div16;5;d16
        v;xo_div32;6;d32
        v;xo_div64;7;d64

r;clk_en_ctl;0x46
    f;clk_en;7;7;rw;e
        v;divclk_en;1;e
        v;divclk_dis;0;d
#follow_pc_en does not make sense as there is no PIN to follow
#    f;follow_pc_en;0;0;rw;fpe
#        v;not_follow_pin;0;no
#        v;follow_pin;1;yes

g;pmic_sleep_clock
r;sleep_clk_en_ctl;0x46
    f;clk_en;7;7;rw;e
        v;slp_clk_buf_enabled;1;e
        v;slp_clk_buf_disable;0;d

r;sleep_clk_smpl_ctl1;0x48
    f;smpl_en;7;7;rw
        v;smpl_enable;1;e
        v;smpl_disable;0;d
    f;smpl_delay;1;0;rw
        v;half_sec;0;0.5s
        v;one_sec;1;1s
        v;oneandhalf_sec;2;1.5s
        v;two_sec;3;2s

r;sleep_clk_cal_rc3;0x5A
    f;lfrc_drift_det_en_batt;0;0;rw
        v;drift_det_disabled;0
        v;drift_det_enabled;1

r;sleep_clk_cal_rc4;0x5B
    f;calrc_en;7;7;rw
        v;calrc_disabled;0
        v;calrc_enabled;1
    f;coincell_good;6;6;rw
        v;weak_coincap;0
        v;strong_coincap;1
    f;lfrc_drift_det_en_coin;4;4;rw
        v;drift_det_disabled;0
        v;drift_det_enabled;1
    f;calrc_dtest_en;0;0;rw
        v;normal;0
        v;calrc_state_on_dtest;1