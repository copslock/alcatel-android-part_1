#register description file for msm8996

g;msm_gpio

r;gpio_cfg;0
	f;gpio_hihys_en;10;10;rw;he
		v;enable;1;e
		v;disable;0;d
	f;gpio_oe;9;9;rw;oe
		v;enable;1;e
		v;disable;0;d
	f;drv_strength;8;6;rw;d
		v;2mA;0;2m
		v;4mA;1;4m
		v;6mA;2;6m
		v;8mA;3;8m
		v;10mA;4;10m
		v;12mA;5;12m
		v;14mA;6;14m
		v;16mA;7;16m
	f;func_sel;5;2;rw;f
	f;gpio_pull;1;0;rw;p
		v;no_pull;0;np
		v;pull_down;1;pd
		v;keeper;2;k
		v;pull_up;3;pu

r;gpio_in_out;4
	f;gpio_out;1;1;rw;o
		v;high;1;h
		v;low;0;l
	f;gpio_in;0;0
		v;high;1;h
		v;low;0;l

r;gpio_intr_cfg;8
	f;dir_conn_en;8;8;rw;dce
		v;enable;1;e
		v;disable;0;d
	f;target_proc;7;5;rw;tp
		v;reserved0;0;r0
		v;sensors;1;s
		v;lpa_dsp;2;dsp
		v;rpm;3
		v;kpss;4
		v;mss;5
		v;tz;6
		v;reserved1;7;r1
	f;intr_raw_status_en;4;4;rw;irse
		v;enable;1;e
		v;disable;0;d
	f;intr_dect_ctl;3;2;rw;idc
		v;level;0;l
		v;pos_edge;1;pe
		v;neg_edge;2;ne
		v;dual_edge;3;de
	f;intr_pol_ctl;1;1;rw;ipc
		v;polarity_1;1;h
		v;polarity_0;0;l
	f;intr_enable;0;0;rw;ie
		v;enable;1;e
		v;disable;0;d

r;intr_status;0x0c
	f;intr_status;0;0;rw;is
		v;set;1;s
		v;clear;0;c