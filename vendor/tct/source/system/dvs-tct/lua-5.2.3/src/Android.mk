LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := eng debug
LOCAL_MODULE      := liblua
LOCAL_C_INCLUDES  += $(LOCAL_PATH)
LOCAL_SHARED_LIBRARIES := libm
# libreadline
LOCAL_CFLAGS      += -O2 -Wall -DLUA_COMPAT_ALL -DLUA_USE_LINUX -DLUA_ANDROID
LOCAL_SRC_FILES   := lapi.c lcode.c lctype.c ldebug.c ldo.c ldump.c lfunc.c lgc.c llex.c lmem.c \
	lobject.c lopcodes.c lparser.c lstate.c lstring.c ltable.c ltm.c lundump.c lvm.c lzio.c \
	lauxlib.c lbaselib.c lbitlib.c lcorolib.c ldblib.c liolib.c lmathlib.c loslib.c lstrlib.c \
	ltablib.c loadlib.c linit.c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := eng debug
LOCAL_SRC_FILES   := lua.c
LOCAL_MODULE      := lua
LOCAL_SHARED_LIBRARIES := libdl
LOCAL_STATIC_LIBRARIES := liblua
LOCAL_C_INCLUDES  += $(LOCAL_PATH)
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
include $(BUILD_EXECUTABLE)
