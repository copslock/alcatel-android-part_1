#!/bin/bash
#
# when run from a linux box...
#
#  sideloads the 8992 DVS package
#


main () {
    # create the DVS release
    if [ -e release.sh ]; then
        echo "creating DVS release ..."
        # create release
        . release.sh -n
        if [ -z $DVS_RELEASE ]; then
            echo "DVS release failed, no files loaded!!!"
            return
        fi
        # change to the release directory so the files get loaded from the proper place
        cd $DVS_RELEASE
    fi

    # make adb root, wait if it wasn't already
    echo "making adb root ..."
    adb root 2>/dev/null | grep already || sleep 3 # wait for adb to come back after root

    # mount /system rw
    echo "mounting file system as rw ..."
    if ! adb remount 2>&1 | grep "remount succeeded" >/dev/null; then
        echo "Cannot install DVS files, remount failed"
        exit
    fi

    # load the files on the device
    echo "loading DVS files ..."
    adb push target/. /

    echo Done
}

main "$@"
