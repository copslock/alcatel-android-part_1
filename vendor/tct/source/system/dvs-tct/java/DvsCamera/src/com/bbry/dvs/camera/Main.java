package com.bbry.dvs.camera;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.WindowManager;
import android.widget.Toast;

public class Main extends Activity {
	private final static String TAG = "dvs_camera";
	private final static String ACTION_CAMERA_CLOSE = "com.bbry.dvs.camera.close";
	private CameraTextureView mCameraTextureView_main;
	private CameraTextureView mCameraTextureView_front;
	private BroadcastReceiver broadcastReceiver;
	private IntentFilter intentFilter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		Intent intent = getIntent();
		if (intent != null) {
			int mode = intent.getIntExtra("mode", -1);
			Log.d(TAG, "mode = " + mode);
			if (mode == 0 || mode == 1 || mode == 2) {
				broadcastReceiver = new Receiver();
				intentFilter = new IntentFilter(ACTION_CAMERA_CLOSE);
			}
			switch (mode) {
			case 0:
				setContentView(R.layout.back_camera);
				openBack();
				break;
			case 1:
				setContentView(R.layout.front_camera);
				openFront();
				break;
			case 2:
				setContentView(R.layout.double_camera);
				openFront();
				openBack();
				break;
			default:
				Toast.makeText(this, "dvs error parameter: " + mode, Toast.LENGTH_LONG).show();
				finish();
				break;
			}
		}
	}

	private void openFront() {
		int front = Camera.CameraInfo.CAMERA_FACING_FRONT;
		CameraInfo info_front = initCameraInfo(front);
		if (info_front == null) {
			Toast.makeText(this, "no front camera!", Toast.LENGTH_LONG).show();
		} else {
			TextureView textureView_front = (TextureView) findViewById(R.id.preview_content_front);
			mCameraTextureView_front = new CameraTextureView(this, textureView_front, front, info_front);
		}
	}

	private void openBack() {
		int back = Camera.CameraInfo.CAMERA_FACING_BACK;
		CameraInfo info = initCameraInfo(back);
		if (info == null) {
			Toast.makeText(this, "no back camera!", Toast.LENGTH_LONG).show();
		} else {
			TextureView textureView = (TextureView) findViewById(R.id.preview_content_back);
			mCameraTextureView_main = new CameraTextureView(this, textureView, back, info);
		}
	}

	private CameraInfo initCameraInfo(int target) {
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		int cameraCount = Camera.getNumberOfCameras();
		for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
			Camera.getCameraInfo(camIdx, cameraInfo);
			if (cameraInfo.facing == target) {
				return cameraInfo;
			}
		}
		return null;
	}

	private class Receiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context arg0, Intent intent) {
			String action = intent.getAction();
			Log.d(TAG, "action = " + action);
			if (action.equals(ACTION_CAMERA_CLOSE)) {
				finish();
			}
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated m
		super.onResume();
		if (broadcastReceiver != null) {
			registerReceiver(broadcastReceiver, intentFilter);
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (mCameraTextureView_main != null) {
			mCameraTextureView_main.setPause();
		}
		if (mCameraTextureView_front != null) {
			mCameraTextureView_front.setPause();
		}
		if (broadcastReceiver != null) {
			unregisterReceiver(broadcastReceiver);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
}
