#!/system/xbin/lua

local dvs = require("dvs")
local memRead, memWrite

local function usage()
    printl("\nUSAGE: ")
    printl("mem_poke.lua -u unit -c count -p [address] [data]")
    printl("    -u unit    unit can be 1, 2 or 4 (bytes). default to 4")
    printl("    -c count   count of units. default to 1")
    printl("    -p         when specified, just printout the provided data. No read or write")
    printl("               address and count are not needed (not used if provided)")
    printl("    address    the address, in hex(with or without prefix 0x)")
    printl("    [data]     the data, in hex(with or without 0x). If provided, the command")
    printl("               is to fill the memory with this unit (1, 2 or 4 bytes) of data.")
    printl("\nExamples:")
    printl("mem_poke.lua -u 4 -c 10 fd511000      #read 10 4-byte starting from 0xfd511000")
    printl("mem_poke.lua -c 100 0xE000  10        #fill memory 0xE000-0xE190 with 0x10")
    printl("mem_poke.lua -p -u4 FE2356            #print out 0xFE2356 in binary")
end

-- uses busybox devmem for now. Will implement this in our own library
-- address - must be a number
-- unit    - 1,2,4 default to 4
-- return data array
local function memget(address, unit, count)
    if unit == nil then unit = 4 end
    if count == nil then count = 1 end

    if unit ~= 1 and unit ~= 2 and unit ~=4 then
        logErr("unit %d is not supported",unit)
        return nil
    end

    local data_array = {}
    for i=0, count-1, 1 do
        local cmd = string.format("busybox devmem 0x%x %d", address+i*unit, unit*8)
        local f = assert(io.popen(cmd, 'r'))
        local s = assert(f:read('*a'))
        f:close()
        logInf("devmem got: %s",s)
        data_array[i+1] = dvs.str2num(s)
    end

    return data_array
end

-- uses busybox devmem for now. Will implement this in our own library
-- address - must be a number
-- unit    - 1,2,4,8, default to 4
-- count   - default to 1
local function memset(address, value, unit, count)
    if unit == nil then unit = 4 end
    if count == nil then count = 1 end

    if unit ~= 1 and unit ~= 2 and unit ~=4 then
        logErr("unit %d is not supported",unit)
        return false
    end

    for i = 0, count-1, 1 do
        if not os.execute(string.format("busybox devmem 0x%x %d 0x%x", address+i*unit, unit*8, value)) then
            logErr("failed writing addres 0x%x, value 0x%x, unit %d, count %d", address, value, unit, count)
            return false
        end
    end
    return true
end

-- use memRead (from libdvsc.so) to read memory
local function memget2(address, unit, count)
    if unit == nil then unit = 4 end
    if count == nil then count = 1 end

    if unit ~= 1 and unit ~= 2 and unit ~=4 then
        logErr("unit %d is not supported",unit)
        return nil
    end

    local data_array = {}
    local result = 0;
    for i=1, count do
        data_array[i], result = memRead(address, unit)
        if result == -1 then
            logErr("failed at reading address 0x%x", address);
            return nil
        end
        address = address + unit
    end

    return data_array
end

-- use memWrite (from libdvsc.so) to write memory
local function memset2(address, value, unit, count)
    if unit == nil then unit = 4 end
    if count == nil then count = 1 end

    if unit ~= 1 and unit ~= 2 and unit ~=4 then
        logErr("unit %d is not supported",unit)
        return false
    end

    for i=1, count do
        if memWrite(address, value, unit) == -1  then
            logErr("failed at writing address 0x%x", address);
            return false;
        end
        address = address + unit
    end

    return true
end

local function main(arg)

    local optarg, optind = dvs.init(arg, "hvpu:c:", usage)

    if #arg == 0 then usage() exit() end

    local unit = dvs.str2num(optarg["u"]) or 4
    local count = dvs.str2num(optarg["c"]) or 1
    local printonly = optarg["p"] or false

    local address = dvs.str2hex(arg[optind])

    if address == nil  then
        logErr("%s is not a valid address.",arg[optind])
        exit()
    end

    local data = nil
    if printonly then
        data = address
        address = 0
    else
        data = dvs.str2hex(arg[optind+1])
    end

    if #arg > optind + 2 then
        logErr("Too many arguments.")
        exit()
    end

    if unit ~= 1 and unit ~= 2 and unit ~= 4 then
        logErr("unit is is invalid ")
        exit()
    end

    if count > 65536 then
        logErr( "count is more than 65536")
        exit()
    end

    if printonly then
        if data == nil then
            logErr("Data is required.")
            usage()
            exit()
        end
        local data_array = {}
        data_array[1] = data
        memdump(data_array, unit, 1, address)
        exit()
    end

    if data == nil then
        -- read
        logInf("read address = 0x%x, unit = %d, count = %d",address, unit, count)
        local data_array = memget(address, unit, count)
        memdump(data_array, unit, count, address)
        exit()
    else
        -- write
        logInf("write data = 0x%x address = 0x%x, unit = %d, count = %d",data, address, unit, count)
        memset(address, data, unit, count)
        exit()
    end
end

local function initModule()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    local dvscEntry
    if get_platform() == "msm8974" then
        dvscEntry = package.loadlib("/system/lib/libdvsc.so", "dvscEntry")
    else
        dvscEntry = package.loadlib("/system/lib64/libdvsc.so", "dvscEntry")
    end

    if dvscEntry then
        dvscEntry()
        memRead = dvscMemRead
        memWrite = dvscMemWrite
        memget = memget2
        memset = memset2
    else
        print("Failed to load libdvs.so. Use busybox devmem")
    end
end

local exports = {
    main      = main,
    memget    = memget2,
    memset    = memset2,
}

initModule()
if ... == "mem_poke" then
    return exports --library, export functions
else
    main(arg)
end

