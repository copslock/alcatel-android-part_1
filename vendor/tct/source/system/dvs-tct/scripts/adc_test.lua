#!/system/xbin/lua
--[[
-----------------------------------
-- This script reads ADC values using the debug sysfs
-----------------------------------
]]

local gDvs = require("dvs")

local ADC_PATH="/sys/bus/spmi/devices/qpnp-vadc*/"

-- get adc info for channel_name.
-- returns adc value (number) and a raw value (hexstring).
local function getAdc(sys_path, channel_name)
    assert( type(sys_path) == "string" )
    assert( type(channel_name) == "string" )

    -- find full path
    local rc, full_path = run_cmd("ls "..sys_path..channel_name)

    log("Attempting to open: %s", full_path)
    local line=fread_all(full_path) -- looks like "Result:31 Raw:7f24"
    if line == nil then
        err("couldn't open "..full_path)
        return nil
    end

    local value, rawval = line:match("(%d+) Raw:(%x+)") -- get values
    return value, rawval

end

-- returns all available adc channels
-- designed for use in a generic for loop
local function getAdcChannelNames(sys_path)
    assert( type(sys_path) == "string" )
    local ignore_files = "power uevent hwmon driver subsystem"
    local rc, out, err = run_cmd_save_output("ls "..sys_path)
    local f = assert(io.open(out))
    local channel_name

    -- fancy closure iterator..
    return function()
        -- grab next line, unless it's not a ADC channel
        repeat
            local done = true
            channel_name = f:read("*line")

            if not channel_name then
                -- end of output
                f:close()
                assert(os.remove(out))
                assert(os.remove(err))
            elseif ignore_files:match(channel_name) then
                -- must ignore these files, loop again
                done = false
            end
        until done
        return channel_name
    end
end

-- returns all available adc sys path
-- designed for use in a generic for loop
local function getAdcSysPath()
    local rc, out, err = run_cmd_save_output("ls -d "..ADC_PATH)
    local f = assert(io.open(out))
    local sys_path

    -- fancy closure iterator..
    return function()
        -- grab next line, unless end of file
        local done = true
        sys_path = f:read("*line")

        if not sys_path then
            -- end of output
            f:close()
            assert(os.remove(out))
            assert(os.remove(err))
        end
        return sys_path
    end
end

local function doCommandList(arg)
    print("Available ADC channels")
    for sys_path in getAdcSysPath() do
        printf("    %s:\n", sys_path)
        for channel in getAdcChannelNames(sys_path) do
            printf("        %s\n",channel)
        end
    end
end

local function doCommandDisplay(arg)
    local strTable = {}
    local pathTable = {}

    for sys_path in getAdcSysPath() do
        table.insert(pathTable, sys_path);
    end

    if #arg == 0 then
        for _,sys_path in ipairs(pathTable) do
            for channel in getAdcChannelNames(sys_path) do
                local channel_info, raw_info = getAdc(sys_path, channel)
                table.insert(strTable, {sys_path, channel, channel_info, raw_info})
            end
        end
    else
        for i,channel in ipairs(arg) do
            for _,sys_path in ipairs(pathTable) do
                if fexists(sys_path..channel) then
                    local channel_info, raw_info = getAdc(sys_path, channel)
                    if channel_info then
                        table.insert(strTable, {sys_path, channel, channel_info, raw_info})
                    else
                        err("No data for "..channel)
                    end
                end
            end
        end
    end

    if #strTable ~= 0 then
        table.insert(strTable, 1, {"path", "name", "uV or degC", "raw(in hex)"})
        gDvs.printAlignedTable(strTable, nil, "    ")
    end
end


local usageStr = [[
Usage:
adc_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported
    l                          list all channels
    d  [channel] [channel ...] display the specified channel(s), or,
                               display all channels when there is no specified channel
]]
local function usage()
    print(usageStr)
end

local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)

    local cmd = arg[optind]
    if cmd == nil then
        usage()
        exit()
    end

    if cmd == "l" or cmd == "ls" or cmd == "list" then
        doCommandList(gDvs.slice(arg, optind+1))

    elseif cmd == "d" or cmd == "ds" or cmd == "display" then
        doCommandDisplay(gDvs.slice(arg, optind+1))
    else
        logErr("Wrong parameter %s, Need l or d", arg[optind])
    end
    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main                = main,
    getAdc              = getAdc,
    getAdcChannelNames  = getAdcChannelNames,
}

initModule()
if ... == "adc_test" then
    return exports --library, export functions
else
    main(arg)
end
