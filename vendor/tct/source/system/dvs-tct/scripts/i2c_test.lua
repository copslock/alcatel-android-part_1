#!/system/xbin/lua
--[[
-----------------------------------
-- This script runs the existing i2c tools (i2cdetect, i2cget and i2cset)
-----------------------------------
]]

local gDvs = require "dvs"

local print, sf, printf = print, sf, printf


local usageStr = [[
This is a utility for manually exercising the i2c busses.

Currently this is a wrapper for the existing i2c tools (i2cdetect, i2cdump, i2cget
and i2cset).

UASGE:
    i2c_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    l                                       list available busses
    r bus slave-addr data-addr [count]      examine/read i2c registers
    s bus                                   scan for slaves on a bus
    w bus slave-addr data-addr [byte1 ...]  write i2c register

sub-command l (ls, list)
List the available i2c busses

sub-command r (read)
Examine/read i2c register
    bus         number or name of i2c bus
    slave-addr  address of chip on the bus
    data-addr   address to read from
    count       number of bytes to read (default is 1)

sub-command s (sc, scan)
Scan an i2c bus for available devices
    bus         number or name of i2c bus

sub-command w (write)
Write to an i2c register
    bus         number or name of i2c bus
    slave-addr  address of chip on the bus
    data-addr   address to read from
    [byte1 ...] bytes to be written

Examples:
    i2c_test.lua l                              #list all i2c busses
    i2c_test.lua s 12                           #scan i2c bus 12 for available devices
    i2c_test.lua r 12 0x40 0x30 10              #read 10 bytes from i2c bus 12, slave 0x40, address 0x30
    i2c_test.lua w 12 0x40 0x30 f8 f7 00 20 14  #write 5 bytes to i2c bus 12, slave 0x40, address 0x30

Note: peripherals may need their power supplies enabled first.
]]

local usage = function()
    printf(usageStr)
end

-- gather the available i2c busses
local availableI2Cbusses = function()
    -- this runs the "i2cdetect -l" command
    -- all this really does is list the stuff in /sys/class/i2c-dev
    -- instead of running the command, just list it
    local I2C_PATH="/sys/class/i2c-dev"
    local rc, out, err = run_cmd("ls "..I2C_PATH)
    return out
end

-- check that the i2c bus is a valid one
local validI2Cbus = function(bus)
    local busses = availableI2Cbusses()
    local lines = strsplit(busses, "\n")

    for i, line in ipairs(lines) do
        if line == "i2c-"..bus then
            return true
        end
    end
    logErr("i2c-%s is not an i2c bus", bus)
    return false
end

-- list available i2c busses
local doCmdList = function(args)
    local busses = availableI2Cbusses()
    print("Available i2c busses:")
    print(busses)
end

-- validate slave-address
local validateSlaveAddr = function(addr)
    local slave_addr = gDvs.str2hex(addr)
    if slave_addr == nil or slave_addr > 0x7f then
        slave_addr = nil
        logErr("slave-addr:0x%s is not valid", addr)
    end
    return slave_addr
end

-- validate data-address
local validateDataAddr = function(addr)
    local data_addr = gDvs.str2hex(addr)
    if data_addr == nil then
        logErr("data-addr:0x%s is not valid", addr)
    end
    return data_addr
end

-- examine/read i2c registers
local doCmdRead = function(args)
    -- this runs "i2cget [-f] [-y] I2CBUS CHIP-ADDRESS [DATA-ADDRESS [MODE]]
    --  I2CBUS        an integer or i2c bus name
    --  CHIP-ADDRESS  slave address of chip
    --  DATA-ADDRESS  address in chip
    --  MODE is one of (in this implementation always byte):
    --   b (read byte data, default)
    --   w (read word data)
    --   c (write byte/read byte)
    --   Append p for SMBus PEC

    -- validate the args
    if #args < 3 then
        logErr("Need at least bus, slave-addr and data-addr")
        return
    end

    -- check the bus and make sure it is valid
    local bus = args[1]:gsub("i2c%-", '')
    if not validI2Cbus(bus) then return end

    -- make sure slave-addr is in valid range
    local slave_addr = validateSlaveAddr(args[2])
    if slave_addr == nil then return end

    -- make sure data-addr is a valid hex number
    local data_addr = validateDataAddr(args[3])
    if data_addr == nil then return end

    local len = tonumber(args[4]) or 1

    -- read the data
    local noData
    printf("reading %s bytes from bus:i2c-%s, slave:0x%02x, starting address:0x%02x\n", len, bus, slave_addr, data_addr)
    for i=1, len do
        local rc, out, err = run_cmd("i2cget -f -y "..bus.." "..slave_addr.." "..data_addr)
        if out == "" then
            out = "XX"
            noData = 1
        end
        printf("0x%02x:  %s\n", data_addr, out:gsub("0x", ''))
        data_addr = data_addr + 1
    end
    if noData then
        print("  XX - no data returned for the address.")
    end
end

-- display devices connected to an i2c bus
local doCmdScan = function(args)
    -- this runs "i2cdetect [-y] [-a] [-q|-r] I2CBUS [FIRST LAST]"
    --  I2CBUS is an integer or an I2C bus name
    --  If provided, FIRST and LAST limit the probing range.

    local next = next
    if next(args) == nil then
        logErr("no i2c bus specified")
        return
    end

    for i, v in ipairs(args) do
        -- allow either i2c-x or just the i2c number
        local bus = v:gsub("i2c%-", '')
        if validI2Cbus(bus) then
            printf("\n-- scanning bus i2c-%s for slaves --\n", bus)
            print("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f")
            -- since the bus may be slow (especially bus 1 which takes 2-3 seconds per address the other busses seem OK),
            -- to improve user feedback, read one line (16 addresses) at a time and display it
            -- the data received from i2cdetect looks like this:
            -- "     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f"
            -- "00: -- -- -- -- 04 05 06 07 -- -- -- -- -- -- -- --"
            -- "10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --"
            local uu
            for i=0, 7 do
                local first = i*16
                local last = first + 0xf

                local rc, out, err = run_cmd("i2cdetect -y -a -r "..bus.." "..first.." "..last)
                local lines = strsplit(out, "\n")
                for j, line in ipairs(lines) do
                    if line:match("^"..i.."0:") then print(line) end
                    if line:match("UU") then uu=1 end
                end
            end
            if uu then
                print("  UU indicates probing was skipped because this address is currently in use by")
                print("  a driver. This strongly suggests that there is a chip at this address.")
            end
        end
    end
end

-- write to an i2c register
local doCmdWrite = function(args)
    -- this runs "i2cset -f -y I2CBUS CHIP-ADDRESS DATA-ADDRESS [VALUE] ... [MODE]"
    --  I2CBUS        an integer or i2c bus name
    --  CHIP-ADDRESS  slave address of chip
    --  DATA-ADDRESS  address in chip
    --  VALUE         data to write
    --  MODE is one of (in this implementation always byte):
    --   c (byte, no value)
    --   b (byte data, default)
    --   w (word data)
    --   i (I2C block data)
    --   s (SMBus block data)
    --   Append p for SMBus PEC

    -- validate the args
    if #args < 4 then
        logErr("Need bus, slave-addr, data-addr and at least one byte to write")
        return
    end

    -- check the bus and make sure it is valid
    local bus = args[1]:gsub("i2c%-", '')
    if not validI2Cbus(bus) then return end

    -- make sure slave-addr is in valid range
    local slave_addr = validateSlaveAddr(args[2])
    if slave_addr == nil then return end

    -- make sure data-addr is a valid hex number
    local data_addr = validateDataAddr(args[3])
    if data_addr == nil then return end

    -- write the data
    printf("writing %s bytes to bus:i2c-%s, slave:0x%02x, starting address:0x%02x\n", #args-3, bus, slave_addr, data_addr)
    local i = 4
    local written = 0
    while i <= #args do
        local data = gDvs.str2hex(args[i])
        -- check that data is a valid hex number
        if data == nil then
            logErr("Write failed at addr:0x%02x, data:%s is not valid", data_addr, args[i])
        else
            local rc, out, err = run_cmd("i2cset -f -y "..bus.." "..slave_addr.." "..data_addr.." "..data)
            if rc == nil then
                logErr("Write failed at addr:0x%02x, data:0x%02x", data_addr, data)
            else
                written = written +1
            end
        end
        data_addr = data_addr + 1
        i = i + 1
    end
    printf("%s bytes written\n", written)
end

local main = function(arg)
    local optarg, optind=gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "l"  or subcmd == "ls" or subcmd == "list" then
        doCmdList(subargs)
    elseif subcmd == "w" or subcmd == "write" then
        doCmdWrite(subargs)
    elseif subcmd == "r" or subcmd == "read" then
        doCmdRead(subargs)
    elseif subcmd == "s" or subcmd == "sc" or subcmd == "scan" then
        doCmdScan(subargs)
    else
        logErr("Incorrect sub-command %s", subcmd)
    end

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main      = main,
}

initModule()
if ... == "i2c_test" then
    return exports --library, export functions
else
    main(arg)
end
