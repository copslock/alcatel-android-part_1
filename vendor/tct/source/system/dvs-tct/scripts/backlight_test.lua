#!/system/xbin/lua
--[[
-----------------------------------
-- This script controls the keypad backlight leds
-----------------------------------
]]

local gDvs = require("dvs")

local backlight_max

local usageStr = [[
This is a utility for testing keypad backlight.

Usage:
    backlight_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    c <setting=value> [...]  Configure keypad backlight
    d <setting=value> [...]  Display keypad backlight settings

sub-command c (cs, configure)
Configure keypad backlight settings. The following settings and their values are supported:
b|brightness
    Set brightness of keypad backlight
        <value>  Any value between 0 and "max_brightness"

fc|fs_curr_ua
    Backlight current limit (in microamps) at full scale
        <value>  Any value between 0 and 30000

mo|manual_override_en
    Disables keypad brightness ramp
        1|e|enable   enable manual override
        0|d|disable  disable manual override

mb|max_brightness
    Maximum keypad backlight brightness
        <value>  Any value between 0 and 255

rs|ramp_step
    Set keypad backlight ramp step size
        <value>  ramp step size

rt|ramp_ms
    Set keypad backlight ramp step time
        <value>  ramp step time in milliseconds

sr|start_ramp
    Start keypad backlight ramp
        1|e|enable   enable keypad backlight ramp

sub-command d (ds, display)
Display backlight settings.

Examples
    # display keypad backlight settings
    backlight_test.lua d
    # turn keypad backlight on at full intensity
    backlight_test.lua c b=255
    # turn keypad backlight off
    backlight_test.lua c b=0
    # start keypad backlight ramp
    backlight_test.lua c sr=e
    # set keypad backlight ramp time to 50, ramp step to 5 and start ramp
    backlight_test.lua c rt=50 rs=5 sr=1
]]

local function usage()
    print(usageStr)
end

local backlight_path="/sys/class/leds/kpd-backlight/"

-- read system file
local function readSystemFile(backlight_file)
    local file=backlight_path..backlight_file
    return fread_all(file)
end

-- configure backlight
local function doCmdConfigure(subargs)
    local settings = gDvs.getSettings(subargs, {
        {"brightness", "b"},  {"fs_curr_ua", "fc"}, {"manual_override_en", "mo"},
        {"max_brightness", "mb"}, {"ramp_step", "rs"}, {"ramp_ms", "rt"}, {"start_ramp", "sr"} })
    local start_ramp

    if settings == nil then return end

    for k, s in ipairs(settings) do
        local setting, val = s.n, s.v
        if setting == "brightness" then
            local value = tonumber(val)
            if value == nil or value < 0 or value > backlight_max then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            printf("Setting brightness to %s\n", value)
            fwrite(backlight_path.."brightness_override", tostring(value))

        elseif setting == "fs_curr_ua" then
            local value = tonumber(val)
            if value == nil or value < 0 or value > 30000 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            printf("Setting fs_current to %s\n", value)
            fwrite(backlight_path.."fs_curr_ua", tostring(value))

        elseif setting == "manual_override_en" then
            if val == "enable" or val == "e" or val == "1" then
                print("Enabling manual override")
                fwrite(backlight_path.."manual_override_en", tostring(1))
            elseif val == "disable" or val == "d" or val == "0" then
                print("Disabling manual override")
                fwrite(backlight_path.."manual_override_en", tostring(0))
            else
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end

        elseif setting == "max_brightness" then
            local value = tonumber(val)
            if value == nil or value < 0 or value > 255 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            printf("Setting max_brightness to %s\n", value)
            fwrite(backlight_path.."max_brightness", tostring(value))

        elseif setting == "ramp_step" then
            local value = tonumber(val)
            if value == nil or value < 0 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            printf("Setting ramp_step to %s\n", value)
            fwrite(backlight_path.."ramp_step", tostring(value))

        elseif setting == "ramp_ms" then
            local value = tonumber(val)
            if value == nil or value < 0 then
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
            printf("Setting ramp_ms to %s\n", value)
            fwrite(backlight_path.."ramp_ms", tostring(value))

        elseif setting == "start_ramp" then
        -- in case there are other things being set, can't start the ramp here
        -- have to do it outside the loop
            if val == "enable" or val == "e" or val == "1" then
                start_ramp=1
            else
                logErr("Wrong value %s for setting %s", val, setting)
                return
            end
        end
    end

    if start_ramp then
        print("Starting keypad backlight ramp")
        local rc, start = run_cmd("echo $EPOCHREALTIME")
        fwrite(backlight_path.."start_ramp", tostring(e))
        local rc, stop = run_cmd("echo $EPOCHREALTIME")
        printf("elapsed time: %.2f seconds\n", stop - start)
    end

end

-- display backlight settings
local function doCmdDisplay(subargs)
    local items = {"brightness", "brightness_override", "dim_mode", "fs_curr_ua",
        "manual_override_en", "max_brightness", "ramp_ms", "ramp_step"}
    local backlightTable = {}

    print(sf("Backlight system parameters (located at: %s)", backlight_path))
    for i, line in ipairs(items) do
        table.insert(backlightTable, {line, readSystemFile(line)})
    end
    gDvs.printAlignedTable(backlightTable, nil, "  ")
end

local function main(arg)
    local optarg, optind=gDvs.init(arg, "hvf", usage)
    if #arg == 0 then
        usage()
        return
    end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local function initModule()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    -- lookup keypad max brightness
    local f=assert(io.open(backlight_path.."max_brightness", "r"),
        "keypad backlight files may have moved. not at "..backlight_path)
    backlight_max = assert(f:read("*number"))
    f:close()
end

local exports = {
    main          = main,
}

initModule()
if ... == "backlight_test" then
    return exports --library, export functions
else
    main(arg)
end
