#!/system/xbin/lua

local gDvs = require("dvs")

local gRdPath = "/system/etc/dvs/"

local getrdfile = function(fid)
    if fid == nil then
        logErr("function %s is not supported", name)
        return
    end

    if fid.group == "msm_gpio" then
        local summary = {
            {fname = "func_sel",          fdname = "func"},
            {fname = "gpio_pull",         fdname = "pull"},
            {fname = "drv_strength",      fdname = "drv"},
            {fname = "gpio_oe",           fdname = "oe"},
            {fname = "gpio_out",          fdname = "out"},
            {fname = "gpio_in",           fdname = "in"},
            {fname = "gpio_hihys_en",     fdname = "hihys"},
            {fname = "intr_enable",       fdname = "intr_en"},
            {fname = "intr_status",       fdname = "intr"},
        }
        return {
            fid     = fid,
            rdfile  = gRdPath.."msm8996_gpio.rd",
            base    = 0x01010000 + (fid.id) * 0x1000,
            summary = summary,
        }
    elseif fid.group == "pmic_gpio" then
        local summary = {
            {fname = "gpio_ok",           fdname = "enabled"},
            {fname = "gpio_val",          fdname = "input"},
            {fname = "mode",              },
            {fname = "en_and_source_sel", fdname = "src"},
            {fname = "voltage_sel",       fdname = "voltage"},
            {fname = "pullup_sel",        fdname = "pull"},
            {fname = "output_type",       fdname = "out-type"},
            {fname = "output_drv_sel",    fdname = "drive"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_gpio.rd",
            base    = 0xC000 + (fid.id -1 ) * 0x100 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_ldo" then
        local summary = {
            {fname = "vreg_ok"},
            {fname = "vreg_on"},
            {fname = "range"},
            {fname = "vset"},
            {fname = "bypass_en"},
            {fname = "pulldn_en"},
            {fname = "vs_en"},
            {fname = "sec_unlock"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_ldo.rd",
            base    = 0x14000 + (fid.id -1 ) * 0x100 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_mpp" then
        local summary = {
            {fname = "mpp_ok",           fdname = "enabled"},
            {fname = "mpp_val",          fdname = "input"},
            {fname = "mode",              },
            {fname = "en_and_source_sel", fdname = "src"},
            {fname = "voltage_sel",       fdname = "voltage"},
            {fname = "pullup_sel",        fdname = "pull"},
            {fname = "current_sel",       fdname = "current"},
            {fname = "route_sel",         fdname = "route"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_mpp.rd",
            base    = 0xA000 + (fid.id -1 ) * 0x100 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_lvs" then
        local summary = {
            {fname = "overcurrent",       },
            {fname = "vreg_ok",           fdname = "status"},
            {fname = "ocp_override",      fdname = "ocp"},
            {fname = "soft_start_en",     fdname = "soft-start"},
            {fname = "auto",              },
            {fname = "pd_en",             },
            {fname = "soft_start_sel",    fdname = "pd_cur"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_lvs.rd",
            base    = 0x18000 + (fid.id -1 ) * 0x100 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_ft_smps" then
        local summary = {
            {fname = "subtype",           fdname = "type" },
            {fname = "npm_flag",          },
            {fname = "mv_range",          },
            {fname = "vset",              },
            {fname = "npm",               },
            {fname = "auto_mode",         },
            {fname = "freq_ctl",          },
            {fname = "clk_div"            },}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_ft_smps.rd",
            base    = 0x11400 + (fid.id -1 ) * 0x300 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_hf_smps" then
        local summary = {
            {fname = "subtype",           fdname = "type" },
            {fname = "range",             },
            {fname = "v_set",             },
            {fname = "pfm_type_i",        fdname = "pfm"},
            {fname = "auto_mode",         },
            {fname = "perph_en",          fdname = "buck_en"},
            {fname = "clk_div",           },}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_hf_smps.rd",
            base    = 0x11400 + (fid.id -1 ) * 0x300 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_boost" then
        local summary = {
            {fname = "subtype",                 fdname = "type"},
            {fname = "set_output_voltage",      fdname = "vout"},
            {fname = "boost_enable",            fdname = "boost"},
            {fname = "set_current_max",         fdname = "cur"},
            {fname = "clk_div",                 fdname = "freq"},
            {fname = "set_output_voltage_bb_l", fdname = "vout_bb_l"},
            {fname = "set_output_voltage_bb_h", fdname = "vout_bb_h"},
            {fname = "bbyp_mode",               fdname = "byp_mode"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pmi8996_boost.rd",
            base    = 0x1A000 + (fid.id -1 ) * 0x300 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_bb_rf_clock" then
        local summary = {
            {fname = "clk_en"},
            {fname = "clk_ok"},
            {fname = "out_edge"},
            {fname = "out_drv"},
            {fname = "follow_pc_en"},
        }
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_clock.rd",
            base    = 0x5100 + (fid.id -1) * 0x100,
            summary = summary,
        }
    elseif fid.group == "pmic_div_clock" then
        local summary = {
            {fname = "clk_en"},
            {fname = "clk_ok"},
            {fname = "div_factor"},}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_clock.rd",
            base    = 0x5B00 + (fid.id-1) * 0x100,
            summary = summary,
        }
    elseif fid.group == "pmic_sleep_clock" then
        local summary = {
            {fname = "clk_en"},
            {fname = "smpl_en"},
            {fname = "smpl_delay"}}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pm8996_clock.rd",
            base    = 0x5A00,
            summary = summary,
        }
    elseif fid.group == "pmic_haptics" then
        local summary = {
            {fname = "haptics_en",     fdname = "enable"},
            {fname = "busy"},
            {fname = "actuator_type",  fdname = "type"},
            {fname = "vmax"}}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pmi8996_haptics.rd",
            base    = 0x1C000 + fid.pmic*2*0x10000,
            summary = summary,
        }
    elseif fid.group == "pmic_wled1" then
        local summary = {
            {fname = "en_module",         fdname = "enable"},
            {fname = "feedback_control",  fdname = "fdb_ctrl"},
            {fname = "wled_ilim",         fdname = "bost_cur_lim"}}
        return {
            fid     =  fid,
            rdfile  = gRdPath.."pmi8996_wled.rd",
            base    = 0x1D800 + fid.pmic*2*0x10000,
            summary = summary,
        }
    end
end

-- populates gSupportedNames, gSupportedFids and gSupportedNameInfo
local msmSupportedNames = function(gSupportedNames)
    local supportedFids = {} -- a table of supported function identifiers (FIDs)
    local nameInfo={}        -- a string table which has name information used by list command
    local ta_gpio={}

    nameInfo = {
        "MSM GPIO: gpio0..149  (or 0..149)",
    }
    for i=0, 149 do
        local fid = {name = sf("gpio%d", i), sname = sf("%d", i), group = "msm_gpio", id = i}
        table.insert(supportedFids, fid)
    end
    for k,i in ipairs(ta_gpio) do
	for j,fid in ipairs(supportedFids) do
	    if i == fid.id then
		table.remove(supportedFids, j)
	    end
	end
    end
    for i,fid in ipairs(supportedFids) do
        gSupportedNames[fid.name] = fid
        if fid.sname then gSupportedNames[fid.sname] = fid end
    end
    return nameInfo,supportedFids
end

-- populates gSupportedNames, gSupportedFids and gSupportedNameInfo
local pmicSupportedNames = function(gSupportedNames)

    local supportedFids = {}            -- a table of supported function identifiers (FIDs)
    local nameInfo={}                   -- a string table which has name information used by list command
    local pm_fts = {1,2,6,8,9,10,11,12} -- a list of pm ft type smps
    local pm_hfs = {3,4,5,7}            -- a list of pm hf type smps
    local pmi_fts = {2,3}               -- a list of pmi ft type smps
    local pmi_hfs = {1}                 -- a list of pmi hf type smps

    nameInfo = {
        "PM8996  GPIO: gpio1..22  (or g1..22)",
        "PMI8994 GPIO: pgpio1..10 (or pg1..10)",
        "PM8996  LDO:  ldo1..32   (or l1..32)",
        "PMI8994 LDO:  pldo1      (or pl1)",
        "PM8996  MPP:  mpp1..8    (or m1..8)",
        "PMI8994 MPP:  pmpp1..4   (or pm1..4)",
        "PM8996  LVS:  lvs1..2    (or lv1..2)",
        "PM8996  SMPS: smps1..12  (or s1..12)",
        "PMI8994 SMPS: psmps1..3  (or ps1..3)",
        "PMI8994 SMPS: boost      (or b)",
        "System clocks:",
        "  Baseband clocks:           bb_clk1..2  (or bb1..2)",
        "  Low-noise baseband clock:  ln_bb_clk   (or lnbb)",
        "  clocks for RF circuits:    rf_clk1..2  (or rf1..2)",
        "  Divider clocks:            div_clk1..3 (or div1..3)",
        "  Sleep clock:               sleep_clk   (or slp)",
        "PMI8994 HAPTICS: haptics (or h)",
        "PMI8994 WLED: wled (or w)",
    }

    for i=1, 22 do
        local fid = {name = sf("gpio%d", i), sname = sf("g%d", i), group = "pmic_gpio", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for i=1, 10 do
        local fid = {name = sf("pgpio%d", i), sname = sf("pg%d", i), group = "pmic_gpio", id = i, pmic = 1}
        table.insert(supportedFids, fid)
    end

    for i=1, 32 do
        local fid = {name = sf("ldo%d", i), sname = sf("l%d", i), group = "pmic_ldo", id = i, pmic = 0 }
        table.insert(supportedFids, fid)
    end

    local fid = {name = "pldo1", sname = "pl1", group = "pmic_ldo", id = 1, pmic = 1 }
    table.insert(supportedFids, fid)

    for i=1, 8 do
        local fid = {name = sf("mpp%d", i), sname = sf("m%d", i), group = "pmic_mpp", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for i=1, 4 do
        local fid = {name = sf("pmpp%d", i), sname = sf("pm%d", i), group = "pmic_mpp", id = i, pmic = 1}
        table.insert(supportedFids, fid)
    end

    for i=1, 2 do
        local fid = {name = sf("lvs%d", i), sname = sf("lv%d", i), group = "pmic_lvs", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for k,i in ipairs(pm_fts) do
        local fid = {name = sf("smps%d", i), sname = sf("s%d", i), group = "pmic_ft_smps", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for k,i in ipairs(pm_hfs) do
        local fid = {name = sf("smps%d", i), sname = sf("s%d", i), group = "pmic_hf_smps", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for k,i in ipairs(pmi_fts) do
        local fid = {name = sf("psmps%d", i), sname = sf("ps%d", i), group = "pmic_ft_smps", id = i, pmic = 1}
        table.insert(supportedFids, fid)
    end

    for k,i in ipairs(pmi_hfs) do
        local fid = {name = sf("psmps%d", i), sname = sf("ps%d", i), group = "pmic_hf_smps", id = i, pmic = 1}
        table.insert(supportedFids, fid)
    end

    local fid = {name = "boost", sname = "b", group = "pmic_boost", id = 1, pmic = 1}
    table.insert(supportedFids, fid)

    for i=1, 2 do
        local fid = {name = sf("bb_clk%d", i), sname = sf("bb%d", i), group = "pmic_bb_rf_clock", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    local fid = {name = "ln_bb_clk", sname="lnbb", group = "pmic_bb_rf_clock", id = 8, pmic = 0}
    table.insert(supportedFids, fid)

    for i=1, 2 do
        local fid = {name = sf("rf_clk%d", i), sname = sf("rf%d", i), group = "pmic_bb_rf_clock", id = i+3, pmic = 0}
        table.insert(supportedFids, fid)
    end

    for i=1, 3 do
        local fid = {name = sf("div_clk%d", i), sname = sf("div%d", i), group = "pmic_div_clock", id = i, pmic = 0}
        table.insert(supportedFids, fid)
    end

    local fid = {name = "sleep_clk", sname="slp", group = "pmic_sleep_clock", 1, pmic = 0}
    table.insert(supportedFids, fid)

    local fid = {name = "haptics", sname="h", group = "pmic_haptics", id = 1, pmic = 1}
    table.insert(supportedFids, fid)

    local fid = {name = "wled", sname="w", group = "pmic_wled1", id = 1, pmic = 1}
    table.insert(supportedFids, fid)

    for i,fid in ipairs(supportedFids) do
        gSupportedNames[fid.name] = fid
        if fid.sname then gSupportedNames[fid.sname] = fid end
    end
    return nameInfo,supportedFids
end



local main = function(arg)
    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end
    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main                 =   main,
    msmSupportedNames    =   msmSupportedNames,
    pmicSupportedNames   =   pmicSupportedNames,
    getrdfile            =   getrdfile,
}

initModule()
if ... == "msm8996" then
    return exports --library, export functions
else
    main(arg)
end


