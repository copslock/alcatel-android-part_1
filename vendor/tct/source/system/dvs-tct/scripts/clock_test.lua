#!/system/xbin/lua
--[[
----------------------------------------------
-- This script attempts to control/display clocks
----------------------------------------------
]]

local gDvs = require("dvs")
local print, arg = print, arg
local gSupprotedNames = {}
local CLOCK_PATH = "/d/clk/"
local gUsageStr =[[
This script uses clock framework (/d/clk) to display and configure clocks.
Usage:
    clock_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    l                                    list supported clocks
    c  <name>  <setting=value> [...]     configure a clock
    d [name_pattern] [name_pattern ...]  display clock(s)

sub-command l (ls, list)
    List supported clocks.

sub-command c (cs, configure)
    sub-command c takes a clock name and additional one or more <setting=value>
    parameters to configure the clock. Available settings and their values are:
        e|enable   enable or disable the clock, allowed values are
                   1|e|enable    enable the clock
                   0|d|disable   disable the clock

        r|rate     set the rate (in Hz) of this clock. Not all clocks support rate setting.
sub-command d (ds, display)
    Display the settings and status of one or more clocks.
    One or more name_patterns are allowed. A name_pattern can be an exact name, or a pattern
    to match the clock names.

Examples:
    clock_test.lua l    #list all names of all supported clocks
    clock_test.lua c mdss_ahb_clk e=1  #enable mdss_ahb_clk
    clock_test.lua c sdcc3_apps_clk_src e=1 rate=25000000 # enable sdcc3_apps_clk_src and set
                                                          # rate to 25MHz
    clock_test.lua d rf_clk1        # display rf_clk1
    clock_test.lua d "*rf_*"  *bb_* # display the matched clocks (such as rf_clk1, bb_clk1, etc.)
]]

-----------------------------------
-- kernel clock framework API
-----------------------------------
-- lookup full path for clock - plus existing check
local function getClockPath( clockName )
    assert(type(clockName) == "string", "clock name isn't a string: "..tostring(clockName))
    local fullPath = CLOCK_PATH..clockName
    if fexists(fullPath) == false then
        logErr("clock name "..clockName.." looks bad. ")
        return nil
    end
    logInf("full path is: %s/", fullPath)
    return fullPath.."/"
end

-- collects in a table all info about a clock for easy access
local function collectClockInfo( clockName )
    local clockFiles = {
        "enable", "rate",
    }
    local clockPath = getClockPath( clockName )
    if clockPath == nil then
        log("no path to get info from.")
        return
    end
    local returnTable = {}

    for i,v in ipairs( clockFiles ) do
        local content = fread_all(clockPath..v)
        if content then
            returnTable[v] = content
        else
            returnTable[v] = ""
        end
    end
    return returnTable
end

-- for use in generic for loop, returns an iterator that steps through all clocks
local function getNextClockName(namePattern)
    local rc, out, err = run_cmd_save_output("cd "..CLOCK_PATH.."; ls -d "..namePattern)
    local f = assert(io.open(out)) -- f points to a file with the contents of clock_PATH
    local clockName

    -- fancy closure iterator..
    return function()
        -- grab next line, unless it's not a real clock
        repeat
            local done = true
            clockName = f:read("*line") -- read a directory name at a time

            if not clockName then
                -- end of output
                f:close()
                assert(os.remove(out))
                assert(os.remove(err))
            end
        until done
        return clockName
    end
end

local function getClockSummary( clockName)
    local i = collectClockInfo( clockName )
    if i == nil then
        err("No info, exiting")
        exit()
    end

    if      i.enable == "1" then i.enable = "enabled"
    elseif  i.enable == "0" then i.enable = "disabled"
    end

    return {clockName, i.enable, "rate:"..i.rate}
end

-- state is 0 or 1
local function enableClock( clock, state )
    assert( tonumber(state) ~= nil )
    state = tostring(state)
    local clockPath = getClockPath( clock )
    if clockPath == nil then
        log( "%s not found", clock )
        return
    end
    print("set "..clock.." to state "..state)
    -- check current state
    local currentState = fread_all(clockPath.."enable")
    if not currentState then
        logErr("couldn't read current clock state")
    elseif currentState and currentState == state then
        print("Already in that state")
    else
        -- write new state to framework
        if fwrite(clockPath.."enable", state) then
            logErr("set clock state failed")
        end
    end
end

-- set clock rate
local function setClockRate( clock, rate)
    local clockPath = getClockPath( clock )
    if clockPath == nil then
        logInf( "%s not found", clock )
        return
    end
    print("set "..clock.." to rate "..rate)
    local e = fwrite(clockPath.."rate", tostring(rate))
    if e then
        logErr("set clock mode failed")
    end
end

local function matchClockName(clockName)
    for i, name in ipairs(gSupprotedNames) do
        if clockName == name then return name end
    end
end

local function doCmdDisplay(args)
    local optarg, optind = gDvs.parseOptions(args, "c")
    local client = optarg.c

    -- all names if no name is specified
    if #args < optind then args[optind] = "*" end

    local names={}
    for i=optind, #args do
        local name = matchClockName(args[i])
        if name then names[name] = 1
        else
            for name in getNextClockName(args[i]) do
                names[name] = 1
            end
        end
    end

    local sortedNames={}
    for key, v in pairs(names) do table.insert(sortedNames, key) end
    table.sort(sortedNames)

    if #sortedNames == 0 then
        logErr("No clock matches the name(s). Unfamiliar with clock names? Try subcommand l first.")
        exit()
    end

    local summaries={}
    for i, name in ipairs(sortedNames) do
        table.insert(summaries, getClockSummary( name))
    end

    gDvs.printAlignedTable(summaries)
end

local function doCmdConfigure(args)

    if args[1] == nil then
        logErr("Need a clock name")
        return
    end

    local name = matchClockName(args[1])
    if name == nil then
        logErr("Wrong clock name %s", args[1])
        return
    end

    local settings = gDvs.getSettings(gDvs.slice(args,2),
        { {"enable", "e"},  {"rate", "r"}})
    if settings == nil then return end

    local enableValue, rateValue
    for k, s in ipairs(settings) do
        local setting, value = s.n, s.v
        if setting == "enable" then
            if value == "enable" or value == "e" or value == "1" then
                enableValue = 1
            elseif value == "disable" or value == "d" or value == "0" then
                enableValue = 0
            else
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        elseif setting == "rate" then
            rateValue = gDvs.str2num(value)
            if rateValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        end
    end

    if enableValue then enableClock(name, enableValue) end
    if rateValue   then setClockRate(name, rateValue) end
end

local function doCmdList(args)
    print("Available clocks are:")
    gDvs.printAlignedTable(gDvs.to2dTable(gSupprotedNames,3))
end

local function usage()
    print(gUsageStr)
end

local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    local rc = false
    if subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    elseif subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "l"  or subcmd == "ls" or subcmd == "list" then
        doCmdList(subargs)
    else
        logErr("Incorrect sub-command %s", subcmd)
    end
end

local function moduleInit()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    for name in getNextClockName("*") do
        table.insert(gSupprotedNames, name)
    end
end

local exports = {
    main      = main,
}

moduleInit()
if ... == "clock_test" then
    return exports --library, export functions
else
    main(arg)
end
