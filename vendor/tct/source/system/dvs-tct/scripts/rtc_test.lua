#!/system/xbin/lua
--[[
-----------------------------------
-- This script reads/controls and the PMIC RTC.
-----------------------------------
]]
local gDvs  = require("dvs")
local gSpmi = require("pmic_test")

local rtcPmicRegs = {
    w_data   = "0x6040",
    time_adj = "0x6044",
    en_ctl   = "0x6046",
    r_data   = "0x6048",
}

local usageStr = [[
This is a utility for read/write enable/disable the PMIC Real Time Clock (RTC).

USAGE:
    rtc_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    c  configure RTC
    d  display current RTC state

sub-command c (cs, configure)
Configure RTC settings. The following settings and their values are supported:
e|enable
    Enable or Disable RTC. Values can be
        1|e|enable    enable rtc
        0|d|disable   disable rtc
t|time
    Write the epoch time to the PMIC.
        <value>       value in seconds

sub-command d (ds, display)
Display the current state of RTC.

Examples:
rtc_test.lua d               # display RTC summary
rtc_test.lua c e=disable     # disable RTC
rtc_test.lua c t=1427390732  # set RTC to March 26, 2015 17:25:31


Note: TZ issues may prevent some RTC commands from being executed properly.
      See "Install Custom TrustZone for DVS test" wiki for more information.
]]

local function usage()
    print(usageStr)
end

-- utility functions
local function rtcReadSystemFile( rtc_file )
    local system_rtc_path = "/sys/class/rtc/rtc0/"
    local file=system_rtc_path..rtc_file
    print(sf("outputing %s:", file))
    return fread_all(file)
end

local function rtcEpochToRtc( epoch )
    local data = {} -- throw into a table and use concat to output as a string
    for i=1,4 do
        data[#data+1] = sf("0x%02x", epoch % 0x100)
        epoch = math.modf( epoch / 0x100)
    end
    return table.concat(data, " ")
end

local function rtcRtcToEpoch( rtc_string )
    local d0, d1, d2, d3 = rtc_string:match("(%x+) (%x+) (%x+) (%x+)")
    d0 = tonumber(d0, 16)
    d1 = tonumber(d1, 16)
    d2 = tonumber(d2, 16)
    d3 = tonumber(d3, 16)
    log("%x %x %x %x", d0, d1, d2, d3)
    return d0+d1*0x100+d2*0x10000+d3*0x1000000
end

-- turn RTC on/off
local function rtcSetState( enable )
    local reg = rtcPmicRegs.en_ctl
    local write_data = tohexstring(enable*0x80)
    local info_string = "?"
    if enable == 1 then info_string = "on"
    elseif enable == 0 then info_string = "off"
    end
    local read_string = gSpmi.spmiTrim( gSpmi.spmiRead( reg, 1 ))
    if read_string == write_data:gsub("0x","") then
        print(sf("RTC is already %s", info_string))
        return
    end
    print(sf("Turning RTC %s. SPMI reg: %s with: %s", info_string, reg, write_data))
    gSpmi.spmiWrite( reg, write_data )
end

-- write a new RTC value to the PMIC - resets the time
-- optional enable - determines if we re-enable the RTC
local function rtcWrite( epoch, enable )
    if enable then
        rtcSetState(enable)
    end

    if epoch then
        local rtc_state
        if gSpmi.spmiReadRegister( rtcPmicRegs.en_ctl ) == 0 then
            log("RTC is off, keep it in that state after writing to RTC")
            rtc_state = "off"
        end
        rtcSetState( 0 ) -- disable rtc, required before writing to RTC
        local reg = rtcPmicRegs.w_data
        local write_data = rtcEpochToRtc(epoch)
        print(sf("Writing RTC data %s to SPMI register: %s", write_data, reg))
        gSpmi.spmiWrite( reg, write_data )
        if rtc_state ~= "off" then
            rtcSetState( 1 ) -- re-enable RTC
        end
    end
end

-- reads the current value/state of the rtc
local function rtcRead()
    local reg = rtcPmicRegs.r_data
    local read_string = gSpmi.spmiTrim( gSpmi.spmiRead( reg, 4 ))
    print("pmic rtc_rdata registers data: "..read_string)
    print(rtcRtcToEpoch(read_string).."s since epoch")
    reg = rtcPmicRegs.en_ctl
    read_string = gSpmi.spmiTrim( gSpmi.spmiRead( reg, 1 ))
    if read_string == "00" then
        print("RTC is turned off!")
    elseif read_string == "80" then
        print("RTC is turned on")
    end
end

local function doCmdConfigure(subargs)
    local settings = gDvs.getSettings(subargs, { {"enable", "e"}, {"time", "t"} })
    local enableValue, epoch

    if settings == nil then return end

    for i, v in ipairs(settings) do
        local setting, value = v.n, v.v
        if setting == "enable" then
            if value == "enable" or value == "e" or value == "1" then
                enableValue = 1
            elseif value == "disable" or value == "d" or value == "0" then
                enableValue = 0
            else
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        elseif setting == "time" then
            epoch = tonumber(value)
            if epoch == nil or epoch <0 then
                err("t must be a number >0")
                return
            end
        end
    end
    rtcWrite(epoch, enableValue)
end

local function doCmdDisplay(subarg)
    rtcRead()
    print("\nRTC system parameters:")
    print(rtcReadSystemFile("date"))
    print(rtcReadSystemFile("time"))
    print(rtcReadSystemFile("since_epoch"))
end


-----------------------------------
-- main
-----------------------------------
local function main(arg)

    local optarg, optind=gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main                = main,

    rtcReadSystemFile  = rtcReadSystemFile,
    rtcEpochToRtc      = rtcEpochToRtc,
    rtcRtcToEpoch      = rtcRtcToEpoch,
    rtcSetState        = rtcSetState,
    rtcWrite           = rtcWrite,
    rtcRead            = rtcRead,

}

initModule()
if ... == "rtc_test" then
    return exports --library, export functions
else
    main(arg)
end
