#!/system/xbin/lua
--[[
-----------------------------------
-- This script controls the camera flash led using the debug sysfs
-----------------------------------
]]

local dvs = require("dvs")

local flash_leds  = { t0="led:torch_0", t1="led:torch_1", f0="led:flash_0", f1="led:flash_1"}
local flash_switch  = "/sys/class/leds/led:switch/brightness"
local flash_files = {}
local flash_max = {}

-- init function for camflash
local function camflash_init()
    local flash_path="/sys/class/leds/"

    if get_platform() == "msm8974" then
        -- override to msm8974 specific values
        flash_leds.t0="led:flash_torch"
        flash_leds.t1="torch-light"
    end

    for k, v in pairs(flash_leds) do
        flash_files[k] = flash_path..v
        log("%s led is found at %s", k, flash_files[k])
        -- lookup max brightness
        local f=assert(io.open(flash_files[k].."/max_brightness", "r"),
            "led files may have moved. not at "..flash_path)
        flash_max[k] = assert(f:read("*number"))
        f:close()
    end
end

-- sets the current for a particular flash/torch led
local function set_flash_current( led, brightness )
    assert( flash_files["t0"], "Need to call camflash_init() first")
    local file = flash_files[led]
    assert( file, "What led is "..tostring(led).."?")
    assert( type(brightness) == "number" )
    assert( brightness <= flash_max[led] )

    log("writing %d to file: %s/brightness", brightness, file)
    fwrite( file.."/brightness", tostring(brightness))
end

-- sets the current for a particular flash/torch led
local function led_switch( led)
    fwrite(flash_switch, tostring(led))
end

-- reports the maximum current for a particular flash/torch led
local function get_max_current( led )
    assert( flash_files["t0"], "Need to call camflash_init() first")
    assert( flash_files[led], "What led is "..tostring(led).."?")
    return flash_max[led]
end

-- dvs module requires usage be defined
local function usage()
    print()
    print("USAGE: ")
    print("    camflash_test.lua f|flash <current>          # run in flash mode ")
    print("    camflash_test.lua t|torch <current>  <time>  # run flash in torch mode")
    print()
    print("current: current in mA")
    print("time:    time in second to run flash in torch mode. 0 = forever")

    print("Examples:")
    print("camflash_test.lua f 1000   # turn on flash (1 second) with 1000 mA current")
    print("camflash_test.lua t 100 5  # run flash in torch mode for  5 seconds with 100 mA current")
end

local function main(arg)
    local optarg, optind=dvs.init(arg, "hv", usage)

    local torch, flash, current
    local cmd = arg[optind]
    if cmd == nil then
        usage()
        exit()
    end

    if cmd == "f" or cmd == "flash" then
        optind = optind + 1
        current = arg[optind]
        if current == nil then
            logErr("Need current for flash")
            exit()
        end
        current = tonumber(current)
        if current == nil or current <0  then
            logErr("Wrong parameter %s for current", arg[optind])
            exit()
        end
        flash = 1

    elseif cmd == "t" or cmd == "torch" then
        optind = optind + 1
        current = arg[optind]
        if current == nil then
            logErr("Need current for torch")
            exit()
        end
        current = tonumber(current)
        if current == nil or current <0  then
            logErr("Wrong parameter %s for current", arg[optind])
            exit()
        end

        optind = optind+1
        torch =  arg[optind]
        torch = tonumber(torch)
        if torch == nil then
            logErr("Wrong parameter %s for torch time", arg[optind])
            exit()
        end
    else
        logErr("Wrong parameter %s. Need f or t", arg[optind])
        exit()
    end

    if arg[optind+1] then
        printf("Warning: extra parameter %s is ignored\n", arg[optind+1])
    end

    -- pull and display arguments
    log("")
    log_param("flash", flash)
    log_param("torch", torch)
    log_param("current", current)

    local leds = {} -- table of values to set_flash_current() on
    local delay = 1
    if flash then
        leds[1] = "f0" -- match the keys in flash_leds table
        leds[2] = "f1"
    end
    if torch then
        leds[1] = "t0" -- match the keys in flash_leds table
        leds[2] = "t1"
        delay = torch
    end

    camflash_init()

    local one_led_max = get_max_current(leds[1])
    if current > #leds*one_led_max then
        printf("Warning: current %d is adjusted to  maximum allowed current %d.\n", current,
            #leds*one_led_max)
        current =  #leds*one_led_max
    end

    -- do real work now
    if current>0 then
        -- for each led set the brightness
        for k,v in ipairs(leds) do
            log("k %s, v %s", k, v)
            local this_led_current = math.min(current, one_led_max)
            print("Setting "..flash_leds[v].." led brightness to "..this_led_current)
            set_flash_current( v, this_led_current)
            current = current - this_led_current
        end
        -- now turn on
        led_switch(1)

        if current ~= 0 then
            err("Wasn't able to satisfy current requirement. shortfall: "..current)
        end

        print("Waiting "..delay.." seconds...")
        sleep(delay)  -- don't turn off immediately

        if delay == 0 then
            print("Leaving torch on.")
            exit()
        end

    end

    -- now turn off
    led_switch(0)
    for k,v in ipairs(leds) do
        print("Turning off "..flash_leds[v])
        set_flash_current( v, 0)
    end

    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main      = main,
    camflash_init = camflash_init,
    get_max_current = get_max_current,
    set_flash_current = set_flash_current,
    led_switch = led_switch,
}

initModule()
if ... == "camflash_test" then
    return exports --library, export functions
else
    main(arg)
end

