#!/system/xbin/lua
-------------------------------------------------
-- DVS utility library for lua based DVS scripts
-------------------------------------------------

-- require("strict") -- uncomment to check for uninitialized globals

-- globals are slow, bind common globals to locals for this file scope
local io, os, math, bit32, string, table, pairs, ipairs, type, assert =
    io, os, math, bit32, string, table, pairs, ipairs, type, assert
local tostring, tonumber, print, next = tostring, tonumber, print, next


-- global stuff
local verbose = nil   -- controls output of log()s via -z commandline option
local returncode = 0  -- exit uses this returncode, calling err() sets it

---- check DVS version
local function dvs_version()
    local DVS_VERSION="1.4.1"
    return DVS_VERSION
end

function exit() os.exit(returncode) end -- simplified exit()

-------------------------------------------------------------------------------------
-- prints and logs
-------------------------------------------------------------------------------------
function printf(...)
    io.write(string.format(...))
end

function printl(formatstr,...)
    if formatstr == nil then formatstr = "" end
    io.write(string.format(formatstr.."\n",...))
end

-- log function
-- avoid ..  eg. log("output " .. var .. ".") not recommended.
-- instead use log("output %s .", var)
-- as string concats are expensive and are evaluated even if verbose isnt true.
function log(log_str, ...)
    assert( type(log_str) == "string")
    if verbose then
        io.write(string.format("  "..log_str.."\n", ...))
    end
end
logInf = log

function logErr(err_string, ...)
    assert( type(err_string) == "string")
    io.stderr:write(string.format("ERROR: "..err_string.."\n", ...))
    returncode = 1
end

function logDbg(dbg_string, ...)
    assert( type(dbg_string) == "string")
    if feature_debug then
        io.stderr:write(string.format("DBG: "..dbg_string.."\n", ...))
        returncode = 1
    end
end

-- print an error, continue but ensure final exit returns a failure
-- TODO: remove all reference to err function
function err(error_string, ...)
    assert( type(error_string) == "string")
    io.stderr:write("Error: "..error_string.."\n", ...)
    returncode=1
end

-- log the name and value of a variable. eg log_param("list", list)
function log_param(param_name, value)
    log ("%10s  = %s (%s)", param_name, tostring(value), type(value))
end

-------------------------------------------------------------------------------------
-- string
-------------------------------------------------------------------------------------
sf = string.format  -- for printf style formatting sf("%s %08.2f", string, number)

function strtrim(str)
    return str:match"^%s*(.*)":match"(.-)%s*$"
end

function strsplit(s, p)
    local temp = {}
    local index = 0
    local last_index = string.len(s)

    while true do
        local i, e = string.find(s, p, index)

        if i and e then
            local next_index = e + 1
            local word_bound = i - 1
            table.insert(temp, string.sub(s, index, word_bound))
            index = next_index
        else
            if index > 0 and index <= last_index then
                table.insert(temp, string.sub(s, index, last_index))
            elseif index == 0 then
                temp = nil
            end
            break
        end
    end

    return temp
end

-- tobool() for all sorts of input return true or false:
-- "Enable", "on", "1", "100", 5, -21 -> true
-- "disable", "OFF", "0", 0 -> false
-- nil -> nil
function tobool(input)
    if input == nil then return nil end
    assert( type(input) =="string" or type(input) == "number" )

    if type(input) == "string" then
        input = input:lower()
        if input:match("on") or input:match("en") or input:match("true") then
            return true
        end
        if input:match("off") or input:match("dis") or input:match("false") then
            return false
        end
        input = tonumber(input) -- attempt to convert to a number
    end
    if type(input) == "number" then
        if input == 0 then
            return false
        end
        return true
    else -- tonumber failed to find a number
        return nil
    end
end

-- tohexstring() convert a number into hex string, even if it might be nil
-- if nil just return "nil"
function tohexstring(value)
    if value then
        if type(value) == "number" then
            return sf("0x%02x", value)
        elseif type(value) == "string" then
            return value -- assume it's already a hexstring
        else
            return "?"
        end
    else
        return "nil"
    end
end

-- str2num() convert to a number from a number of different formats
-- if definitely not a number return nil
local function str2num( input )
    if type( input ) == "number" then
        return input
    elseif type(input) == "string" then
        -- try converting a hexstring
        local substring = input:match("^%s*0[xX](%x+)%s*$")
        if substring then
            return tonumber(substring, 16)
        end
        -- try regular decimal string
        substring = input:match("^%s*(%d+)%s*$")
        if substring then
            return tonumber(substring)
        end
    end
    -- not sure what this is
    return nil
end

-- Get hex value from different formats
-- input can start with or without 0x (or 0X)
local function str2hex(input)
    if input == nil then return end
    local value = tonumber(input:gsub("^%s*0[xX]", ""), 16)
    return value
end

-- utility function to remove the last character if it's a line break
function remove_last_linebreak( str )
    if str == nil then
        log("can't remove linebreak from nil")
        return nil -- not an error - just return
    end
    assert( type(str) == "string" )
    if str:match("\n", -1) then
        --log("removed line break from: %s", str)
        return str:sub(1,-2)
    end
    return str
end

-- math functions
max = math.max -- max( 1, 2, 3) -> 3
min = math.min
floor = math.floor
ceil = math.ceil
abs = math.abs

-- bit field operations
band = bit32.band
btest = bit32.btest -- like band() but returns true/false not a bitfield
bor = bit32.bor
bnot = bit32.bnot
bxor = bit32.bxor

-- sleep delay seconds
function sleep( delay )
    assert ( tonumber(delay) , "delay must be a number or number string")
    log ("delaying %d seconds", delay)
    os.execute("sleep "..delay)
end

-- reads the contents of a file into a variable and returns that variable
function fread_all( filename )
    assert( type(filename) == "string", "Filename must be a string")
    local f, e = io.open(filename, "r")
    if f == nil or e then
        log("Couldn't open %s for read. %s", filename, tostring(e))
        return nil
    end
    local content = f:read("*all")
    content = remove_last_linebreak(content)
    f:close()
    if content then
        log("- read all %s", filename)
    else
        log("- read nothing from %s", filename)
    end
    return content
end

-- function that writes a string to a file.
-- With error handling, logging etc.
-- returns errorstring/nil
function fwrite( filename, str, silent_fail, no_linebreak )
    assert( type(filename) == "string", "Filename must be a string")
    assert( type(str) == "string", "Will only fwrite() a string")
    local f, e = io.open(filename, "w")
    if f == nil or e then
        err("can't open "..filename.." for write. "..tostring(e))
        return e
    end
    local linebreak
    if no_linebreak then
        f:setvbuf("no") -- no buffer, write immediately
        linebreak = ""
    else
        -- assume we're writing text, with linebreaks
        f:setvbuf("line")
        linebreak = "\n"
    end

    log("+ writing %s to %s", str, filename)
    local ret
    ret, e = f:write(str..linebreak)
    if (ret == nil or e) and not silent_fail then
        err("Write to "..filename.." failed: "..tostring(e))
    end
    f:close()
    return e
end

-- check a file exists
function fexists( filename )
    local f, e = io.open(filename, "r")
    if f == nil then
        log("file %s doesn't exist? %s", filename, tostring(e))
        return false
    end
    f:close()
    return true
end

-- run a shell command and capture output, stderr (and returncode)
-- returns filenames to output and stderr files
function run_cmd_save_output(cmd)
    assert( type(cmd) == "string")

    local stdout_file = assert(os.tmpname(), "unable to generate random filename")
    local stderr_file = assert(os.tmpname(), "unable to generate random filename")
    log ("* executing command: %s", cmd)
    log ("saving stdout to %s, stderr to %s", stdout_file, stderr_file)
    local retcode = os.execute(cmd.." 2> "..stderr_file.." 1> "..stdout_file)

    return retcode, stdout_file, stderr_file
end

-- runs a shell command - returns returncode, plus output and stderr as strings
-- note that these string are possibly huge...
--  test like this: print( run_cmd("ls /tmp"))
--  nil	/tmp   	<emptystderr>: No such file or directory
function run_cmd(cmd)
    assert( type(cmd) == "string")

    local retcode, out, err = run_cmd_save_output(cmd)
    local stdout = fread_all(out)
    if stdout == nil then err("Can't open "..out) end
    assert(os.remove(out))
    local stderr = fread_all(err)
    if stderr == nil then err("Can't open "..err) end
    assert(os.remove(err))

    return retcode, stdout, stderr
end

-- check we've got debugfs
local function debugfs()
    if fexists("/sys/kernel/debug")==false then
        err("No sign of debugfs: /sys/kernel/debug\n")
        return false
    end
    if fexists("/sys/bus/spmi/devices")==false then
        err("No sign of sys fs: /sys/bus/spmi/devices\n")
        return false
    end
    -- add check for /sys/class too?
    return true
end

-- check we've got root
local function root()
    local rc, stdout = run_cmd("id")
    local root = stdout:match("root")
    if not root then
        err("Not root! To become the superuser, type:\n  su\n")
        return false
    end
    return true
end

-----------------
-- Table utilities
-----------------
local function slice(theArray, startInd, endInd)
    local temp = {}
    if endInd == nil then endInd = #theArray end
    for i= startInd, endInd do
        table.insert(temp, theArray[i])
    end
    return temp
end

-- check if a table is empty
local function table_empty( t )
    assert( type(t) == "table" )
    return next(t) == nil
end

-- sort according to the keys within a table
-- returns a sorted index of the keys in a table t
local function sort_keys( t )
    -- make integer key table of from table
    local index_array = {}
    for k, v in pairs(t) do
        table.insert(index_array, k)
    end
    -- sort the integer key table
    table.sort(index_array)
    return index_array
end

-- sort according to the values within a table
-- returns a sorted index of the keys in a table t
local function sort_values( t )
    -- make integer key table of from table
    local index_array = {}
    for k, v in pairs(t) do
        table.insert(index_array, k)
    end
    -- sort the integer key table, according to the values in the table
    table.sort(index_array, function(a,b) return t[a] < t[b] end)
    return index_array
end

-- iterator for sort*_pairs() - don't use directly
local function sort_iter( state )
    state.i = state.i+1
    local i = state.i
    --log("i:"..i.." key:"..state.s[i].." value:"..state.t[state.s[i]])
    if state.s[i] then
        return state.s[i], state.t[state.s[i]]  -- return key and value
    end
end

-- pairs() replacement that iterates over a table by key order
local function sortkey_pairs( tabl )
    assert( type(tabl) == "table" )
    local state = { s=sort_keys(tabl), t=tabl, i=0 } --store: sorted keys, original table, index

    return sort_iter, state
end

-- pairs() replacement that iterates over a table by value order
local function sortval_pairs( tabl )
    assert( type(tabl) == "table" )
    local state = { s=sort_values(tabl), t=tabl, i=0 } --store: sorted keys, original table, index

    return sort_iter, state
end

-- pretty print a table as a table - best for short key/values
-- dump_table( { key1=1, key2="value2", k3="" }, "Label" )
--> label:
-->   key1    key2  k3
--> ---------------------
-->      1  value2
-- keys will be sorted in alphabetical order
-- if table is empty, return without printing anything
function dump_table( tabl, label, no_empties)
    if table_empty( tabl ) then return end
    local heading, data = "", ""
    local line_len = 0
    local dump_out = function()
        -- output what we have so far
        print(heading)
        print(string.rep("-", line_len))
        print(data)
        print()
        heading,data = "", ""
        line_len = 0
    end
    if label then print(label..":") end
    for k,v in sortkey_pairs( tabl ) do
        if v == "" and no_empties then
            break -- don't print empty entries
        end
        local len = max( tostring(k):len(), tostring(v):len())
        if line_len + len >= 80 then
            dump_out()
        end
        len = len + min(line_len, 2)  -- add 2 spaces unless at start of line
        line_len = line_len + len
        heading = sf("%s%s%s", heading,
            string.rep(" ",len-tostring(k):len()), k)
        data = sf("%s%s%s", data,
            string.rep(" ",len-tostring(v):len()), v)
    end
    if (line_len > 0) then
        dump_out()
    end
end

-- pretty print a table as a list of key = value pairs. nicer for long key/values
-- dump_list( { key1=1, key2=value2, k3="" }, "Label" )
--> label:
--> ---------------------
--> key1 =      1
--> key2 = value2
--> k3   =
-- keys will be sorted in alphabetical order
-- if table is empty, return without printing anything
function dump_list( tabl, label, no_empties)
    if table_empty( tabl ) then return end
    local max_key_len, max_val_len = 0,0
    for k,v in pairs(tabl) do
        max_key_len = max( tostring(k):len(), max_key_len)
        max_val_len = max( tostring(v):len(), max_val_len)
    end
    if label then print(label..":") end
    print(string.rep("-", max_key_len + max_val_len + 3))
    for k,v in sortkey_pairs( tabl ) do
        if v == "" and no_empties then
            break -- dont print empty entries
        end
        print(sf("%s%s = %s%s", k,
            string.rep(" ",max_key_len-tostring(k):len()),
            string.rep(" ",max_val_len-tostring(v):len()), v))
    end
    print()
end

-- align strings in same column in the two dimension string table (strTable) by adding spaces to the right,
-- or to the left if alignRight is not nil
local function alignTable(strTable, alignRight)
    local columns = {}

    for i, row in ipairs(strTable) do
        for j, str in ipairs(row) do
            local len = str:len()
            if columns[j] == nil then columns[j] = len
            elseif columns[j] < len then columns[j] = len
            end
        end
    end

    -- format the strings according to column lengths
    local align  = "-"
    local aligned = {}
    if alignRight ~= nil then align = "" end
    for i, row in ipairs(strTable) do
        aligned[i] = {}
        for j, str in ipairs(row) do
            local formatstr = sf("%%%s%ds", align, columns[j])
            aligned[i][j] = sf(formatstr, str)
        end
    end

    return aligned
end

local function printAlignedTable(strTable, alignRight, header, spacer, trailer)
    local aligned = alignTable(strTable, alignRight)
    header  = header or ""
    spacer  = spacer or "  "
    trailer = trailer or ""

    for i, row in ipairs(aligned) do
        printf("%s", header)
        for j, str in ipairs(row) do
            printf("%s", str)
            if j ~= #row then printf("%s", spacer) end
        end
        printf("%s\n", trailer)
    end
end

-- convert 1-D array (table1d) to a 2-D array (table2d)
local function to2dTable(table1d, columns)
    local table2d={}
    local count = 1
    local curr
    for i, name in ipairs(table1d) do
        if count == 1 then
            table.insert(table2d, {})
            curr = table2d[#table2d]
        end
        table.insert(curr, name)
        count = count + 1
        if count == columns+1 then count = 1 end
    end
    return table2d

end

local function tobits(value, bit_num)
    if value == nil then return nil end

    local bit_num = tonumber(bit_num)
    if bit_num == nil or  bit_num > 32 then bit_num = 32 end

    local t={}
    for i = 0, bit_num-1 do
        t[i+1] = bit32.extract(value, i)
    end

    return t
end

-- start_address - must be number (or number string)
-- data - integer array
-- unit - 1, 2, or 4
function memdump(data, unit, count, start_address, noheader)
    if unit == nil then unit = 4 end
    if count == nil then count = 1 end
    start_address = start_address or 0

    local headstr = ""
    if not noheader then
        headstr = "address        data"
        if unit == 1 then     headstr = headstr.."   76543210"
        elseif unit == 2 then headstr = headstr.."      54321098 76543210"
        elseif unit == 4 then headstr = headstr.."            10987654 32109876 54321098 76543210"
        else
            logErr("memdump unit parameter must be 1, 2 or 4")
            return
        end
        print(headstr)
    end

    for i = 1, count do
        local valuestr = ""
        local bitstr = ""
        for j= unit, 1, -1 do
            local byte = bit32.extract(data[i], (j-1)*8, 8)
            valuestr = valuestr.." " ..string.format("%02x",byte)
            bitstr = bitstr.." "..string.reverse(table.concat(tobits(byte,8)))
        end
        print(string.format("0x%08x    %s    %s", start_address+(i-1)*unit, valuestr, bitstr))
    end

    if not noheader then
        print(headstr)
    end
end

-------------------------------------------------------------------------------------
-- Command line processing
-------------------------------------------------------------------------------------
--
-- alt_getopt - for commandline processing
--
-- Copyright (c) 2009 Aleksey Cheusov <vle@gmx.net>
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

local function convert_short2long (opts)
    local i = 1
    local len = #opts
    local ret = {}

    for short_opt, accept_arg in opts:gmatch("(%w)(:?)") do
        ret[short_opt]=#accept_arg
    end

    return ret
end

local function exit_with_error (msg)
    err(msg)
    exit()
end

local function err_unknown_opt (opt)
    exit_with_error ("Unknown option `-" ..
        (#opt > 1 and "-" or "") .. opt .. "'\n")
end

local function canonize (options, opt)
    if not options [opt] then
        err_unknown_opt (opt)
    end

    while type (options [opt]) == "string" do
        opt = options [opt]

        if not options [opt] then
            err_unknown_opt (opt)
        end
    end

    return opt
end

-- phil - made this local
local function get_ordered_opts (arg, sh_opts, long_opts)
    local i      = 1
    local count  = 1
    local opts   = {}
    local optarg = {}

    local options = convert_short2long (sh_opts)
    for k,v in pairs (long_opts) do
        options [k] = v
    end

    while i <= #arg do
        local a = arg [i]

        if a == "--" then
            i = i + 1
            break

        elseif a == "-" then
            break

        elseif a:sub (1, 2) == "--" then
            local pos = a:find ("=", 1, true)

            if pos then
                local opt = a:sub (3, pos-1)

                opt = canonize (options, opt)

                if options [opt] == 0 then
                    exit_with_error ("Bad usage of option `" .. a .. "'\n")
                end

                optarg [count] = a:sub (pos+1)
                opts [count] = opt
            else
                local opt = a:sub (3)

                opt = canonize (options, opt)

                if options [opt] == 0 then
                    opts [count] = opt
                else
                    if i == #arg then
                        exit_with_error ("Missed value for option `" .. a .. "'\n")
                    end

                    optarg [count] = arg [i+1]
                    opts [count] = opt
                    i = i + 1
                end
            end
            count = count + 1

        elseif a:sub (1, 1) == "-" then
            local j
            for j=2,a:len () do
                local opt = canonize (options, a:sub (j, j))

                if options [opt] == 0 then
                    opts [count] = opt
                    count = count + 1
                elseif a:len () == j then
                    if i == #arg then
                        exit_with_error ("Missed value for option `-" .. opt .. "'\n")
                    end

                    optarg [count] = arg [i+1]
                    opts [count] = opt
                    i = i + 1
                    count = count + 1
                    break
                else
                    optarg [count] = a:sub (j+1)
                    opts [count] = opt
                    count = count + 1
                    break
                end
            end
        else
            break
        end

        i = i + 1
    end

    return opts,i,optarg
end

-- phil - made this local
local function get_opts (arg, sh_opts, long_opts)
    local ret = {}

    local opts,optind,optarg = get_ordered_opts (arg, sh_opts, long_opts)
    for i,v in ipairs (opts) do
        if optarg [i] then
            ret [v] = optarg [i]
        else
            ret [v] = 1
        end
    end

    return ret,optind
end

-- End porting from alt_getopt
-- We can remove alt_getopt in the future - I do not like complicated options. We can easily
-- implement our own simple options handling from scratch.

local function parseOptions(arg, short_opts)
    return get_opts (arg, short_opts, {})
end

local function init(arg, short_opts, usage)
    local arg = arg or {}  -- {} allows from shell: lua -ldvs -e'print(dvs_version())'
    local usage = usage or function() print(arg[0]..": -h (help) -v (verbose)") exit() end
    local short_opts = short_opts or "hv"

    -- commandline argument processing
    local optarg,optind = parseOptions (arg, short_opts)

    if optarg["v"] ~= nil then
        verbose=optarg["v"]
    end

    -- check for help/usage
    if optarg["h"] then
        usage()
        exit()
    end

    -- check environment is ok
    if not root() then exit() end
    if not debugfs() then exit() end
    return optarg,optind
end

--[[
arg = {"setting1=value1", "setting2=value2", ...}
rules specified the allowed settings
 {
    -- for setting 1
    {settting1_name1, setting1_name2, ...},
    -- for setting 2
    {settting2_name1, setting2_name2, ...},
 }
for examples
rules = { {"enable","e"}, {"voltage", "v"} }
]]
local function getSettings(args, rules)
    if args == nil then return end

    local settings = {}
    for i=1, #args do
        local arg = args[i]
        local items = strsplit(arg, "=")
        if items == nil then
            logErr("%s is not a correct setting=value string", arg)
            return
        end

        local setting, value = items[1], items[2]
        if setting == nil or value == nil then
            logErr("%s is not a correct setting=value string", arg)
            return
        end

        local found = false
        for j, names in ipairs(rules) do
            for k, name in ipairs(names) do
                if setting == name then
                    table.insert(settings, {n = names[1], v = value})
                    found = true
                    break
                end
            end
            if found then break end
        end

        if found == false then
            logErr("Wrong setting %s in %s", setting, arg)
            return
        end
    end

    if #settings == 0 then
        logErr("I did not find any setting=value string")
        return
    end

    return settings
end


-- return the platform name
local dvs_platform
function get_platform()
    if dvs_platform then return dvs_platform end
    local rc, err
    rc, dvs_platform, err = run_cmd("getprop ro.board.platform")
    log("platform is %s", dvs_platform )
    assert(dvs_platform ~= "", "platform string is empty!")
    return dvs_platform
end

local supportedModules = {
    adc       = {"adc_test",       "Display PMIC ADC channel names and values"},
    bms       = {"bms_test",       "Display and configure battery/fuel gauge/charger"},
    flash     = {"camflash_test",  "Display and set camera flash"},
    gpio      = {"msm_gpio",       "Display and configure MSM GPIOs"},
    led       = {"led_test",       "Set RGB LEDs"},
    mem       = {"mem_poke",       "Read, write and display memory"},
    pmic      = {"pmic_function",  "Display and configure PMIC functions(GPIO, LDO, SMPS, and more)"},
    regulator = {"regulator_test", "Display and configure regulators using regulator framework"},
    rtc       = {"rtc_test",       "Display and configure PMIC RTC"},
    spmi      = {"pmic_test",      "Read, write and display PMIC registers"},
    thermal   = {"thermal_test",   "Display thermal zones"},
    clock     = {"clock_test",     "Display and set clocks"},
    i2c       = {"i2c_test",       "Manually exercise the i2c busses"},
    vibrator  = {"vibrator_test",  "Manually exercise the vibrator"},
    backlight = {"backlight_test", "Display and configure keypad backlight"},
    radio     = {"radio_test",     "Display and setting modem operating mode"},
    sim       = {"sim_test",       "Read or write from or to the SIM card"},
    camera    = {"camera_test",    "Controls cameras"},
}

local supportedOnPlatform = {
    msm8974 = {"adc_test", "bms_test", "camflash_test", "msm_gpio", "led_test", "mem_poke", "pmic_function", "regulator_test", "rtc_test", "pmic_test", "thermal_test", "clock_test", "i2c_test", "vibrator_test", "backlight_test"},
    msm8992 = {"adc_test", "bms_test", "camflash_test", "msm_gpio", "led_test", "mem_poke", "pmic_function", "regulator_test", "rtc_test", "pmic_test", "thermal_test", "clock_test", "i2c_test", "vibrator_test", "backlight_test"},
    msm8952 = {"pmic_test"},
    msm8996 = {"adc_test", "bms_test", "camflash_test", "msm_gpio", "led_test", "mem_poke", "pmic_function", "regulator_test", "rtc_test", "pmic_test", "thermal_test", "clock_test", "i2c_test", "vibrator_test", "camera_test", "radio_test", "sim_test","msm8996"},
    msm8953 = {"adc_test", "bms_test", "camflash_test", "msm_gpio", "led_test", "mem_poke", "pmic_function", "regulator_test", "rtc_test", "pmic_test", "thermal_test", "clock_test", "i2c_test", "vibrator_test", "backlight_test", "camera_test", "radio_test", "sim_test"}
}

function platformSupported(script)
    local table = supportedOnPlatform[get_platform()]
    -- get the name of the running script and chop off the ".lua" part
    local module = string.sub(script, string.find(script, "/[^/]*$") +1, -5)

    if table ~= nil then
        for key,value in pairs (table) do
            if value == module then return true end
        end
    end
    printl("  %s module is not supported on this platform.", module)
    printl("  Please enter a jira if you need support for this module.")
    return false
end

local function runCmd(args)
    local module = supportedModules[args[1]]
    if module then
        require(module[1]).main(slice(args, 2))
    else
        logErr("Want a new module called %s?", args[1])
    end
end

local function usage()
    print()
    printf("Welcome to DVS version %s.\n\n",dvs_version())
    print("Usage:")
    print("    dvs.lua <module> [arguments]")
    print("The following modules are supported:")
    local temp={}
    for k, cmd in sortkey_pairs(supportedModules) do
        table.insert(temp, {k, sf("%s (%s.lua)",cmd[2], cmd[1])})
    end
    printAlignedTable(temp, nil, "    ")
    print()
    print("Use \"dvs.lua <module>\" to get help on a particular module")
    print()
end

local function main(arg)
    if #arg == 0 then
        usage()
    else
        runCmd(arg)
    end
    exit()
end

local exports= {
    -- command line processing
    init         = init,
    parseOptions = parseOptions,
    getSettings  = getSettings,

    -- string
    str2num      = str2num,
    str2hex      = str2hex,

    -- table
    slice             = slice,
    sortkey_pairs     = sortkey_pairs,
    sortval_pairs     = sortval_pairs,
    alignTable        = alignTable,
    printAlignedTable = printAlignedTable,
    to2dTable         = to2dTable,

}

if ... == "dvs" then
    return exports --library, export functions
else
    main(arg)
end
