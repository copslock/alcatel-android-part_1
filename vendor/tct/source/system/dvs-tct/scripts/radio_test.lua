#!/system/xbin/lua
--[[
-----------------------------------
-- This script display and set modem operating mode
-----------------------------------
]]

local gDvs = require("dvs")

local mode2name = { [0]="ONLINE", [2]="FACTORY TEST", [3]="OFFLINE" }
local name2mode = { online=0, FTM=2, offline=3 }

local usageStr = [[
This is a utility for displaying and setting modem operating mode.

Usage:
    radio_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    d                          display current modem operating mode
    c <mode>                   set modem operating mode to <mode>,
                               <mode> can be 0 (online), 2 (ftm), 3 (offline)

Examples
    # set mode to ftm
    radio_test.lua c 2
]]

local function usage()
    print(usageStr)
end

-- display operating mode
local function doCmdDisplay(arg)
    local mode = getOpMode()
    if mode == -1 then
        logErr("Get operating mode failed")
        return
    end
    print("Current modem operating mode is: "..mode2name[mode])
end

-- configure operating mode
local function doCmdConfigure(subargs)
    if #subargs ~= 1 then
        logErr("Please append 'mode' to sub-command")
        return
    end
    local mode = tonumber(subargs[1])
    if mode == nil or mode2name[mode] == nil then
        logErr("Wrong value %s for setting mode", mode)
        return
    end
    print("Setting modem operating mode to "..mode2name[mode])
    if setOpMode(mode) == -1 then
        logErr("Set operating mode %d failed", mode)
    end
end

-----------------------------------
-- main
-----------------------------------
local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local qmicEntry = package.loadlib("/system/lib64/libqmic.so", "qmicEntry")

    if qmicEntry then
        qmicEntry()
        getOpMode = qmicGetOpMode
        setOpMode = qmicSetOpMode
    else
        print("Failed to load libqmic.so.")
    end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    if subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    else
        logErr("Incorrect sub-command '%s'", subcmd)
    end

    exit()
end

local exports = {
    main        = main,
    doCmdDisplay    = doCmdDisplay,
    doCmdConfigure  = doCmdConfigure,
}

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

initModule()
if ... == "radio_test" then
    return exports --library, export functions
else
    main(arg)
end
