#!/system/xbin/lua
--[[
----------------------------------------------
-- This script attempts to control/display PMIC regulators
----------------------------------------------
]]

local gDvs = require("dvs")

local print, arg = print, arg
local gName2ShortMap = {}
local gSupprotedNames = {}
local REGULATOR_PATH = "/d/regulator/"

local shorUsageStr =[[
This script uses regulator framework to display and configure regulators.
Usage:
    regulator_test.lua <sub-command> [sub-command arguments]

The following sub-commands are supported:
    l                                         list supported regulators
    c  <name>  <setting=value> [...]          configure a regulator
    d [-c] [name_pattern] [name_pattern ...]  display regulator(s)
]]

local additionalUsageStr=[[
sub-command l (ls, list)
    List the full names and short names of supported regulators.
    Not listed regulators are not supported by this command.

sub-command c (cs, configure)
    sub-command c takes a regulator name and additional one or more <setting=value>
    parameters to configure the regulator. Available settings and their values are:
        e|enable   enable or disable the regulator, allowed values are
                   1|e|enable    enable the regulator
                   0|d|disable   disable the regulator

        m|mode     set the mode of this regulator, the value of this setting must be
                   a mode number
        v|voltage  set the voltage of this regulator, the value of this setting is a
                   voltage number (in uV)
sub-command d (ds, display)
    Display the settings and status of one or more regulators.
    One or more name_patterns are allowed. A name_pattern can be an exact name, or a pattern
    to match the full names.
    sub-command d by default displays the summary of the regulators. With -c option,
    it will display the client information of these regulators.

Examples:
    regulator_test.lua l    #list all names of all supported regulators
    regulator_test.lua c l27 e=1 v=1050000  #enable regulator 27 and set its voltage to 1.05V
    regulator_test.lua d l1        #display LDO 1 (pm8994_l1)
    regulator_test.lua d *_l*  *_s* #display all LDO (*_l*) and all # SMPS (*_s*)
]]

-----------------------------------
-- kernel regulator framework API
-----------------------------------
-- /sys/class/regulator/ has min/max limits, but can't currently use framework to
-- select voltage anyway

-- lookup full path for regulator - plus existing check
local function getRegPath( regName )
    assert(type(regName) == "string", "regulator name isn't a string: "..tostring(regName))
    local full_path = REGULATOR_PATH..regName
    if fexists(full_path) == false then
        err("regulator name "..regName.." looks bad. ")
        return nil
    end
    log("full path is: %s/", full_path)
    return full_path.."/"
end

-- collects in a table all info about a regulator for easy access
local function collectRegInfo( regName )
    local regFiles = {
        "consumers", "enable", "force_disable",
        "open_count", "use_count", "bypass_count",
        "voltage", "mode", "optimum_mode",
    }
    local reg_path = getRegPath( regName )
    if reg_path == nil then
        log("no path to get info from.")
        return
    end
    local return_table = {}

    for i,v in ipairs( regFiles ) do
        local content = fread_all(reg_path..v)
        if content then
            --log("%s%s has :%s", reg_path, v, content)
            return_table[v] = content
        else
            return_table[v] = ""
        end
    end
    return return_table
end

-- for use in generic for loop, returns an iterator that steps through all regulators
local function getNextRegName(name_pattern)
    local ignore_files = "dummy supply_map"
    local rc, out, err = run_cmd_save_output("cd "..REGULATOR_PATH.."; ls -d "..name_pattern)
    local f = assert(io.open(out)) -- f points to a file with the contents of REGULATOR_PATH
    local regulator_name

    -- fancy closure iterator..
    return function()
        -- grab next line, unless it's not a real regulator
        repeat
            local done = true
            regulator_name = f:read("*line") -- read a directory name at a time

            if not regulator_name then
                -- end of output
                f:close()
                assert(os.remove(out))
                assert(os.remove(err))
            elseif regulator_name:match("dummy") or regulator_name:match("supply_map") then
                -- must ignore these files, loop again
                done = false
            end
        until done
        return regulator_name
    end
end

-- prints a summary of regulator reg
-- regulator must be a name like pm8994_s7 as found under /d/regulator/
-- heading is either true or false
local function printRegSummary( reg)
    local i = collectRegInfo( reg )
    if i == nil then
        err("No info, exiting")
        exit()
    end

    if      i.enable == "1" then i.enable = "enabled "
    elseif  i.enable == "0" then i.enable = "disabled"
    else                         i.enable = sf("%8s",i.enable)
    end

    local summary = sf("%-23s  %s  uses/opens:%2s/%-2s  bypass:%2s  voltage:%7s  mode:%2s",
        reg, i.enable, i.use_count, i.open_count, i.bypass_count,
        i.voltage, i.mode ) -- condensed set of parameters
    print(summary)
    --[[
    local data, address = pmic_get_regulator_register( reg, "en_ctl1" )
    log("regulator reports %s compare with %s", tohexstring(data), i.enable)
    if data then
        log("compare logic: %s~%s", tostring(tobool(data)), tostring(tobool(i.enable)))
        if tobool(data) ~= tobool(i.enable) then
            print(sf("Warning, PMIC en_ctl %s: 0x%02x disputes the supply Enable state: %s",
                address, data, i.enable))
        end
    end

    local pmic_volt = pmic_get_regulator_voltage( reg )
    if data and btest(data, 0x80) and pmic_volt and pmic_volt ~= tonumber(i.voltage) then
        print(sf("Warning, PMIC says voltage is: %d, regulator framework says: %s", pmic_volt, i.voltage))
    end

    data, address = pmic_get_regulator_register( reg, "reg_mode_ctl2" )
    log("regulator reports %s compare with %s", tohexstring(data), i.bypass_count)
    if data and btest(data, 0x20) then
        print(sf("Warning, bypass is set on this regulator: %s: 0x%02x", address, data))
    end
]]
end

-- print client info for a regulator
-- regulator must be a name like pm8994_s7 as found under /d/regulator/
local function printRegClients( regulator )
    local info = collectRegInfo( regulator )
    if info == nil then
        err("No info, exiting")
        exit()
    end
    if tonumber(info.open_count) < 1 then
        print("apparently no-one uses this supply!")
    end
    print("Clients of "..regulator)
    if info.consumers ~= "" then
        local lines = strsplit(info.consumers, "\n")
        for i,line in ipairs(lines) do
            printf("%8s %s\n", "", line)
        end
    else
        err("no client info")
    end
end

-- set state of a regulator
-- regulator must be a name like pm8994_s7 as found under /d/regulator/
-- state is 0 or 1
local function setRegState( reg, state )
    assert( tonumber(state) ~= nil )
    state = tostring(state)
    local reg_path = getRegPath( reg )
    if reg_path == nil then
        log( "%s not found", reg )
        return
    end
    print("set "..reg.." to state "..state)
    -- check current state
    local current_state = fread_all(reg_path.."enable")
    if not current_state then
        err("couldn't read current regulator state")
    elseif current_state and current_state == state then
        print("Already in that state")
    else
        -- write new state to framework
        local e
        local users = tonumber(fread_all(reg_path.."use_count"))
        local rep = 1
        if state == "0" and users >1 then
            rep = users -- multiple users means we might need to "vote" multiple times
            log("Will disable the regulator %d times", rep)
        end
        for i=1,rep do
            e = fwrite(reg_path.."enable", state)
        end
        if e then
            err("set regulator state failed")
        end
    end
end

-- set mode of a regulator
-- regulator must be a name like pm8994_s7 as found under /d/regulator/
-- mode values are: unknown..?
local function setRegMode( reg, mode)
    local reg_path = getRegPath( reg )
    if reg_path == nil then
        log( "%s not found", reg )
        return
    end
    print("set "..reg.." to mode "..mode)
    local e = fwrite(reg_path.."mode", tostring(mode))
    if e then
        err("set regulator mode failed")
    end
end

-- set voltage of a regulator
-- regulator must be a name like pm8994_s7 as found under /d/regulator/
-- voltage will be in microVolts (uV)
local function setRegVoltage( reg, voltage)
    local reg_path = getRegPath( reg )
    if reg_path == nil then
        err( reg.." not found" )
        return
    end
    print("set "..reg.." to "..voltage.."uV")
    local e = fwrite(reg_path.."voltage", tostring(voltage))
    if e then
        err("set regulator voltage failed")
        return
    end
    return true
end

local function matchRegulatorName(nameOrShort)
    for i, name in ipairs(gSupprotedNames) do
        if name == nameOrShort then return name end
    end

    for name, short in pairs(gName2ShortMap) do
        if short == nameOrShort then return name end
    end
end

local function doCmdDisplay(args)
    local optarg, optind = gDvs.parseOptions(args, "c")
    local client = optarg.c

    -- all names if no name is specified
    if #args < optind then args[optind] = "*" end

    local names={}
    for i=optind, #args do
        local name = matchRegulatorName(args[i])
        if name then names[name] = 1
        else
            for name in getNextRegName(args[i]) do
                names[name] = 1
            end
        end
    end

    local sortedNames={}
    for key, v in pairs(names) do table.insert(sortedNames, key) end
    table.sort(sortedNames)

    if #sortedNames == 0 then
        logErr("No regulator matches the name(s). Unfamiliar with regulator names? Try subcommand l first.")
        exit()
    end

    if client then
        for i, name in ipairs(sortedNames) do
            printRegClients( name)
            print()
        end
    else
        for i, name in ipairs(sortedNames) do
            printRegSummary( name)
        end
    end
end

local function doCmdConfigure(args)

    if args[1] == nil then
        logErr("Need a regulator name")
        return
    end

    local name = matchRegulatorName(args[1])
    if name == nil then
        logErr("Wrong regulator name %s", args[1])
        return
    end

    local settings = gDvs.getSettings(gDvs.slice(args,2),
        { {"enable", "e"},  {"mode", "m"}, {"voltage", "v"}})
    if settings == nil then return end

    local enableValue, modeValue, voltageValue
    for k, s in ipairs(settings) do
        local setting, value = s.n, s.v
        if setting == "enable" then
            if value == "enable" or value == "e" or value == "1" then
                enableValue = 1
            elseif value == "disable" or value == "d" or value == "0" then
                enableValue = 0
            else
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        elseif setting == "mode" then
            modeValue = gDvs.str2num(value)
            if modeValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        elseif setting == "voltage" then
            voltageValue = gDvs.str2num(value)
            if voltageValue == nil then
                logErr("Wrong value %s for setting %s", value, setting)
                return
            end
        end
    end

    if enableValue then setRegState(name, enableValue) end
    if modeValue   then setRegMode(name, modeValue) end

    -- TDO - if setRegVoltage failed, we used to use PMIC register
    -- to set voltage
    if voltageValue then setRegVoltage(name, voltageValue) end
end

local function doCmdList(args)
    print()
    print("Available regulators:")
    local tempTable={}
    for i, name in ipairs(gSupprotedNames) do
        if gName2ShortMap[name] then
            table.insert(tempTable, sf("%s|%s", gName2ShortMap[name], name))
        else table.insert(tempTable, name)
        end
    end
    gDvs.printAlignedTable(gDvs.to2dTable(tempTable,3))
end

local function usage()
    print(shorUsageStr)
    print()
    print(additionalUsageStr)
end

local function main(arg)

    local optarg, optind = gDvs.init(arg, "hv", usage)
    if #arg == 0 then usage() exit() end

    local subcmd = arg[optind]
    optind = optind + 1
    local subargs = gDvs.slice(arg, optind)

    local rc = false
    if subcmd == "d" or subcmd == "ds" or subcmd == "display" then
        doCmdDisplay(subargs)
    elseif subcmd == "c" or subcmd == "cs" or subcmd == "configure" then
        doCmdConfigure(subargs)
    elseif subcmd == "l"  or subcmd == "ls" or subcmd == "list" then
        doCmdList(subargs)
    else
        logErr("Incorrect sub-command %s", subcmd)
    end

    exit()
end

local function initModule()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end

    for name in getNextRegName("*") do
        table.insert(gSupprotedNames, name)
    end

    if get_platform() == "msm8992" then
        for i=1, 32 do
            local lname = sf("pm8994_l%d",i)
            gName2ShortMap[lname]=sf("l%d",i)
        end

        for i=1, 12 do
            local lname = sf("pm8994_s%d",i)
            gName2ShortMap[lname]=sf("s%d",i)
        end

        gName2ShortMap.pm8994_lvs1 = "lvs1"
        gName2ShortMap.pm8994_lvs2 = "lvs2"
        gName2ShortMap.pm8994_s1_corner = "s1c"
        gName2ShortMap.pm8994_s1_corner_ao = "s1ca"
        gName2ShortMap.pm8994_s1_floor_corner = "s1fc"
        gName2ShortMap.pm8994_s2_corner = "s2c"
        gName2ShortMap.pm8994_s2_corner_ao = "s2ca"

        -- PMIC for platform (PMI8994)
        gName2ShortMap.pmi8994_boost_5v = "pb5v"
        gName2ShortMap.pmi8994_boost_pin_ctrl = "pbpc"
        gName2ShortMap.pmi8994_boostbypass = "pbb"
        gName2ShortMap.pmi8994_s1 = "ps1"
        gName2ShortMap.pmi8994_s2_corner = "ps2c"
        gName2ShortMap.pmi8994_s2_floor_corner = "ps2fc"
    else
        for i=1, 24 do
            local lname = sf("8941_l%d",i)
            gName2ShortMap[lname]=sf("l%d",i)
        end

        for i=1, 3 do
            local lname = sf("8941_s%d",i)
            gName2ShortMap[lname]=sf("s%d",i)
        end

        gName2ShortMap["8941_l12_ao"] = "l12ao"
        gName2ShortMap["8941_lvs1"] = "lvs1"
        gName2ShortMap["8941_lvs2"] = "lvs2"
        gName2ShortMap["8941_lvs3"] = "lvs3"
        gName2ShortMap["8941_s1_ao"] = "s1ao"
        gName2ShortMap["8941_s1_so"] = "s1so"
        gName2ShortMap["8941_s2_corner"] = "s2c"
        gName2ShortMap["8941_s2_corner_ao"] = "s2ca"
        gName2ShortMap["8941_s2_floor_corner"] = "s2fc"
        gName2ShortMap["8941_s4_corner"] = "s4c"
        gName2ShortMap["8941_s4_floor_corner"] = "s4fc"
    end
end

local exports = {
    main      = main,
}

initModule()
if ... == "regulator_test" then
    return exports --library, export functions
else
    main(arg)
end
