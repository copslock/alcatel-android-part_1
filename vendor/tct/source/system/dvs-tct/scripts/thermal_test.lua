#!/system/xbin/lua
--[[
-----------------------------------
-- This script reads thermal values using the debug sysfs
-----------------------------------
]]

local gDvs = require "dvs"

local THERM_PATH="/sys/devices/virtual/thermal/"
-- pulls info out of thermal framework
local function collect_zone_info( zone )
    if fexists(THERM_PATH..zone) == false then
        return nil
    end
    local files = {
        "mode", "passive", "policy", "temp", "type",
        "trip_point_0_temp", "trip_point_0_type",
        "trip_point_1_temp", "trip_point_1_type",
        "trip_point_2_temp", "trip_point_2_type",
    }
    local return_table = {}
    for i,f in ipairs( files ) do -- f steps through files for each d above
        local content = fread_all(THERM_PATH..zone.."/"..f)
        if content then
            --log("%s%s has :%s", reg_path, v, content)
            return_table[f] = content
        else
            return_table[f] = ""
        end
    end
    return return_table
end

-- display basic info from thermal framework for a particular zone.
local function display_thermal_zone( zone, header )
    assert( type(zone) == "string" )

    local i = collect_zone_info( zone )
    if i then
        if header then
            print(sf("%-17s %8s %5s %10s %8s",
                "type", "mode", "temp", "policy", "passive"))
            print(string.rep("-", 70))
        end
        print(sf("%-17s %8s %5s %10s %8s",
            i.type, i.mode, i.temp, i.policy, i.passive))
    else
        err(sf("Thermal Zone %s not found", zone))
    end
end

-- display just the trip points for a zone
local function display_thermal_zone_trips( zone )
    assert( type(zone) == "string" )

    local i = collect_zone_info( zone )
    if i then
        local trip0 = i.trip_point_0_type.." : "..i.trip_point_0_temp
        local trip1 = i.trip_point_1_type.." : "..i.trip_point_1_temp
        local trip2 = i.trip_point_2_type.." : "..i.trip_point_2_temp
        local count = 3
        if trip0 == " : " then
            trip0 = ""
            count = count - 1
        end
        if trip1 == " : " then
            trip1 = ""
            count = count - 1
        end
        if trip2 == " : " then
            trip2 = ""
            count = count - 1
        end
        if count == 0 then
            print(sf("%-17s has no trip points", i.type))
        else
            print(sf("%-17s %-23s %-23s %s", i.type, trip0, trip1, trip2))
        end
    else
        err(sf("Thermal Zone %s not found", zone))
    end
end

-- returns an iterator to all available thermal zones
-- designed for use in a generic for loop - see thermal_test.lua
local function get_thermal_zones()
    local rc, out, err = run_cmd_save_output("ls "..THERM_PATH)
    local f = assert(io.open(out))
    local zone

    -- fancy closure iterator..
    return function()
        -- grab next line (zone), lookup the name of the sensor in zone/type
        zone = f:read("*line")
        local name = nil
        if zone then
            name = fread_all(THERM_PATH..zone.."/type")
        else
            -- end of output, returning nil will finish the for loop
            f:close()
            assert(os.remove(out))
            assert(os.remove(err))
        end
        return zone, name
    end
end

local usageStr = [[
Usage:
thermal_test.lua <sub-command> [sub-command arguments]

The following sub-command are supported
    l                       list thermal zones / channels
    d  [-t] [thermal_zone]  display thermal zone(s)

thermal_zone can be the full name of a thermal zone, or the number of a thermal zone.
Without a thermal_zone parameter, the d sub-command displays all thermal zones.
With -t option, trip point information is also displayed. Note -t only takes effect when
displaying all thermal zones. Trip point information is always displayed when displaying only one
thermal zone.

Examples:
    thermal_test.lua l    # List all thermal_zones
    thermal_test.lua d 0  # display thermal_zone0,     including trip point information
    thermal_test.lua d -t # display all thermal zones, including trip point information
    thermal_test.lua d    # display all thermal zones, not including trip point information
]]

local function usage()
    print(usageStr)
end

local function doCommandList(arg)
    if arg[1] then
        printf("Warning: parameter(s) after %s (including this one) is/are ignored\n", arg[1])
    end
    print("Available thermal channels\n")
    print(sf("%-20s   %-15s", "channel", "zone"))
    print(string.rep("-", 30))
    for zone, channel in get_thermal_zones() do
        print(sf("%-20s : %s", channel, zone))
    end
    print()
end

local function doCommandDisplay(arg)
    local optarg, optind=gDvs.parseOptions(arg, "t")

    local trip = optarg.t
    local zone
    if arg[optind] then
        zone = tonumber(arg[optind])
        if zone ~= nil then zone = sf("thermal_zone%d", zone)
        else zone = arg[optind]
        end
        if arg[optind+1] then
            printf("Warning: parameter(s) after %s (including this one) is/are ignored\n", arg[optind+1])
        end
    end

    if zone and trip then
        print("Warning: -t option is not needed when displaying individual zone")
    end

    print()
    if zone then
        -- display one zone if required
        display_thermal_zone(zone, true)
        print("\ntrip points")
        display_thermal_zone_trips(zone)
        print()
    else
        print("Displaying all thermal channels ( temps in degC or millidegC )")
        local header = true
        for zone, channel in get_thermal_zones() do
            display_thermal_zone(zone, header)
            header = false
        end
        print()

        if trip then
            print("Displaying all thermal channel trip points")
            for zone, channel in get_thermal_zones() do
                display_thermal_zone_trips( zone )
            end
            print()
        end
    end
end

local function main(arg)

    local optarg, optind=gDvs.init(arg, "hv", usage)

    local cmd = arg[optind]
    if cmd == nil then
        usage()
        exit()
    end

    if cmd == "l" or cmd == "ls" or cmd == "list" then
        doCommandList(gDvs.slice(arg, optind+1))

    elseif cmd == "d" or cmd == "ds" or cmd == "display" then
        doCommandDisplay(gDvs.slice(arg, optind+1))
    else
        logErr("Wrong parameter %s, Need l or d", arg[optind])
    end
    exit()
end

local initModule = function ()
    -- get the name of this script
    local info = debug.getinfo(1,'S')

    if not platformSupported(info.source) then
        exit()
    end
end

local exports = {
    main      = main,
}

initModule()
if ... == "thermal_test" then
    return exports --library, export functions
else
    main(arg)
end
