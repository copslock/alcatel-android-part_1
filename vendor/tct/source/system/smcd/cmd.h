/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef CMD_H
#define CMD_H

/*
 * Version number rule
 *
 * the 3th number should add 1 or more with every submit
 * the 2th number should add 1 when there is a new feature
 * the 1th number should add 1 when the source code has an important change
 *
 */
#define SMCDVERSION "2.1.14"

#define LINE_MAX 1024
#define COMMAND_Length_MAX 200  // NAME_MAX - timestrlen - buildverlen
#define NAME_MAX 255
#define PATH_MAX 4096
#define LOG_MAX_SIZE    1073741824L // 1024MB
#define LOG_BLOCK_SIZE  52428800L   // 50MB
#define DISK_FREE_LIMIT 52428800L   // 50MB

#define SMC_COMMAND       'a'
#define SMC_BUGREPORT     'b'
#define SMC_QXDM          'c'
#define SMC_DROPBOX       'd'
#define SMC_CMD_EXEC      'e'
#define SMC_CPU_LOGGING   'f'
#define SMC_GET_STATUS    'g'
#define SMC_CHARGE_LOG    'h'
#define SMC_ONETIME_LOG   'i'
#define SMC_TCPDUMP_LOG   'j'
#define SMC_AFTERSALE     'k'
#define SMC_RAM_DUMP      'q'
#define SMC_CMD_STOP      's'
#define SMC_TZ_LOG        't'
#define SMC_VERSION       'v'
#define SMC_DEBUG         'z'

// command status
#define CMDSTARTING "starting"
#define CMDRUNNING "running"
#define CMDSTOPPED "stopped"

#define LOGCAT_FOLDER "Logcat/"
#define KMSG_FOLDER "Kernel/"
#define BUGREPORT_FOLDER "BugReport/"
#define CPU_FOLDER "CPUTemp/"
#define TZ_FOLDER "QSEE/"
#define SYSAGENT_FOLDER "SysAgentSvc/"
#define TCP_FOLDER "TCPDump/"
#define DEFAULTLOGCATCMD "logcat -b all -v threadtime"
#define DEFAULTKMSGCMD "cat proc/kmsg"
#define DEFAULT_CPU_CMD "65"

typedef struct cmd_Handler{
  int (*start)(char *);
  int (*stop)(char *);
  int (*getState)(char *);
}cmdHandler;

typedef struct smcd_cmd{
   char logCmd[COMMAND_Length_MAX];
   int (*func)(char *);
}smcdCommand;

typedef struct cmd_Node{
     smcdCommand data;
     struct cmd_Node *next;
}cmdNode;

typedef struct cmd_Queuet{
  cmdNode *head;
  cmdNode *tail;
}cmdQueuet;

extern cmdHandler logcatHandler;
extern cmdHandler shellHandler;
extern cmdHandler CPUHandler;
extern cmdHandler kmsgHandler;
extern cmdHandler TZHandler;
extern cmdHandler TCPHandler;
/*--- smcdQueue.c ---*/
void initQueue();
void SendCommandQueue(smcdCommand *cmd);
int GetCommandQueue(smcdCommand *cmd);
int isEmptyQueue();
void clearQueue();
void printALLCommand();

/*--- smcdLogFiles.c ---*/
int ramDumpFolderCheckThread(void);
int create_file_name(char *oFile, char *command,char *postfix);
int rename_for_compress(const char *file);
int  create_dir(const char *folder);
int read_logs_config(char *logcat_command,char *chpTempTime);
void trigger_logs_compress(char *folder);
void renameAllFile(void);
void getLogRootPath(char *path);

/*--- smcdConnect.c ---*/
int connectInit();
void smcd_command_handler(void);
void mainloop(void);
int ctrl_data_write(char cmd,char subcmd,char *buf);
int ctrl_data_read(char *buf,size_t bufsz);

/*--- smcdMass.c ---*/
int mass_enable_charging_log(void);
int mass_disable_charging_log(void);
void mass_tplog_handler();
void mass_aftersale_handler(char subcmd);
int mass_traceability_ram_dump_reader(void);
int mass_traceability_ram_dump_handler(char *command);
#ifdef SMCD_DEBUG
#define BSMCDDEBUG 1
#else
#define BSMCDDEBUG 0
#endif // SMCD_DEBUG

extern volatile int bSmcdDebug;

#if 0
#define ALOG(priority, tag, ...) \
            __android_log_print(ANDROID_##priority, tag, __VA_ARGS__)
#define ALOGD(...) \
            __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define ALOGE(...) \
            __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define __ALOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
#if SMCD_DEBUG
#define ALOGV(...) do { if (0) { __ALOGV(__VA_ARGS__); } } while (0)
#else
#define ALOGV(...) __ALOGV(__VA_ARGS__)
#endif
#endif

#endif
