/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <log/log.h>
#include <cutils/klog.h>
#include <cutils/properties.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <sys/wait.h>
#include "cmd.h"
#include "smcdUtils.h"

#define TIMESTR_MAX 35
#define DEVICERAMSIZE_MB 4160 // 4GB,4*1024*1024*1024B

#define DEFAULT_DIRECTORY   "/sdcard/BugReport/"
#define CACHEFOLDER "/cache/BugReport/"
#define AFTER_SALE_MODE_LOGPATH "/tctpersist/tctlog/"
#define FACTORY_CONFIG_PATH "/tctpersist/smc_sharedpreference.xml"
#define APP_DATA_PATH  "/data/data/com.tct.endusertest/"
#define CONFIG_PATH "/data/endusertest/smc_sharedpreference.xml"
#define CHARGING_LOGCONFIG "/cache/charging_log.config"
#define INTERNAL_RAM_DUMP_FILE "/data/endusertest/ramdump.bin"

static char logPath[PATH_MAX]=DEFAULT_DIRECTORY;
uint64_t blockSize;
uint64_t totalSize;

const static char chPartition[5][20] = {
        "/storage/emulated", // Internal Storage partition
        "/storage/sdcard1", // SD Card
        "/tctpersist", // tctpersist partition
        "/cache", // Cache partition
        "/data" // data partition
};

enum partitionType{
    INTERNAL_STORAGE_TYPE = 0,
    EXTERNAL_SDCARD_TYPE,
    TCTPERSIST_TYPE,
    CACHE_TYPE,
    DATA_TYPE
};

void getLogRootPath(char *path){
  strcpy(path,logPath);
}

int getTimestamp(char * tStr){
    if (NULL == tStr){
        ALOGE("In %s line %d, time string is NULL",__FUNCTION__,__LINE__);
        return -1;
    }
    char str_time[20];
    char str_loc[20];

    struct tm *curr_tm;
    time_t t;

    //set time info
    if((t = time(NULL)) == ((time_t) - 1)){
        return -1;
    }
    if((curr_tm = localtime(&t)) == NULL){
        return -1;
    }

    strftime(str_time,sizeof(str_time),"%Y%m%d_%H%M%S",curr_tm);

    //append timezone
    if(curr_tm->tm_gmtoff < 0){
        snprintf(str_loc,sizeof(str_loc),"-%02ld%02ld",(-curr_tm->tm_gmtoff) / 3600,
                (-curr_tm->tm_gmtoff) / 60 % 60);
    }else{
        snprintf(str_loc,sizeof(str_loc),"+%02ld%02ld",curr_tm->tm_gmtoff / 3600,
                curr_tm->tm_gmtoff / 60 % 60);
    }

    snprintf(tStr,TIMESTR_MAX,"%s_%s",str_time,str_loc);
    if(bSmcdDebug) ALOGD("In %s line %d, get time string %s",__FUNCTION__,__LINE__,tStr);
    return 0;
}

void check_file_str(char * src){
    unsigned int i;
    char * start = src;
    for(i = 0;i < strlen(src);i++){
        if(start[i] == ' ') start[i] = '_';
    }
}

//create log file name
int create_file_name(char *oFile, char *command,char *postfix){
    if (NULL == oFile){
        ALOGE("In %s line %d, the oFile is NULL",__FUNCTION__,__LINE__);
        return -1;
    }
    if (NULL == command){
        ALOGE("In %s line %d, the command is NULL",__FUNCTION__,__LINE__);
        return -1;
    }
    char build_id[100] = {0};
    char tStr[TIMESTR_MAX] = {0};
    char folder[16] = {0};
    property_get("ro.build.version.incremental",build_id,"");
    if( getTimestamp(tStr) < 0 ){
        strlcpy(tStr,"error_time_string",sizeof(tStr));
    }

    if((strstr(command,"kmsg") != 0)
        || !strcmp(command,DEFAULTKMSGCMD)){
        strlcpy(folder,KMSG_FOLDER,sizeof(folder));
        sprintf(oFile,"%s%sV%s_%s_kmsg_current.txt",logPath,folder,build_id,tStr);
        check_file_str(oFile);
        if(bSmcdDebug) ALOGD("In %s line %d, create log file %s",__FUNCTION__,__LINE__,oFile);
        return 0;
    }
    else if(strstr(command,"logcat") != 0){
        strlcpy(folder,LOGCAT_FOLDER,sizeof(folder));
    }
    else if(strstr(command,"bugreport") != 0){
        strlcpy(folder,BUGREPORT_FOLDER,sizeof(folder));
    }else if(strstr(command,"cpu_temp")!=0 ){
        strlcpy(folder,CPU_FOLDER,sizeof(folder));
    }else if(strstr(command,"tzdbg")!=0 ){
        strlcpy(folder,TZ_FOLDER,sizeof(folder));
    }else
        strcpy(folder,"");

    if ( (strlen(logPath) + strlen(folder)
        + strlen(build_id) + strlen(command)) > (PATH_MAX -20) ){
        ALOGE("In %s line %d,log file name %s%sV%s_%s_%s is too long"
                ,__FUNCTION__,__LINE__,logPath,folder,build_id,tStr,command);
        return -1;
    }

    // memset(oFile,0,sizeof(oFile));
    if(postfix==NULL)
        sprintf(oFile,"%s%sV%s_%s_%s_current.txt",logPath,folder,build_id,tStr,command);
    else
        sprintf(oFile,"%s%sV%s_%s_%s%s",logPath,folder,build_id,tStr,command,postfix);
    check_file_str(oFile);
    if(bSmcdDebug) ALOGD("In %s line %d, create log file %s",__FUNCTION__,__LINE__,oFile);
    return 0;
}

int create_dir(const char *folder){
    if (NULL == folder){
        ALOGE("In %s line %d, folder is NULL",__FUNCTION__,__LINE__);
        return -1;
    }
    int i = 0;
    char dir_name[PATH_MAX];
    mode_t mode = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;
    mode_t oldmod = umask(0);
    if(strlen(logPath)==0)
        strlcpy(logPath,DEFAULT_DIRECTORY,sizeof(logPath));
    int len = (int) strlen(logPath);
    snprintf(dir_name,sizeof(dir_name),"%s%s",logPath,folder);

    if(dir_name[len - 1] != '/') strncat(dir_name,"/",sizeof(dir_name)-strlen(dir_name)-1);
    if(bSmcdDebug) ALOGD("In %s line %d, create log dir %s",__FUNCTION__,__LINE__,dir_name);
    len = (int) strlen(dir_name);
    for(i = 1;i < len;i++){
        if(dir_name[i] == '/'){
            dir_name[i] = 0;
            if(access(dir_name,F_OK | R_OK) != 0){
                if(mkdir(dir_name,mode) != 0){
                    ALOGE("In %s line %d, make %s failed, errno=%d, %s",__FUNCTION__
                            ,__LINE__,dir_name,errno,strerror(errno));
                    umask(oldmod);
                    return -1;
                }
            }
            dir_name[i] = '/';
        }
    }
    umask(oldmod);
    return 0;
}

/*read_logs_config
* Func: read config file and check if some log need auto start when boot up
*
* logcat:  the command of logcat
* return 0x1:   logcat
*        0x2:   kmsg
*        0x4    QXDM
*        0x8    Temperature
*        0x10   FingerprintLog // MODIFIED by lu wenshen, 2016-06-27,BUG-2152462
*
*/
int read_logs_config(char *logcat_command,char *chpTempTime){
    FILE *fp = NULL;
    char line[LINE_MAX] = {0};
    char boot_mode[100] = {0};
    int  result=0;
    char *str = NULL;
    blockSize=LOG_BLOCK_SIZE;
    totalSize=LOG_MAX_SIZE;
    property_get("ro.boot.mode", boot_mode, "");
    if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, get ro.boot.mode as %s\n",__FUNCTION__,__LINE__,boot_mode);
    if(!strcmp("aftersale",boot_mode)){
        blockSize = 1024*1024*3;
        totalSize = 1024*1024*6;
        if(bSmcdDebug) ALOGD("In %s line %d,set logPath as AFTER_SALE_MODE_LOGPATH",__FUNCTION__,__LINE__);
        strcpy(logPath,AFTER_SALE_MODE_LOGPATH);
        return 3;
    }else if(!strcmp("charger",boot_mode)){
        fp = fopen(CHARGING_LOGCONFIG,"r");
        if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, open CHARGING_LOGCONFIG\n",__FUNCTION__,__LINE__);
        if(fp != NULL){
            while(fgets(line,LINE_MAX,fp) != NULL){
                if((strstr(line,"charging_log") != NULL)
                        && (strstr(line,"true") != NULL)){
                    if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, smcd will to catch charging kernel log\n",__FUNCTION__,__LINE__);
                    mass_enable_charging_log();
                    strlcpy(logPath,CACHEFOLDER,sizeof(logPath));
                    fclose(fp);
                    return 2;
                }
            }
            fclose(fp);
        }
    }
    //This path used to auto start when have config file on tctpersist
    //not use extern sdcard because it mounted after boot completed
    if( !access(FACTORY_CONFIG_PATH,F_OK|R_OK) ){
        blockSize = 1024*1024*3;
        totalSize = 1024*1024*6;
        fp = fopen(FACTORY_CONFIG_PATH,"r");
        strcpy(logPath,AFTER_SALE_MODE_LOGPATH);
    }else if(!access(APP_DATA_PATH,F_OK|R_OK)){
        ALOGD("In %s line %d, read config from %s",__FUNCTION__,__LINE__,CONFIG_PATH);
        fp = fopen(CONFIG_PATH,"r");
        strcpy(logPath,DEFAULT_DIRECTORY);
    }
    if(fp != NULL){
        while(fgets(line,LINE_MAX,fp) != NULL){
            int i = 0;
            if((strstr(line,"cmd") != NULL)
                    && (str = strstr(line,"logcat")) != NULL){
                while((str[i] != '<') && (str[i] != '\n') && (i < COMMAND_Length_MAX-3)){
                    logcat_command[i] = str[i];
                    i++;
                }
                if (i >= COMMAND_Length_MAX-3){
                    ALOGE("In %s line %d, logcat_command %s is too long, will clear the command",__FUNCTION__,__LINE__,logcat_command);
                    memset(logcat_command,0,COMMAND_Length_MAX-2);
                }else logcat_command[i] = '\0';
                result= result| 0x1;
            }else if((strstr(line,"kmsg:") != NULL) && (strstr(line,"true") != NULL)){
                result= result| 0x2;
            }else if((strstr(line,"QXDM") != NULL)
                    && (strstr(line,"true") != NULL)){
                result= result| 0x4;
            }else if(strstr(line,"temp_sample") != NULL){
                str = strstr(line,">");
                while ( (str[i+1] >= '0') && (str[i+1] <= '9') && (i < 7)){
                    chpTempTime[i] = str[i+1];
                    i++;
                }
                if (i >= 7){
                    ALOGE("In %s line %d, Temperature time %s is too long, will clear the value",__FUNCTION__,__LINE__,chpTempTime);
                    memset(chpTempTime,0,8);
                }else chpTempTime[i] = '\0';
                result= result| 0x8;
            /* MODIFIED-BEGIN by lu wenshen, 2016-06-27,BUG-2152462*/
            }else if((strstr(line,"FingerprintLog") != NULL)
                    && (strstr(line,"true") != NULL)){
                result= result| 0x10;
                /* MODIFIED-END by lu wenshen,BUG-2152462*/
            }
        }
        fclose(fp);
    }
    if(bSmcdDebug) ALOGD("In %s line %d, Read config file result=%d",__FUNCTION__,__LINE__,result);
    return result;
}

int prepareBugreportRamDumpFolder(char *tarFolderName){
    int iTryTime = 0;
    char chTarFolder[FILENAME_MAX] = {0};
    struct statfs stcpartitionInfo;
    while ((iTryTime < 30 ) && (statfs("/sdcard/", &stcpartitionInfo) != 0)) {
        ALOGE("In %s line %d, get statfs of /sdcard/ failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
        sleep(1);
        iTryTime++;
    }
    if (iTryTime == 30) {
        if (bSmcdDebug) ALOGD("In %s line %d, can't get the status of file system",__FUNCTION__,__LINE__);
        return -1;
    }
    // get free size of partition
    if (stcpartitionInfo . f_bsize / 1024 * stcpartitionInfo.f_bavail / 1024 < DEVICERAMSIZE_MB) {
        if (bSmcdDebug) ALOGD("In %s line %d, there is no enough space for Ram Dump Compress",__FUNCTION__,__LINE__);
        return -1;
    }
    if (!access(DEFAULT_DIRECTORY, F_OK | R_OK | W_OK)) {
        if (bSmcdDebug) ALOGD("In %s line %d, there is %s",__FUNCTION__,__LINE__,DEFAULT_DIRECTORY);
    } else if (mkdir(DEFAULT_DIRECTORY,0755) == -1) {
        ALOGE("In %s line %d, create %s failed, errno=%d, %s",__FUNCTION__,__LINE__, DEFAULT_DIRECTORY,errno,strerror(errno));
        return -1;
    }
    sprintf(chTarFolder,"%s/RamDump",DEFAULT_DIRECTORY);
    if (!access(chTarFolder, F_OK | R_OK | W_OK)) {
        if (bSmcdDebug) ALOGD("In %s line %d, there is RamDump Folder",__FUNCTION__,__LINE__);
    } else if (mkdir(chTarFolder,0755) == -1) {
        ALOGE("In %s line %d, create %s failed, errno=%d, %s",__FUNCTION__,__LINE__, chTarFolder,errno,strerror(errno));
        return -1;
    }
    strlcpy(tarFolderName,chTarFolder,FILENAME_MAX);
    return 0;
}

void ramDumpFolderOpt(char *ramDumpFilePath){
    int iTryTime = 0;
    char chTimestamp[TIMESTR_MAX] = {0};
    char chTarFolder[FILENAME_MAX] = {0};
    char chTarFileName[FILENAME_MAX] = {0};
    char chFileList[LINE_MAX] = {0};
    char build_id[100] = {0};
    char chTarCmd[LINE_MAX] = {0};
    DIR *dir = NULL;
    struct dirent *stcFolder = NULL;

    if (prepareBugreportRamDumpFolder(chTarFolder) < 0) {
        goto ramDumpFolderOptExit;
    }

    // get compress path and generate file name with timestamp
    property_get("ro.build.version.incremental",build_id,"");
    getTimestamp(chTimestamp);

    iTryTime = 0;
    while ((iTryTime < 3) && ((dir = opendir(ramDumpFilePath)) == NULL)) {
        ALOGE("In %s line %d, open direction %s failed, errno=%d, %s",__FUNCTION__,__LINE__,ramDumpFilePath,errno,strerror(errno));
        iTryTime++;
    }
    if (dir == NULL) {
        goto ramDumpFolderOptExit;
    }

    if (bSmcdDebug) ALOGD("In %s line %d, will scan %s", __FUNCTION__, __LINE__, ramDumpFilePath);
    // scan path
    while((stcFolder = readdir(dir)) != NULL){
        if (bSmcdDebug) ALOGD("In %s line %d, read dir %s",__FUNCTION__,__LINE__,stcFolder->d_name);
        if (!strcmp(stcFolder->d_name,".") || !strcmp(stcFolder->d_name,"..")) continue;
        strlcat(chFileList," ",LINE_MAX);
        strlcat(chFileList,stcFolder->d_name,LINE_MAX);
    }
    closedir(dir);
    snprintf(chTarFileName, sizeof(chTarFileName),"%s/V%s_%s_RamDump.tar.gz",chTarFolder,build_id,chTimestamp);
    snprintf(chTarCmd, sizeof(chTarCmd),"tar -zchm -f %s -C %s %s",chTarFileName,ramDumpFilePath,chFileList);
    if (bSmcdDebug) ALOGD("In %s line %d, will excuse command %s",__FUNCTION__,__LINE__,chTarCmd);
    if (system(chTarCmd) == 0){
        // rename ram dump folder if it's writable
        if (!access(ramDumpFilePath,F_OK|R_OK|W_OK)) {
            int len = (int) strlen(ramDumpFilePath);
            char chNewPathName[FILENAME_MAX] = {0};
            for (int i = len; i > 0; i--) {
                // avoid ramDumpFilePath end with '/'
                if ((ramDumpFilePath[i] == '/') && (i < len - 1)) {
                    strlcpy(chNewPathName, ramDumpFilePath, (size_t) (i+1));
                    snprintf(chNewPathName, sizeof(chNewPathName),"%s/V%s_%s_RamDump",chNewPathName,build_id,chTimestamp);
                    if (bSmcdDebug) ALOGD("In %s line %d, get new path name as %s",__FUNCTION__,__LINE__,chNewPathName);
                    break;
                }
            }
            if (bSmcdDebug) ALOGD("In %s line %d, will rename %s as %s",__FUNCTION__,__LINE__,ramDumpFilePath,chNewPathName);
            if (rename(ramDumpFilePath,chNewPathName) < 0 ){
                ALOGE("In %s line %d, rename %s as %s failed, errno=%d, %s",__FUNCTION__,__LINE__,
                      ramDumpFilePath,chNewPathName,errno,strerror(errno));
            }
        }
    }

ramDumpFolderOptExit:
    ALOGD("In %s line %d",__FUNCTION__,__LINE__);
    free(ramDumpFilePath);
    pthread_detach(pthread_self());
    pthread_exit(NULL);
}

void ramDumpFolderCheck(){
    const char chRamDumpFolderName[] = "ram_dump";
    const char chPartition[1][27] = {"/storage/sdcard1"};
    char chRamDumpFolder[FILENAME_MAX] = {0};
    char response[LINE_MAX] = {0};
    char chCmd[LINE_MAX] = {0};
    int iRamDumpCount = 0;
    int bRamDump = 0;
    int iTryTime = 0;
    int i=0;
    int ret = -1;
    DIR *dir;

    if (access("/data/endusertest", F_OK | R_OK | W_OK)) {
        mode_t preUmask = umask(022);
        if (bSmcdDebug) ALOGD("In %s line %d, Pre Umask %04o", __FUNCTION__, __LINE__,preUmask);
        mkdir("/data/endusertest",0755);
        chown("/data/endusertest", 1000, 1000);
        preUmask = umask(preUmask);
        if (bSmcdDebug) ALOGD("In %s line %d, create folder /data/endusertest and uid to 1000", __FUNCTION__, __LINE__);
    }

    memset(chCmd,0, sizeof(chCmd));
    strlcpy(chCmd,"preprd -c", sizeof(chCmd));
    if (bSmcdDebug) ALOGD("In %s line %d, will excute command %s", __FUNCTION__, __LINE__,chCmd);
    ret = system(chCmd);
    if (bSmcdDebug) ALOGD("In %s line %d, command stattus:WIFEXITED(ret) = %d, WEXITSTATUS(ret) = %d, WIFSIGNALED(ret) = %d,"
                                  "WTERMSIG(ret) = %d, WIFSTOPPED(ret) = %d, WSTOPSIG(ret) = %d", __FUNCTION__, __LINE__,
                          WIFEXITED(ret),WEXITSTATUS(ret),WIFSIGNALED(ret),WTERMSIG(ret),WIFSTOPPED(ret),WSTOPSIG(ret));

    // Check whether the partition has been mounted
    for ( i = 0; i < (int)(sizeof(*chPartition) / sizeof(chPartition[0])); i++ ) {
        iRamDumpCount =0;
        if (bSmcdDebug) ALOGD("In %s line %d, check partition %s", __FUNCTION__, __LINE__, chPartition[i]);
        if (is_mountpoint_mounted(chPartition[i]) <= 0) continue;

        iTryTime = 0;
        while ( (iTryTime < 60) && (access(chPartition[i], F_OK | R_OK) == -1) ) {
            sleep(10);
            iTryTime++;
            continue;
        }
        if (60 == iTryTime) {
            if(bSmcdDebug) ALOGD("In %s line %d, can't access %s",__FUNCTION__,__LINE__,chPartition[i]);
            continue; // for ( i = 0; i <= (int)(sizeof(*chPartition) / sizeof(chPartition[0])); i++ )
        }
        snprintf(chRamDumpFolder, sizeof(chRamDumpFolder), "%s/%s", chPartition[i], chRamDumpFolderName);
        // Exit if /storage/sdcard1/ram_dump is unaccessable
        if (-1 == access(chRamDumpFolder, F_OK | R_OK)){
            if (bSmcdDebug) ALOGD("In %s line %d, can't access %s, errno=%d, %s",
                                  __FUNCTION__, __LINE__, chRamDumpFolder, errno, strerror(errno));
            continue;
        }

        if ((dir = opendir(chRamDumpFolder)) == NULL) {
            ALOGE("In %s line %d, open direction failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
            continue;
        }

        if (bSmcdDebug) ALOGD("In %s line %d, will scan %s", __FUNCTION__, __LINE__, chRamDumpFolder);
        // scan path
        struct dirent *stcFolder = NULL;
        while((stcFolder = readdir(dir)) != NULL){
            if (bSmcdDebug) ALOGD("In %s line %d, read dir %s",__FUNCTION__,__LINE__,stcFolder->d_name);
            char chRamDumpSubFolder[FILENAME_MAX] = {0};
            char chRamDumpBin[FILENAME_MAX] = {0};

            if (!strcmp(stcFolder->d_name,".") || !strcmp(stcFolder->d_name,"..")) continue;
            snprintf(chRamDumpSubFolder, sizeof(chRamDumpSubFolder), "%s/%s", chRamDumpFolder, stcFolder->d_name);
            ALOGD("In %s line %d, chRamDumpSubFolder:%s",__FUNCTION__,__LINE__,chRamDumpSubFolder);
            if (stcFolder->d_type == DT_DIR) {
                snprintf(chRamDumpBin, sizeof(chRamDumpBin), "%s/rawdump.bin", chRamDumpSubFolder);
                if (bSmcdDebug) ALOGD("In %s line %d, chRamDumpBin = %s",__FUNCTION__,__LINE__,chRamDumpBin);
                if (access(chRamDumpBin,F_OK|R_OK) == 0) {
                    iRamDumpCount++;
                    bRamDump = 1;
                    // if the name contain RamDump, that means the folder has been renamed and copied to /sdcard/BufReport
                    if (strcasestr(stcFolder->d_name,"RamDump") != NULL) continue;
                    pthread_t tidRamDumpOpt;
                    char *chpRamDumpSubFolder = (char *)malloc(strlen(chRamDumpSubFolder)+2);
                    memset(chpRamDumpSubFolder,0,strlen(chRamDumpSubFolder)+2);
                    strlcpy(chpRamDumpSubFolder,chRamDumpSubFolder, FILENAME_MAX);
                    if (pthread_create(&tidRamDumpOpt, NULL, (void *)ramDumpFolderOpt, chpRamDumpSubFolder) != 0) {
                        ALOGE("In %s line %d, create thread ramDumpFolderOpt failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
                    }
                    sleep(3);
                }
            }
        }
        if (bSmcdDebug) ALOGD("In %s line %d, i=%d, iRamDumpCount=%d",__FUNCTION__,__LINE__,i,iRamDumpCount);
        if (iRamDumpCount > 0) {
            if (response[0] == 0) {
                snprintf(response, sizeof(response), "Find ram dump file in %s, num:%d;",chPartition[i], iRamDumpCount);
            } else {
                snprintf(response, sizeof(response), "%s in %s, num:%d;",response, chPartition[i], iRamDumpCount);
            }
        }
        closedir(dir);
    } // end of for ( i = 0; i <= (sizeof(*chPartition) / sizeof(chPartition[0])); i++ )

    // if INTERNAL_RAM_DUMP_FILE if ok, check ram dump file and zip it to /sdcard/BugReport/RamDump
    memset(chCmd,0, sizeof(chCmd));
    snprintf(chCmd, sizeof(chCmd),"unpackrd %s -c",INTERNAL_RAM_DUMP_FILE);
    if (bSmcdDebug) ALOGD("In %s line %d, will excute command %s", __FUNCTION__, __LINE__,chCmd);
    ret = system(chCmd);
    if (bSmcdDebug) ALOGD("In %s line %d, command stattus:WIFEXITED(ret) = %d, WEXITSTATUS(ret) = %d,"
                                  "WIFSIGNALED(ret) = %d, WTERMSIG(ret) = %d, WIFSTOPPED(ret) = %d, WSTOPSIG(ret) = %d"
        , __FUNCTION__, __LINE__,WIFEXITED(ret),WEXITSTATUS(ret),WIFSIGNALED(ret),WTERMSIG(ret),WIFSTOPPED(ret),WSTOPSIG(ret));
    if (WIFEXITED(ret) && !WEXITSTATUS(ret)) {
        char tarFileName[FILENAME_MAX] = {0};
        char chTimestamp[TIMESTR_MAX] = {0};
        char build_id[100] = {0};
        bRamDump = 1;
        if (response[0] == 0) {
            snprintf(response, sizeof(response), "Find ram dump file in %s, num:%d;",INTERNAL_RAM_DUMP_FILE, 1);
        } else {
            snprintf(response, sizeof(response), "%s in %s, num:%d;",response, INTERNAL_RAM_DUMP_FILE, 1);
        }
        if (prepareBugreportRamDumpFolder(tarFileName) == 0) {
            property_get("ro.build.version.incremental",build_id,"");
            getTimestamp(chTimestamp);
            snprintf(tarFileName, sizeof(tarFileName),"%s/V%s_%s_RamDump.tar.gz",tarFileName,build_id,chTimestamp);
            memset(chCmd,0, sizeof(chCmd));
            snprintf(chCmd, sizeof(chCmd),"unpackrd %s -z %s",INTERNAL_RAM_DUMP_FILE,tarFileName);
            if (bSmcdDebug) ALOGD("In %s line %d, will excute command %s", __FUNCTION__, __LINE__,chCmd);
            ret = system(chCmd);
            if (bSmcdDebug) ALOGD("In %s line %d, command stattus:WIFEXITED(ret) = %d, WEXITSTATUS(ret) = %d,"
                                          "WIFSIGNALED(ret) = %d, WTERMSIG(ret) = %d, WIFSTOPPED(ret) = %d, WSTOPSIG(ret) = %d"
                , __FUNCTION__, __LINE__,WIFEXITED(ret),WEXITSTATUS(ret),WIFSIGNALED(ret),WTERMSIG(ret),WIFSTOPPED(ret),WSTOPSIG(ret));
        }
    }

    memset(chCmd,0, sizeof(chCmd));
    snprintf(chCmd, sizeof(chCmd),"preprd %s",INTERNAL_RAM_DUMP_FILE);
    if (bSmcdDebug) ALOGD("In %s line %d, will excute command %s", __FUNCTION__, __LINE__,chCmd);
    ret = system(chCmd);
    if (bSmcdDebug) ALOGD("In %s line %d, command stattus:WIFEXITED(ret) = %d, WEXITSTATUS(ret) = %d, WIFSIGNALED(ret) = %d,"
                                  "WTERMSIG(ret) = %d, WIFSTOPPED(ret) = %d, WSTOPSIG(ret) = %d", __FUNCTION__, __LINE__,
                          WIFEXITED(ret),WEXITSTATUS(ret),WIFSIGNALED(ret),WTERMSIG(ret),WIFSTOPPED(ret),WSTOPSIG(ret));

    // if there isn't preprd, but there is INTERNAL_RAM_DUMP_FILE, delete it
    if (access("/system/bin/preprd", F_OK | R_OK) && !access(INTERNAL_RAM_DUMP_FILE, F_OK | R_OK | W_OK)) {
        unlink(INTERNAL_RAM_DUMP_FILE);
        if (bSmcdDebug) ALOGD("In %s line %d, prepare ram dump bin failed, delete %s now"
                    , __FUNCTION__, __LINE__,INTERNAL_RAM_DUMP_FILE);
    }

    if (bRamDump > 0) {
        property_set("tct.debug.rdcheck","1");
        ctrl_data_write(SMC_RAM_DUMP,'c',response);
    }
}

int ramDumpFolderCheckThread(){
    pthread_t tid;
    if(pthread_create(&tid, NULL, (void *) &ramDumpFolderCheck, NULL)){
        ALOGE("In %s line %d, create ramDumpFolderCheck thread failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -1;
    }
    return 0;
}

int rename_for_compress(const char *file){
    char newName[PATH_MAX];
    char *chpStrIndex = NULL;
    if (-1 == access(file,F_OK|R_OK|W_OK)){
        ALOGE("In %s, file %s is not accessable",__FUNCTION__,file);
        return -1;
    }
    memset(newName,0,sizeof(newName));
    chpStrIndex = strstr(file,"_current");
    if (!chpStrIndex){
        return 0;
    }
    strlcpy(newName, file, (size_t)(chpStrIndex - file) + 1);
    strncat(newName,chpStrIndex+8,sizeof(newName)-strlen(newName)-1);
    if(bSmcdDebug) ALOGD("In %s line %d,rename %s as %s",__FUNCTION__,__LINE__,file,newName);

    if(rename(file,newName)!=0)
        ALOGE("In %s line %d, rename file %s for compress failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,file,errno,strerror(errno));
    return 0;
}

void renameAllFile(void){
    if (access(logPath,F_OK|R_OK|W_OK)){
        ALOGE("In %s line %d, %s is unaccessible, errno=%d, %s",__FUNCTION__,__LINE__,logPath,errno,strerror(errno));
        return;
    }
    DIR *dir;
    struct dirent * filep;
    char current_file[PATH_MAX]={0};
    char chpLogPath[PATH_MAX]={0};
    snprintf(chpLogPath,sizeof(chpLogPath),"%s%s",logPath,LOGCAT_FOLDER);
    dir = opendir(chpLogPath);
    if(dir == NULL){
        ALOGE("In %s line %d, Can't open dir %s,errno %d,%s",__FUNCTION__,__LINE__
                ,chpLogPath,errno,strerror(errno));
    }else{
        while((filep = readdir(dir)) != NULL){
            if (filep->d_type != DT_DIR){
                snprintf(current_file,sizeof(current_file),"%s%s",chpLogPath,filep->d_name);
                rename_for_compress(current_file);
            }
        }
        closedir(dir);
    }
    snprintf(chpLogPath,sizeof(chpLogPath),"%s%s",logPath,KMSG_FOLDER);
    dir = opendir(chpLogPath);
    if(dir == NULL){
        ALOGE("In %s line %d, Can't open dir %s,errno %d,%s",__FUNCTION__,__LINE__
                ,chpLogPath,errno,strerror(errno));
    }else{
        while((filep = readdir(dir)) != NULL){
            if (filep->d_type != DT_DIR){
                snprintf(current_file,sizeof(current_file),"%s%s",chpLogPath,filep->d_name);
                rename_for_compress(current_file);
            }
        }
        closedir(dir);
    }
}

void compress_file(char *folder){
    if (NULL == folder){
        ALOGE("In %s line %d, folder can't be NULL'",__FUNCTION__,__LINE__);
        return;
    }
    DIR * dir;
    uint64_t iDiskFreeLimit = DISK_FREE_LIMIT;
    struct dirent * filep;
    uint64_t current_size = 0;
    uint64_t free_disk_size = 0;
    struct statfs pstatfs;
    struct stat pstat;

    char zip_cmd[PATH_MAX];
    char file_dir[PATH_MAX];

    snprintf(file_dir,sizeof(file_dir),"%s%s",logPath,folder);
    if (!strcmp(logPath,AFTER_SALE_MODE_LOGPATH)){
        iDiskFreeLimit = totalSize;
    }

    dir = opendir(file_dir);
    if(dir == NULL){
        ALOGE("In %s line %d, Can't open dir %s,errno %d,%s"
                ,__FUNCTION__,__LINE__,file_dir,errno,strerror(errno));
        closedir(dir);
        return;
    }
    while((filep = readdir(dir)) != NULL){
        char current_file[PATH_MAX];
        memset(current_file,0,sizeof(current_file));
        if((strstr(filep->d_name,"current") != NULL)   //not compress current write file
                || (filep->d_type == DT_DIR)) continue;

        snprintf(current_file,sizeof(current_file),"%s%s",file_dir,filep->d_name);

        if(strstr(current_file,".gz") == NULL){
            int cmd_result;
            snprintf(zip_cmd,sizeof(zip_cmd),"gzip %s",current_file);
            cmd_result = system(zip_cmd);
            strncat(current_file,".gz",sizeof(current_file)-strlen(current_file)-1);
            if(WEXITSTATUS(cmd_result) != 0){
                unlink(current_file);
                continue;
            }
        }

        if(stat(current_file,&pstat) == -1) {
            ALOGE("In %s line %d, Can't get stat of file %s,errno %d,%s"
                    ,__FUNCTION__,__LINE__,current_file,errno,strerror(errno));
            return;
        }
        current_size += pstat.st_size;

    }
    closedir(dir);
    if(statfs(file_dir,&pstatfs) != -1){
        free_disk_size = pstatfs.f_bsize * pstatfs.f_bfree;
    }
    while(free_disk_size <= iDiskFreeLimit
            || (totalSize != 0 && current_size >= totalSize)){

        char select_file[PATH_MAX];
        time_t select_time = (time_t)0;
        long select_size = 0;
        char current_file[PATH_MAX];
        DIR * dp;
        struct dirent * dirp;
        struct stat stat_buf;
        memset(current_file,0,sizeof(current_file));
        memset(select_file,0,sizeof(current_file));
        if((dp = opendir(file_dir)) != NULL){
            while((dirp = readdir(dp)) != NULL){
                if(dirp->d_type == DT_DIR || strstr(dirp->d_name, "current"))
                    continue;
                snprintf(current_file,sizeof(current_file),"%s%s",file_dir,dirp->d_name);
                if(stat(current_file,&stat_buf) != -1){
                if((signed int)(stat_buf.st_mtime) < select_time
                            || select_time == 0){
                        select_time = stat_buf.st_mtime;
                        select_size = stat_buf.st_size;
                        strlcpy(select_file,current_file,sizeof(select_file));
                    }
                }
            }
        }
        if (dp){
            closedir(dp);
        }
        if (!access(select_file,F_OK|W_OK)){
            unlink(select_file);
            if(bSmcdDebug) ALOGD("In %s, unlink file %s",__FUNCTION__,select_file);
        }
        current_size -= select_size;
        if(current_size <= 0){
            ALOGE("In %s line %d, Disk is full, no space left for Logs",__FUNCTION__,__LINE__);
            break;
        }
    }
    return;
}

void trigger_logs_compress(char *folder){
    pthread_t tid = 0;
    pthread_attr_t attr;
    if((pthread_attr_init(&attr) != 0)
            || (pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED) != 0)){
    }
    if(pthread_create(&tid,&attr,(void *)compress_file,folder) != 0){
        return;
    }
    pthread_attr_destroy(&attr);
} // MODIFIED by lu wenshen, 2016-06-27,BUG-2152462
