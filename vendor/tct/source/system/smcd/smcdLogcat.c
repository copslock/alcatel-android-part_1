/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/wait.h>
#include <pthread.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include <cutils/klog.h>
#include "cmd.h"

static volatile int logcat_state=0;  // 0 stop; 1 running
extern uint64_t  blockSize;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void setLogcatStatus(int state){
        pthread_mutex_lock(&mutex);
        logcat_state = state;
        pthread_mutex_unlock(&mutex);
}

void logcat_cmd_execute(char *command){
    FILE * fpp,*fl;
    int iTryTime = 0;
    uint64_t total_read = 0;
    uint64_t onceSize = 0;
    int read_num,write_num,result,stat,fd,fieldes[2];
    pid_t pid;
    char line[LINE_MAX];
    char logFile[PATH_MAX];
    char cmd[COMMAND_Length_MAX];
    memset(cmd,0,sizeof(cmd));
    if(command==NULL)
      strcpy(cmd,DEFAULTLOGCATCMD);
    else
      strlcpy(cmd,&command[2],sizeof(cmd));
    // compress file first
    trigger_logs_compress(LOGCAT_FOLDER);
    if( create_file_name(logFile,cmd,NULL) < 0 ){
        ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
        return;
    }
    fl = fopen(logFile,"w+");
    if(fl != NULL){
        // Try 3 times when the command exit abnormally
        while(1){
            onceSize = 0;
            setLogcatStatus(1);
            if( pipe(fieldes) < 0 ){
                ALOGE("In %s line %d,  create pipe failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
                KLOG_ERROR(LOG_TAG,"In %s line %d, create pipe failed, errno=%d, %s\n"
                        ,__FUNCTION__,__LINE__,errno,strerror(errno));
            }
            pid = fork( );
            if(pid == 0){
                if(bSmcdDebug){
                    ALOGD("In %s line %d,  will execl command %s as PPID:%d PID:%d TID:%d"
                            ,__FUNCTION__,__LINE__,cmd,getppid(),getpid(),gettid());
                    KLOG_DEBUG(LOG_TAG,"In %s line %d, will execl command %s as PPID:%d PID:%d TID:%d\n"
                                ,__FUNCTION__,__LINE__,cmd,getppid(),getpid(),gettid());
                }
                close(fieldes[0]);
                if( dup2(fieldes[1],STDOUT_FILENO) < 0 ){
                    ALOGE("In %s line %d,  duplicate STDOUT_FILENO to fieldes[1] failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
                    KLOG_ERROR(LOG_TAG,"In %s line %d, create pipe failed, errno=%d, %s\n"
                                ,__FUNCTION__,__LINE__,errno,strerror(errno));
                }
                close(fieldes[1]);
                result = execl("/system/bin/sh","sh","-c",cmd,(char*)0);
                if(result == -1){
                    ALOGE("In %s line %d, execl %s failed, errno=%d, %s",__FUNCTION__,__LINE__,cmd,errno,strerror(errno));
                }
            }
            else if(pid > 0){
				close(fieldes[1]);
                fcntl(fieldes[0],F_SETFL,O_NONBLOCK);
                if(bSmcdDebug){
                    ALOGD("In %s line %d, PPID:%d PID:%d TID:%d",__FUNCTION__,__LINE__,getppid(),getpid(),gettid());
                    KLOG_DEBUG(LOG_TAG,"In %s line %d, PPID:%d PID:%d TID:%d\n"
                                ,__FUNCTION__,__LINE__,getppid(),getpid(),gettid());
                }
                while((read_num = read(fieldes[0],line,LINE_MAX)) != 0){
                    if (iTryTime && (onceSize >= 2*1024*1024) ){
                        if(bSmcdDebug) ALOGD("In %s line %d, logcat is stable, reset iTryTime as 0"
                                                ,__FUNCTION__,__LINE__);
                        onceSize = 0;
                        iTryTime = 0;
                    }
                    if(total_read >= blockSize){
                        total_read = 0;
                        fclose(fl);
                        rename_for_compress(logFile); //remove current on file name to allow compress
                        trigger_logs_compress(LOGCAT_FOLDER);

                        if( create_file_name(logFile,cmd,NULL) < 0 ){
                            ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
                            setLogcatStatus(0);
                            break;
                        }
                        fl = fopen(logFile,"w+");
                        if(fl == NULL){
                            ALOGE("In %s line %d, fopen file %s failed, errno=%d, %s"
                                    ,__FUNCTION__,__LINE__,logFile,errno,strerror(errno));
                            break;
                        }
                        //gzip_in_thread(para);
                    }

                    if((read_num == -1) && (errno == EAGAIN)){
                        usleep(500000);
                    }
                    else if(read_num > 0){
                        total_read += read_num;
                        if(iTryTime) onceSize += read_num;
                        fwrite(line,sizeof(char),read_num,fl);
                    }
                    else{
                        ALOGE("In %s line %d, pipe error,errno=%d, %s",__FUNCTION__
                                ,__LINE__,errno,strerror(errno));
                        KLOG_ERROR(LOG_TAG,"In %s line %d, pipe error, errno=%d, %s\n"
                                ,__FUNCTION__,__LINE__,errno,strerror(errno));
                        break;
                    }

                    if(!logcat_state){
                        ALOGE("In %s line %d, thread exit, running state logcat_state : %s"
                                ,__FUNCTION__,__LINE__,logcat_state?"true":"false");
                        break;
                    }

                } // end of while((read_num = read(fieldes[0],line,LINE_MAX)) != 0)
                //if logcat exit unexpected,try another 2 times to restart log
                 if(logcat_state){
                    kill(pid,SIGKILL);
                    waitpid(pid, &stat, WUNTRACED);
                    close(fieldes[0]);
                    ALOGE("In %s line %d, logcat has been broken,will try No.%d time",__FUNCTION__,__LINE__,iTryTime+1);
                    if(bSmcdDebug) KLOG_DEBUG(LOG_TAG,"In %s line %d, logcat has been broken,will try No.%d time\n"
                                ,__FUNCTION__,__LINE__,iTryTime+1);
                    iTryTime++;
                    if(iTryTime < 120){
                        if(0 == (iTryTime%20)){
                            ALOGD("In %s line %d, will clear all logcat buffer",__FUNCTION__,__LINE__);
                            system("logcat -b all -c");
                            sleep(1);
                        }
                        sleep(1);
                        continue;
                    }
                  }

                kill(pid,SIGKILL);
                waitpid(pid, &stat, WUNTRACED);
                close(fieldes[0]);
            } // end of if(pid > 0)
            break; // break while(1)
        } // end of while(1)
        fclose(fl);
        rename_for_compress(logFile);
        setLogcatStatus(0);

        if(bSmcdDebug){
            KLOG_DEBUG(LOG_TAG,"In %s line %d, thread PID:%d TID:%d exit\n",__FUNCTION__,__LINE__,getpid(),gettid());
            ALOGD("In %s line %d, thread PID:%d TID:%d exit",__FUNCTION__,__LINE__,getpid(),gettid());
        }

    } // end of if(fl != NULL)

    free(command);
    if(fl != NULL){
        if(bSmcdDebug) ALOGD("In %s line %d, file %s is closed",__FUNCTION__,__LINE__,logFile);
        fclose(fl);
    }
}

static int logcat_start(char *command){
    char *chpCommand = (char *)malloc(COMMAND_Length_MAX);
    memset(chpCommand,0,COMMAND_Length_MAX);
    if (NULL == command){
        snprintf(chpCommand,COMMAND_Length_MAX,"%c%c%s",SMC_CMD_EXEC,'a',DEFAULTLOGCATCMD);
    }else strlcpy(chpCommand,command,COMMAND_Length_MAX);
    pthread_t tid_cmd_exec;
    if(logcat_state==1) return -1;
    create_dir(LOGCAT_FOLDER);

    pthread_attr_t attr_cmd_exec;
    if((pthread_attr_init(&attr_cmd_exec) != 0)
            || (pthread_attr_setdetachstate(&attr_cmd_exec,
                    PTHREAD_CREATE_DETACHED) != 0)){
    }
    if(bSmcdDebug) ALOGD("In %s line %d,command:%s",__FUNCTION__,__LINE__,chpCommand);
    if(pthread_create(&tid_cmd_exec,&attr_cmd_exec,(void *)logcat_cmd_execute,chpCommand)
            != 0){
        ALOGE("In %s line %d , create pthread logcat_cmd_execute failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        return -2;
    }
    pthread_attr_destroy(&attr_cmd_exec);
    return 0;
}

static int logcat_stop(char *command){
  if(bSmcdDebug) ALOGD("In %s line %d",__FUNCTION__,__LINE__);
  setLogcatStatus(0);
  ctrl_data_write(SMC_CMD_STOP,command[1],"logcat has been stopped");
  return 1;
}

static int logcat_getState(char *command){
    if(logcat_state){
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDRUNNING);
    }else{
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDSTOPPED);
    }
    return logcat_state;
}

void testInit(){
  return;
}

cmdHandler logcatHandler={
  .start=logcat_start,
  .stop =logcat_stop,
  .getState = logcat_getState
};
