################################################################################
#                                                                  Date:06/2016
#                                 PRESENTATION
#
#         Copyright 2016 TCL Communication Technology Holdings Limited.
#
# This material is company confidential, cannot be reproduced in any form
# without the written permission of TCL Communication Technology Holdings
# Limited.
#
# -----------------------------------------------------------------------------
#  Author :  guobing.miao
#  Email  :  guobing.miao@tcl.com
#  Role   :
#  Reference documents :
# -----------------------------------------------------------------------------
#  Comments :
#  File     :
#  Labels   :
# =============================================================================
#      Modifications on Features list / Changes Request / Problems Report
# -----------------------------------------------------------------------------
#    date   |        author        |         Key          |      comment
# ----------|----------------------|----------------------|--------------------
#  6/7/2016 |     guobing.miao     |                      |   make the script
# ----------|----------------------|----------------------|--------------------
# 6/21/2016 |       heng.chen      |                      |   reconfiguration
# ----------|----------------------|----------------------|--------------------
################################################################################

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := main.c \
                   smcdUtils.c \
                   smcdQueue.c \
                   smcdLogcat.c \
                   smcdShell.c \
                   smcdCPU.c \
                   smcdKmsg.c \
                   smcdLogFiles.c \
                   smcdTZ.c \
                   smcdMass.c \
                   smcdTCP.c \
                   smcdConnect.c

LOCAL_SHARED_LIBRARIES := liblog libm libc libprocessgroup libcutils
LOCAL_C_INCLUDES := $(LOCAL_PATH) \
                    $(LOCAL_PATH)/dump2file

LOCAL_CFLAGS := -Werror -std=c11
ifeq ($(TARGET_BUILD_MMITEST),true)
LOCAL_CFLAGS += -DTARGET_BUILD_MMITEST
endif #TARGET_BUILD_MMITEST=true

LOCAL_MODULE := smcd

LOCAL_INIT_RC := smcd.rc

# remove some feature in mp branch
ifeq ($(TCT_TARGET_DEBUG_FEEDBACK),true)
LOCAL_CFLAGS += -DSMCD_DEBUG
endif

# remove smcd in mp branch
#ifeq ($(TCT_TARGET_DEBUG_FEEDBACK),true)
include $(BUILD_EXECUTABLE)
#endif

ifeq ($(TCT_TARGET_DEBUG_FEEDBACK),true)

include $(CLEAR_VARS)
LOCAL_MODULE := libf2bs
LOCAL_CFLAGS := -Werror
LOCAL_SRC_FILES := dump2file/f2bs.c dump2file/crc16.c
include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := libf2bs
LOCAL_CFLAGS := -Werror
LOCAL_SRC_FILES := dump2file/f2bs.c dump2file/crc16.c
include $(BUILD_HOST_STATIC_LIBRARY)

include $(CLEAR_VARS)
LOCAL_MODULE := preprd
LOCAL_CFLAGS := -Werror
LOCAL_CFLAGS += -DBUILD_STANDALONE
LOCAL_SRC_FILES := dump2file/prepareRamDump.c
LOCAL_STATIC_LIBRARIES := libf2bs
LOCAL_C_INCLUDES := system/core/include
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := unpackrd
LOCAL_CFLAGS := -Werror
LOCAL_CFLAGS += -DBUILD_STANDALONE
LOCAL_SRC_FILES := dump2file/unpackRamDump.c dump2file/tar.c
LOCAL_STATIC_LIBRARIES := libf2bs
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := unpackrd
LOCAL_CFLAGS := -Werror
LOCAL_CFLAGS += -DBUILD_HOST -DBUILD_STANDALONE -DBUILD_NOMMAP
LOCAL_LDFLAGS := -static
LOCAL_SRC_FILES := dump2file/unpackRamDump.c dump2file/tar.c
LOCAL_STATIC_LIBRARIES := libf2bs
include $(BUILD_HOST_EXECUTABLE)

endif #TCT_TARGET_DEBUG_FEEDBACK==true
# MODIFIED-END by Heng Chen,BUG-2902752
