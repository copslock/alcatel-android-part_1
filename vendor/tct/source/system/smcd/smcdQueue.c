/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <log/log.h>
#include "cmd.h"

static cmdQueuet CommandList;

void initQueue(){
    CommandList.head = NULL; //队头标志位
    CommandList.tail = NULL; //队尾标志位
}

void SendCommandQueue(smcdCommand *cmd){
    cmdNode * node= (cmdNode *)malloc(sizeof(cmdNode));
    if (node == NULL ){
        ALOGE("In %s line %d, malloc command failed",__FUNCTION__,__LINE__);
        return;
    }
    memcpy(&(node->data),cmd,sizeof(smcdCommand));
    node->next = NULL;
    if (CommandList.head == NULL){
        CommandList.head = node;
        CommandList.tail = node;
    } else {
        //hq->tail->data = x;
        CommandList.tail->next = node;
        CommandList.tail = node;
    }
    if(bSmcdDebug) ALOGD("In %s line %d, add command to queue successful",__FUNCTION__,__LINE__);
    return;
}

/*GetCommandQueue
 *
 * return 0: not command
 *        1:have command
 */

int GetCommandQueue(smcdCommand *cmd){
        cmdNode *p;
        if (CommandList.head == NULL){
            if(bSmcdDebug) ALOGD("In %s line %d, These is no command in list",__FUNCTION__,__LINE__);
            return 0;
        }
        memset(cmd,0,sizeof(smcdCommand));
        memcpy(cmd, &(CommandList.head->data),sizeof(smcdCommand) );
        p = CommandList.head;
        CommandList.head = CommandList.head->next;
        if(CommandList.head == NULL){
            CommandList.tail = NULL;
        }
        free(p);
        return 1;
    }

int isEmptyQueue(){
    if (CommandList.head == NULL){
        return 1;
    } else {
        return 0;
    }
}

void clearQueue(){
    cmdNode * p = CommandList.head;
    while(p != NULL){
        CommandList.head = CommandList.head->next;
        free(p);
        p = CommandList.head;
    }
    CommandList.tail = NULL;
    return;
}

void printALLCommand(){
    cmdNode * p = CommandList.head;
    while(p != NULL){
        if(bSmcdDebug) ALOGD("In %s line %d, COMMAND:%s",__FUNCTION__,__LINE__,p->data.logCmd);
        p = p->next;
    }
    return;
}
