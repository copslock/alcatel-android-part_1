#ifndef __FILE2BLOCKS_H__
#define __FILE2BLOCKS_H__

#include <stdint.h>


#define F2BS_MAGIC          0x53463242  /* string "F2BS" */
#define F2BS_FLAG_VALID     0x00000001

struct f2bs_header {
    uint32_t magic;     /* magic number */
    uint32_t flag;      /* f2bs flags */
    uint32_t blocks;    /* total blocks */
    uint32_t blksize;   /* block size */
    uint16_t ranges;    /* total ranges */
    uint16_t crc16;     /* ranges crc16 value */
    uint32_t reserved;  /* reserved for future */
} __attribute__((packed));

/* TODO, use start and length instead of to reduce calculation */
struct f2bs_range {
    uint32_t range[2];  /* half open range, [range[0], range[1]) */
} __attribute__((packed));

struct f2bs_range_info {
    uint64_t relative_offset;
    uint64_t absolute_offset;
    uint64_t size;
};

#define F2BS_HEADER_SIZE    (sizeof(struct f2bs_header))
#define F2BS_RANGE_SIZE     (sizeof(struct f2bs_range))

#ifndef F2BS_MAX_RANGES
#define F2BS_MAX_RANGES 128
#endif /* F2BS_MAX_RANGES */


/**
 * Find file's blocks, and fill header, ranges_table and range_info.
 * Return 0 on success, -1 on failure.
 */
int f2bs_find_blocks(const char *path, struct f2bs_header *header,
        struct f2bs_range ranges_table[], struct f2bs_range_info range_info[]);

/* Print blocks' information to stderr. */
void f2fs_dump_blocks_info(struct f2bs_header *header,
        struct f2bs_range ranges_table[], struct f2bs_range_info range_info[]);

/* Return a pointer to the mapped area, NULL on failure. */
void *f2bs_ranges_mmap(struct f2bs_header *header,
        struct f2bs_range ranges_table[], int fd);

/* Return 0 on success, -1 on failure. */
int f2bs_ranges_check(struct f2bs_header *header,
        struct f2bs_range ranges_table[]);


#endif /* __FILE2BLOCKS_H__ */
