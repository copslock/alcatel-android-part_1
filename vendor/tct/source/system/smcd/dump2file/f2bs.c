#define _LARGEFILE64_SOURCE

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <linux/fs.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <inttypes.h>
#include <sys/mman.h>

#include "crc16.h"

#include "f2bs.h"

int f2bs_find_blocks(const char *path, struct f2bs_header *header,
        struct f2bs_range ranges_table[], struct f2bs_range_info range_info[]) {
    int fd;
    if ((fd = open(path, O_RDONLY)) < 0) {
        fprintf(stderr, "failed to open %s for reading: %s\n", path,
                strerror(errno));
        return -1;
    }

    struct stat sb;
    if (fstat(fd, &sb) != 0) {
        fprintf(stderr, "failed to stat %s: %s\n", path, strerror(errno));
        return -1;
    }
    if (!S_ISREG(sb.st_mode)) {
        fprintf(stderr, "%s is not a regular file\n", path);
        return -1;
    }

    memset(header, 0x0, F2BS_HEADER_SIZE);
    memset(&ranges_table[0], 0x0, F2BS_RANGE_SIZE * F2BS_MAX_RANGES);

    /* Do not use sb.st_blocks, since sb.st_size = sb.st_blocks * 512 */
    header->blocks = ((sb.st_size - 1) / sb.st_blksize) + 1;
    header->blksize = sb.st_blksize;

    unsigned int i;

    int block;
    int pos = 0;
    ranges_table[pos].range[0] = 0; //init first range
    for (i = 0; i < header->blocks; ++i) {
        block = i;
        if (ioctl(fd, FIBMAP, &block) != 0) {
            fprintf(stderr, "failed to find block %d\n", block);
            return -1;
        }
        if (ranges_table[pos].range[0] == 0) { //first range
            ranges_table[pos].range[0] = block;
            ranges_table[pos].range[1] = block + 1;
        } else if ((uint64_t) block == ranges_table[pos].range[1]) { //current range
            ++ranges_table[pos].range[1];
        } else { //next range
            ++pos;
            ranges_table[pos].range[0] = block;
            ranges_table[pos].range[1] = block + 1;
        }
    }
    header->ranges = pos + 1;

    if (range_info != NULL) {
        range_info[0].relative_offset = 0;
        range_info[0].absolute_offset = (uint64_t) header->blksize
                * (uint64_t) ranges_table[0].range[0];
        range_info[0].size = (uint64_t) header->blksize
                * (uint64_t) (ranges_table[0].range[1]
                        - ranges_table[0].range[0]);
        for (i = 1; i < header->ranges; ++i) {
            range_info[i].relative_offset = range_info[i - 1].relative_offset
                    + range_info[i - 1].size;
            range_info[i].absolute_offset = (uint64_t) header->blksize
                    * (uint64_t) ranges_table[i].range[0];
            range_info[i].size = (uint64_t) header->blksize
                    * (uint64_t) (ranges_table[i].range[1]
                            - ranges_table[i].range[0]);
        }
    }

    /* Calculate crc16, set magic and flag finally */
    header->crc16 = crc16(0xFFFF, (uint8_t *) &ranges_table[0],
            header->ranges * F2BS_RANGE_SIZE);
    header->magic = F2BS_MAGIC;
    header->flag = F2BS_FLAG_VALID;

    return 0;
}

void f2fs_dump_blocks_info(struct f2bs_header *header,
        struct f2bs_range ranges_table[], struct f2bs_range_info range_info[]) {
    fprintf(stderr, "header->magic=0x%08X\n", header->magic);
    fprintf(stderr, "header->flag=0x%08X\n", header->flag);
    fprintf(stderr, "header->blocks=%u\n", header->blocks);
    fprintf(stderr, "header->blksize=%u\n", header->blksize);
    fprintf(stderr, "header->ranges=%u\n", header->ranges);
    fprintf(stderr, "header->crc16=0x%04X\n", header->crc16);

    unsigned int i;
    for (i = 0; i < header->ranges; ++i) {
        fprintf(stderr, "[%u, %u)\n", ranges_table[i].range[0],
                ranges_table[i].range[1]);
    }
    if (range_info != NULL) {
        for (i = 0; i < header->ranges; ++i) {
            fprintf(stderr, "%"PRIu64", %"PRIu64", %"PRIu64"\n",
                    range_info[i].relative_offset,
                    range_info[i].absolute_offset, range_info[i].size);
        }
    }
}

void *f2bs_ranges_mmap(struct f2bs_header *header,
        struct f2bs_range ranges_table[], int fd) {
    /* Check input firstly */
    if (f2bs_ranges_check(header, ranges_table) != 0) {
        fprintf(stderr, "f2bs_ranges_check failed, can not mmap\n");
        return NULL;
    }

    // Reserve enough contiguous address space for the whole file.
    uint64_t whole_size = (uint64_t) header->blocks * header->blksize;
    void *reserve = mmap64(NULL, whole_size, PROT_NONE, MAP_PRIVATE | MAP_ANON,
            -1, 0);
    if (reserve == MAP_FAILED) {
        fprintf(stderr, "failed to reserve address space: %s\n",
                strerror(errno));
        return NULL;
    }
    //fprintf(stderr, "reserve=%p\n", reserve);

    /* Map ranges */
    int rc = 0;
    unsigned char *next;
    unsigned int i;
    uint64_t length;
    for (i = 0, next = reserve; i < header->ranges; ++i, next += length) {
        length =
                (uint64_t) (ranges_table[i].range[1] - ranges_table[i].range[0])
                        * header->blksize;
        //fprintf(stderr, "next=%p, length=%"PRIu64"\n", next, length);

        /* Map from next */
        if (MAP_FAILED
                == mmap64(next, length, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd,
                        (uint64_t) ranges_table[i].range[0]
                                * header->blksize)) {
            fprintf(stderr, "failed to map range %u: %s\n", i, strerror(errno));
            rc = -1;
            break;
        }
    }
    if (rc) {
        munmap(reserve, whole_size);
        return NULL;
    }

    return reserve;
}

int f2bs_ranges_check(struct f2bs_header *header,
        struct f2bs_range ranges_table[]) {
    /* Check magic, flag and crc16. */
    return ((header->magic == F2BS_MAGIC) && (header->flag == F2BS_FLAG_VALID)
            && (header->crc16
                    == crc16(0xFFFF, (uint8_t *) &ranges_table[0],
                            header->ranges * F2BS_RANGE_SIZE))) ? 0 : -1;
}
