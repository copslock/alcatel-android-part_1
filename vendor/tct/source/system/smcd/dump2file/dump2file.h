/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef __DUMP2FILE_H__
#define __DUMP2FILE_H__

#include <stdint.h>


/**
 * Prepare a large empty file for saving ramdump.
 * Return 0 on success, -1 on failure.
 */
int do_prepare(const char *path, uint64_t size);

/**
 * Clean block ranges header.
 * Return 0 on success, -1 on failure.
 */
int do_clean();

/**
 * Unpack files from raw ramdump, to a directory or compressed .tgz.
 * Return 0 on success, -1 on failure.
 */
int do_unpack(const char *path, const char *path2, int compress);

/**
 * Check if raw ramdump present.
 * Return 0 on present, -1 on absent or failure.
 */
int do_check_ramdump(const char *path);

#endif /* __DUMP2FILE_H__ */

