/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef __TAR_H__
#define __TAR_H__

#include <stdint.h>


/* Transit data to a tar file. */
void tar_add_file(int fd, const char *fname, unsigned char *addr, int read_fd,
        uint64_t offset, uint64_t size);

/* Append 1K zero to tar data */
void tar_append_eof(int fd);

#endif /* __TAR_H__ */
