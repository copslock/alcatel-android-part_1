#define LOG_TAG "Feedback_smcd"

#include <errno.h>
#include <signal.h>
#include <sys/epoll.h>
#include <sys/wait.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include "cmd.h"

//-w  file_count 3
//-c  file size 10M
// -tttt timestamp on log
#define TCP_COMMAND     "tcpdump -p -vv -s 0 -i any  -W 3 -C 10 -tttt -w %s%stcpdump_`date +%%Y%%m%%d_%%H%%M%%S_%%Z`.pcap"
#define TZ_LOG                  "cat /sys/kernel/debug/tzdbg/log"

static  pid_t tcp_pid=-1;

static void TCPDump_exec(){

     create_dir(TCP_FOLDER);
     tcp_pid = fork( );
     if(tcp_pid == 0){
          if(tcp_pid<0){
               ALOGE("In %s line %d, fork tcp dump process failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
               ctrl_data_write(SMC_TCPDUMP_LOG,'s',"Command Failed,tcpdump failed");
               return ;
           }
           char cmd[256];
           char logPath[256];
           getLogRootPath(logPath);
           sprintf(cmd,TCP_COMMAND,logPath,TCP_FOLDER);
           if(bSmcdDebug) ALOGD("tcpdump command pid=%d  cmd=%s",getpid(),cmd);
            execl("/system/bin/sh","sh","-c",cmd,(char*)0);
    } 

     ctrl_data_write(SMC_TCPDUMP_LOG,'s',
                    "Command success,tcpdump start");
   // return;
}

static int TCP_stop(){
  int stat;
  ALOGD("tcp stop %d",tcp_pid);
  if(tcp_pid>0){
      kill(tcp_pid,SIGKILL);
      waitpid(tcp_pid, &stat, WUNTRACED);
      tcp_pid=-1;
  }
  return 1;
}

static int TCP_getState(){
   if(tcp_pid>0)
        return 1;
   else
        return 0;
}

int TCP_start(char * command){
    int subcmd = command[1];
    int result;
    if(bSmcdDebug)
        ALOGD("In %s line %d, TCPdump start %c",__FUNCTION__,__LINE__,subcmd);
    if(access("/system/xbin/tcpdump",X_OK)!=0 )
         ctrl_data_write(SMC_TCPDUMP_LOG,'c',
                        "Command failed,TCP dump not support");
    
    create_dir(TCP_FOLDER);

    switch(subcmd){
        case 's':  //start
            if(tcp_pid<0)
                TCPDump_exec( );
            else ctrl_data_write(SMC_TCPDUMP_LOG,'s',
                        "Command failed,TCP dump already running");
            break;
        case 'k':   //stop
            TCP_stop();
            break;
        case 'c':   //check status
           // ALOGE("tzlog_status is %d ",tzlog_status);
            if(tcp_pid>0 ){
                ctrl_data_write(SMC_TCPDUMP_LOG,'c',
                        "Command success,TCP dump running");
            }
            else{
                ctrl_data_write(SMC_TCPDUMP_LOG,'c',
                        "Command success,TCP dump stopped");
            }
            break;
    }
  return 0;
}

cmdHandler TCPHandler={
  .start=TCP_start,
  .stop =TCP_stop,
  .getState = TCP_getState
};