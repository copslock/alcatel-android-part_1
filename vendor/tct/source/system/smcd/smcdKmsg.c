/* Copyright (C) 2016 Tcl Corporation Limited */

#define LOG_TAG "Feedback_smcd"

#include <arpa/inet.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <cutils/sockets.h>
#include <log/log.h>
#include "cmd.h"

static volatile int kmsg_state=0;  //0 stop; 1 running
extern uint64_t  blockSize;
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void setKmsgStatus(int state){
    pthread_mutex_lock(&mutex);
    kmsg_state = state;
    if (bSmcdDebug) ALOGD("In %s line %d, set kmsg_state as %d", __FUNCTION__, __LINE__,state);
    pthread_mutex_unlock(&mutex);
}

void kmsg_cmd_execute(char *command){
    FILE *fl = NULL;
    int iTryTime = 0;
    uint64_t total_read = 0;
    uint64_t onceSize = 0;
    int fdPkmsg = -1;
    int read_num = 0;
    char line[LINE_MAX];
    struct timeval to;
    char logFile[PATH_MAX];
    char cmd[COMMAND_Length_MAX];

    if(command==NULL){
        if(bSmcdDebug) ALOGD("In %s line %d, command is NULL,use DEFAULTKMSGCMD",__FUNCTION__,__LINE__);
        strcpy(cmd,DEFAULTKMSGCMD);
    } else strlcpy(cmd,&command[2],sizeof(cmd));

    trigger_logs_compress(KMSG_FOLDER);
    if( create_file_name(logFile,cmd,NULL) < 0 ){
        ALOGE("In %s line %d, failed to create file",__FUNCTION__,__LINE__);
        return;
    }
    fl = fopen(logFile,"w+");
    if(fl == NULL){
        ALOGE("In %s line %d open %s failed, errno=%d, %s",__FUNCTION__,__LINE__,logFile,errno,strerror(errno));
        goto kmsgExecExit;
    }
    iTryTime = 0;
    while ((iTryTime < 5) && (fdPkmsg <= 0)) {
        iTryTime++;
        fdPkmsg = open("/proc/kmsg", O_RDONLY | O_NDELAY);
        usleep(1000);
        if(bSmcdDebug) ALOGD("In %s line %d,fdPmesg:%d",__FUNCTION__,__LINE__,fdPkmsg);
    }
    if ((iTryTime >= 5) && (fdPkmsg <= 0)) {
        ALOGE("In %s line %d, open /proc/kmsg failed",__FUNCTION__,__LINE__);
        goto kmsgExecExit;
    }
    setKmsgStatus(1);
    iTryTime = 0;
    while(iTryTime < 120){
        if(!kmsg_state){
            if(bSmcdDebug) ALOGD("In %s line %d, kmsg_flag is false, thread exit",__FUNCTION__,__LINE__);
            break;
        }
        to.tv_sec = 0;
        to.tv_usec = 500*1000;
        fd_set read_fds;
        int status = 0;
        FD_ZERO(&read_fds);
        FD_SET(fdPkmsg, &read_fds);

        status = select(fdPkmsg+1, &read_fds,NULL, NULL, &to);
        if (status == -1) {
            ALOGE("In %s: Error calling select for read, %s, errno: %d\n",
                __FUNCTION__, strerror(errno), errno);
        }else if( status == 0 ){
            continue;
        }else if (!FD_ISSET(fdPkmsg, &read_fds)) {
            ALOGE("In %s: FD_ISSET is false after read select call\n",__FUNCTION__);
        }else{
            if ((read_num = (int) read(fdPkmsg, line, LINE_MAX)) != 0 ){
                if (iTryTime && (onceSize >= 5*1024)){
                    if(bSmcdDebug) ALOGD("In %s line %d, Kernel log is stable, reset iTryTime as 0",__FUNCTION__,__LINE__);
                    onceSize = 0;
                    iTryTime = 0;
                }
                if(total_read >= blockSize){
                    rename_for_compress(logFile);
                    trigger_logs_compress(KMSG_FOLDER);
                    int i = 0;
                    while( (i < 5) && (create_file_name(logFile,cmd,NULL) < 0) ){
                        ALOGE("In %s line %d, failed to create file %s",__FUNCTION__,__LINE__,logFile);
                        usleep(500);
                        i++;
                    }
                    if ((i >= 5) && (access(logFile,F_OK|W_OK) < 0)) {
                        iTryTime++;
                        continue;
                    }
                    if (fl != NULL) fclose(fl);
                    i = 0;
                    while((i < 5) && ( ( fl = fopen(logFile,"w+") ) == NULL)){
                        ALOGE("In %s line %d, open file %s failed, errno=%d, %s"
                                ,__FUNCTION__,__LINE__,logFile,errno,strerror(errno));
                        usleep(500);
                        i++;
                    }
                    if ((i >= 5) && (fl == NULL)) {
                        iTryTime++;
                        continue;
                    }
                    total_read = 0;
                }
                if((read_num == -1) && (errno == EAGAIN)){
                    usleep(500000);
                } else if(read_num > 0){
                    total_read += read_num;
                    if(iTryTime) onceSize += read_num;
                    fwrite(line, sizeof(char), (size_t) read_num, fl);
                } else {
                    ALOGE("In %s line %d, read error, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
                    iTryTime++;
                    continue;
                }
            }
            status = fflush(fl);
            if(status != 0 ){
                ALOGE("In %s line %d, fflush error, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
            }
        }
    }//end for while(1) is restart?
    setKmsgStatus(0);

kmsgExecExit:
    if(fl != NULL){
        if(bSmcdDebug) ALOGD("In %s line %d, file %s is closed",__FUNCTION__,__LINE__,logFile);
        fclose(fl);
        free(fl);
        rename_for_compress(logFile);
    }
    if ((fdPkmsg > 0) && (close(fdPkmsg) != 0)){
        ALOGE("In %s line %d, close fdPmesg failed, errno=%d, %s",__FUNCTION__,__LINE__,errno,strerror(errno));
    }
    setKmsgStatus(0);
    free(command);
}

static int kmsg_start(char *command){
    if(kmsg_state==1){
        ALOGE("In %s line %d, command %s alread is running",__FUNCTION__,__LINE__,command);
        return -1;
    }
    char *chpCommand = (char *)malloc(COMMAND_Length_MAX);
    memset(chpCommand,0,COMMAND_Length_MAX);
    if(NULL == command){
        snprintf(chpCommand,COMMAND_Length_MAX,"%c%c%s",SMC_CMD_EXEC,'a',DEFAULTKMSGCMD);
    }else strlcpy(chpCommand,command,COMMAND_Length_MAX);
    pthread_t tid_cmd_exec;

    create_dir(KMSG_FOLDER);

    pthread_attr_t attr_cmd_exec;
    if((pthread_attr_init(&attr_cmd_exec) != 0)
            || (pthread_attr_setdetachstate(&attr_cmd_exec,
                    PTHREAD_CREATE_DETACHED) != 0)){
    }
    if(pthread_create(&tid_cmd_exec,&attr_cmd_exec,(void *)kmsg_cmd_execute,chpCommand)!= 0){
        ALOGE("In %s line %d , create pthread kmsg_cmd_execute failed, errno=%d, %s"
                ,__FUNCTION__,__LINE__,errno,strerror(errno));
        free(chpCommand);
        return -2;
    }
    pthread_attr_destroy(&attr_cmd_exec);
    return 0;
}

static int kmsg_stop(char *command){
  if(bSmcdDebug) ALOGD("In %s line %d",__FUNCTION__,__LINE__);
  setKmsgStatus(0);
  ctrl_data_write(SMC_CMD_STOP,command[1],"kmsg has been stopped");
  return 0;
}

static int kmsg_getState(char *command){
    if(kmsg_state){
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDRUNNING);
    }else{
        ctrl_data_write(SMC_GET_STATUS,command[1],CMDSTOPPED);
    }
    return kmsg_state;
}

cmdHandler kmsgHandler={
  .start=kmsg_start,
  .stop =kmsg_stop,
  .getState = kmsg_getState
};
