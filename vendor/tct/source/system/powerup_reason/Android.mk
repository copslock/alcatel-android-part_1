# Copyright (C) 2016 Tcl Corporation Limited
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES:= main.c
LOCAL_SHARED_LIBRARIES := libc libcutils liblog

LOCAL_MODULE:= powerup_reason
LOCAL_MODULE_TAGS := optional

LOCAL_INIT_RC := powerup_reason.rc

libsysv-hash-table-library_ldflags := Wl,-hash-style=sysv

include $(BUILD_EXECUTABLE)
