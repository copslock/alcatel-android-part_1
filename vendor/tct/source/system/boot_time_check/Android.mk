LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := boot_time_check
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/bin

LOCAL_FORCE_STATIC_EXECUTABLE := true

LOCAL_SRC_FILES := boot_time_check.c
LOCAL_STATIC_LIBRARIES := libc libcutils
LOCAL_SHARED_LIBRARIES := liblog \
                          libutils

include $(BUILD_EXECUTABLE)

