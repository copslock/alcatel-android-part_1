
#ifndef __RF_POWER_H__
#define __RF_POWER_H__

#include <cutils/log.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "RF_SAR_DAEMON"

int set_rf_power(int rf_stat);

#endif
