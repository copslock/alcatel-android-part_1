
#include "rf_power.h"
#include <cutils/properties.h>
#include <stdlib.h>
#include <string.h>

#ifdef __FUNC__
#undef __FUNC__
#endif
#define  __FUNC__ "sar_daemon main"

int main (int argc,char *argv[])
{
  char value[PROPERTY_VALUE_MAX]={'\0'};
  property_get("persist.sys.sar.enabled", value, "0");
  ALOGD("%s,persist.sys.sar.enabled is %s.\n",__FUNC__,value);

  if (strcmp(value, "1") != 0)
  {
    ALOGE("%s,SAR Feature is disabled.\n",__FUNC__);
    return -1;
  }

  int sar_level =0;
  if (argc > 1) {
    sar_level = atoi(argv[1]);
    if ( (sar_level <0) || (sar_level > 8))
    {
       ALOGD("%s level is invalid,should between 0 and 8.\n",__FUNC__);
       return -1;
    }
  }
  ALOGD("%s set SAR to %d.\n",__FUNC__,sar_level);
  int result = set_rf_power(sar_level);
  ALOGD("%s set sar resut is %d.\n",__FUNC__,result);
  return result;
}


