LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := ADruntime.c
LOCAL_MODULE := ADruntime
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)/system/bin
LOCAL_SHARED_LIBRARIES := libcutils \
                          liblog
LOCAL_MODULE_TAGS := optional

LOCAL_INIT_RC := ADruntime.rc
include $(BUILD_EXECUTABLE)
