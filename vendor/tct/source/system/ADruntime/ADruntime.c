#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/un.h>

#include <cutils/sockets.h>
#include <private/android_filesystem_config.h>
#include <cutils/log.h>

#define LOG_TAG "ADruntime"

#define MAX_DAEMON_CMD_LEN 4096
#define MAX_DAEMON_PARAMS_REQUIRED 5
#define TOKEN_PARAMS_DELIM  ";"
#define DAEMON_SOCKET "pps"

int main (int argc, char **argv) {

	int daemon_socket = -1, ret = 0, index = 0, nargs = 0;
	char cmd[MAX_DAEMON_CMD_LEN];
	char reply[32];
	char *params;
	char *token[MAX_DAEMON_PARAMS_REQUIRED];

	params = argv[1];
	token[index++] = strtok(params, TOKEN_PARAMS_DELIM);
	if (!strncmp(token[0], "pp:set:hsic", strlen("pp:set:hsic")))
		nargs = 4;
	else if (!strncmp(token[0], "cabl:set", strlen("cabl:set")) ||
        !strncmp(token[0], "pp:calib:load:profile", strlen("pp:calib:load:profile")))
		nargs = 1;
	if (!strncmp(token[0], "ad:init", strlen("ad:init")) ||
		!strncmp(token[0], "ad:config", strlen("ad:config")) ||
		!strncmp(token[0], "ad:input", strlen("ad:input")) ||
		!strncmp(token[0], "ad:on", strlen("ad:on")) ||
		!strncmp(token[0], "bl:set", strlen("bl:set")) ||
		!strncmp(token[0], "set", strlen("set")) ||
		!strncmp(token[0], "ad:assertiveness", strlen("ad:assertiveness"))) {
		token[index++] = strtok(NULL, ":");
	} else {
		while (nargs--)
			token[index++] = strtok(NULL, TOKEN_PARAMS_DELIM);
	}
	SLOGI("command: %s argc=%d\n", params,argc);
	daemon_socket = socket_local_client(DAEMON_SOCKET,
			ANDROID_SOCKET_NAMESPACE_RESERVED, SOCK_STREAM);
	if(!daemon_socket) {
		SLOGE("Connecting to socket failed: %s\n", strerror(errno));
		return -1;
	}

	/* make a daemon parseable command */
	if (!strncmp(params, "pp:set:hsic", strlen("pp:set:hsic")))
		sprintf(cmd, "%s %d %f %d %f\n", token[0], atoi(token[1]),
				atof(token[2]), atoi(token[3]), atof(token[4]));
	else if (!strncmp(token[0], "cabl:set", strlen("cabl:set")) ||
        !strncmp(token[0], "pp:calib:load:profile", strlen("pp:calib:load:profile"))){
		char *ptemp = NULL;
		ptemp = strstr(token[1], "\r\n");
		if (ptemp)
		     *ptemp = '\0';
		snprintf(cmd, MAX_DAEMON_CMD_LEN, "%s %s", token[0], token[1]);
        }
	else if (!strncmp(token[0], "ad:init", strlen("ad:init")) ||
		!strncmp(token[0], "ad:config", strlen("ad:config")) ||
		!strncmp(token[0], "ad:input", strlen("ad:input")) ||
		!strncmp(token[0], "ad:on", strlen("ad:on")) ||
		!strncmp(token[0], "bl:set", strlen("bl:set")) ||
		!strncmp(token[0], "set", strlen("set")) ||
		!strncmp(token[0], "ad:assertiveness", strlen("ad:assertiveness"))) {
		char *ptemp = NULL;
		ptemp = strstr(token[1], "\r\n");
		if (ptemp)
		     *ptemp = '\0';
		snprintf(cmd, MAX_DAEMON_CMD_LEN, "%s;%s", token[0], token[1]);
        }
	else
		sprintf(cmd, "%s", token[0]);

	SLOGI("writing %s to the socket\n", cmd);
	ret = write(daemon_socket, cmd, strlen(cmd));
	if(ret < 0) {
		SLOGE("%s: Failed to send data over socket %s\n",
				strerror(errno), DAEMON_SOCKET);
		close(daemon_socket);
		return -1;
	}

	/* listen for daemon responses */
	ret = read(daemon_socket, reply, sizeof(reply));
	if(ret < 0) {
		SLOGE("%s: Failed to get data over socket %s\n", strerror(errno),
				DAEMON_SOCKET);
		close(daemon_socket);
		return -1;
	}
	SLOGI("Daemon response: %s\n", reply);
	close(daemon_socket);
	return 0;
}
