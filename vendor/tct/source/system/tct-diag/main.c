/* Copyright (C) 2016 Tcl Corporation Limited */

/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*

                Sample Application for Diag Consumer Interface // MODIFIED by wu.yan, 2016-11-11,BUG-3443853

GENERAL DESCRIPTION
This is a sample application to demonstrate using Diag Consumer Interface APIs.

EXTERNALIZED FUNCTIONS
  None

INITIALIZATION AND SEQUENCING REQUIREMENTS

Copyright (c) 2013-2014 Qualcomm Technologies, Inc.  All Rights Reserved.
Qualcomm Technologies Proprietary and Confidential.

Author: Ravi Aravamudhan
Source: Jamila Murabbi
Modified: Wu YAN // MODIFIED by wu.yan, 2016-11-11,BUG-3443853
*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*/

/*===========================================================================*/

#include "event.h"
#include "msg.h"
#include "log.h"
#include "diag_lsm.h"
#include "diag_lsmi.h"
#include "diag_lsm_dci.h"
#include "diag_shared_i.h"
#include "stdio.h"
#include "string.h"
#include "malloc.h"
#include "diagpkt.h"
#include "diagdiag.h" /* For macros used in this sample. */
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/klog.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/time.h>
#include <pthread.h>
#include <time.h> // MODIFIED by wu.yan, 2016-11-11,BUG-3443853
#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "TCT_DIAG"
#include "cutils/log.h"
#define DIAG_LOGE ALOGW

/* MODIFIED-BEGIN by wu.yan, 2016-11-11,BUG-3443853*/
#define TOTAL_LOG_CODES 1
#define TOTAL_LOG_CODES_APPS 3
#define DIAG_SAMPLE_SIGNAL SIGRTMIN + 15

#define DIAG_CMD_CODE_IDX 0
#define DIAG_SUBSYS_ID_IDX 1
#define DIAG_STRESS_TEST_OP_IDX 8
#define ARRAY_SIZE(a) sizeof(a) / sizeof(((a)[0]))

/* Channel proc set to MSM by default */
static int channel = MSM;
static int cmd_num = 0;
/* struct to store diag request packet */
struct {
    int size;
    unsigned char buf[DIAG_MAX_RX_PKT_SIZ];
} diag_req_pkt;

/* mutex*/
pthread_mutex_t mutex;
pthread_cond_t cond;

/* Set flag to print the bytes */
static inline void print_bytes(unsigned char *buf, int len, int flag) {
    int i = 0;
    if (!flag) return;
    if (!buf || len <= 0) return;
    printf("\n");
    for (i = 0; i < len; i++) {
        printf(" %02x ", buf[i]);
    }
    printf("\n");
}

static void usage(char *progname) {
    printf("\n Usage for %s:\n", progname);
    printf(
        "\n-c  --channel name:\t the channel name for all operations 0 - MSM "
        "and 1 - MDM\n");  // MODIFIED by wu.yan, 2016-11-11,BUG-3443853
    printf("-p --packet packet:\t Diag Request Packet\n");
    exit(0);
}

static void parse_packet(char *s) {
    char *p;
    while (*s == ' ') ++s;
    diag_req_pkt.size = 0;
    // assume token is space
    while (*s) {
        p = strchr(s, ' ');
        if (p) *p = '\0';
        diag_req_pkt.buf[diag_req_pkt.size++] = to_integer(s) & 0xff;
        if (!p) {
            break;
        } else {
            while (*++p == ' ')
                ;
        }
        s = p;
    }
    // print_bytes(diag_req_pkt.buf, diag_req_pkt.size, TRUE);
}

static void parse_args(int argc, char **argv) {
    int command, proc_id;
    int file_num = 0;
    struct option longopts[] = {{"channelname", 1, NULL, 'c'},
                                {"help", 0, NULL, 'h'},
                                {"packet", 1, NULL, 'p'},
                                {"number", 1, NULL, 'n'}, };

    while ((command = getopt_long(argc, argv, "c:p:b:h", longopts, NULL)) !=
           -1) {
        switch (command) {
            case 'c':
                proc_id = atol(optarg);
                if (proc_id >= MSM && proc_id <= MDM) channel = proc_id;
                break;
            case 'p':
                parse_packet(optarg);
                break;
            case 'h':
            default:
                usage(argv[0]);
                break;
        }
    }
}

void process_response(unsigned char *ptr, int len, void *data_ptr) {
    int i = 0;
    uint8 operation = 0;
    (void)data_ptr;

    if (!ptr || len < 0) return;

    DIAG_LOGE(" Received Response of size %d bytes.\n", len);
    print_bytes(ptr, len, TRUE);
    /* MODIFIED-BEGIN by wu.yan, 2016-11-11,BUG-3443853*/
    pthread_mutex_lock(&mutex);
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex);
    /* MODIFIED-END by wu.yan,BUG-3443853*/
}

/* Main Function. This demonstrates using the DCI APIs defined in
 * diag_lsm_dci.h*/
int main(int argc, char *argv[]) {
    int err, client_id;
    int signal_type = SIGCONT;
    boolean bInit_success = FALSE;
    diag_dci_peripherals list = DIAG_CON_MPSS | DIAG_CON_APSS | DIAG_CON_LPASS;
    // struct diag_dci_health_stats *dci_health_stats; /* To collect DCI Health
    // Statistics */
    unsigned char *dci_rsp_pkt = NULL;
    (void)argc;
    (void)argv;

    /* init mutex and cond */
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond, NULL);

    parse_args(argc, argv);
    /* Registering with Diag which gives the client a handle to the Diag driver
     */
    bInit_success = Diag_LSM_Init(NULL);
    if (!bInit_success) {
        DIAG_LOGE(" Couldn't register with Diag LSM, errno: %d\n", errno);
        return -1;
    }

    dci_rsp_pkt = (unsigned char *)malloc(
        DIAG_MAX_RX_PKT_SIZ);  // MODIFIED by wu.yan, 2016-11-11,BUG-3443853
    if (!dci_rsp_pkt) {
        DIAG_LOGE("  Unable to allocate memory for DCI rsp pkt, errno: %d",
                  errno);
        return -1;
    }

    /* Registering with DCI - This assigns a client ID */
    /* Channel 0 - MSM, 1 - MDM */
    err = diag_register_dci_client(&client_id, &list, channel, &signal_type);
    if (err != DIAG_DCI_NO_ERROR) {
        DIAG_LOGE(" Could not register with DCI, err: %d, errno: %d\n", err,
                  errno);
        free(dci_rsp_pkt);
        return -1;
    } else
        DIAG_LOGE(" Successfully registered with DCI, client ID = %d\n",
                  client_id);

    /* Getting supported Peripherals list */
    DIAG_LOGE(" DCI Status on Processors:\n");
    err = diag_get_dci_support_list_proc(channel, &list);
    if (err != DIAG_DCI_NO_ERROR) {
        printf(" Could not get support list, err: %d, errno: %d\n", err, errno);
        free(dci_rsp_pkt);
        return -1;
    }
    DIAG_LOGE("   MPSS:\t ");
    DIAG_LOGE((list & DIAG_CON_MPSS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   LPASS:\t ");
    DIAG_LOGE((list & DIAG_CON_LPASS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   WCNSS:\t ");
    DIAG_LOGE((list & DIAG_CON_WCNSS) ? "UP\n" : "DOWN\n");
    DIAG_LOGE("   APSS:\t ");
    DIAG_LOGE((list & DIAG_CON_APSS) ? "UP\n" : "DOWN\n");

    DIAG_LOGE(" Sending Asynchronous Command \n");

    /* Diag Log stress test command - one Log packet comes out every second */
    // unsigned char buf[24] = {75, 18, 0, 0, 1, 0, 0, 0, 16, 1, 1, 0, 0, 1, 0,
    // 0, 232, 3, 0, 0, 1, 0, 0, 0};
    err = diag_send_dci_async_req(client_id, diag_req_pkt.buf,
                                  diag_req_pkt.size, dci_rsp_pkt,
                                  DIAG_MAX_RX_PKT_SIZ, &process_response, NULL);
    if (err != DIAG_DCI_NO_ERROR) {
        DIAG_LOGE(" Error sending SEND_REQUEST_ASYNC to peripheral %d\n", err);
    } else {
        struct timespec time;
        clock_gettime(CLOCK_REALTIME, &time);
        time.tv_sec += 3;  // 3s timeout
        pthread_mutex_lock(&mutex);
        pthread_cond_timedwait(&cond, &mutex, &time);
        pthread_mutex_unlock(&mutex);
    }

    /* destroy mutex and cond */
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);

    /* Releasing DCI connection */
    DIAG_LOGE(" Releasing DCI connection \n");
    err = diag_release_dci_client(&client_id);
    if (err != DIAG_DCI_NO_ERROR)
        DIAG_LOGE(" Error releasing DCI connection, err: %d, errno: %d\n", err,
                  errno);

    Diag_LSM_DeInit();
    printf("\n");
    free(dci_rsp_pkt);
    return 0;
    /* MODIFIED-END by wu.yan,BUG-3443853*/
}
