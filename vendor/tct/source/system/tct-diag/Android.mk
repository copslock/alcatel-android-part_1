# Copyright (C) 2016 Tcl Corporation Limited
################################################################################
# @file pkgs/stringl/Android.mk
# @brief Makefile for building the string library on Android.
################################################################################

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

libdiag_includes:= \
    vendor/qcom/proprietary/diag/include \
	vendor/qcom/proprietary/diag/src


LOCAL_C_INCLUDES := $(libdiag_includes)
LOCAL_C_INCLUDES += $(TARGET_OUT_HEADERS)/common/inc
LOCAL_C_INCLUDES += system/core/include/cutils/

LOCAL_SRC_FILES:= \
        main.c

LOCAL_LDLIBS += -pthread
commonSharedLibraries :=libdiag

LOCAL_MODULE := tct-diag
LOCAL_MODULE_TAGS := optional eng
LOCAL_SHARED_LIBRARIES := $(commonSharedLibraries)
LOCAL_SHARED_LIBRARIES += libcutils

include $(BUILD_EXECUTABLE)
