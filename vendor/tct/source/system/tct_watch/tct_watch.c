#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <ctype.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <time.h>

#define OLD 0
#define NEW 1
#define CPU_MAX_NUM 8

#define GPU_BUSY_PATH   	"/sys/class/kgsl/kgsl-3d0/gpubusy"
#define GPU_FREQ_PATH		"/sys/kernel/debug/clk/gpu_gx_gfx3d_clk/measure"
#define	IO_PATH			"/proc/vmstat"

struct cpu_info
{
	unsigned int   core_sum;
	unsigned int   freq[CPU_MAX_NUM];
	double load[CPU_MAX_NUM+1];
};

struct cpu_state
{
		unsigned int user[CPU_MAX_NUM+1];
		unsigned int nice[CPU_MAX_NUM+1];
		unsigned int system[CPU_MAX_NUM+1]; 
		unsigned int idle[CPU_MAX_NUM+1]; 
		unsigned int iowait[CPU_MAX_NUM+1]; 
		unsigned int irq[CPU_MAX_NUM+1];
		unsigned int softirq[CPU_MAX_NUM+1];
};

struct gpu_info
{
	unsigned int freq;
	double load;
};

struct io_info
{
	unsigned int  in;
	unsigned int out;
};

static int get_cpu( struct cpu_info *cpu )
{
	int rc;
	FILE *fd;
	unsigned int i;
	char   *str;
	char   path[64];
	char   buf[1024];
	char   dump[64];
	double temp;
	static struct cpu_state state[2];
	str=buf;

	/* get cpu freq*/
	for(i = 0 ;i < cpu->core_sum; i++)
		{	
			snprintf(path, sizeof(path)-1,"/sys/devices/system/cpu/cpu%u/cpufreq/scaling_cur_freq",i);
			rc = access(path,F_OK);
			if(!rc)
				{
					fd=fopen(path, "r");
					if(!fd)
						rc = -1 ;
					else	
					{
							rc = fscanf(fd,"%u",&cpu->freq[i]);
							if (rc != 1)
								{
			 						fclose(fd);
			 						return -1;
								}
					}
					fclose(fd);
				}
			else
				cpu->freq[i] = 0 ;
		}

		/* get cpu load */
		fd = fopen("/proc/stat", "r");
		if(!fd)
			rc = -1 ;
		else
			{
	        rc = fread(&buf,sizeof(char),sizeof(buf)-1,fd);

	        if( rc != sizeof(buf)-1 )
			{
				fclose(fd);	
				return -1 ;
			}
		fclose(fd);

		/* get cpu all info */
				sscanf(str,"%s %u %u %u %u %u %u %u",
						    dump,
						    &state[NEW].user[cpu->core_sum],
						    &state[NEW].nice[cpu->core_sum],
						    &state[NEW].system[cpu->core_sum],		
						    &state[NEW].idle[cpu->core_sum],
						    &state[NEW].iowait[cpu->core_sum],
						    &state[NEW].irq[cpu->core_sum],
						    &state[NEW].softirq[cpu->core_sum]
			       	      );
				/* row end*/
				for(;;)	
					{		
						str ++;
						if( *str == '\n' )
							break;
					}

		/* get cpu every info */
				for(i = 0 ;i < cpu->core_sum; i++)
				{
					if(cpu->freq[i] != 0)
					{
					sscanf(str,"%s %u %u %u %u %u %u %u ",
						    dump,
						    &state[NEW].user[i],
						    &state[NEW].nice[i],
						    &state[NEW].system[i],		
						    &state[NEW].idle[i],
						    &state[NEW].iowait[i],
						    &state[NEW].irq[i],
						    &state[NEW].softirq[i]
						    );
					for(;;)	
						{	
							str ++;
							if( *str == '\n' )
								break;
						}
					}
				}

		if( state[OLD].user[cpu->core_sum] == 0 ) 
			{
			for(i = 0 ; i <= cpu->core_sum; i++)
				{
				/* save old data */
				state[OLD].user[i]    = state[NEW].user[i]; 
				state[OLD].nice[i]    = state[NEW].nice[i];
				state[OLD].system[i]  = state[NEW].system[i];		
				state[OLD].idle[i]    = state[NEW].idle[i];
				state[OLD].iowait[i]  = state[NEW].iowait[i];
				state[OLD].irq[i]     = state[NEW].irq[i];
				state[OLD].softirq[i] = state[NEW].softirq[i];
				}
			rc = 1 ;
			}
		else
			{
			/* set CPU all */ 
			temp  = ( state[NEW].user[cpu->core_sum]    - state[OLD].user[cpu->core_sum]  ) 
					+ ( state[NEW].nice[cpu->core_sum]    - state[OLD].nice[cpu->core_sum]  )
					+ ( state[NEW].system[cpu->core_sum]  - state[OLD].system[cpu->core_sum]);

			cpu->load[cpu->core_sum] = temp / 
						 ( temp + 
					  	        + ( state[NEW].idle[cpu->core_sum]    - state[OLD].idle[cpu->core_sum])
						       	+ ( state[NEW].iowait[cpu->core_sum]  - state[OLD].iowait[cpu->core_sum])
						        + ( state[NEW].irq[cpu->core_sum]     - state[OLD].irq[cpu->core_sum] )
						        + ( state[NEW].softirq[cpu->core_sum] - state[OLD].softirq[cpu->core_sum])
						 );

			state[OLD].user[cpu->core_sum]    = state[NEW].user[cpu->core_sum]; 
			state[OLD].nice[cpu->core_sum]    = state[NEW].nice[cpu->core_sum];
			state[OLD].system[cpu->core_sum]  = state[NEW].system[cpu->core_sum];		
			state[OLD].idle[cpu->core_sum]    = state[NEW].idle[cpu->core_sum];
			state[OLD].iowait[cpu->core_sum]  = state[NEW].iowait[cpu->core_sum];
			state[OLD].irq[cpu->core_sum]     = state[NEW].irq[cpu->core_sum];
			state[OLD].softirq[cpu->core_sum] = state[NEW].softirq[cpu->core_sum];

			for(i = 0 ;i < cpu->core_sum; i++)
				{
				if(cpu->freq[i] != 0)
				{		
				  temp  = ( state[NEW].user[i]    - state[OLD].user[i]  ) 
					+ ( state[NEW].nice[i]    - state[OLD].nice[i]  )
					+ ( state[NEW].system[i]  - state[OLD].system[i]);
					
				  cpu->load[i] = temp / 
					       ( temp + 
					  	      + ( state[NEW].idle[i]    - state[OLD].idle[i])
						      + ( state[NEW].iowait[i]  - state[OLD].iowait[i])
						      + ( state[NEW].irq[i]     - state[OLD].irq[i] )
						      + ( state[NEW].softirq[i] - state[OLD].softirq[i])
						);
				  state[OLD].user[i]    = state[NEW].user[i]; 
				  state[OLD].nice[i]    = state[NEW].nice[i];
				  state[OLD].system[i]  = state[NEW].system[i];		
				  state[OLD].idle[i]    = state[NEW].idle[i];
				  state[OLD].iowait[i]  = state[NEW].iowait[i];
				  state[OLD].irq[i]     = state[NEW].irq[i];
				  state[OLD].softirq[i] = state[NEW].softirq[i];
				}
				else
					cpu->load[i] = 0;
					}
				}
			}

	return rc;
}
		
static int get_gpu( struct gpu_info *gpu )
		{
			int rc;
			unsigned int one;
			unsigned int two;
			FILE *fd; 

			fd=fopen(GPU_FREQ_PATH, "r");
			if (!fd) 
				return -1;
			rc = fscanf(fd,"%u",&gpu->freq);
			if (rc != 1)
				{
				 fclose(fd);
				 return -1;
				}
			fclose(fd);

			fd=fopen(GPU_BUSY_PATH, "r");
			if (!fd) 
				return -1;
			rc = fscanf(fd,"%u%u",&one,&two);
			if (rc != 2)
				{
				 fclose(fd);
				 return -1;
				}
			fclose(fd);
			if( one==0 || two==0 ) 
					gpu->load = 0 ;
			else 
					gpu->load = (double)one / two ;
	
			return 0;
		}

static int get_io( struct io_info *io )
	{
	
	char   buf[1024];
	char   dump[64];
	char   *str;
	int rc;
	unsigned int i = 0;

	static unsigned int  in[2] ={0,0};
	static unsigned int out[2] ={0,0};

	FILE *fd; 
	fd=fopen(IO_PATH, "r");
	if (!fd) 
		return -1;

	rc = fread(&buf,sizeof(char),sizeof(buf)-1,fd);
	if( rc != sizeof(buf)-1 )
		{
			fclose(fd);	
			return -1 ;
		}
		
	fclose(fd);

	str=buf;
	for(;;)
		{		

		if( *str == '\n' )
				i++;
		if( i == 36 )
			sscanf(str,"%63s%u",dump, &in[1]);
		if( i == 37 )
			{
			sscanf(str,"%63s%u",dump,&out[1]);
			break;
			}
		str++;
		}


	if ( in[OLD] == 0 && out[OLD] == 0 )
		{
			 in[OLD] =  in[NEW];
			out[OLD] = out[NEW];
			return 1 ;	
		}
	else 
		{
			io->  in =  in[NEW] -  in[OLD];
			io-> out = out[NEW] - out[OLD];
		 	 in[OLD] =  in[NEW];
			out[OLD] = out[NEW];	
		}
	return 0;
	}


static void usage(char *cmd) {
    fprintf(stderr, "Usage: %s [ -f file ] [ -s space ] [ -t time ] [ -v version ] [ -h help ] \n"

		    "    -c cpu     get CPU info .\n"
		    "    -g gpu     get GPU info .\n"
		    "    -i io      get IO  info .\n"
                    "    -f file    set save path.\n"			
                    "    -s space   set sampling time (second). \n"			
                    "    -t time    set uptime (second).\n"
                    "    -v version Display this version screen.\n"
                    "    -h help    Display this help screen.\n",
        cmd);
}

static void version(){
    fprintf(stderr,"version:  MSM8996.\n"
		   "          MSM8996 PRO.\n"
	    );
}

struct sw 
{
	unsigned int bit;
	unsigned int delay;
	unsigned int space; 
	char *save_path;
};



int main(int argc , char **argv)
{
	unsigned int i;
	struct sw out;
	out.bit = 0 ;
	out.space = 1 ;
	out.delay = 5*(60/out.space) ;

	
	static struct option const long_opts[]={ 
		{ "cpu"    , no_argument, NULL, 'c' }, 
		{ "gpu"    , no_argument, NULL, 'g' }, 
		{ "io"     , no_argument, NULL, 'i' },
		{ "space"  , no_argument, NULL, 's' },
		{ "time"   , no_argument, NULL, 't' },
		{ "file"   , no_argument, NULL, 'f' },
		{ "help"   , no_argument, NULL, 'h' },      
		{ "version", no_argument, NULL, 'v' },
    		{ NULL     , no_argument, NULL,  0  },           
		}; 

		
	int c;
	while( (c=getopt_long(argc,argv,":cgif:s:t:hv",long_opts, NULL) ) != -1 )
	{
		switch(c) {

		            case 'c':
		  		  out.bit |= 1;
		                  break;

		            case 'g':     		  
				  out.bit |= 2;
				  break;

		            case 'i':     		  
				  out.bit |= 4;
				  break;

	   		    case 'f':
		   		  out.bit |= 8;
	    	   		  out.save_path = optarg;
		   		  break;

			   case  's':     		  
		  			out.space=atoi(optarg);
		  			if ( out.space <= 0 )
					{ 
						fprintf(stderr,"erro :  -s \n");
						return 0;
					}	
				  break;

		            case 't':     		  
		  		if( (out.delay=atoi(optarg)) <= 0 )
					{
						fprintf(stderr,"erro :  -t \n");
						return 0;
					}
				  break;

		            case 'h':
				  usage(argv[0]);
		                  return 0;

		            case 'v':
		  		  version();
		                  return 0;

		            case ':':
				 printf("Missing parameter -%c\n",(char)optopt);	 		 
				 return 0;
		     	    case '?':
		 		 printf("Erro : invalid option -- %c\nTry '%s -h ' for more information.\n",(char)optopt,argv[0]);
		 		 return 0;
			    }	
		}	

	/* set delay time */
	out.delay/=out.space;


	if(!(out.bit & 7))
		out.bit |= 7 ;

	if(out.bit & 8 ) /* save as file */
		{ 
			FILE *fd;
			fd=fopen(out.save_path, "w+");
			if (!fd )
		 		{
					fprintf(stderr, "erro: cannot create %s.\n",out.save_path);
					exit(0);
		 		}

			if(daemon(0,0) == -1) 
				{
					fprintf(stderr, "erro: daemon.\n");
					exit(0);
				}


			struct io_info io;
				memset(&io,0,sizeof(io));
			get_io(&io);

			struct gpu_info gpu;
				memset(&gpu,0,sizeof(gpu));
			get_gpu(&gpu);

			struct cpu_info cpu;
				memset(&cpu,0,sizeof(cpu));
			cpu.core_sum=sysconf(_SC_NPROCESSORS_CONF);
			get_cpu(&cpu);

			sleep(1);
			/*
			get_io(&io);
			get_gpu(&gpu);
			get_cpu(&cpu);
			*/

			time_t now;
			struct tm *tm_now;
			time(&now);
			tm_now = localtime(&now);

			/* write start time */
			fprintf(fd, "start  [%d|%d:%d:%d]\n",
				tm_now->tm_mday, 
				tm_now->tm_hour+8, 
				tm_now->tm_min,
				tm_now->tm_sec
				);
		
			/* write start init info */
			fprintf(fd,"uptime %u\n"
				   "space  %u\n",out.delay,out.space);

			for(;out.delay>0;out.delay--)
			{
				fprintf(fd,"\n--------------------\n");
				/* Display time */
				time(&now);
				tm_now = localtime(&now);
				fprintf(fd,"[TIME][%d|%d:%d:%d]\n",
						tm_now->tm_mday, 
						tm_now->tm_hour+8, 
						tm_now->tm_min,
						tm_now->tm_sec
					);

				/* Display CPU/GPU/IO info */
				if( out.bit & 1 ) 
					{
						get_cpu(&cpu);
						fprintf(fd,"CPU offonline");
						for(i=0;i<cpu.core_sum;i++)
				   			if( cpu.freq[i] == 0 )
									fprintf(fd," %d",i);
						fprintf(fd," .\n");	

						fprintf(fd,"CPU_all_load: %.2lf %%\n",cpu.load[cpu.core_sum]*100);
						for(i=0;i<cpu.core_sum;i++)
						fprintf(fd,"[%u] CPU_freq: %u MHZ CPU_load: %.2lf %%\n",i,cpu.freq[i]/1000,cpu.load[i]*100);
					}	

				if( out.bit & 2 )
					{
						get_gpu(&gpu); 
						fprintf(fd,"[GPU] freq %u MHZ load %.2lf %%\n",gpu.freq/1000000,gpu.load*100);
					}
				if( out.bit & 4 )   
					{
						get_io(&io);
						fprintf(fd,"[IO ] in %u  out %u\n",io.in,io.out);
					}	

			sleep(out.space);	
			}
			fclose(fd);
		}
	else		
		{	/* Display to terminal */

			struct io_info io;
				memset(&io,0,sizeof(io));
			get_io(&io);

			struct gpu_info gpu;
				memset(&gpu,0,sizeof(gpu));
			get_gpu(&gpu);

			struct cpu_info cpu;
				memset(&cpu,0,sizeof(cpu));
			cpu.core_sum=sysconf(_SC_NPROCESSORS_CONF);
			get_cpu(&cpu);


			sleep(1);

			for(;;)
			{
				printf("\n--------------------\n");

				/* Display time */
				time_t now;
				struct tm *tm_now;
				time(&now);
				tm_now = localtime(&now);
				printf("[TIME][%d|%d:%d:%d]\n",
						tm_now->tm_mday, 
						tm_now->tm_hour+8, 
						tm_now->tm_min,
						tm_now->tm_sec
					);

				/* Display CPU/GPU/IO info */
				if( out.bit & 1 ) 
					{
						get_cpu(&cpu);
						printf("CPU offonline");
						for(i=0;i<cpu.core_sum;i++)
				   			if( cpu.freq[i] == 0 )
									printf(" %d",i);
						printf(" .\n");	

						printf("CPU_all_load: %.2lf %%\n",cpu.load[cpu.core_sum]*100);
						for(i=0;i<cpu.core_sum;i++)
							printf("[%u] CPU_freq: %u MHZ CPU_load: %.2lf %%\n",i,cpu.freq[i]/1000,cpu.load[i]*100);
					}	

				if( out.bit & 2 )
					{
						get_gpu(&gpu); 
						printf("[GPU] freq %u MHZ load %.2lf %%\n",gpu.freq/1000000,gpu.load*100);
					}
				if( out.bit & 4 )   
					{
						get_io(&io);
						printf("[IO ] in %u  out %u\n",io.in,io.out);
					}	
				sleep(out.space);
			}
		}

return 0;
}









