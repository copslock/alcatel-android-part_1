# Copyright (C) 2016 Tcl Corporation Limited
################################################################################
# @file 
# @brief system awake test tool.
################################################################################

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        tct_awake.c

#LOCAL_LDFLAGS := -static
LOCAL_MODULE := tct_awake
LOCAL_MODULE_TAGS := optional debug

include $(BUILD_EXECUTABLE)

