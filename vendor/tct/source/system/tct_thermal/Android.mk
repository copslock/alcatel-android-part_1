# Copyright (C) 2016 Tcl Corporation Limited
################################################################################
# @file 
# @brief thermal test tool.
################################################################################

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES:= \
        tct_thermal.c

#LOCAL_LDLIBS += -pthread
#LOCAL_LDFLAGS := -static
LOCAL_MODULE := tct_thermal
LOCAL_MODULE_TAGS := optional debug

include $(BUILD_EXECUTABLE)

