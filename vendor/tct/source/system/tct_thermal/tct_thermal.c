/*
* 
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include <unistd.h>
#include <getopt.h>

#include <time.h>
 
#define CPU_THERMAL_PATH	"/sys/class/thermal/thermal_zone4/temp"	
#define GPU_THERMAL_PATH	"/sys/class/thermal/thermal_zone15/temp"
#define PMIC_THERMAL_PATH	"/sys/class/thermal/thermal_zone22/temp"
#define BATTERY_THERMAL_PATH	"/sys/class/thermal/thermal_zone29/temp"
#define QUIET_THERMAL_PATH 	"/sys/class/thermal/thermal_zone27/temp"


static int cpu_thermal(void)
{
	int thermal=-1;	
	FILE *fd; 
	fd=fopen(CPU_THERMAL_PATH, "r");
	if (!fd) return -1;
	fscanf(fd,"%d",&thermal);
	fclose(fd);
	return thermal;
}
	
static int gpu_thermal(void)
{
	int thermal=-1;
	FILE *fd; 
	fd=fopen(GPU_THERMAL_PATH, "r");
	if (!fd) return -1;
	fscanf(fd,"%d",&thermal);
	fclose(fd);
	return thermal;
}


static int pmic_thermal(void)
{
	int thermal=-1;
	FILE *fd; 
	fd=fopen(PMIC_THERMAL_PATH, "r");
	if (!fd) return -1;	
	fscanf(fd,"%d",&thermal);
	fclose(fd);
	return thermal;
	}


int battery_thermal(void)
{
	int thermal=-1;
	FILE *fd; 
	fd=fopen(BATTERY_THERMAL_PATH, "r");
	if (!fd) return -1;		
	fscanf(fd,"%d",&thermal);
	fclose(fd);
	return thermal;
	}



int quiet_thermal(void)
{
	int thermal=-1;
	FILE *fd; 
	fd=fopen(QUIET_THERMAL_PATH, "r");
	if (!fd) return -1;			
	fscanf(fd,"%d",&thermal);
	fclose(fd);
	return thermal;
}



static void usage(char *cmd) {
    fprintf(stderr, "Usage: %s [ -f file ] [ -s space ] [ -t time ] [ -v version ] [ -h help ]\n"

		    "    -c cpu     get CPU thermal.\n"
		    "    -g gpu     get gpu thermal.\n"
		    "    -p pmic    get pmic thermal.\n"
		    "    -b battery get battery thermal.\n"
		    "    -q quiet   get quiet/phone thermal.\n"
                    "    -f file    set save path.\n"			
                    "    -s space   set sampling time (second). \n"			
                    "    -t time    set uptime (second).\n"
                    "    -v version Display this version screen.\n"
                    "    -h help    Display this help screen.\n",
        cmd);
}


static void version(){
    fprintf(stderr,"version:  MSM8996.\n"
		   "          MSM8996 PRO.\n"
		   "          MSM8953.\n" 
	    );
}


struct sw 
{
	unsigned int bit;
	unsigned int delay;
	unsigned int space; 
	char *save_path;
};



int
main(int argc , char **argv)
{

	struct sw out;
	out.bit = 0;
	out.space=1; 
	out.delay=5*(60/out.space);


	static struct option const long_opts[]={ 
		{ "cpu"    , no_argument, NULL, 'c' }, 
		{ "gpu"    , no_argument, NULL, 'g' }, 
		{ "pmic"   , no_argument, NULL, 'p' },
		{ "battery", no_argument, NULL, 'b' },
		{ "quiet"  , no_argument, NULL, 'q' },
		{ "file"   , no_argument, NULL, 'f' },
		{ "space"  , no_argument, NULL, 's' },
		{ "time"   , no_argument, NULL, 't' },
		{ "help"   , no_argument, NULL, 'h' },      
		{ "version", no_argument, NULL, 'v' },
   		{ NULL     , no_argument, NULL,  0  },           
		}; 


	if( argc==1 )
		out.bit = 31;		
	int c;
	while( (c=getopt_long(argc,argv,":cgpbqf:s:t:hv",long_opts, NULL) ) != -1 )
	{
	  switch(c) {

            case 'c':
		  out.bit	|= 1;
                  break;

            case 'g':
		  out.bit	|= 2;
                  break;

            case 'p':     		  
		  out.bit	|= 4;
		  break;

            case 'b':     		  
		  out.bit	|= 8;
		  break;
	    case 'q':     		  
		  out.bit	|= 16;
		  break;

	   case 'f':
		   out.bit	|= 32;
	    	   out.save_path=optarg;
		   break;

	    case 's':     		  
		  out.space=atoi(optarg);
		  if ( out.space <= 0 )
				{ 
					fprintf(stderr,"erro :  -s \n");
					return 0;
				}	
		   	break;

            case 't':     		  
		  if( (out.delay=atoi(optarg)) <= 0 )
				{
					fprintf(stderr,"erro :  -t \n");
					return 0;
				}
		  break;

            case 'h':
		  usage(argv[0]);
                  return 0;

            case 'v':
		  version();
                  return 0;

           case ':':
		 printf("Missing parameter -%c\n",(char)optopt);	 		 
		 return 0;

	   case '?':
		 printf("Erro : invalid option -- %c\nTry '%s -h ' for more information.\n",(char)optopt,argv[0]);
		 return 0;
			}	
	}
	
	out.delay/=out.space;

	if(!(out.bit & 31))
		out.bit |= 31 ;

	if(out.bit & 32 )
	 {	
		FILE *fd;
		fd=fopen(out.save_path, "w+");
		if (!fd )
		 {
			fprintf(stderr, "erro: cannot create %s.\n",out.save_path);
			exit(0);
		 }
	
		if(daemon(0,0) == -1) 
				{
					fprintf(stderr, "erro: daemon.\n");
					exit(0);
				}

		time_t now;
		struct tm *tm_now;

		for(;out.delay>0;out.delay--)
			{

			time(&now);
			tm_now = localtime(&now);
			fprintf(fd, 
				   "\n-----------------\n"
				    "[%d|%d:%d:%d]\n",
				tm_now->tm_mday, 
				tm_now->tm_hour+8, 
				tm_now->tm_min,
				tm_now->tm_sec
				);
			if( out.bit & 1  )   fprintf(fd,"[CPU temp]     %.2f \n",(float)cpu_thermal()/10);
			if( out.bit & 2  )   fprintf(fd,"[GPU temp]     %.2f \n",(float)gpu_thermal()/10);
			if( out.bit & 4  )   fprintf(fd,"[PMIC temp]    %.2f \n",(float)pmic_thermal()/1000);
			if( out.bit & 8  )   fprintf(fd,"[battery temp] %.2f \n",(float)battery_thermal()/1000);
			if( out.bit & 16 )   fprintf(fd,"[quiet temp]   %.2f \n",(float)quiet_thermal());
			sleep(out.space);	
			}
		fclose(fd);

	 }
	else
	 {
		for(;;)
			{
				printf("\n--------------------\n");
				if( out.bit & 1 )   printf("[CPU temp]     %.2f \n",(float)cpu_thermal()/10);
				if( out.bit & 2 )   printf("[GPU temp]     %.2f \n",(float)gpu_thermal()/10);
				if( out.bit & 4 )   printf("[PMIC temp]    %.2f \n",(float)pmic_thermal()/1000);
				if( out.bit & 8 )   printf("[battery temp] %.2f \n",(float)battery_thermal()/1000);
				if( out.bit & 16)   printf("[quiet temp]   %.2f \n",(float)quiet_thermal());
				sleep(out.space);
			}
	}
return 0;
}





















