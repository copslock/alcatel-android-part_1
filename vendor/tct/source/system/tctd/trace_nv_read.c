/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:12/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  peng.xiong                                                        */
/* E-Mail:  peng.xiong@tcl-mobile.com                                         */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : development/trace_proxy/trace_nv_read.c                          */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 14/07/04| Shishun Liu     | PR-722337          | Read nv,  porting from nv backup code    */
/******************************************************************************/


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <fcntl.h>

#define LOG_TAG "trace_nv_read"
#include <cutils/log.h>

#include "trace_nv_read.h"
#include "qmi_client.h"
#include "qmi_idl_lib.h"
#include "qmi_cci_target_ext.h"
#include "qmi_tct_api_v01.h"

//#define BUF (512 * 20)                      /* buffer length */
#define LOG_TAG "TraceabilityManager"


/*===========================================================================

FUNCTION nv_read_item

DESCRIPTION
  Read any integer type NV item

DEPENDENCIES
  None.

RETURN VALUE
  success or not

SIDE EFFECTS
  Only a meaningful return value if the entry is valid which is determined
  by calling nv_op_is_valid_entry

===========================================================================*/

static retrofit_nv_read_resp_msg_v01 resp;
static uint32 nv_read_item (qmi_client_type clnt, uint32 item_id,byte *result,uint32_t result_len)
{
    int  nv_status = -1;
    uint32_t ret=0;
    int i=0;
    retrofit_nv_read_req_msg_v01 req;

    memset(&resp,0,sizeof(retrofit_nv_read_resp_msg_v01));

    if (item_id < 20000) {
        sprintf(req.efs_path, "/nvm/num/%u", item_id);
    } else {
        sprintf(req.efs_path, "/nv/item_files/rfnv/%08u", item_id);
    }
    ret = qmi_client_send_msg_sync(clnt,
            QMI_RETROFIT_NV_READ_REQ_V01,
            &req,
            sizeof(retrofit_nv_read_req_msg_v01),
            &resp,
            sizeof(retrofit_nv_read_resp_msg_v01),
            0);
    if (ret != 0)
    {
        ALOGE("Error==========ret=%d item_id=%d\n",ret,item_id);
        return -1;
    }
    if( resp.result!=0){
        ALOGE("nv read WARNING=======return(%d) item_id=%d\n",resp.result,item_id);
        return -1;
    }

    if(result_len<resp .nv_data_len){
        memcpy(result,(byte *)resp.nv_data,result_len);
    }
    else {
        memcpy(result,(byte *)resp.nv_data,resp.nv_data_len);
    }
    return resp.nv_data_len;
}


/*===========================================================================

FUNCTION NV_EFS_GENERATE

DESCRIPTION
  1. loop the backup table for each item abs
  2. call each api

DEPENDENCIES
  None.

RETURN VALUE
  success or not

SIDE EFFECTS
  Only a meaningful return value if the entry is valid which is determined
  by calling nv_op_is_valid_entry

===========================================================================*/
static int nv_efs_generate(uint32_t item_id, char *nv_result, uint32_t nv_result_size)
{
    int status = 0,rc=0,service_connect=0;
    int32 len;

    qmi_cci_os_signal_type os_params;
    uint32_t num_services, num_entries=0;
    qmi_client_type clnt, notifier;
    qmi_service_info info[10];

    //init qmi  =========================
    qmi_idl_service_object_type test_service_object = tct_get_service_object_v01();
    if (!test_service_object)
    {
        ALOGE("TCT_QMI: tct_get_service_object_internal_v01 failed.\n");
        return -1;
    }
    rc=  qmi_client_notifier_init(test_service_object, &os_params, &notifier);
    /* Check if the service is up, if not wait on a signal */
    while(1)
    {
        rc = qmi_client_get_service_list( test_service_object, NULL, NULL, &num_services);
        ALOGE("TCT_QMI: qmi_client_get_service_list() returned %d num_services = %d\n", rc, num_services);
        if(rc == QMI_NO_ERR)
            break;
        /* wait for server to come up */
        QMI_CCI_OS_SIGNAL_WAIT(&os_params, 0);
    }
    num_entries = num_services;
    /* The server has come up, store the information in info variable */
    rc = qmi_client_get_service_list( test_service_object, info, &num_entries, &num_services);
    ALOGE("TCT_QMI: qmi_client_get_service_list() returned %d num_entries = %d num_services = %d\n", rc, num_entries, num_services);

    if (service_connect >= num_services)
    {
        ALOGE("TCT_QMI: Only %d Test Services found: Choosing the default one)\n",num_services);
        service_connect = 0;
    }

    rc = qmi_client_init(&info[service_connect], test_service_object, NULL, NULL, NULL, &clnt);
    ALOGE("TCT_QMI : qmi_client_init returned %d\n", rc);
    //init qmi  end=========================

    //item_id
    len=nv_read_item(clnt,item_id,nv_result,nv_result_size);
    rc= qmi_client_release(clnt);
    rc= qmi_client_release(notifier);
    ALOGE("Qmi_client_release of notifier returned %d\n", rc);

    return len;
}


int tct_nv_read_register(uint32_t item_id, char *nv_result, uint32_t nv_result_size)
{
    return nv_efs_generate(item_id, nv_result, nv_result_size);
}

