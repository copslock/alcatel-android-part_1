/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_ft_cmd.c                   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/10/18| He.Zhang       | FR-334604          | Recovery old pickup func   */
/* 12/09/21| He.Zhang       | FR-334604          | Create for factory test    */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <cutils/properties.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_ft_cmd.h"

static const diagpkt_user_table_entry_type tct_ft_cmd_tbl[] =
{
    {TCT_FT_CMD_EXEC, TCT_FT_CMD_EXEC, tct_ft_cmd_exec_func},
    {TCT_FT_CMD_PICKUP, TCT_FT_CMD_PICKUP, tct_ft_cmd_pickup_func},
    {TCT_FT_CMD_GETPROP, TCT_FT_CMD_GETPROP, tct_ft_cmd_getprop_func},
    {TCT_FT_CMD_PICKUP_V2, TCT_FT_CMD_PICKUP_V2, tct_ft_cmd_pickup_v2_func},
};

//#define TCT_FT_CMD_WHITE_LIST

#ifdef TCT_FT_CMD_WHITE_LIST
static char white_list[][TCT_FT_CMD_MAX_COMMAND_LEN] = {
    "ls",
};

static unsigned int white_list_size =
        sizeof(white_list) / TCT_FT_CMD_MAX_COMMAND_LEN;

static boolean isinwhitelist(const char *cmd)
{
    int i;

    for (i = 0; i < white_list_size; ++i) {
        if (!strcmp(cmd, white_list[i])) {
            return TRUE;
        }
    }

    return FALSE;
}
#endif

PACK(void *) tct_ft_cmd_exec_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    req_type_tct_ft_cmd_exec *req = (req_type_tct_ft_cmd_exec *) req_pkt;
    rsp_type_tct_ft_cmd_exec *rsp = NULL;
    char *command = NULL;
    int ret = -1;

    DIAG_LOGE("tct_ft_cmd_exec_func\n");

    command = (char *)malloc(pkt_len);
    if (command != NULL) {
        memset(command, 0, pkt_len);
        strncpy(command, req->command, pkt_len - TCT_FT_CMD_PACK_HEAD_SIZE);
        DIAG_LOGE("command %s\n", command);
    } else {
        DIAG_LOGE("malloc command failed\n");
        return NULL;
    }

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_ft_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_FT_CMD_EXEC,
            sizeof(rsp_type_tct_ft_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_ft_cmd_exec));
    memcpy(rsp, req, TCT_FT_CMD_PACK_HEAD_SIZE);

#ifdef TCT_FT_CMD_WHITE_LIST
    if (isinwhitelist(command)) {
        ret = system(command);
    } else {
        DIAG_LOGE("no permission to exec\n");
    }
#else
    ret = system(command);
#endif

    DIAG_LOGE("result %d\n", rsp->result);
    rsp->result = ret;

    return (rsp);
}

PACK(void *) tct_ft_cmd_pickup_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    req_type_tct_ft_cmd_pickup *req = (req_type_tct_ft_cmd_pickup *) req_pkt;
    rsp_type_tct_ft_cmd_pickup *rsp = NULL;
    char *path;
    FILE *fp;

    DIAG_LOGE("tct_ft_cmd_pickup_func\n");

    path = (char *)malloc(pkt_len);
    if (path != NULL) {
        memset((void *)path, 0, pkt_len);
        strncpy(path, req->path, pkt_len - TCT_FT_CMD_PACK_HEAD_SIZE);
        DIAG_LOGE("path %s\n", path);
    } else {
        DIAG_LOGE("malloc path failed\n");
        return NULL;
    }

    rsp = (rsp_type_tct_ft_cmd_pickup *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_FT_CMD_PICKUP,
            sizeof(rsp_type_tct_ft_cmd_pickup));
    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset((void *)rsp, 0, sizeof(rsp_type_tct_ft_cmd_pickup));
    memcpy((void *)rsp, (void *)req, TCT_FT_CMD_PACK_HEAD_SIZE);
    fp = fopen(path, "rb");
    if (fp != NULL) {
        fread(rsp->content, 1, TCT_FT_CMD_MAX_CONTENT_LEN, fp);
        fclose(fp);
    } else {
        DIAG_LOGE("open %s failed\n", path);
    }

    return rsp;
}

PACK(void *) tct_ft_cmd_getprop_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    req_type_tct_ft_cmd_getprop *req = (req_type_tct_ft_cmd_getprop *) req_pkt;
    rsp_type_tct_ft_cmd_getprop *rsp = NULL;
    char prop_name[PROPERTY_KEY_MAX];

    DIAG_LOGE("tct_ft_cmd_getprop_func\n");

    if (PROPERTY_KEY_MAX - 1 < pkt_len - TCT_FT_CMD_PACK_HEAD_SIZE) {
        DIAG_LOGE("prop_name too long\n");
        return NULL;
    }

    memset(prop_name, 0, PROPERTY_KEY_MAX);
    memcpy(prop_name, req->key, pkt_len - TCT_FT_CMD_PACK_HEAD_SIZE);
    DIAG_LOGE("prop_name=%s\n", prop_name);

    rsp = (rsp_type_tct_ft_cmd_getprop *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_FT_CMD_GETPROP,
            sizeof(rsp_type_tct_ft_cmd_getprop));
    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset((void *)rsp, 0, sizeof(rsp_type_tct_ft_cmd_getprop));
    memcpy((void *)rsp, (void *)req, TCT_FT_CMD_PACK_HEAD_SIZE);
    property_get(prop_name, rsp->value, NULL);

    return rsp;
}

PACK(void *) tct_ft_cmd_pickup_v2_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    req_type_tct_ft_cmd_pickup_v2 *req = (req_type_tct_ft_cmd_pickup_v2 *) req_pkt;
    rsp_type_tct_ft_cmd_pickup_v2 *rsp = NULL;
    char *path;
    FILE *fp;

    DIAG_LOGE("tct_ft_cmd_pickup_v2_func\n");

    path = (char *)malloc(pkt_len);
    if (path != NULL) {
        memset((void *)path, 0, pkt_len);
        strncpy(path, req->path, pkt_len - PICKUP_V2_PACK_HEAD_SIZE);
        DIAG_LOGE("path %s\n", path);
    } else {
        DIAG_LOGE("malloc path failed\n");
        return NULL;
    }

    rsp = (rsp_type_tct_ft_cmd_pickup_v2 *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_FT_CMD_PICKUP_V2,
            sizeof(rsp_type_tct_ft_cmd_pickup_v2));
    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset((void *)rsp, 0, sizeof(rsp_type_tct_ft_cmd_pickup_v2));
    memcpy((void *)rsp, (void *)req, TCT_FT_CMD_PACK_HEAD_SIZE);
    fp = fopen(path, "rb");
    if (fp != NULL) {
        fseek(fp, req->start_sector * TCT_FT_CMD_MAX_CONTENT_LEN, SEEK_SET);
        rsp->read_size = fread(rsp->content, 1, TCT_FT_CMD_MAX_CONTENT_LEN, fp);
        fclose(fp);
    } else {
        DIAG_LOGE("open %s failed\n", path);
    }

    return rsp;
}

void tct_ft_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_ft_cmd_tbl);
}

