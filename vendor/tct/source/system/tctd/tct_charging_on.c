/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_debug_on.c             */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/07/10| yongliang.wang | FR-483802          | Send command to turn on adb*/
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/


#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_charging_on.h"

static const diagpkt_user_table_entry_type tct_charging_on_tbl[] =
{
    {TCT_CHARGING_ON, TCT_CHARGING_ON, tct_charging_on_func},
};

PACK(void *) tct_charging_on_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    uint16 switcher;
    DIAG_LOGE("tct_charging_on_func \n");

    req_type_tct_charging_on_exec *req = (req_type_tct_charging_on_exec *) req_pkt;
    rsp_type_tct_charging_on_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_charging_on_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_CHARGING_ON,
            sizeof(rsp_type_tct_charging_on_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_charging_on_exec));
    memcpy(rsp, req, sizeof(req_type_tct_charging_on_exec) - 1);
    rsp->result = 0;

    switcher = req->switcher;

    pthread_t tid;

    printf("before thread_create delay: switch %d  \n",switcher);
    if(switcher == 1)
    {
        if(pthread_create(&tid,NULL,(void*)&charging_on_thread,NULL)!=0)
        {
            printf("thread create failed\n");
        }
    }
    else
    {
       if(pthread_create(&tid,NULL,(void*)&charging_off_thread,NULL)!=0)
        {
            printf("thread create failed\n");
        }
    }
    return (rsp);
}

void charging_on_thread()
{
    //sleep(delay);
    //system("echo 0x01>/sys/kernel/debug/spmi/spmi-0/count");
    //system("echo 0x1347>/sys/kernel/debug/spmi/spmi-0/address");
    //system("echo 0x00>/sys/kernel/debug/spmi/spmi-0/data");
    system("echo 1 > /sys/class/power_supply/battery/charging_enabled");
}
void charging_off_thread()
{
    //sleep(delay);
    //system("echo 0x01>/sys/kernel/debug/spmi/spmi-0/count");
    //system(" echo 0x1347>/sys/kernel/debug/spmi/spmi-0/address");
    //system("echo 0x01>/sys/kernel/debug/spmi/spmi-0/data");
    system("echo 0 > /sys/class/power_supply/battery/charging_enabled");
}

void tct_charging_on_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_charging_on_tbl);
}

