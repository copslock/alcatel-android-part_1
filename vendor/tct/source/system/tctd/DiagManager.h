/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _DIAGMANAGER_H
#define _DIAGMANAGER_H

#ifdef __cplusplus
#include <sysutils/SocketListener.h>

class DiagManager {
private:
    static DiagManager *sInstance;

private:
    SocketListener        *mBroadcaster;

public:
    virtual ~DiagManager();

    int start();
    int stop();

    void setBroadcaster(SocketListener *sl) { mBroadcaster = sl; }
    SocketListener *getBroadcaster() { return mBroadcaster; }

    static DiagManager *Instance();

private:
    DiagManager();
};
#endif /* __cplusplus */

#endif
