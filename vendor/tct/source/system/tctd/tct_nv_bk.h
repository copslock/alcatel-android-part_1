/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_nv_bk.h                    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/12/21| Guobing.Miao   | FR-376196          | add new nv/rfnv            */
/* 12/12/4 | Guobing.Miao   | FR-346355          | change rpc to qmi interface       */
/* 12/09/21| He.Zhang       | FR-334604          | Adjust to fit new role     */
/* 12/09/21| Guobing.Miao   | FR-334604          | Create for nv backup       */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_NV_BK_H__
#define __TCT_NV_BK_H__
//[BUGFIX]-Add-BEGIN by Miao, 2012/11/30
//FR-346355, change rpc to qmi interface
//#include "nv.h"
//#include "oncrpc.h"
//nv read qmi command id

#define QMI_DMS_VENDOR_TCT_NV_READ_REQ_V01 0x5556

typedef struct
{
    uint8_t sys_id;
    uint8_t sub_sys_id;
    uint16_t sub_cmd;
} __attribute__((packed)) req_type_tct_backup;

typedef struct
{
    uint8_t sys_id;
    uint8_t sub_sys_id;
    uint16_t sub_cmd;
    uint8_t result;
} __attribute__((packed)) rsp_type_tct_backup;

PACK(void *) backup_execute_func(PACK(void *)req_pkt, uint16 pkt_len);
PACK(void *) tunning_backup_check_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_NV_BK_H__ */
