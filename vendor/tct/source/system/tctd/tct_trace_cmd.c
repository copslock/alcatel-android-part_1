/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :                                                                    */
/* Reference documents :                                                      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_trace_cmd.c                */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 04/12/12| Pan Qianbo     | PR-315079          | Trace read write command   */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_trace_cmd.h"

#include "trace_partition.h"

#ifdef TARGET_BUILD_TCT_DIAG_STANDALONE
#define TRACEABILITY_BUFFER_SIZE           2048

#define FAIL_BASE   (-99)
#define FAIL_MMC_READ   (FAIL_BASE + 0)
#define FAIL_NO_MAGIC   (FAIL_BASE + 1)
#define FAIL_CHECKSUM   (FAIL_BASE + 2)

static unsigned char trace_region_image[TRACEABILITY_BUFFER_SIZE];
#else
#include "TraceabilityManager.h"
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */

static const diagpkt_user_table_entry_type tct_trace_cmd_tbl[] =
{
    {TCT_TRACE_READ_CMD, TCT_TRACE_READ_CMD, tct_read_trace_func},
    {TCT_TRACE_WRITE_CMD, TCT_TRACE_WRITE_CMD, tct_write_trace_func},
};

#ifdef TARGET_BUILD_TCT_DIAG_STANDALONE
static int read_trace_parti(void)
{
    int status = 0;
    rtrf_header *p_header = NULL;
    int fd = 0;
    char buf[TRACEABILITY_BUFFER_SIZE];
    //the buf contains header and region.
    tracability_region_struct_t *p_re_mmc =
        (tracability_region_struct_t *)( buf + sizeof(rtrf_header));

    //read patti
    if( (fd = open(MMCBLKP, O_RDONLY)) < 0 ) {
        DIAG_LOGE("open %s failed\n", MMCBLKP);
        return FAIL_MMC_READ;
    }

    memset(buf, 0, sizeof(buf));

    if( (status = read(fd, buf, TRACABILITY_TOTAL_SIZE)) < 0){
        DIAG_LOGE("read %s failed : %d\n", MMCBLKP, errno);
        close(fd);
        return FAIL_MMC_READ;
    }
    close(fd);
    /* check the region is valid */
    /*p_header = (rtrf_header *)buf;

    if( p_header->magic1 != RETROFIT_MAGIC1 ||
            p_header->magic2 != RETROFIT_MAGIC2 ){
        //there is no tracability or it is invalid
        //return FAIL_NO_MAGIC;
        //now, Magic1/2 is not critical.
        DIAG_LOGE("check trace-patition header failed.\n");
    }
    if(strncmp((char *)p_re_mmc->name, "TRAC", 4)){
        return FAIL_NO_MAGIC;
    }*/

    memcpy(&trace_region_image, buf, sizeof(trace_region_image));

    return 0;
}

static int write_trace_partition(char* data, size_t len,
        const char* partition) {
    DIAG_LOGE("write_trace_partition Begin \n");

    // Sanity check
    if (data == NULL) {
        DIAG_LOGE("invalid pData!");
        return -1;
    }

    if (partition == NULL) {
        DIAG_LOGE("bad partition target name \"%s\"\n", partition);
        return -1;
    }
    // Check if we're trying to read more than the partition size
    if (TRACEABILITY_BUFFER_SIZE < len) {
        DIAG_LOGE("Trying to read more than the partition size");
        return -1;
    }

    FILE* f = fopen(partition, "wb");
    if (f == NULL) {
        DIAG_LOGE("failed to open emmc partition \"%s\": %s\n", partition,
                strerror(errno));
        return -1;
    }

    if (fwrite(data, 1, len, f) != len) {
        DIAG_LOGE("short write writing to %s (%s)\n", partition, strerror(errno));
        return -1;
    }

    if (fclose(f) != 0) {
        DIAG_LOGE("error closing %s (%s)\n", partition, strerror(errno));
        return -1;
    }
    DIAG_LOGE("write_trace_partition End \n");
    return 0;

}

PACK(void *) tct_read_trace_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_read_trace_func\n");

    req_type_read_cmd_exec *req = (req_type_read_cmd_exec *) req_pkt;
    rsp_type_read_cmd_exec *rsp = NULL;
    int addr = (req->addrW << 8) + req->addrL;
    int read_state = -1;

    DIAG_LOGE("addr = %d\n", addr);
    DIAG_LOGE("len = %d\n", req->len);

    if (req->len >0 && ((addr+req->len) <= TRACEABILITY_MMITEST_SIZE) ) {
        read_state = read_trace_parti();
    }

    if (read_state < 0) {
        DIAG_LOGE("read_trace_parti failed \n");
        rsp = (rsp_type_read_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_TRACE_READ_CMD,
            sizeof(rsp_type_read_cmd_exec));
    } else {
        rsp = (rsp_type_read_cmd_exec *)diagpkt_subsys_alloc_v2(
                DIAG_SUBSYS_ID_TCT_FT, TCT_TRACE_READ_CMD,
                sizeof(rsp_type_read_cmd_exec)+req->len-1);
    }

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_read_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_read_cmd_exec));

    if (read_state < 0) {
        return (rsp);
    }

    memset(rsp->result, 0, req->len);
    memcpy(&rsp->result, &trace_region_image[TRACEABILITY_MMI_OFFSET + addr], req->len);

    return (rsp);
}

PACK(void *) tct_write_trace_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_write_trace_func\n");

    req_type_write_cmd_exec *req = (req_type_write_cmd_exec *) req_pkt;
    rsp_type_write_cmd_exec *rsp = NULL;
    char *text = NULL;
    int addr = (req->addrW << 8) + req->addrL;
    int result = -1;
    int text_len = pkt_len - sizeof(req_type_write_cmd_exec) + 1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_write_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_TRACE_WRITE_CMD,
            sizeof(rsp_type_write_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_write_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_write_cmd_exec) - 1);

    DIAG_LOGE("addr = %d\n", addr);
    DIAG_LOGE("text_len = %d\n", text_len);

    if (text_len != req->len ) {
        DIAG_LOGE("text_len != req->len \n");
        return (rsp);
    }

    if (req->len ==0 || ((addr+text_len) > TRACEABILITY_MMITEST_SIZE )) {
        DIAG_LOGE("len or add is not allow \n");
        return (rsp);
    }

    if (read_trace_parti() < 0) {
        DIAG_LOGE("read_trace_parti failed \n");
        return (rsp);
    }

    text = (char *)malloc(req->len);

    if (text != NULL) {
        memset(text, 0, req->len);
        memcpy(text, req->text, req->len);
        DIAG_LOGE("text = %s\n", text);

        memcpy(&trace_region_image[TRACEABILITY_MMI_OFFSET + addr], text, req->len);

        result = write_trace_partition((char*) &trace_region_image,
            sizeof(trace_region_image), MMCBLKP);
    } else {
        DIAG_LOGE("malloc command failed\n");
    }
    if (result == 0) {
        DIAG_LOGE("Success to write trace data.");
        rsp->result = 1;
    }
    free(text);
    return (rsp);
}
#else
PACK(void *) tct_read_trace_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_read_trace_func\n");

    req_type_read_cmd_exec *req = (req_type_read_cmd_exec *) req_pkt;
    rsp_type_read_cmd_exec *rsp = NULL;
    int addr = (req->addrW << 8) + req->addrL;

    DIAG_LOGE("addr = %d\n", addr);
    DIAG_LOGE("len = %d\n", req->len);

    if (req->len == 0 || addr + req->len > TRACEABILITY_MMITEST_SIZE) {
        return NULL;
    }

    rsp = (rsp_type_read_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_TRACE_READ_CMD,
            sizeof(rsp_type_read_cmd_exec) + req->len - 1);

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_read_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_read_cmd_exec));

    trace_readData(addr, &rsp->result[0], req->len);

    return (rsp);
}

PACK(void *) tct_write_trace_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_write_trace_func\n");

    req_type_write_cmd_exec *req = (req_type_write_cmd_exec *) req_pkt;
    rsp_type_write_cmd_exec *rsp = NULL;
    int addr = (req->addrW << 8) + req->addrL;
    int text_len = pkt_len - sizeof(req_type_write_cmd_exec) + 1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_write_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_TRACE_WRITE_CMD,
            sizeof(rsp_type_write_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_write_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_write_cmd_exec) - 1);

    DIAG_LOGE("addr = %d\n", addr);
    DIAG_LOGE("text_len = %d\n", text_len);

    if (text_len != req->len) {
        DIAG_LOGE("text_len != req->len \n");
        return (rsp);
    }

    if (req->len == 0 || addr + text_len > TRACEABILITY_MMITEST_SIZE) {
        DIAG_LOGE("len or addr is not allow \n");
        return (rsp);
    }

    if (!trace_writeData(addr, (char *) req->text, req->len)) {
        DIAG_LOGE("Success to write trace data.");
        rsp->result = 1;
    }

    return (rsp);
}
#endif /* TARGET_BUILD_TCT_DIAG_STANDALONE */

void tct_trace_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_trace_cmd_tbl);
}

