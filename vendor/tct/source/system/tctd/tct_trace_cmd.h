/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :                                                                    */
/* Reference documents :                                                      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_trace_cmd.h                */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 04/12/12| Pan Qianbo     | PR-315079          | Trace read write command   */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_TRACE_CMD_H__
#define __TCT_TRACE_CMD_H__

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 addrW;
    uint8 addrL;
    uint8 len;
} __attribute__((packed)) req_type_read_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 addrW;
    uint8 addrL;
    uint8 len;
    uint8 result[1];
} __attribute__((packed)) rsp_type_read_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 addrW;
    uint8 addrL;
    uint8 len;
    uint8 text[1];
} __attribute__((packed)) req_type_write_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 addrW;
    uint8 addrL;
    uint8 len;
    uint8 result;
} __attribute__((packed)) rsp_type_write_cmd_exec;

PACK(void *) tct_write_trace_func(PACK(void *)req_pkt, uint16 pkt_len);
PACK(void *) tct_read_trace_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_CHARGE_CMD_H__ */
