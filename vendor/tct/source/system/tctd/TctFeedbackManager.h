/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _TCTFEEDBACKMANAGER_H
#define _TCTFEEDBACKMANAGER_H

#ifdef __cplusplus
#include <sysutils/SocketListener.h>

class TctFeedbackManager {
private:
    static TctFeedbackManager *sInstance;

private:
    SocketListener        *mBroadcaster;

public:
    virtual ~TctFeedbackManager();

    int start();
    int stop();

    void setBroadcaster(SocketListener *sl) { mBroadcaster = sl; }
    SocketListener *getBroadcaster() { return mBroadcaster; }

    static TctFeedbackManager *Instance();
    int dumpPmic(SocketClient *cli);

private:
    TctFeedbackManager();

};
#endif /* __cplusplus */

#endif
