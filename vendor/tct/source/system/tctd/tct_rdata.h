/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:07/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 14/7/23 | xiaoyun.wei    | FR-743191          | diag provision              */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_SECURE_CMD_H__
#define __TCT_SECURE_CMD_H__

#define TCT_RDATA_CMD 0x0000

#define BUFLEN (1024 * 4)

typedef enum
{
    DCPKEY=0,
    CRYPTOKEY=1,
    XFILE=2
}FileType;

typedef struct
{
    FileType ft;
    uint32 len;
}__attribute__((packed)) FileInfo;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint16 sub_cmd1;
    uint16 size;
}__attribute__((packed)) req_type_tct_rdata_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
//    uint16 sub_cmd1;
    uint32 result;
}__attribute__((packed)) rsp_type_tct_rdata_cmd_exec;

typedef struct
{
  uint8 sys_id;
  uint8 sub_sys_id;
  uint16 sub_cmd;
  uint32 result;
  uint32 v1;
  uint32 v2;
}__attribute__((packed)) ss_rsp;

PACK(void *) tct_rdata_cmd_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_SECURE_CMD_H__ */
