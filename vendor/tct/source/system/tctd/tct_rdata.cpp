/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:07/2014 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 14/7/23 | xiaoyun.wei    | FR-743191          | diag provision              */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
//#include "diagdiag.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_rdata.h"

#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <math.h>
#include <poll.h>
//#include <linux/cdev.h>
#include <linux/ioctl.h>
#include "QSEEComAPI.h"

#include <binder/IServiceManager.h>
#include <binder/IBinder.h>
#include <binder/Parcel.h>
#include <binder/ProcessState.h>
#include <binder/IPCThreadState.h>
#include <private/binder/binder_module.h>
#include "cutils/log.h"

using namespace android;

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "hdcpprov"

//bool exit = 0;


#define ALISVR "tcl.alipay"
#define FTYSVR "tcl.fty.sec"

class MySetupCallback : public BBinder
{
public:
  MySetupCallback()
  {
    mydescriptor = String16("android.os.ISetupCallback");
  }
  virtual ~MySetupCallback() {}

  virtual const String16& getInterfaceDescriptor() const
  {
    return mydescriptor;
  }

protected:
  virtual status_t onTransact( uint32_t code,
    const Parcel& data,
    Parcel* reply,
    uint32_t flags = 0)
  {
    ALOGD("enter MySetupCallback onTransact, code=%u", code);
    if (data.checkInterface(this))
      ALOGD("checkInterface OK");
    else
    {
      ALOGW("checkInterface failed");
      return -1;
    }
    switch (code)
    {
    default:
      return BBinder::onTransact(code, data, reply, flags);
      break;
    }
    return 0;
  }

private:
  String16 mydescriptor;
};


int writefile(uint8 *buf, int len, char *str)
{
    int ret = 0;
    FILE *fp = NULL;
    fp = fopen(str, "wb");

    if(!fp)
    {
        DIAG_LOGE("open %s error", str);
        return -3;
    }
    ret =  fwrite(buf, 1, len, fp);
    fclose(fp);

    return ret;
}

static uint8 * dcpdata=NULL;
static int dcplen=0;
static uint8 *keydata=NULL;
static int keylen = 0;


void init()
{
        dcpdata = NULL;
        dcplen = 0;
        keydata = NULL;
        keylen = 0;
}

int provisionRpmb()
{
    int ret = 0;
    struct qseecom_rpmb_provision_key send_buf = {0};
    send_buf.key_type = 0;
    ret = QSEECom_send_service_cmd((void*) &send_buf, sizeof(send_buf), NULL, 0, QSEECOM_RPMB_PROVISION_KEY_COMMAND);
    if(ret != 0)
    {
        send_buf.key_type = 1;
        ret = QSEECom_send_service_cmd((void*) &send_buf, sizeof(send_buf), NULL, 0, QSEECOM_RPMB_PROVISION_KEY_COMMAND);
    }
    return ret;
}


int provisionHDCPKEY(uint8 *buf, int len, FileType ft)
{


    int ret;
    if(buf == NULL || len == 0 || ft < DCPKEY || ft > CRYPTOKEY)
        return -11;

    if(ft == DCPKEY)
    {
        dcpdata = buf;
        dcplen = len;
    }
    else if(ft == CRYPTOKEY)
    {
        keydata = buf;
        keylen = len;
    }

    if(dcpdata && dcplen && keydata && keylen)
    {
        //system("/system/bin/hdcpService &");
        sp<IServiceManager> sm = defaultServiceManager();
        sp<IBinder> b = sm->getService(String16(FTYSVR));
        if (b == NULL)
        {
          sleep(1);
          b = sm->getService(String16(FTYSVR));
          if (b == NULL)
          {
            ALOGE("Can't find binder service %s", FTYSVR);
            return -12;
          }
        }
        Parcel in1,out1;
        in1.writeInt32(dcplen);
        in1.write(dcpdata, dcplen);
        in1.write(keydata, 16);
        ret = b->transact(1, in1, &out1, 0);
        if(!ret)
        {
          ret = out1.readInt32();
          ALOGE("transact exec return %d", ret);
          b->transact(2, in1, &out1, 0);
          if(ret)
            return ret;
        }
        else
        {
          ALOGE("transact return %d", ret);
          return -13;
        }

	sync();
        dcpdata = NULL;
        dcplen = 0;
        keydata = NULL;
        keylen = 0;
    }

    return 0;
}

int processPkt(req_type_tct_rdata_cmd_exec * req, rsp_type_tct_rdata_cmd_exec *rsp)
{
    uint8 *buf = (uint8 *) req;
    buf= buf + sizeof(req_type_tct_rdata_cmd_exec);
    FileInfo *fi;
    int ret = 0;

    int i;
    uint8 *data=NULL;
    uint32 len = 0;

    for(i = 0; i<2; i++)
    {
        fi = (FileInfo *)buf;
        data =buf +  sizeof(FileInfo);
        len = fi->len;
        if((ret = provisionHDCPKEY(data, len, fi->ft)) != 0)
        {
            return ret;
        }
        buf = data+ len;
    }
    if(i > 0)
        return i;
    else
        return -2;
}


static const diagpkt_user_table_entry_type tct_rdata_cmd_tbl[] =
{
    {DIAG_SUBSYS_ID_TCT_RDATA, DIAG_SUBSYS_ID_TCT_RDATA, tct_rdata_cmd_func},
};

int ss=0;

int processSecState(PACK(void *)req, PACK(void **)rsp_pkt)
{
  ss_rsp *rsp = (ss_rsp *)diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, DIAG_SUBSYS_ID_TCT_RDATA,
            sizeof(ss_rsp)+4 );

  if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return -1;
  }
  *rsp_pkt=rsp;
  rsp->result=0;
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> b = sm->getService(String16(ALISVR));
  if (b == NULL)
  {
    DIAG_LOGE("Get bind service %s failed", ALISVR);
    return -1;
  }
  Parcel in1,out1;
  b->transact(9, in1, &out1, 0);
  if( b->transact(9, in1, &out1, 0) == 0 && out1.readInt32() == 0)
  {
    rsp->result=2;
    out1.read(&(rsp->v1),  12);

    DIAG_LOGE("ss=0x%x, 0x%x, 0x%x", rsp->v1, rsp->v2, *((int *)(rsp+1)));
    if(0x0 == (rsp->v1 & 0x3f)) // && rsp->v2 == 0xf)
    {
      DIAG_LOGE("Secure boot enabled");
      ss=1;
    }
    else
    {
      DIAG_LOGE("Secure boot not enabled");
      ss=0;
    }
  }
  else
  {
    DIAG_LOGE("getSecureStatus failed");
    return -1;
  }

  return 0;
}

int processHDCP(PACK(void *)req_pkt, PACK(void **)rsp_pkt)
{
  req_type_tct_rdata_cmd_exec *req = (req_type_tct_rdata_cmd_exec *)req_pkt;
  rsp_type_tct_rdata_cmd_exec *rsp = (rsp_type_tct_rdata_cmd_exec *)diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, DIAG_SUBSYS_ID_TCT_RDATA,
            sizeof(rsp_type_tct_rdata_cmd_exec)+8 );

  if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return -1;
  }
  *rsp_pkt=rsp;
  rsp->result=0;
  if(!ss)
  {
    rsp->result=1;
    return 0;
  }
  init();
  rsp->result = processPkt(req, rsp);
  return 0;
}

int processAli(PACK(void *)req_pkt, PACK(void **)rsp_pkt)
{
  req_type_tct_rdata_cmd_exec *req = (req_type_tct_rdata_cmd_exec *)req_pkt;
  rsp_type_tct_rdata_cmd_exec *rsp = (rsp_type_tct_rdata_cmd_exec *)diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, DIAG_SUBSYS_ID_TCT_RDATA,
            sizeof(rsp_type_tct_rdata_cmd_exec)+8 );

  if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return -1;
  }
  *rsp_pkt=rsp;
  rsp->result=0;
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> b = sm->getService(String16(ALISVR));
  if (b == NULL)
  {
    DIAG_LOGE("Get bind service %s failed", ALISVR);
    return -1;
  }
  Parcel in1,out1;
  in1.writeInt32(req->size - sizeof(req_type_tct_rdata_cmd_exec));
  in1.write(req+1, req->size - sizeof(req_type_tct_rdata_cmd_exec));
  if(b->transact(6, in1, &out1, 0) == 0 && out1.readInt32() == 0 )
  {
    DIAG_LOGE("write ali data ok");
    if(b->transact(7, in1, &out1, 0) == 0 && out1.readInt32() == 0 )
    {
      DIAG_LOGE("ali data auth ok");
      rsp->result=2;
    }
    else
      DIAG_LOGE("ali data auth ok");
  }
  else
  {
    DIAG_LOGE("write ali data failed");
    return -1;
  }

  return 0;
}

int processWC(PACK(void *)req_pkt, PACK(void **)rsp_pkt)
{
  req_type_tct_rdata_cmd_exec *req = (req_type_tct_rdata_cmd_exec *)req_pkt;
  rsp_type_tct_rdata_cmd_exec *rsp = (rsp_type_tct_rdata_cmd_exec *)diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, DIAG_SUBSYS_ID_TCT_RDATA,
            sizeof(rsp_type_tct_rdata_cmd_exec)+1024);

  if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return -1;
  }
  *rsp_pkt=rsp;
  rsp->result=0;
  sp<IServiceManager> sm = defaultServiceManager();
  sp<IBinder> b = sm->getService(String16(FTYSVR));
  if (b == NULL)
  {
    DIAG_LOGE("Get bind service %s failed", FTYSVR);
    return -1;
  }
  Parcel in1,out1;
  int len=0;
  char *buf = (char *)(rsp+1);
  LOGD("cmd=%d", *((int *)(req+1)));
  *((int *)buf)=*((int *)(req+1));
  *(((int *)buf)+1)=0;
  in1.writeInt32(8);
  in1.write(buf,8);
  if(b->transact(2, in1, &out1, 0) == 0 && out1.readInt32() == 0 )
  {
    len = out1.readInt32();
    out1.read(buf, len);
    LOGD("read wc ok len=%d", len);
    rsp->result=2;
  }
/*
  if(!ss)
    rsp->result=1;
*/
  return 0;
}

/*
const int dsLen=4;
int (*process[dsLen])(PACK(void *)req, PACK(void **)rsp);
process[0] = processSecState;
process[1] = processHDCP;
process[2] = processAli;
process[3] = processWC;
*/


PACK(void *) tct_rdata_cmd_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
  req_type_tct_rdata_cmd_exec * req = (req_type_tct_rdata_cmd_exec *)req_pkt;
  PACK(rsp_type_tct_rdata_cmd_exec *)rsp = NULL;

  DIAG_LOGE("tct_rdata_cmd_func sub_cmd1=0x%x\n", req->sub_cmd1);


/*
  if(req->sub_cmd1 > 0 && req->sub_cmd1 < (dsLen+1))
    (*process[req->sub_cmd1-1])(req, (void **)&rsp);
  else
    DIAG_LOGE("Not support sub_cmd1=0x%x", req->sub_cmd1);processSecState(req, (void **)&rsp);
//*/
  switch(req->sub_cmd1)
  {
  case 1:
  {
    processSecState(req, (void **)&rsp);
    break;
  }
  case 2:
  {
    processHDCP(req, (void **)&rsp);
    break;
  }
  case 3:
  {
    processAli(req, (void **)&rsp);
    break;
  }
  case 4:
  {
    processWC(req, (void **)&rsp);
    break;
  }
  default:
    DIAG_LOGE("Not support sub_cmd1=0x%x", req->sub_cmd1);
    break;
  }


  return (rsp);
}

void tct_rdata_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_rdata_cmd_tbl);
}

