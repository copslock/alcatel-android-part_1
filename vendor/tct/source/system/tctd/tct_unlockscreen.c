/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_unlockscreen.c             */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/02/18| Qianbo.Pan     | FR-392908          | Screen Unlock Tool         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/


#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_unlockscreen.h"
#include "tct_handler.h"

static int tct_service_initialized = 0;

static const diagpkt_user_table_entry_type tct_unlockscreen_tbl[] =
{
    {TCT_UNLOCKSCREEN, TCT_UNLOCKSCREEN, tct_unlockscreen_func},
};

static const diagpkt_user_table_entry_type tct_unlockscreen_v2_tbl[] =
{
    {TCT_UNLOCKSCREEN1, TCT_UNLOCKSCREEN1, tct_unlockscreen_v2_func},
    {TCT_UNLOCKSCREEN2, TCT_UNLOCKSCREEN2, tct_unlockscreen_v2_func},
    {TCT_UNLOCKSCREEN3, TCT_UNLOCKSCREEN3, tct_unlockscreen_v2_func},
    {TCT_UNLOCKSCREEN3, TCT_UNLOCKSCREEN4, tct_unlockscreen_v2_func},
};

PACK(void *) tct_unlockscreen_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_unlockscreen_func \n");

    req_type_tct_unlockscreen_exec *req = (req_type_tct_unlockscreen_exec *) req_pkt;
    rsp_type_tct_unlockscreen_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_unlockscreen_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_AS, TCT_UNLOCKSCREEN,
            sizeof(rsp_type_tct_unlockscreen_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_unlockscreen_exec));
    memcpy(rsp, req, sizeof(req_type_tct_unlockscreen_exec));
    rsp->result = 0;

    if (tct_service_initialized == 0) {
        initializePowerManagerService();
        tct_service_initialized = 1;
    }
    rsp->result = TCT_DIAG_setLockScreenMode();

    return (rsp);
}

PACK(void *) tct_unlockscreen_v2_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    DIAG_LOGE("tct_unlockscreen_v2_func \n");

    req_type_tct_unlockscreen_exec *req = (req_type_tct_unlockscreen_exec *) req_pkt;
    rsp_type_tct_unlockscreen_exec *rsp = NULL;
    int ret = -1;

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_unlockscreen_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_AS, TCT_UNLOCKSCREEN,
            sizeof(rsp_type_tct_unlockscreen_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_unlockscreen_exec));
    memcpy(rsp, req, sizeof(req_type_tct_unlockscreen_exec));
    rsp->result = 0;

    int mode = (req->sub_cmd >> 8)-1;
    rsp->result = TCT_DIAG_setLockScreenModeV2(mode);

    return (rsp);
}

void tct_unlockscreen_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_AS, tct_unlockscreen_tbl);
}

void tct_unlockscreen_v2_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_AS, tct_unlockscreen_v2_tbl);
}

