/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:12/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  peng.xiong                                                        */
/* E-Mail:  peng.xiong@tcl-mobile.com                                         */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : development/trace_proxy/trace_nv_read.h                            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/06/11| Qianbo Pan     | PR-467223          | Read nv                    */
/******************************************************************************/

#ifndef __TCT_NV_READ_H__
#define __TCT_NV_READ_H__

#include "nv_items.h"

#ifdef __cplusplus
extern "C" {
#endif


#define QMI_DMS_VENDOR_TCT_NV_READ_REQ_V01 0x5556
#define T_DATA_NV_SIZE 128

static uint32 FTM = NV_FTM_MODE_I;

static uint32 nv_items_tct_bk[] = {
      NV_FM_RSSI_I
};

static int32 nv_items_enum_size[] = {
    1,      /*nv_item 95*/
};


/* TODO, please add interface of diag command registers here */
extern int tct_nv_read_register(uint32_t item_id, char *nv_result, uint32_t nv_result_size);

#ifdef __cplusplus
}
#endif

#endif /* __TCT_NV_READ_H__ */
