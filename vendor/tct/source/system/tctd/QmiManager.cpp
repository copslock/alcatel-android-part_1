/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "QmiManager"

#include <cutils/log.h>

#include "qmi_tct_api_v01.h"
#include "QmiManager.h"

QmiManager *QmiManager::sInstance = NULL;

QmiManager *QmiManager::Instance() {
    if (!sInstance)
        sInstance = new QmiManager();
    return sInstance;
}

QmiManager::QmiManager() {
}

QmiManager::~QmiManager() {
}

int QmiManager::start() {
    return 0;
}

int QmiManager::stop() {
    return 0;
}

int QmiManager::qmiClientInit(qmi_client_ind_cb ind_cb, void *ind_cb_data, qmi_client_type *user_handle) {
    return 0;
}

int QmiManager::qmiClientRelease(qmi_client_type *user_handle) {
    return 0;
}
