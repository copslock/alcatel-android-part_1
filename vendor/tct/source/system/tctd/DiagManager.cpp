/* Copyright (C) 2016 Tcl Corporation Limited */
#define LOG_TAG "DiagManager"
#include <cutils/log.h>

#include "DiagManager.h"
#include "tct_diag.h"
#include <time.h>

#include <string.h>
#include <strings.h>
#include <stdlib.h>

DiagManager *DiagManager::sInstance = NULL;

DiagManager *DiagManager::Instance() {
    if (!sInstance)
        sInstance = new DiagManager();
    return sInstance;
}

DiagManager::DiagManager() {
}

DiagManager::~DiagManager() {
}

int DiagManager::start() {
    if (tct_diag_init()) {
        SLOGE("tct_diag_init() failed.");
        return 1;
    }
    SLOGE("tct_diag_init succeeded.");

    register_all_commands();

    return 0;
}

int DiagManager::stop() {
    if (tct_diag_deinit()) {
        SLOGE("tct_diag_deinit() failed.");
        return 1;
    }
    SLOGE("tct_diag_deinit succeeded.");

    return 0;
}
