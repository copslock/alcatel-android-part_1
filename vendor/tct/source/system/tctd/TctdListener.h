/* Copyright (C) 2016 Tcl Corporation Limited */
#ifndef _TCTDSOCKETLISTENER_H
#define _TCTDSOCKETLISTENER_H

#include "SocketListener.h"
#include "TctdCommand.h"

class SocketClient;

class TctdListener : public SocketListener {
public:
    static const int CMD_ARGS_MAX = 26;
    static const int CMD_BUF_SIZE = 1024;

    /* 1 out of errorRate will be dropped */
    int errorRate;

private:
    int mCommandCount;
    bool mWithSeq;
    TctdCommandCollection *mCommands;

public:
    TctdListener(const char *socketName);
    TctdListener(const char *socketName, bool withSeq);
    TctdListener(int sock);
    virtual ~TctdListener() {}

protected:
    void registerCmd(TctdCommand *cmd);
    bool onDataAvailable(SocketClient *c);

private:
    static void dumpArgs(int argc, char **argv, int argObscure);
    bool dispatchCommandWithRawData(SocketClient *c, char *data, int size);
    void dispatchCommand(SocketClient *c, char *data);
    TctdCommand *lookupCommand(const char *command);

    void init(const char *socketName, bool withSeq);

    class TraceabilityCmd : public TctdCommand {
    public:
        TraceabilityCmd();
        virtual ~TraceabilityCmd() {}
        int runCommand(SocketClient *cli, int argc, char ** argv);
        int runCommandWithRawData(SocketClient *cli, char *data, int size);
    };

    class PrivilegeCmd : public TctdCommand {
    public:
        PrivilegeCmd();
        virtual ~PrivilegeCmd() {}
        int runCommand(SocketClient *cli, int argc, char ** argv);
        int runCommandWithRawData(SocketClient *cli, char *data, int size);
    };

    class TctFeedbackCmd : public TctdCommand {
    public:
        TctFeedbackCmd();
        virtual ~TctFeedbackCmd() {}
        int runCommand(SocketClient *cli, int argc, char ** argv);
        int runCommandWithRawData(SocketClient *cli, char *data, int size);
    };
};
#endif
