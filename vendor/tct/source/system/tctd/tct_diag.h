/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_diag.h                     */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/09/05| Linjian.Xiang  | FR-508414          | Screen Unlock Tool         */
/* 13/04/23| Xinjian.Fang   | CR-443080          | Add efuse check            */
/* 13/01/24| Qianbo.Pan     | PR-397767          | Audio loop command         */
/* 12/12/06| Qianbo.Pan     | PR-315079          | Trace read write command   */
/* 12/09/21| He.Zhang       | FR-334604          | Add ft_cmd & backup        */
/* 12/09/21| He.Zhang       | FR-334604          | Create for tct diag        */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_DIAG_H__
#define __TCT_DIAG_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Max sleeping time 0x7fffffff*/
#define MAX_SLEEP_TIME          0x7fffffff

/* Subsystem IDs defined in diagcmd.h reserved for OEM use. */

/* DIAG_SUBSYS_ID_TCT_FT for factory use. */
#define DIAG_SUBSYS_ID_TCT_FT               DIAG_SUBSYS_RESERVED_OEM_0 //0xFA
  /* Define command codes here, 2 bytes, litter endian. */
  #define TCT_BACKUP_BACKUP                 0x0200 //0x80 0xFA 0x00 0x02
  #define TCT_BACKUP_RESULT_CHECK           0x0300
  #define TCT_FT_CMD_EXEC                   0x0000
  #define TCT_FT_CMD_PICKUP                 0x0001
  #define TCT_FT_CMD_GETPROP                0x0002
  #define TCT_FT_CMD_PICKUP_V2              0x0011
  #define TCT_SHUTDOWN                      0x0072 //114
  #define TCT_DEBUG_ON                      0x0073 //115
  #define TCT_AUDIOLOOP_CMD                 0x0076 //118
  #define TCT_TRACE_READ_CMD                0x0077 //119
  #define TCT_TRACE_WRITE_CMD               0x0078 //120
  #define TCT_CHARGING_ON                   0x0079 //121
  #define TCT_EMMC_CMD                      0x007C //124
  #define DIAG_SUBSYS_ID_TCT_RDATA          0x007D //125

/* DIAG_SUBSYS_ID_TCT_AS for after-sales use. */
#define DIAG_SUBSYS_ID_TCT_AS               DIAG_SUBSYS_RESERVED_OEM_1 //0xFB
  /* Define command codes here, 2 bytes, litter endian. */
  #define TCT_UNLOCKSCREEN                  0x0075 //0x80 0xFB 0x75 0x00
  #define TCT_UNLOCKSCREEN1                 0x0175 //0x80 0xFB 0x75 0x01
  #define TCT_UNLOCKSCREEN2                 0x0275 //0x80 0xFB 0x75 0x02
  #define TCT_UNLOCKSCREEN3                 0x0375 //0x80 0xFB 0x75 0x03
  #define TCT_UNLOCKSCREEN4                 0x0475 //0x80 0xFB 0x75 0x04

/* TODO, please add interface of diag command registers here */
extern void tct_ft_cmd_register();
extern void tct_backup_register();
/*[BUGFIX]-Add-BEGIN by TCTNB.(Qianbo Pan), 2013/01/24, for PR397767 Audio loop command*/
extern void tct_audioloop_cmd_register();
/*[BUGFIX]-Add-END by TCTNB.(Qianbo Pan)*/

/*[BUGFIX]-Add-BEGIN by TCTNB.(Qianbo Pan), 2012/12/06, for PR315079, Trace read write command*/
extern void tct_trace_cmd_register();
/*[BUGFIX]-Add-END by TCTNB.(Qianbo Pan)*/
/*[FEATURE]-Add-BEGIN by TCTNB.Linjian.Xiang, 2013/09/05, for FR-508414 Screen Unlock Tool*/
extern void tct_unlockscreen_register();
/*[FEATURE]-Add-END by TCTNB.Linjian.Xiang*/
extern void tct_shutdown_register();    //yongliang.wang added for shutdown diag coammand

extern void tct_debug_on_register();
extern void tct_charging_on_register();

/*[FEATURE]-Add-BEGIN by TCTNB.(Zhengyang.ma), 2014/09/03, for PR780904 get emmc capacity*/
extern void tct_emmc_cmd_register();
/*[FEATURE]-Add-END by TCTNB.(Zhengyang.ma)*/

extern void tct_rdata_cmd_register();

int tct_diag_init();
int tct_diag_deinit();
void register_all_commands();

#ifdef __cplusplus
}
#endif

#endif /* __TCT_DIAG_H__ */


