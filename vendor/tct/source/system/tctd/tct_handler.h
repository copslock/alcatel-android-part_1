/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_handler.h                  */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/02/18| Qianbo.Pan     | FR-392908          | Screen Unlock Tool         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_HANDLER_H__
#define __TCT_HANDLER_H__

#ifdef __cplusplus
extern "C" {
#endif

int initializePowerManagerService();
int TCT_DIAG_setLockScreenMode();
int TCT_DIAG_setLockScreenModeV2(int mode);

#ifdef __cplusplus
}
#endif
#endif /* __TCT_HANDLER_H__ */
