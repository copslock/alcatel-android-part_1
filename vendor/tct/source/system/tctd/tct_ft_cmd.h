/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_ft_cmd.h                   */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/10/18| He.Zhang       | FR-334604          | Recovery old pickup func   */
/* 12/09/21| He.Zhang       | FR-334604          | Create for factory test    */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#ifndef __TCT_FT_CMD_H__
#define __TCT_FT_CMD_H__

#define TCT_FT_CMD_PACK_HEAD_SIZE             4
#define PICKUP_V2_PACK_HEAD_SIZE              6
#define TCT_FT_CMD_MAX_CONTENT_LEN         2048
#define TCT_FT_CMD_MAX_COMMAND_LEN         1024

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 command[1];
} __attribute__((packed)) req_type_tct_ft_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    int result;
} __attribute__((packed)) rsp_type_tct_ft_cmd_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 path[1];
} __attribute__((packed)) req_type_tct_ft_cmd_pickup;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 content[TCT_FT_CMD_MAX_CONTENT_LEN];
} __attribute__((packed)) rsp_type_tct_ft_cmd_pickup;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 key[1];
} __attribute__((packed)) req_type_tct_ft_cmd_getprop;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 value[TCT_FT_CMD_MAX_CONTENT_LEN];
} __attribute__((packed)) rsp_type_tct_ft_cmd_getprop;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint16 start_sector;
    uint8 path[1];
} __attribute__((packed)) req_type_tct_ft_cmd_pickup_v2;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint16 read_size;
    uint8 content[TCT_FT_CMD_MAX_CONTENT_LEN];
} __attribute__((packed)) rsp_type_tct_ft_cmd_pickup_v2;

PACK(void *) tct_ft_cmd_exec_func(PACK(void *)req_pkt, uint16 pkt_len);
PACK(void *) tct_ft_cmd_pickup_func(PACK(void *)req_pkt, uint16 pkt_len);
PACK(void *) tct_ft_cmd_getprop_func(PACK(void *)req_pkt, uint16 pkt_len);
PACK(void *) tct_ft_cmd_pickup_v2_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_FT_CMD_H__ */
