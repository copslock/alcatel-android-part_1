/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_shutdown.h             */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/02/18| Yongliang.wang | FR-483802          | Send command to shutdown   */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/


#ifndef __TCT_UNLOCKSCREEN_H__
#define __TCT_UNLOCKSCREEN_H__

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 delay;

} __attribute__((packed)) req_type_tct_shutdown_exec;

typedef struct
{
    uint8 sys_id;
    uint8 sub_sys_id;
    uint16 sub_cmd;
    uint8 result;

} __attribute__((packed)) rsp_type_tct_shutdown_exec;

void shutdown_thread();

PACK(void *) tct_shutdown_func(PACK(void *)req_pkt, uint16 pkt_len);

#endif /* __TCT_UNLOCKSCREEN_H__ */
