/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:                                                                    */
/* E-Mail:                                                                    */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_handler.cpp                */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 13/02/18| Qianbo.Pan     | FR-392908          | Screen Unlock Tool         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#define LOG_TAG "tct_handler"

#include <cutils/log.h>
#include <powermanager/IPowerManager.h>
#include <binder/BpBinder.h>
#include <binder/IBinder.h>
#include <binder/IServiceManager.h>
#include <stdio.h>
#include <stdlib.h>
#include <cutils/properties.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <private/android_filesystem_config.h>

//#include <linux/delay.h>
//#include <asm/delay.h>

#include "tct_handler.h"
#include "aes.h"

#define MAX_SERVICE_RETRIES 10
// Seconds to wait between retries to connect to android services
#define SERVICE_RETRY_DELAY 30

namespace android {

sp<IPowerManager> gPowerManager;  //Power Manager sevice object
pthread_mutex_t gPowerManagerMutex = PTHREAD_MUTEX_INITIALIZER;

class CkpdDeadRecipient: public virtual IBinder::DeathRecipient
{
    void binderDied(const wp<IBinder>& who);
};

sp<CkpdDeadRecipient> gDeadRecipient(new CkpdDeadRecipient);


extern "C" int initializePowerManagerService()
{
    ALOGI("\n ####### DiagApp_MultiThread: initializePowerManagerService() #######\n");
    sp<IServiceManager> sm = defaultServiceManager();

    pthread_mutex_lock(&gPowerManagerMutex);
    gPowerManager.clear();

    if (sm != NULL) {
        for (int retry = 0;
                retry < MAX_SERVICE_RETRIES && gPowerManager == NULL;
                retry ++) {

            sp<IBinder> binder = sm->getService(String16("power"));

            if (binder == NULL) {
                pthread_mutex_unlock(&gPowerManagerMutex);
                ALOGE("Failed to connect to PowerManager. Attempt: %d", retry);
                sleep(SERVICE_RETRY_DELAY);
                pthread_mutex_lock(&gPowerManagerMutex);
                continue;
            }
            binder->linkToDeath(gDeadRecipient);
            gPowerManager = interface_cast<IPowerManager>(binder);
        }
    } else {
        ALOGE("Could not get defaultServiceManager!\n");
    }

    pthread_mutex_unlock(&gPowerManagerMutex);

    if (gPowerManager == 0)
    {
        ALOGE("Could not get Power manager service\n");
        return -1;
    }
    ALOGI("initializePowerManagerService returns 0\n");

    return 0;
}

void CkpdDeadRecipient::binderDied(const wp<IBinder>& who)
{
    if (gPowerManager != 0 /*&& gPowerManager->asBinder() == who*/) {
        ALOGI("gPowerManager has died. Attempt to reconnect");
        if (initializePowerManagerService() != 0) {
            ALOGE("Unable to reconnect!");
            exit(-1);
        }
    } else {
        ALOGE("Unkown binder died: %p", who.get_refs());
    }
}

#define  UNLOCKSCREEN "/data/system/magickey/UnlockScreen"
static inline char hex2char(char n)
{
   if(n > 9)
      n += 'A' - 10;
   else
      n += '0';
   return n;
}

extern "C" char* genKey(int mode)
{
    char tmp[PROP_VALUE_MAX];
    char *key = (char *)malloc(AES_BLOCK_SIZE*4);
    if (!key) {
       ALOGE("TCT DIAG: in %s line %s: allocate memory failed.", __func__, __LINE__);
       return NULL;
    }

    // read serialno and add/cut to 8 bytes
    // then append "TAlcatel" to 16 bytes.
    property_get("ro.serialno", tmp, "");
    int i = strlen(tmp);
    for(; i < 8; ++i) {
       tmp[i] = '0';
    }
    tmp[8]='\0';
    strcat(tmp, "TAlcatel");

    // Encrypt serialno
    char in[AES_BLOCK_SIZE] = {0};
    char out[AES_BLOCK_SIZE << 1] = {0};
    memcpy(in, tmp, AES_BLOCK_SIZE);
    Encrypt((uint8 *)out, (uint8 *)in, AES_BLOCK_SIZE);

    // convert out to hex string
    for(i = 0; i < AES_BLOCK_SIZE; ++i) {
      key[i << 1] = hex2char(out[i] >> 4);
      key[(i << 1) + 1] = hex2char(out[i] & 0x0f);
    }

    // get timestamp and convert to 13 bytes,
    // then add 3 bytes mode on head
    // we assume time < 1e13 s
    // mode: 000 - timestamp work
    // mode: 001 - timestamp not work, super screen unlock once
    // mode: 002 - timestamp not work, super screen unlock forever
    time_t t;
    time(&t);
    for(i = AES_BLOCK_SIZE - 1; i > 2 ; --i) {
       in[i] = '0' + t % 10;
       t /= 10;
    }
    for(; i >= 0; --i) {
       in[i]= '0' + mode % 10;
       mode /= 10;
    }
    // Encrypt timestamp
    Encrypt((uint8 *)(out + AES_BLOCK_SIZE), (uint8 *)in, AES_BLOCK_SIZE);
    for(i = AES_BLOCK_SIZE; i < (AES_BLOCK_SIZE<<1); ++i) {
      key[i << 1] = hex2char(out[i] >> 4);
      key[(i << 1) + 1] = hex2char(out[i] & 0x0f);
    }
    return key;
}

extern "C" bool genUnlockScreen(int mode)
{
   char *key = genKey(mode);
   if (!key)
      return 0;

   // mkdir  /data/local/tmp/magickey/
   mode_t oldmask = umask(0000); // set umask to 0000
   char *p, *fn;
   p = fn = strdup(UNLOCKSCREEN);
   while (p = strchr(p + 1, '/')) {
      *p = '\0';
      if (!mkdir(fn, 0755) || errno == EEXIST) {
         chown(fn, AID_SYSTEM, AID_SYSTEM);
         *p = '/';
         continue;
      }
      ALOGE("TCT DIAG: in %s line %s: mkdir %s failed (%s)\n",
            __func__, __LINE__, fn, strerror(errno));
      return 0;
   }

   // create UnlockScreen
   int fd = creat(fn, 0644);
   if (fd < 0) {
      ALOGE("TCT DIAG: in %s line %s: create %s failed (%s)\n",
            __func__, __LINE__, fn, strerror(errno));
      return 0;
   }
   chown(fn, AID_SYSTEM, AID_SYSTEM);
   int sz = AES_BLOCK_SIZE*4;
   if(write(fd, key, sz) != sz) {
      ALOGE("TCT DIAG: in %s line %s: write %s failed (%s)\n",
            __func__, __LINE__, fn, strerror(errno));
      close(fd);
      return 0;
   }
   close(fd);
   property_set("sys.tct.screenunlock", "true");
   umask(oldmask);
   return 1;
}

extern "C" bool rmUnlockScreen(void)
{
   if (unlink(UNLOCKSCREEN)) {
      ALOGE("TCT DIAG: unlink %s failed(%s)\n", UNLOCKSCREEN, strerror(errno));
      return 0;
   }
   return 1;
}
extern "C" int TCT_DIAG_setLockScreenMode()
{
    ALOGI("TCT DIAG: TCT_DIAG_setLockScreenMode \n");

    bool returnVal = 0;
   //disable to let tctd make through
    pthread_mutex_lock(&gPowerManagerMutex);

    //returnVal = gPowerManager->tct_unLockScreen();

    pthread_mutex_unlock(&gPowerManagerMutex);

    return returnVal;
}

extern "C" int TCT_DIAG_setLockScreenModeV2(int mode)
{
    ALOGI("TCT DIAG: TCT_DIAG_setLockScreenMode \n");
    /* mode 0-2: generate UnlockScreen in  data/local/tmp/magickey/
     * mode 3  : remove UnlockScreen in  data/local/tmp/magickey/
     */
    switch(mode) {
    case 0:
    case 1:
    case 2:
       return genUnlockScreen(mode);
    case 3:
       return rmUnlockScreen();
    default :
       ALOGE("TCT DIAG: TCT_DIAG_setLockScreenMode invalid mode (%d)\n", mode);
       return 0;
    }
}



};  /* namespace android */
