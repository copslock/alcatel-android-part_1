/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  Shishun.Liu                                                         */
/* E-Mail:  Shishun.Liu@tcl.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tctd/tct_emmc_cmd.c            */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 09/03/14| Zhengyang.ma   | PR780904           | EMMC Capacity command      */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/
#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_emmc_cmd.h"

int get_emmc_capacity(){

   char major[8];
   char minor[8];
   char blocks[32];
   char names[16];
   char reference[] = "mmcblk0";
   int MAX = 1024;
   int res = 0;
   char line[MAX];
   FILE *f = fopen("/proc/partitions","r");
    while ( fgets(line,MAX,f) != NULL)
    {
        line[strlen(line)-1]='\0';
        sscanf(line, "%8s %8s %32s %16s\n", major, minor, blocks,names);
        if(!strcmp(names,reference)){
            //printf("%s\n",blocks);
            res = atoi(blocks)/1024/1024;
            break;
       }
    }

     fclose(f);
     if(res > 3 && res <= 4){
       res = 4;
     }else if(res >=6 && res <= 8){
       res = 8;
     }else if(res >=12 && res <= 16){
       res = 16;
     }else if(res >=25 && res <= 32){
       res = 32;
     }

     return res;
}

static const diagpkt_user_table_entry_type tct_emmc_cmd_tbl[] =
{
    {TCT_EMMC_CMD, TCT_EMMC_CMD, tct_emmc_cmd_func},
};

PACK(void *) tct_emmc_cmd_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{

    DIAG_LOGE("tct_emmc_cmd_func \n");

    req_type_tct_emmc_cmd_exec *req = (req_type_tct_emmc_cmd_exec *) req_pkt;
    rsp_type_tct_emmc_cmd_exec *rsp = NULL;
    int ret = -1;

    uint8 command = req->command;
    if (command != 1) {
       DIAG_LOGE("command not used\n");
       return NULL;
    }

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_emmc_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_EMMC_CMD,
            sizeof(rsp_type_tct_emmc_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_emmc_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_tct_emmc_cmd_exec));

    rsp->result = get_emmc_capacity();

    return (rsp);
}

void tct_emmc_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_emmc_cmd_tbl);
}

