/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tct_diag/tct_nv_bk.c                    */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 12/12/21| Guobing.Miao   | FR-376196          | not create nv when value is 0*/
/* 12/12/4 | Guobing.Miao   | FR-346355          | change rpc to qmi interface*/
/* 12/09/21| He.Zhang       | FR-334604          | Adjust to fit new role     */
/* 12/09/21| Guobing.Miao   | FR-334604          | Create for nv backup       */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include <cutils/log.h>


#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <string.h>

#include "diag_lsm.h"

#undef LOG_TAG
#define LOG_TAG "tct_nv_bk"

#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include <fcntl.h>

#include "qmi_client.h"
#include "qmi_idl_lib.h"
#include "qmi_cci_target_ext.h"
#include "qmi_tct_api_v01.h"

#include "tct_nv_bk.h"
#include "tct_efs_bk.h"

#define EFS_LIST_LOCAL_OVERRIDE "/data/local/tmp/efs_list.txt"
#define TEMPDIR "/cache/efs_item_files"
#define DEV_TAR "/cache/nv_efs.tar"         /* nv tar file location */
#define DEV_BK "/dev/block/bootdevice/by-name/tunning" /* backup partition */
#define QMI_ITEM_BUFF_SIZE QMI_RETROFIT_NV_ITEM_DATA_MAX_SIZE_V01

static const diagpkt_user_table_entry_type tct_backup_tbl[] =
{
  {TCT_BACKUP_BACKUP, TCT_BACKUP_BACKUP, backup_execute_func},
  {TCT_BACKUP_RESULT_CHECK, TCT_BACKUP_RESULT_CHECK, tunning_backup_check_func},
};

#if 0
/*===========================================================================

FUNCTION NV_READ_BK_ITEM

DESCRIPTION
  Read any integer type NV item

DEPENDENCIES
  None.

RETURN VALUE
  Nv real size.

SIDE EFFECTS
  None.

===========================================================================*/
static uint32_t nv_read_bk_item (qmi_client_type clnt, uint32_t item_id, uint8_t *result, uint32_t len, int variableSize)
{
    retrofit_nv_read_resp_msg_v01 resp;
    retrofit_nv_read_req_msg_v01 req;
    uint32_t ret = 0;

    memset(&resp, 0, sizeof(retrofit_nv_read_resp_msg_v01));
    req.item_id = item_id;
    ret = qmi_client_send_msg_sync(clnt, QMI_RETROFIT_NV_READ_REQ_V01, &req, sizeof(retrofit_nv_read_req_msg_v01), &resp, sizeof(retrofit_nv_read_resp_msg_v01), 0);
    if (ret != 0) {
        SLOGE("Error==========ret=%d item_id=%u\n", ret, item_id);
        return 0;
    }
    if (resp.result != 0){
        SLOGE("nv read WARNING=======return(%d) item_id=%u\n", resp.result, item_id);
        return 0;
    }

    memcpy(result, resp.nv_data, resp.nv_data_len);
    if (variableSize) {
        len = resp.nv_data_len;
    } else if (len != resp.nv_data_len) {
        SLOGE("nvim size define is not right=========  %u\n", item_id);
    }

    return len;
}



/*===========================================================================

FUNCTION EFS2TAR

DESCRIPTION
  This function tar all the nv files.

DEPENDENCIES
  None.

RETURN VALUE
  Result of running 'tar'.

SIDE EFFECTS
  None.

===========================================================================*/
static int efs2tar()
{
    char cmd[256];

    /* clean the tar created last time */
    sprintf(cmd, "rm %s", DEV_TAR);
    system(cmd);

    sprintf(cmd, "cd /cache/efs_item_files/ && tar -cf %s nvm/num/ nv/", DEV_TAR);

    return system(cmd);
}

/*===========================================================================

FUNCTION WRITE_BACKUP_PARTITION

DESCRIPTION
  1. clear the bakcup partition by write 0
  2. write the data to the backup parition

DEPENDENCIES
  None.

RETURN VALUE
  Result of running 'dd'.

SIDE EFFECTS
  None.

===========================================================================*/

static int write_backup_partition(void)
{
    char cmd[256];

    sprintf(cmd, "dd if=/dev/zero of=%s bs=1024 count=1536", DEV_BK);
    system(cmd);

    sprintf(cmd, "dd if=%s of=%s", DEV_TAR, DEV_BK);

    return system(cmd);
}

/*===========================================================================

FUNCTION NV_BK_CREATE

DESCRIPTION
  1. get the detailed nv infomation from nv_info_table
  2. create the nv file and get it from BP side

DEPENDENCIES
  None.

RETURN VALUE
  Resualt of creating

SIDE EFFECTS
  None.

===========================================================================*/
static int nv_bk_create(qmi_client_type clnt, uint32_t item_id, uint32_t len, int variableSize)
{
    uint32_t _len;
    char nv_efs_item[256];
    uint8_t buffer[QMI_ITEM_BUFF_SIZE];
    int file_efs;

    if (item_id == MCS_TCXOMGR_Field_Cal_Params) {
        sprintf(nv_efs_item, "/cache/efs_item_files/nv/item_files/mcs/tcxomgr/field_cal_params");
    } else if (item_id > 20000) {
        sprintf(nv_efs_item, "/cache/efs_item_files/nv/item_files/rfnv/%08d", item_id);
    } else if(item_id < 20000 && item_id > 0) {
        sprintf(nv_efs_item, "/cache/efs_item_files/nvm/num/%d", item_id);
    }

    memset(buffer, 0, sizeof(buffer));  /* clear the bufferr */
    /* read item from BP side */
    if ((_len = nv_read_bk_item(clnt, item_id, buffer, len, variableSize)) > 0) {
        /*no array files needed to backup*/
        /* creat item file */
        file_efs = open(nv_efs_item, O_WRONLY | O_CREAT, 0666);
        if (file_efs < 0) {
            SLOGE("Creat NV file failed %s errno=%d\n", nv_efs_item, errno);
            return 1;
        }
        if (write(file_efs, buffer, _len) <= 0) {
            SLOGE("Write NV  file   failed %d, errno=%u\n", item_id, errno);
            close(file_efs);
            return 1;
        }
        close(file_efs);
    }

    return 0;
}

/*===========================================================================

FUNCTION NV_EFS_GENERATE

DESCRIPTION
  1. loop the backup table for each item abs
  2. call each api

DEPENDENCIES
  None.

RETURN VALUE
  Resualt of nv_bk_create

SIDE EFFECTS
  None.

===========================================================================*/
static int nv_efs_generate()
{
    int rc = 0, service_connect = 0;
    uint32_t item_id;   /* need backup item */
    int i;
    qmi_cci_os_signal_type os_params;
    uint32_t num_services, num_entries=0;
    qmi_client_type clnt, notifier;
    qmi_service_info info[10];

//init qmi  =========================
 qmi_idl_service_object_type test_service_object = tct_get_service_object_v01();
  if (!test_service_object)
  {
    SLOGE("TCT_QMI: tct_get_service_object_internal_v01 failed.");
    return -1;
  }
rc=  qmi_client_notifier_init(test_service_object, &os_params, &notifier);
/* Check if the service is up, if not wait on a signal */
  while(1)
  {
    rc = qmi_client_get_service_list( test_service_object, NULL, NULL, &num_services);
    SLOGV("TCT_QMI: qmi_client_get_service_list() returned %d num_services = %d", rc, num_services);
    if(rc == QMI_NO_ERR)
      break;
    /* wait for server to come up */
    QMI_CCI_OS_SIGNAL_WAIT(&os_params, 0);
  }
  num_entries = num_services;
  /* The server has come up, store the information in info variable */
  rc = qmi_client_get_service_list( test_service_object, info, &num_entries, &num_services);
  SLOGI("TCT_QMI: qmi_client_get_service_list() returned %d num_entries = %d num_services = %d", rc, num_entries, num_services);

  if (service_connect >= num_services)
  {
    SLOGI("TCT_QMI: Only %d Test Services found: Choosing the default one)",num_services);
    service_connect = 0;
  }

  rc = qmi_client_init(&info[service_connect], test_service_object, NULL, NULL, NULL, &clnt);
  SLOGV("TCT_QMI : qmi_client_init returned %d", rc);
 //init qmi  end=========================

    system("rm -r /cache/efs_item_files");
    system("mkdir -p /cache/efs_item_files/nvm/num/");
    //create for rfnv
    system("mkdir -p /cache/efs_item_files/nv/item_files/rfnv/");
    system("mkdir -p /cache/efs_item_files/nv/item_files/mcs/tcxomgr/");

    /* loop for the nv item we need bk */
    for (i = 0; i < (sizeof(nv_items_tct_bk) / sizeof(nv_items_enum_type)); ++i) {
        item_id = (uint32_t) nv_items_tct_bk[i];
        rc = nv_bk_create(clnt, item_id, nvim_item_info_table[item_id].item_size ,0);
        if (rc) {
            SLOGE("aborted :  at the item  %u \n", item_id);
            return rc;
        }
    }
    for (i = 0; i < (sizeof(rfnv_item_tct_bk) / sizeof(rfnv_item_info_type)); ++i) {
        item_id = rfnv_item_tct_bk[i].item_id;
        rc = nv_bk_create(clnt, item_id, rfnv_item_tct_bk[i].item_size, 1);
        if (rc) {
            SLOGE("aborted :  at the item  %u \n", item_id);
            return rc;
        }
    }

   rc= qmi_client_release(clnt);
   rc= qmi_client_release(notifier);
   SLOGI("Qmi_client_release of notifier returned %d\n", rc);

    return 0;
}

#endif

static int efs_bk_create(qmi_client_type clnt, const char *tempdir, const char *efs_path)
{
    retrofit_nv_read_resp_msg_v01 resp;
    retrofit_nv_read_req_msg_v01 req;
    uint32_t ret;
    int fd;
    char cache_path[FS_PATH_MAX_V01];

    sprintf(&req.efs_path, "%s", efs_path);
    memset(&resp, 0, sizeof(retrofit_nv_read_resp_msg_v01));
    ret = qmi_client_send_msg_sync(clnt,
            QMI_RETROFIT_NV_READ_REQ_V01,
            &req,
            sizeof(retrofit_nv_read_req_msg_v01),
            &resp,
            sizeof(retrofit_nv_read_resp_msg_v01),
            0);
    if (ret != 0) {
        SLOGE("Error==========ret=%d efs_path=%s", ret, efs_path);
        return 1;
    }
    if (resp.result != 0){
        SLOGE("nv read WARNING=======return(%d) efs_path=%s\n", resp.result, efs_path);
        return 0;
    }

    sprintf(cache_path, "%s%s", tempdir, efs_path);
    fd = open(cache_path, O_WRONLY | O_CREAT, 0666);
    if (fd < 0) {
        SLOGE("Creat efs file failed %s errno=%d", cache_path, errno);
        return 1;
    }
    if (write(fd, resp.nv_data, resp.nv_data_len) != resp.nv_data_len) {
        SLOGE("Write efs file failed %s, errno=%d", cache_path, errno);
        close(fd);
        return 1;
    }
    close(fd);

    return 0;
}

//see system/core/toolbox/mkdir.c
static int mkdir_parents(const char *path)
{
    char currpath[FS_PATH_MAX_V01], *pathpiece;
    struct stat st;
    int ret;

    // reset path
    strcpy(currpath, "");
    // create the pieces of the path along the way
    pathpiece = strtok(path, "/");
    if (path[0] == '/') {
        // prepend / if needed
        strcat(currpath, "/");
    }
    while (pathpiece != NULL) {
        if (strlen(currpath) + strlen(pathpiece) + 2/*NUL and slash*/ > FS_PATH_MAX_V01) {
            SLOGE("Invalid path specified: too long");
            return 1;
        }
        strcat(currpath, pathpiece);
        strcat(currpath, "/");
        if (stat(currpath, &st) != 0) {
            ret = mkdir(currpath, 0777);
            if (ret < 0) {
                SLOGE("mkdir failed for %s, %s", currpath, strerror(errno));
                return ret;
            }
        }
        pathpiece = strtok(NULL, "/");
    }

    return 0;
}

//see frameworks/base/services/core/jni/com_android_server_PersistentDataBlockService.cpp
static uint64_t get_block_device_size(int fd)
{
    uint64_t size = 0;
    int ret;

    ret = ioctl(fd, BLKGETSIZE64, &size);

    if (ret)
        return 0;

    return size;
}

static int wipe_block_device(int fd)
{
    uint64_t range[2];
    int ret;
    uint64_t len = get_block_device_size(fd);

    range[0] = 0;
    range[1] = len;

    if (range[1] == 0)
        return 0;

    ret = ioctl(fd, BLKDISCARD, &range);
    if (ret < 0) {
        SLOGE("Discard failed: %s", strerror(errno));
        return -1;
    }

    return 0;
}

static int override_tunning(const char *tarpath)
{
    int fd, tarfd, rc = 0;
    char buf[512];
    size_t sz;

    //discard tunning
    fd = open(DEV_BK, O_WRONLY);
    if (fd < 0) {
        SLOGE("open %s failed: %s", DEV_BK, strerror(errno));
        return -1;
    }
    if (wipe_block_device(fd) != 0) {
        rc = -2;
        goto closefd;
    }

    //write tar
    tarfd = open(tarpath, O_RDONLY);
    if (tarfd < 0) {
        SLOGE("open %s failed: %s", tarpath, strerror(errno));
        rc = -3;
        goto closefd;
    }
    lseek(fd, 0, SEEK_SET);
    while ((sz = read(tarfd, buf, 512)) > 0) {
        if (write(fd, buf, sz) != sz) {
            SLOGE("write failed: %s", strerror(errno));
            rc = -4;
            break;
        }
    }
    if (tarfd > 0) {
        close(tarfd);
    }

closefd:
    if (fd > 0) {
        close(fd);
    }

    return rc;
}

int efs_backup(const char *tempdir, const char *tarpath, int override)
{
    int rc = 0, service_connect = 0;
    int i;
    qmi_cci_os_signal_type os_params;
    uint32_t num_services, num_entries=0;
    qmi_client_type clnt, notifier;
    qmi_service_info info[10];
    char temp[FS_PATH_MAX_V01];
    char temp2[FS_PATH_MAX_V01];
    FILE *fp;
    char *efs_path;
    char *find;
    uint32_t item_id;

//init qmi  =========================
 qmi_idl_service_object_type test_service_object = tct_get_service_object_v01();
  if (!test_service_object)
  {
    SLOGE("TCT_QMI: tct_get_service_object_internal_v01 failed.");
    return -1;
  }
rc=  qmi_client_notifier_init(test_service_object, &os_params, &notifier);
/* Check if the service is up, if not wait on a signal */
  while(1)
  {
    rc = qmi_client_get_service_list( test_service_object, NULL, NULL, &num_services);
    SLOGV("TCT_QMI: qmi_client_get_service_list() returned %d num_services = %d", rc, num_services);
    if(rc == QMI_NO_ERR)
      break;
    /* wait for server to come up */
    QMI_CCI_OS_SIGNAL_WAIT(&os_params, 0);
  }
  num_entries = num_services;
  /* The server has come up, store the information in info variable */
  rc = qmi_client_get_service_list( test_service_object, info, &num_entries, &num_services);
  SLOGI("TCT_QMI: qmi_client_get_service_list() returned %d num_entries = %d num_services = %d", rc, num_entries, num_services);

  if (service_connect >= num_services)
  {
    SLOGI("TCT_QMI: Only %d Test Services found: Choosing the default one)",num_services);
    service_connect = 0;
  }

  rc = qmi_client_init(&info[service_connect], test_service_object, NULL, NULL, NULL, &clnt);
  SLOGV("TCT_QMI : qmi_client_init returned %d", rc);
 //init qmi  end=========================

    //prepare tempdir
    if (NULL == tempdir) {
        tempdir = TEMPDIR;
        SLOGW("use default tempdir: %s", TEMPDIR);
    }
    sprintf(temp, "rm -r %s", tempdir);
    if (system(temp)) {
        SLOGW("rm %s failed: %s", tempdir, strerror(errno));
    }
    sprintf(temp, "%s", tempdir);
    if (mkdir_parents(temp)) {
        SLOGE("mkdir_parents %s failed: %s", temp, strerror(errno));
        rc = 1;
        goto release_clnt;
    }
    sprintf(temp, "%s/nvm/num", tempdir);
    if (mkdir_parents(temp)) {
        SLOGE("mkdir_parents %s failed: %s", temp, strerror(errno));
        rc = 1;
        goto release_clnt;
    }
    sprintf(temp, "%s/nv/item_files/rfnv", tempdir);
    if (mkdir_parents(temp)) {
        SLOGE("mkdir_parents %s failed: %s", temp, strerror(errno));
        rc = 1;
        goto release_clnt;
    }

    //access EFS_LIST_LOCAL_OVERRIDE
    fp = fopen(EFS_LIST_LOCAL_OVERRIDE, "r");
    if (NULL == fp) {
        SLOGE("open %s failed: %s", EFS_LIST_LOCAL_OVERRIDE, strerror(errno));

        //from builtin list
        //nv_items_tct_bk[]
        for (i = 0; i < (sizeof(nv_items_tct_bk) / sizeof(nv_items_enum_type)); ++i) {
            item_id = (uint32_t) nv_items_tct_bk[i];
            sprintf(temp, "/nvm/num/%u", item_id);
            rc = efs_bk_create(clnt, tempdir, temp);
            if (rc) {
                SLOGE("aborted: at the item %u", item_id);
                //return rc;
            }
        }
        //rfnv_item_tct_bk[]
        for (i = 0; i < (sizeof(rfnv_item_tct_bk) / sizeof(rfnv_item_info_type)); ++i) {
            item_id = rfnv_item_tct_bk[i].item_id;
            sprintf(temp, "/nv/item_files/rfnv/%08u", item_id);
            rc = efs_bk_create(clnt, tempdir, temp);
            if (rc) {
                SLOGE("aborted: at the item %u", item_id);
                //return rc;
            }
        }
        //efs_list[]
        for (i = 0; efs_list[i] != NULL; ++i) {
            sprintf(temp2, "%s%s", tempdir, efs_list[i]);
            find = strrchr(temp2, '/');
            if (find) {
                *find = 0;
            }
            SLOGD("%s", temp2);
            if (mkdir_parents(temp2)) {
                SLOGE("mkdir_parents %s failed: %s", temp2, strerror(errno));
            }
            rc = efs_bk_create(clnt, tempdir, efs_list[i]);
            if (rc) {
                SLOGE("aborted: at the item %s", efs_list[i]);
                //return rc;
            }
        }
    } else {
        //from EFS_LIST_LOCAL_OVERRIDE
        while (!feof(fp)) {
            fgets(temp, sizeof(temp), fp);
            find = strchr(temp, '\n');
            if (find) {
                *find = 0;
            }
            //SLOGD("%s", temp);
            if (temp[0] != '/') {
                continue;
            }
            sprintf(temp2, "%s%s", tempdir, temp);
            find = strrchr(temp2, '/');
            if (find) {
                *find = 0;
            }
            //SLOGD("%s", temp2);
            if (mkdir_parents(temp2)) {
                SLOGE("mkdir_parents %s failed: %s", temp2, strerror(errno));
            }

            if (efs_bk_create(clnt, tempdir, temp)) {
                SLOGE("aborted: at the item %s", temp);
            }
        }

        if (NULL != fp) {
            fclose(fp);
        }
    }

    //efs2tar
    if (NULL == tarpath) {
        tarpath = DEV_TAR;
        SLOGW("use default tarball file: %s", DEV_TAR);
    }
    if (unlink(tarpath)) {
        SLOGW("unlink %s failed: %s", tarpath, strerror(errno));
    }
    sprintf(temp, "cd %s && tar -cf %s *", tempdir, tarpath);
    if (system(temp)) {
        SLOGE("efs2tar failed: %s", temp);
        rc = 1;
        goto release_clnt;
    }

    //tarpath.txt
    sprintf(temp, "cd %s && find * -type f | sed -n 's:^:/:p' > %s.txt", tempdir, tarpath);
    if (system(temp)) {
        SLOGE("gen tarpath.txt failed: %s", temp);
    }

    //override tunning
    if (override == 1) {
        rc = override_tunning(tarpath);
        if (rc != 0) {
            SLOGE("override_tunning failed: %d", rc);
        }
    }

release_clnt:
   rc= qmi_client_release(clnt);
   rc= qmi_client_release(notifier);
   SLOGI("Qmi_client_release of notifier returned %d\n", rc);

    return rc;
}

/*===========================================================================

FUNCTION BACKUP_EXECUTE_FUNC

DESCRIPTION
  Main entry of tct diag backup command

DEPENDENCIES
  None.

RETURN VALUE
  success or not

SIDE EFFECTS
  Only a meaningful return value if the entry is valid which is determined
  by calling nv_op_is_valid_entry

===========================================================================*/

PACK(void *) backup_execute_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    rsp_type_tct_backup *rsp;

    rsp = (rsp_type_tct_backup *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_BACKUP_BACKUP,
            sizeof(rsp_type_tct_backup));

    if (rsp != NULL) {
        memset(rsp, 0, sizeof(rsp_type_tct_backup));
        memcpy(rsp, req_pkt, pkt_len);
    } else {
        SLOGE("Alloc rsp failed: %s", strerror(errno));
        return NULL;
    }

#if 0
    /* process command */
    if (!nv_efs_generate()) {
        if (!efs2tar()) {
            if (!write_backup_partition()) {
                sync();
                rsp->result = 1;
            } else {
                SLOGE("Write the tar files failed.");
            }
        } else {
            SLOGE("Tar the nv files failed.");
        }
    } else {
        SLOGE("Create nv files failed.");
    }
#endif
    if (0 == efs_backup(TEMPDIR, DEV_TAR, 1)) {
        rsp->result = 1;
    }

    return (rsp);
}

/*===========================================================================

FUNCTION TUNNING_BACKUP_CHECK_FUNC

DESCRIPTION
  Check if tunning have data

DEPENDENCIES
  None.

RETURN VALUE
  success or not

===========================================================================*/

PACK(void *) tunning_backup_check_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    rsp_type_tct_backup *rsp;
    char hdr[512] = {0};
    char NULL_zero[512];
    char NULL_FF[512];
    int file_bk = -1;


    rsp = (rsp_type_tct_backup *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_FT, TCT_BACKUP_RESULT_CHECK,
            sizeof(rsp_type_tct_backup));
    if (rsp != NULL) {
        memset(rsp, 0, sizeof(rsp_type_tct_backup));
        memcpy(rsp, req_pkt, pkt_len);
    } else {
        SLOGE("Alloc rsp failed: %s", strerror(errno));
        return NULL;
    }

    memset(NULL_zero, 0, 512);
    memset(NULL_FF, 0xFF, 512);

    /* OPEN BACKUP PARTITION AGAIN FOR READ */
    do {
        file_bk = open(DEV_BK, O_RDONLY);
        if (file_bk < 0) {
            SLOGE("Open backup partition failed");
            break;
        }
        if (read(file_bk, hdr, 512) < 0) {
            SLOGE("read study partition error %s", strerror(errno));
            break;
        }
        if ((0 != memcmp(hdr, NULL_zero, 512)) &&
            (0 != memcmp(hdr, NULL_FF, 512))) {
            rsp->result = 1;
        }
    } while (0);

    if (file_bk) {
        close(file_bk);
    }

    return (rsp);
}

void tct_backup_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_FT, tct_backup_tbl);
}

