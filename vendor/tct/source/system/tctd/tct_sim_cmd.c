/* Copyright (C) 2016 Tcl Corporation Limited */
/******************************************************************************/
/*                                                               Date:08/2012 */
/*                             PRESENTATION                                   */
/*                                                                            */
/*      Copyright 2012 TCL Communication Technology Holdings Limited.         */
/*                                                                            */
/* This material is company confidential, cannot be reproduced in any form    */
/* without the written permission of TCL Communication Technology Holdings    */
/* Limited.                                                                   */
/*                                                                            */
/* -------------------------------------------------------------------------- */
/* Author:  He Zhang                                                          */
/* E-Mail:  He.Zhang@tcl-mobile.com                                           */
/* Role  :  GLUE                                                              */
/* Reference documents :  05_[ADR-09-001]Framework Dev Specification.pdf      */
/* -------------------------------------------------------------------------- */
/* Comments:                                                                  */
/* File    : vender/tct/source/system/tctd/tct_sim_cmd.c                      */
/* Labels  :                                                                  */
/* -------------------------------------------------------------------------- */
/* ========================================================================== */
/* Modifications on Features list / Changes Request / Problems Report         */
/* -------------------------------------------------------------------------- */
/* date    | author         | key                | comment (what, where, why) */
/* --------|----------------|--------------------|--------------------------- */
/* 09/07/14| Liu Shishun    | FR-725046          | SIM detect command         */
/*---------|----------------|--------------------|--------------------------- */
/******************************************************************************/

#include "event.h"
#include "msg.h"
#include "log.h"

#include <stdio.h>
#include <stdlib.h>

#include "diag_lsm.h"
#include "diagpkt.h"
#include "diagcmd.h"
#include "diag.h"

#include "tct_diag.h"
#include "tct_sim_cmd.h"

#include "cutils/properties.h"

static const diagpkt_user_table_entry_type tct_sim_cmd_tbl[] =
{
    {TCT_SIM_CMD, TCT_SIM_CMD, tct_sim_cmd_func},
};

PACK(void *) tct_sim_cmd_func(
    PACK(void *)req_pkt,
    uint16 pkt_len
)
{
    //char product[PROP_VALUE_MAX];
    //property_get("ro.tct.product",product, "");

    DIAG_LOGE("tct_sim_cmd_func \n");

    req_type_tct_sim_cmd_exec *req = (req_type_tct_sim_cmd_exec *) req_pkt;
    rsp_type_tct_sim_cmd_exec *rsp = NULL;
    int ret = -1;

    if (!(req->command1 == 0 && req->command2 == 0)) {
        DIAG_LOGE("command not used\n");
        return NULL;
    }

    /*Allocate the same length as the request*/
    rsp = (rsp_type_tct_sim_cmd_exec *) diagpkt_subsys_alloc_v2(
            DIAG_SUBSYS_ID_TCT_SIM_CMD, TCT_SIM_CMD,
            sizeof(rsp_type_tct_sim_cmd_exec));

    if (!rsp) {
        DIAG_LOGE("diagpkt_subsys_alloc_v2 failed\n");
        return NULL;
    }

    memset(rsp, 0, sizeof(rsp_type_tct_sim_cmd_exec));
    memcpy(rsp, req, sizeof(req_type_tct_sim_cmd_exec));
    rsp->result = 0;

    char boardid[PROP_VALUE_MAX];
    uint32_t SIM_MASK = 0b1000;
    memset(boardid, 0, PROP_VALUE_MAX);
    property_get("ro.boot.tct.boardid",boardid, "");
    if(strlen(boardid)>0){
        char *pend;
        unsigned long ul_boardid = strtoul(boardid, &pend, 10);
        if(ul_boardid&SIM_MASK){
            rsp->result = 1;
        }
    }

    return (rsp);
}

void tct_sim_cmd_register()
{
    DIAGPKT_DISPATCH_TABLE_REGISTER_V2_DELAY(DIAG_SUBSYS_CMD_VER_2_F,
            DIAG_SUBSYS_ID_TCT_SIM_CMD, tct_sim_cmd_tbl);
}

