package com.tcl.secretmessage.activity;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.Window;
import android.widget.Toast;

import com.tcl.secretmessage.notification.SecretMessageNotification;

public class FingerprintAuthentionActivity extends Activity {
	public final static String BROADCAST_ACTION = "com.tcl.secretmessage";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		Intent broadcast = new Intent();
		broadcast.setAction(BROADCAST_ACTION);
		broadcast
		        .putExtra("command", SecretMessageNotification.WAIT_AUTHENTION);
		sendBroadcast(broadcast);

		Intent intent = new Intent();
		intent.setAction("com.tct.securitycenter.FingerprintVerify");
		startActivityForResult(intent, 100);

	

	}

	public static void collapseStatusBar(Context context) {
		try {
			Object statusBarManager = context.getSystemService("statusbar");
			Method collapse;

			if (Build.VERSION.SDK_INT <= 16) {
				collapse = statusBarManager.getClass().getMethod("collapse");
			} else {
				collapse = statusBarManager.getClass().getMethod(
				        "collapsePanels");
			}
			collapse.invoke(statusBarManager);
		} catch (Exception localException) {
			localException.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 100 && resultCode == RESULT_OK) {
			Intent intent = new Intent();
			intent.setAction(BROADCAST_ACTION);
			intent.putExtra("command", SecretMessageNotification.AUTHENTION_OK);
			sendBroadcast(intent);
		} else {
			Intent intent = new Intent();
			intent.setAction(BROADCAST_ACTION);
			intent.putExtra("command", SecretMessageNotification.POWER_OFF);
			sendBroadcast(intent);
			Toast.makeText(FingerprintAuthentionActivity.this,
			        "onAuthentication failed", Toast.LENGTH_SHORT).show();
		}
		collapseStatusBar(this);
		finish();
	}
}
