package com.tcl.secretmessage.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.tcl.secretmessage.utils.SharedPreferencesHelper;

public class JudgeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!SharedPreferencesHelper.getSecretMessageConfigedStatus(this)) {

			// the first time start the soft 
			goToHelpActivity();//start help
			//SharedPreferencesHelper.setSecretMessageConfigedStatus(this, true);
		} else {
			startActivity(new Intent(this, MainActivity.class));
			finish();
		}
	}

	private void goToHelpActivity() {
		Intent intent = new Intent(this, HelperActivity.class);
		startActivity(intent);
		finish();
	}

}
