package com.tcl.secretmessage.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.tcl.secretmessage.adapter.MyPagerAdapter;
import com.tcl.secretmessage.utils.SharedPreferencesHelper;
import com.tcl.secretmessage2.R;

public class HelperActivity extends Activity implements
		ViewPager.OnPageChangeListener, View.OnTouchListener {
	private View mViewHelper1, mViewHelper2, mViewHelper3;
	private List<View> mViewList;
	private ViewPager mViewPager;
	private int mCurrentItem;
	private PagerAdapter mPagerAdapter;
	private float startX;
	private float endX;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_helper);
		startHelper();
	}

	// start help
	public void startHelper() {
		mViewPager = (ViewPager) findViewById(R.id.viewpager);

		mViewList = getViewList();

		mPagerAdapter = new MyPagerAdapter(mViewList);
		// System.out.println("pagerAdapter:" + mPagerAdapter);
		mViewPager.setAdapter(mPagerAdapter);
		mViewPager.addOnPageChangeListener(this);
		mViewPager.setOnTouchListener(this);
	}

	public List<View> getViewList() {
		LayoutInflater inflater = getLayoutInflater();
		mViewHelper1 = inflater.inflate(R.layout.helper_page1, null);
		mViewHelper2 = inflater.inflate(R.layout.helper_page2, null);
		mViewHelper3 = inflater.inflate(R.layout.helper_page3, null);

		mViewList = new ArrayList<View>();
		mViewList.add(mViewHelper1);
		mViewList.add(mViewHelper2);
		mViewList.add(mViewHelper3);
		return mViewList;
	}

	private void goToMainActivity() {
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	@Override
	public void onPageSelected(int arg0) {
		mCurrentItem = arg0;// get current page
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			startX = event.getX();
			break;
		case MotionEvent.ACTION_UP:
			endX = event.getX();
			WindowManager windowManager = (WindowManager) getApplicationContext()
					.getSystemService(Context.WINDOW_SERVICE);

			// get the screen width
			Point size = new Point();
			windowManager.getDefaultDisplay().getSize(size);
			int width = size.x;

			// if the last page and turn from right to left the start
			// the mainactivity
			if (mCurrentItem == (mViewList.size() - 1)
					&& startX - endX >= (width / 6)) {
				if (!SharedPreferencesHelper.getSecretMessageConfigedStatus(this)) {
					SharedPreferencesHelper.setSecretMessageConfigedStatus(this, true);
					goToMainActivity();// start mainacitvity
					
				}
				

			}
			break;
		}
		return false;
	}


}
