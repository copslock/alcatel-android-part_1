package com.tcl.secretmessage.broadcast;

import com.tcl.secretmessage.activity.FingerprintAuthentionActivity;
import com.tcl.secretmessage.activity.HelperActivity;
import com.tcl.secretmessage.activity.MainActivity;
import com.tcl.secretmessage.notification.SecretMessageNotification;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;

public class SecretMessageReceiver extends BroadcastReceiver {
	private SecretMessageNotification mNotification;
	FingerprintManager mFingerprintManager;

	@Override
	public void onReceive(Context context, Intent intent) {
		mNotification = new SecretMessageNotification(context);
		mFingerprintManager = context
		        .getSystemService(FingerprintManager.class);
		int flag = intent.getIntExtra(SecretMessageNotification.KEY, 0);
		switch (flag) {
		case SecretMessageNotification.AUTHENTION_OK:
			mNotification
			        .sendCustomerNotification(SecretMessageNotification.POWER_ON);
			break;
		case SecretMessageNotification.WAIT_AUTHENTION:
			mNotification
			        .sendCustomerNotification(SecretMessageNotification.WAIT_AUTHENTION);
			break;
		case SecretMessageNotification.POWER_ON:
				Intent authentionIntent = new Intent(context,
				        FingerprintAuthentionActivity.class);
				context.startActivity(authentionIntent);
			break;
		case SecretMessageNotification.POWER_OFF:
			mNotification
			        .sendCustomerNotification(SecretMessageNotification.POWER_OFF);
			break;
		}

	}
}
