package com.tcl.secretmessage.utils;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.widget.Toast;

public abstract class FingerAuthenticHelper extends
        FingerprintManager.AuthenticationCallback {
	private Context mContext;
	FingerprintManager mFingerprintManager;

	public FingerAuthenticHelper(Context context) {
		if (context == null) {
			return;
		}
		mContext = context;
		mFingerprintManager = mContext
		        .getSystemService(FingerprintManager.class);
	}

	public void startAuthentication() {
		if (mFingerprintManager.isHardwareDetected()
		        && mFingerprintManager.hasEnrolledFingerprints()) {
			mFingerprintManager.authenticate(null, null, 0, this, null);
		}
	}

	public abstract void authenticationSucceeded();

	@Override
	public void onAuthenticationSucceeded(
	        FingerprintManager.AuthenticationResult result) {
		authenticationSucceeded();
	}

	@Override
	public void onAuthenticationError(int errorCode, CharSequence errString) {

		Toast.makeText(mContext, "failed to much time" + errString,
		        Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onAuthenticationHelp(int helpCode, CharSequence helpString) {

		Toast.makeText(mContext,
		        "onAuthentication failed,please try again" + helpString,
		        Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onAuthenticationFailed() {
		Toast.makeText(mContext, "please try another finger",
		        Toast.LENGTH_SHORT).show();
	}

}
