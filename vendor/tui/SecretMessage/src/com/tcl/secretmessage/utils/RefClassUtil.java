package com.tcl.secretmessage.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;

/**
 * This util class is used for invoking some
 * 'framework class' which we has integrated
 * into Android System.
 *
 * 1, android.os.SystemProperties
 * 2, com.tcl.enc.TclEncrype
 */
public class RefClassUtil {

    public static final String TAG = "Mixin";
    private static final String SYS_PROP_ENC_KEY = "sys.tcl_enc_setting";
    private static final String SYS_PROP_AC_KEY = "sys.tcl_enc_target";

    private static RefClassUtil ref;

    private static boolean inited = false;

    private RefClassUtil() {
        inited = (initSysRef() && initEncRef());
    }

    public static RefClassUtil getInstance() {
        if (null == ref) {
            ref = new RefClassUtil();
        }

        return ref;
    }

    public static void desInstance() {
        ref = null;
    }

    // SystemProperties releated
    private Class<?> SP;
    private Method ST, GT;

    // Mixin encrypting releated
    private Class<?> TE;
    private Method TEEN, TEDE, TEIS, TEGI, TEDI;
    private Object TEInstance;

    private boolean initSysRef() {
        try {
            SP = Class.forName("android.os.SystemProperties");
            if (null != SP) {
                GT = SP.getDeclaredMethod("get", String.class, String.class);
                ST = SP.getDeclaredMethod("set", String.class, String.class);
                return true;
            } else {
                Log.e(TAG, "CAN NOT FOUND CLASS <android.os.SystemProperties>");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean initEncRef() {
        try {
            TE = Class.forName("com.tcl.enc.TclEncrypt");
            if (null != TE) {
                TEEN = TE.getDeclaredMethod("encrypt", int.class, String.class);
                TEDE = TE.getDeclaredMethod("decrypt", int.class, String.class);
                TEIS = TE.getDeclaredMethod("isEncrypted", String.class, boolean.class, boolean.class);
                TEGI = TE.getDeclaredMethod("getInstance", null);

                TEInstance = TEGI.invoke(null, null);
                return true;
            } else {
                Log.e(TAG, "CAN NOT FOUND CLASS <com.tcl.enc.TclEncrypt>");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void sysSetValue(String value) {

        if (!inited) {
            Log.e(TAG, "ref class is not inited(sysSetValue)");
            return;
        }

        try {
            ST.invoke(SP, SYS_PROP_ENC_KEY, value);
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "Error in sysSetValue");

    }

    public String sysGetValue() {

        if (!inited) {
            Log.e(TAG, "ref class is not inited(sysGetValue)");
            return "0";
        }

        try {
            String res = (String) (GT.invoke(SP, SYS_PROP_ENC_KEY, "0"));
            return res;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "Error in sysGetValue");
        return "0"; // default value ?

    }

    public String sysGetAcValue() {
        if (!inited) {
            Log.e(TAG, "ref class is not inited(sysGetValue)");
            return "0";
        }

        try {
            String res = (String) (GT.invoke(SP, SYS_PROP_AC_KEY, "null"));
            return res;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "Error in sysGetValue");
        return "0";
    }

    public boolean encIsEnc(String str, boolean inLooseMode) {

        if (!inited) {
            Log.e(TAG, "ref class is not inited(encIsEnc)");
            return false;
        }

        try {
            boolean res = (Boolean) TEIS.invoke(TEInstance, str, inLooseMode, false);
            return res;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "Error in encIsEnc");
        return false;

    }

    public String encDec(String cipherStr) {

        if (!inited) {
            Log.e(TAG, "ref class is not inited(encDec)");
            return "";
        }

        try {
            String res = (String) (TEDE.invoke(TEInstance, 0x1000, cipherStr));
            return res;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return ""; // default value ?

    }

    public String encEnc(String plainStr) {

        if (!inited) {
            Log.e(TAG, "ref class is not inited(encEnc)");
            return "";
        }

        // NOT IMPLEMENTED NOW
        return "";

    }

}
